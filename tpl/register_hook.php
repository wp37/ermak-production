<?php
	
	$types				= array(
									array(
											'post_name'		=> "Ground transportation"
											,"color" 		=> '#FFFF7D'											
											,"hub_name"		=> __("Station", "smp")
										),
									array(
											'post_name'		=> "Sea transportation"
											,"color" 		=> '#D1E2FF'											
											,"hub_name"		=> __("Port", "smp")
										)
								);
	foreach($types as $type)
	{
		$new_loctype_ID 				= wp_insert_post(array(
																  "comment_status"	=> 'closed'
																, 'post_name'		=> $type['post_name']
																, 'post_status'	 	=> 'publish'
																, "post_title"		=> __($type['post_name'], "smp")
																, "post_type"		=> "smp_route_type"
																)
														);
			
		//insert trumbail
		/*
		$src_dir		= SMP_URLPATH . "img/";
		$wp_upload_dir 	= wp_upload_dir();				
		$filename		= download_url($src_dir . $type['thumbnail']);
		//$filename		= 'lp_location_default.jpg';
		
		cyfu_publish_post($new_loctype_ID, $filename);
		*/
		add_post_meta($new_loctype_ID, "color", 		$type['color']);
		add_post_meta($new_loctype_ID, "hub_name", 		$type['hub_name']);
	}
	

?>