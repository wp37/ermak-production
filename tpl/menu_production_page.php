<?php
	global $Soling_Metagame_Constructor;
	$all_lts		= $Soling_Metagame_Constructor->get_all_location_types();
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if($_POST['exec_cron'])
	{
		wp_cron();
	}			
	if($_POST['start_all_productions'])
	{
		$this->options['productions_started']					= true;
	}	
	if($_POST['finish_all_productions'])
	{
		$this->options['productions_started']					= false;
	}
	if($_POST['saved'])
	{
		$this->options['production_interval']					= $_POST['production_interval'];
		$this->options['main_goods_coef']						= $_POST['main_goods_coef'];
		$all_owner_locations									= $_POST['all_owner_locations'];
		/*
		$comma													= explode(",", $all_owner_locations);
		foreach($comma as $cc)
		{
			if(!is_numeric($cc))
			{
				$foo	= true;
				//echo $cc."<BR>";
				break;
			}
		}
		if($foo)
		{
			echo "<div class='updated settings-error' style='padding:20px;border-color:red; background:rgba(255, 0,0,0.75); color:white'>" . __("Put numeric IDs per comma to input.", "smp") . "</div>";
			//$this->options['all_owner_locations']				= "";
		} 
		else
		{
			$this->options['all_owner_locations']				= $all_owner_locations;
		}
		*/
		$ownres				= array();
		foreach($all_lts as $lt)
		{
			$lt_id			= $lt->ID;
			if($_POST["owner_$lt_id"] == "on")
			{
				$ownres[]	= $lt_id;
			}
		}
		$this->options['all_owner_locations']					= implode(",", $ownres);
	}
	
	if($_POST['save_gb'])
	{
		$this->options['gb_settings'] 							= array();
		$this->options['gb_settings']['dislocation_id'] 		= $_POST['gb_dislocation']=="on";
		$this->options['gb_settings']['quality'] 				= $_POST['gb_quality']=="on";
		$this->options['gb_settings']['best_before'] 			= $_POST['gb_best_before']=="on";
		$this->options['gb_settings']['factory_id'] 			= $_POST['gb_factory_id']=="on";
		$this->options['gb_settings']['store'] 					= $_POST['gb_shopting']=="on";
		$this->options['gb_settings']['deleting'] 				= $_POST['gb_deleting']=="on";
		
	}
	if($_POST['update_current_circle'])
	{
		ob_start();
		echo "<div class='lp-hide' id='circle_result'><h2>".__("Circle results", "smp")."</h2>";
		$this->count_circle();
		echo "</div><div class='smc-alert smc_bottom' target_name='circle_result' style='margin-bottom:5px;'>".__("Show results", "smp") ."</div>";
		$results = ob_get_contents();
		ob_end_clean();
		//echo " <BR>THE END";
	}
	if($_POST['do_something'])
	{
		$do_something	.=  "
		<div class='description'>
		<h3>I sorted all Goods Batchs by Industries</h3>";
		
		$arg					= array(
								'numberposts'	=> -1,
								'offset'    	=> 0,
								'orderby'  		=> 'rand',
								'order'     	=> 'ASC',
								'post_type' 	=> GOODS_BATCH_NAME,
								'post_status' 	=> 'publish',	
							);
		$batches			= get_posts($arg);
		$sorted_batches	= Goods_Batch::sort_batches_by_industries($batches);
		foreach($sorted_batches as $sorted_batch=>$val)
		{
			$ind			= SMP_Industry::get_instance($sorted_batch);
			$do_something	.= "<div style='position:relative; display:block; border-bottom:1px dotted #111;margin:10px 0;'>
			<p><board_title>".$ind->get("post_title")."</board_title></p><p>";
			foreach($val as $batch_id)
			{
				$batch					= new Batch($batch_id->ID);
				$do_something.= "".$batch->get_widget_picto()."";
			}
			$do_something	.= "</p></div>";
		}	
		$do_something		.= "</div>";
	}	
	if($_POST['save_settings'])
	{
		$this->options['player_change_aquipment']		= $_POST['player_change_aquipment'] == "on";
	}
	$this->update_options();
	ob_start();
	
	if(!SolingMetagameProduction::is_smc_install())			
	{
	?>		
		<div id="setting-error-settings_updated" class="update-nag">
			<?php _e("Plugin Ermak must be installed!", "smp");?>
		</div>
	<?php 
		return;
	} 			
	else
	{
		$current_circle_id = get_option("current_circle_id");
		//echo Assistants::echo_me($this->options, true);
		$html		= "<h2>".__("Metagame Production", 'smp')."</h2>
		<form name='settings'  method='post'  enctype='multipart/form-data' id='settings'>";
	?>	
		<style>
			.chosen-choices, .default{min-height:25px;}
		</style>			
			<div class='smc_form' style="display:inline-block; ">		
				<div class="h" style=" background:#600F66; color:white; padding: 10px; max-width:100%;">
					<board_title><?php echo sprintf(__("Now is %s circle in during", "smp"), "<input type='number' min='0' name='current_p_circle_id' class='smc_number' value='".$current_circle_id."'/>") ?><div class="smc_bottom" style="" name="update_p_circle_id"><i class="fa fa-check"></i></div></board_title>
				</div>					
				<div class="h0" style=''>
					
					<div class="h" style="background:#183D22; color:white; display: <?php echo $this->options['productions_started'] ? "none" : "display" ?>;">
						<board_title><?php _e("Start All Productions","smp"); ?></board_title>
						<input type="submit" name="start_all_productions" class="smp-button" value="<?php _e("Do Start for all productions","smp");?>"/>
					</div>
					<div class="h" style="background:#183D22; color:white; display: <?php echo $this->options['productions_started'] ? "display" : "none" ?>;">
						<board_title><?php _e("Finish All Productions","smp"); ?></board_title>
						<input type="submit" name="finish_all_productions" class="smp-button" value="<?php _e("Do Pause for all productions","smp");?>" style="background:red;"/>
					</div>		

					<div class="h"  style=" color:white;background:#2A2273;">
						<board_title><?php _e("Set Production Inteval","smp") ?></board_title>
						<input  type="number" step="1" min="1" name="production_interval" class="smc_number" value="<?php echo $this->options["production_interval"] ?>"/>
						<label><?php _e(" minutes", "smp")?></label>	
						<div class="smc_bottom" style="" name="psaved"><i class="fa fa-check"></i></div>	
					</div>
					<div class="h" style=" background:#A3266D; color:white;">
						<input type="submit" class="smc_bottom_width" style="" name="update_current_circle" value="<?php _e("execute circle","smp") ?>"/>
						<?php echo $results; ?>
					</div>
					<!--div class="h">
						<board_title><?php _e("Set Production Inteval","smp"); ?></board_title>
						<input  type="number" step="1" min="1" name="production_interval" value="<?php print_r( $this->options["production_interval"] ); ?>"/>
						<label><?php _e(" minutes", "smp"); ?></label>
					
					</div-->
					
					<div class="h">
						<board_title><?php _e("Location types that may be owner","smp"); ?></board_title>
						<!--label><?php _e("Set IDs per comma (empty for all)", "smp"); ?></label><br>
						<input  class="h2" name="all_owner_locations" value ="<?php print_r( $this->options['all_owner_locations'] );?>"/-->
						<?php
							$checkeds		= explode(",", $this->options['all_owner_locations']);
							foreach($all_lts as $lt)
							{
								$lt_id	= $lt->ID;
								$lt_tit	= $lt->post_title;
								$checked = "";
								foreach($checkeds as $check)
								{
									if($check == $lt_id)
									{
										$checked = "checked";
									}
								}
								echo "<input name='owner_$lt_id' id='$lt_id' type='checkbox' class='css-checkbox' $checked/><label class='css-label' for='$lt_id'>$lt_tit</label><br>" ;
							}
						?>
					</div>	
					<p></p>
					<input type="submit" name="saved" value="<?php _e("Save"); ?>"/>
				</div>
				<div class="h0">
					<!--div class="h"  style='max-width:380px;'>
						<?php echo $do_something; ?>
						<input type="submit" name="do_something" value="get all batchs sorted by Industries."/>
					</div-->	
					<?php $this->production_dashboard_setting(); ?>
					<div class="h"  style='max-width:380px;'>
						<board_title><?php _e("Settings"); ?></board_title>
						
						<p>
							<input type="checkbox" name="player_change_aquipment" id="player_change_aquipment" class="css-checkbox" <?php checked(1, $this->options['player_change_aquipment']) ?> />
							<label for="player_change_aquipment" class="css-label"><?php _e("Players change equipment", "smp"); ?></label>
						</p>
						<input type="submit" name="save_settings" value="<?php _e("Save"); ?>"/>
					</div>	
				</div>
			</div>		
		<script>
			set_chosen(".chosen-select", {max_selected_options: 500, placeholder_text_multiple: "<?php _e("Choose Locations-recievers", "smp");?>"});
		</script>
		
	<?php
	$page1 	= ob_get_contents();
	ob_end_clean();
	
	$storage_design		= (int)$this->options['storage_design'];
	$page2	= "
	<div class=smc_form style='display:inline-block;'>
		<board_title>" . __("Storage design", "smp") . "</board_title>
		<center class='admin_float'>
			<input class=bckgrnd_choose name=stg id=stg1 type=radio value='0' ". checked(0, $storage_design, 0) .  "style='background-image:url(" . SMP_URLPATH . "/img/storage_1.jpg)!important;'/>
			<br>
			<label for='stg1'>". __("Modern", "smp") ."</label>
		</center>	
		<center class='admin_float'>
			<input class=bckgrnd_choose name=stg id=stg2 type=radio value='1' ". checked(1, $storage_design, 0) .  "style='background-image:url(" . SMP_URLPATH . "/img/storage_2.jpg)!important;'/>	
			<br>
			<label for='stg2'>". __("Fantasy", "smp") ."</label>					
		</center>
		<div class='admin_float' style='padding-top:10px;;'>
			<div class='button-primary smc-pad-button' id='save_stg_dsgn'>".__("Save")."</div>
			<div id='wait2'></div>
		</div>
	</div>";
	$page3	= "
	<div class='smc_form' style='display:inline-block;'>
		<div class=h0>
			<div class=h  style='max-width:380px;'>".
				$this->get_scenario_button().
			"</div>	
		</div>
	</div>";
	echo 
	$html . 
	Assistants::get_switcher(
								array(
										array("title" => __("Main"), "slide" => $page1),
										array("title" => __("Decor", "smp"), "slide" => $page2),
										array("title" => __("Presets"), "slide" => $page3),
									 )
							).
	"</form>";
	}
?>