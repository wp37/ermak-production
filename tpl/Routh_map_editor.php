
<?php
	global $Soling_Metagame_Constructor;
	if(!isset($term_meta['start_hub_id']) || !isset($term_meta['finish_hub_id']))
	{
		echo "No one of hubs presents. Set start Hub and Finish Hub correctly.";
	}
	else
	{
	//$start_hub		= get_post($term_meta['start_hub_id']);
	//$fnish_hub		= get_post($term_meta['finish_hub_id']);
	$start_loc_id		= get_post_meta($term_meta['start_hub_id'], 	"dislocation_id", true);
	$fnish_loc_id		= get_post_meta($term_meta['finish_hub_id'], 	"dislocation_id", true);
	//$start_loc		= get_term($start_loc_id, "location");
	//$fnish_loc		= get_term($fnish_loc_id, "location");
	$start				= SMC_Location::get_term_meta( $start_loc_id );//get_option('taxonomy_'.$start_loc_id);
	$fnsih				= SMC_Location::get_term_meta( $fnish_loc_id );//get_option('taxonomy_'.$fnish_loc_id);
	$x_strt				= $start['x_pos'];
	$y_strt				= $start['y_pos'];
	$x_fnsh				= $fnsih['x_pos'];
	$y_fnsh				= $fnsih['y_pos'];
	$height_			= $Soling_Metagame_Constructor->options['smc_height'];
?>
<style>
	
</style>
<div id="main-cont">
	<div class="geom-cont" style="height:<?php echo $height_; ?>px;">
		<div id="geom-svg"> 
			<?php 
				$geom			= $Soling_Metagame_Constructor->get_main_map_svg_data();
				echo $geom;  
			?>	 
		</div>
		<div style="position:absolute;top:0; left:0; height:<?php echo ($height_ - 20); ?>px; width:100%;">
			<svg width="1000" height="<?php echo $height_; ?>" xmlns="http://www.w3.org/2000/svg" >
				<g>
					<?php
						$rouths			= SMP_Routh::get_all_rouths("ids");
						foreach($rouths as $route)
						{
							$term_m		= get_option("smp_routh_".$route);
							echo '<path d="'.$term_m['geometry'].'" stroke="#000000" stroke-width="3" stroke-opacity="0.5" fill-opacity="0"></path>';
						}
					?>
				</g>
			</svg>
		</div>
		<div>
			<input type="hidden" name="term_meta[geometry]" id="geometry" style="width:100%;" value="<?php echo $term_meta['geometry']; ?>"/>
			
		</div>
		<div id="all_geom">
			<div id="routh" style="position:absolute; top:0; left:0; width:100%; height:100%;">
			</div>
			<div id="start_hub" class="dot_location" style="display:block; top:<?php echo $y_strt - 4; ?>px; left:<?php echo $x_strt - 4;?>px;">			
			</div>
			<div id="finish_hub" class="dot_location" style="display:block; top:<?php echo $y_fnsh - 4; ?>px; left:<?php echo $x_fnsh - 4;?>px;">			
			</div>
		</div>
	</div>
	<div class="geom-menu" id="geom-menu">
		<!--div id="menu_drag_btn" class="svg_menu_element">
		</div-->
		<div id='rouths_ids_drop' class="svg_menu_element" 			type="routh_select">
			<?php echo __("Rouths", "smp")." " .SMP_Routh::wp_dropdown_rouths(array("name"=>"rouths_ids", "id"=>'rouths_ids')); ?>
		</div>
		
		<div class="svg_menu_btn unselected" id="draw_path_btn" 	type="draw" item="draw_path">
			<i class="fa fa-pencil"></i> 
		</div>
		<!--div class="svg_menu_btn unselected" id="select_btn" 	type="draw">
			<i class="fa fa-arrows"></i> 
		</div-->
		<div class="svg_menu_btn unselected" id="edit_dot_btn" 		type="draw" item="edit_dot">
			<i class="fa fa-location-arrow"></i> 
		</div>
		<div class="svg_menu_btn unselected" id="delete_dot_btn" 	type="draw" item="delete_dot">
			<i class="fa fa-minus-circle"></i> 
		</div>
		<div class="svg_menu_btn unselected" id="add_dot_btn" 		type="draw" item="add_dot">
			<i class="fa fa-plus-circle"></i> 
		</div>
		<div class="svg_menu_btn unselected" id="zoom_in_btn" 		type="draw" item="soom_in">
			<i class="fa fa-search-plus"></i>
		</div>
		<div class="svg_menu_btn unselected" id="zoom_out_btn" 		type="draw" item="zoom_out">
			<i class="fa fa-search-minus"></i>
		</div>
		<div class="svg_menu_btn2 unselected" id="delete" 			type="draw" item="delete">
			<i class="fa fa-trash-o"></i>
		</div>
	</div>
</div>
<script type="text/javascript">
		
		var ready_routh		= true;
		(function($)
		{
			$("svg").height(<?php echo $height_; ?>);
		})(jQuery)
</script>
<?php } ?>