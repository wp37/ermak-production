
var ready_routh;
var ctid_changes	= new Array();
var rang_chs		= function(){};
jQuery(document).ready(function( $ )
{	
	//
	$("#save_stg_dsgn").live({click:function(evt)
	{
		send(['smp_save_setting', 'srt_dsn', $("[name=stg]:checked").val()], false);
			
	}});

	$("[name=update_p_circle_id]").live({"click": function(e)
	{
		send(["update_prod_circle_id", $("[name=current_p_circle_id]").val()]);
	}});
	
	$("[name=psaved]").live({"click": function(e)
	{
		send(["update_prod_circle", $("[name=production_interval]").val()]);
	}});
	//=======================================================================
	$(".action__inn").live({"change":function()
	{
		$(this).parent().parent().children(".waybill_2").children().hide();
		var val		= $(this).val();
		var type	= $(this).attr("type");
		
		switch(val)
		{
			case "1":
				$(this).parent().parent().children(".waybill_2 ").children("#target_routh").fadeIn("slow");
				break;
			case "2":
				$(this).parent().parent().children(".waybill_2 ").children("#target_location").fadeIn("slow");
				break;
			case "-1":
			case "3":
				$(this).parent().parent().children(".waybill_2 ").children("[rd=target_deleting]").fadeIn("slow");
				break;
			default:
				break;
		}
	}});
	var id	= 0;
	if($("#i").size()>0)
	{
		id	= $("#i").val();						
	}
	$("#add_button_waybill").live({"click":function(e)
	{
		id++;	
		$("#i").val(id);
		var sheme = $( "#ex_waybill" ).clone()
			.attr('id', 'ex_waybill'+(id) )
				.appendTo( "#scheme" )
					.fadeIn("slow");
		var action_s	= sheme.children().children("[name=action_s]");						
		$(action_s).attr("name", "action_s" + id);	
		$(action_s).attr("type", id);	
		$(action_s).attr("id", "action_s" + id);
		
		var rouths	= sheme.children().children("[name=location]");
		$(rouths).attr("name", "location" + id);		
		$(rouths).attr("id", "location" + id);		
		var rouths	= sheme.children().children().children("[name=rouths]");
		$(rouths).attr("name", "rouths" + id);		
		$(rouths).attr("id", "rouths" + id);	
		
		sheme.children("[rd=target_deleting]").fadeIn("slow");
		var target_location	= sheme.children().children().children("[name=t_location]");
		$(target_location).attr("name", "t_location" + id);
		$(target_location).attr("id", "t_location" + id);
		var counts	= sheme.children().children("[name=counts]");
		$(counts).attr("name", "counts" + id);				
		$(counts).attr("id", "counts" + id);				
		var frequency	= sheme.children().children("[name=frequency]");
		$(frequency).attr("name", "frequency" + id);				
		$(frequency).attr("id", "frequency" + id);				
		
		$(this).appendTo(  "#scheme" );	

	}});
	
	$(".minus_button_wb").live({"click":function(e)
		{
			$(this).parent().parent().fadeOut("fast").remove();
		}
	});
	//
	
	$(".smp_fantasy").live("click", function(e)
	{
		send(["install_data_package", "fantasy"]);
	});
	$(".smp_modern").live("click", function(e)
	{
		send(["install_data_package", "modern"]);
	});
	var production_by 	= '0';
	var select			= [];
	//===========================================================================
	
	
	$("input[name=production_by]").change(function(e)
	{
		production_by	= $(this).val();
		switch(production_by)
		{
			case "0":
				$("#factory_production_by_location").fadeIn("slow");
				$("#factory_production_personally").hide();				
				$("#factory_production_by_goods_type").hide();				
				$("#factory_production_by_industry").hide();				
				set_chosen("#production_fac_loc_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_location").attr("placeh")});
				//alert($("#factory_production_by_location").size());
				break;
			case "1":	
				$("#factory_production_by_location").hide();
				$("#factory_production_personally").hide();			
				$("#factory_production_by_goods_type").fadeIn("slow");				
				$("#factory_production_by_industry").hide();		
				set_chosen("#production_fac_gt_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_goods_type").attr("placeh")});				
				break;	
			case "2":				
				$("#factory_production_by_location").hide();
				$("#factory_production_personally").hide();			
				$("#factory_production_by_goods_type").hide();						
				$("#factory_production_by_industry").fadeIn("slow");
				set_chosen("#production_fac_industry_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_industry").attr("placeh")})
				break;
			case "3":					
				$("#factory_production_by_location").hide();
				$("#factory_production_personally").fadeIn("slow");			
				$("#factory_production_by_goods_type").hide();				
				$("#factory_production_by_industry").hide();
				set_chosen("#production_factories_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_personally").attr("placeh")});
				break;	
		}
	});	
	
	
	
	$("#change_production_factories").live("click", function(e)
	{
		var s, s1, coef;
		coef			= $("#production_factories_coef").val();
		switch(production_by)
		{
			case "0":
				s		= "#production_fac_loc_for_change";
				s1 		= '#factory_production_by_location';
				break;
			case "1":
				s		= "#production_fac_gt_for_change";
				s1 		= '#factory_production_by_goods_type';
				break;
			case "2":
				s		= "#production_fac_industry_for_change";
				s1 		= '#factory_production_by_industry';
				break;
			case "3":
				s		= "#production_factories_for_change";
				s1 		= '#factory_production_personally';
				break;	
			default:
				s 		= "ddd";
		}
		select =  $(s).val() || [""];
		var sel='';
		$(s1 + " .search-choice").each(function(n, elem)
		{
			sel			+= $(elem).text() + "\n";
		});
		if(coef ==0  || $(s1 + " .search-choice").size() == 0)
		{
			alert($(this).attr("error"));
			return;
		}
		//select.join(", ")
		var q			=  $(this).attr("question1")  + $("[for=production_by" + production_by + "]").text() + " " + coef + "%\n\n" + sel + "\n\n" + $(this).attr("question2");
		if(confirm(q))
		{
			send(["change_production_coeficient", coef, production_by, select]);
		}
	});	
	// PATH EDITOR
	
	var cur_item;
	var start_draw		= false;
	var xx, yy, path_data, draw;
	
	//see tpl/Routh_map_editor.php
	if(!SVG.supported)
	{
		a_alert("SVG is not supported!");
	}
	
	var draw_routh_presets = function()
	{
		draw				= SVG('routh').size("100%", "100%");
		path_data			= $('#geometry').val();
		if(path_data !="")	draw.path(path_data).stroke({ width: 3, color:"#00FF00"}).fill({opacity:0});
		$("[type=routh_select]").hide();
		$("[type=draw]").hide();
		$("[type=draw]").fadeIn('slow');
	}
	
	if(ready_routh)	
		draw_routh_presets();	
	$('#geom-menu')       
        .live('drag',function( event ){
				
                $( this ).css({
                        top: event.offsetY,
                        left: event.offsetX
                        });
                });
	var active		= function (selector)
	{
		if($(selector).size() < 1)			return;
		if($(".svg_menu_btn").size() < 1) 	return;
		$(".svg_menu_btn").removeClass("selected");
		$(".svg_menu_btn").addClass("unselected");
		$(selector).removeClass("unselected");
		$(selector).addClass("selected");
		cur_item		= $(selector).attr("item");
		//alert(cur_item);
	}
	$(".svg_menu_btn").live("click", function(event)
	{
		active(this);
	});
	$(".dot_location").mousedown(function(event)
	{
		xx			= $(this).offset().left 	- $("#geom-svg").offset().left + 4;
		yy			= $(this).offset().top 		- $("#geom-svg").offset().top + 4;
		start_draw		= !start_draw;
		if(start_draw)
		{
			if(draw)	
				draw.clear();
			path_data 	= "M" + xx + "," + yy + " L";
			
		}
		else
		{
			path_data 	+= xx + "," + yy;
			if(draw)	
				draw.path(path_data).stroke({ width: 3, color:"#00FF00"}).fill({opacity:0});
			
			//a_alert(path_data);
			$('#geometry').val(path_data);
		}
	});
	$("#routh").click( function(event) 
	{
		xx			= event.offsetX;// - $("#geom-svg").offset().left;
		yy			= event.offsetY;// - $("#geom-svg").offset().top;
		switch(cur_item)
		{		
			case "draw_path":
				if(!start_draw)
				{
					a_alert(__("You must start draw in the one of Hubs"));
				}
				else
				{					
					path_data += xx + "," + yy + ",";
					$('#geometry').val(path_data);
				}
				break;
			
		}
	});
	active("#draw_path_btn");
	
	//
	$('.datetimepicker_unixtime').datetimepicker({
	   format:'H:i d.m.Y',
	});
	//goods type components
	
	$("#add_button_gt").live({"click":function(e)
		{
			var n 		= $(e.currentTarget).attr("n");
			var id		= $("#i"+n).val();
			var sheme 	= $( "#example" ).clone()
				.attr('id', 'example'+(id) )
					.appendTo( "#scheme"+n)
						.fadeIn("slow");
			var goods_type	= $(sheme).children("[name=goods_type_choose0]");
			goods_type.attr("name", "goods_type_choose"+n+"" + id);				
			var goods_type	= $(sheme).children("[name=goods_type_cs0]");
			goods_type.attr("name", "goods_type_cs"+n+"" + id);			
			var goods_type_volume	= $(sheme).children("[name=goods_type_volume0]");
			goods_type_volume.attr("name", "goods_type_volume"+n+"" + id);
			$(this).appendTo(  "#scheme" +n);	
			$("#i"+n).val(++id);		
		}
	});
	$("#add_button_factory_gt").live({"click":function(e)
		{
			var id		= $("#i").val();
			var sheme 	= $( "#example" ).clone()
				.attr('id', 'example'+(id) )
					.appendTo( "#scheme")
						.fadeIn("slow");
			var goods_type	= $(sheme).children("[name=goods_type]");
			goods_type.attr("name", "goods_type" + id);			
			var techology_type	= $(sheme).children().children("[name=techology_type]");
			techology_type.attr("name", "techology_type" + id);
			techology_type.attr("id",   "techology_type" + id);
			$(this).appendTo(  "#scheme");	
			$("#i").val(++id);		
		}
	});
	$(".minus_button_gt").live({"click":function(e)
		{
			$(this).parent().fadeOut("fast").remove();
		}
	});
	
	// change Account's count by Currency Type
	
	rang_chs = function()
	{
		set_range(
				'.change_sa_range1', 
				{ 
					min:-100, 
					max:100,
					from:0,
					force_edges:true,
					grid:true,
					grid_num:8,
					postfix:" %",
					onFinish:function(obj)
					{
						ctid_changes[$(obj.input).attr("ctid")] = obj.fromNumber;
					}
				}
			 );
	}
	rang_chs();
	$(".change_ct").live({"click":function()
	{
		var ctid = $(this).attr("ctid");
		if(!ctid_changes[ctid])
		{
			a_alert("Set something.");
			return;
		}
		send(['change_ca_by_ct', ctid, ctid_changes[ctid]]);
	}});
	
	
	
});

document.documentElement.addEventListener("_send_admin_", function(e) 
{
	( function( $ )
	{
		var dat			= e.detail;
		var command		= dat[0];
		var datas		= dat[1];
		switch(command)
		{
			case "change_production_coeficient":
				alert(datas['text']);
				set_chosen("#production_fac_loc_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_location").attr("placeh")});
				set_chosen("#production_fac_gt_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_goods_type").attr("placeh")});	
				set_chosen("#production_fac_industry_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_industry").attr("placeh")})
				set_chosen("#production_factories_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_personally").attr("placeh")});
				$("#production_factories_coef").val(0);
				break;
			case "install_data_package":
				a_alert(datas['text']);
				break;
			case "update_prod_circle_id":
				a_alert(datas['text']);
				break;
			case "update_prod_circle":
				
				reak;
			case "change_ca_by_ct":
				a_alert(datas['text']);
				$("#curr_stat").empty().append("<p>"+datas['data']+"</p>");
				rang_chs();
				break;
			case "smp_save_setting":
				
				break;
		}
	})(jQuery);
});



var draw_path;
