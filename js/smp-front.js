var ed_interval, 
	_resize, 
	is_started, 
	delete_batch_picto, 
	auto_update, 
	drop_down, 
	autoupdater, 
	clear_location_map_muar,
	current_gb_id, current_gb_count,
	blur_muar,
	ddData,
	get_my_currency_accounts = function(){},
	sale_to_location			= new Array(),
	transfer_to_location		= new Array(),
	delete_to_location			= new Array(),
	prices_good_batch			= new Array(),
	is_stor_goods_batch			= new Array(),
	newDis_good_batch			= new Array(),
	split_rate_data	= function(){},
	trans_routh_id, number_tr_gb = -1,
	get_circle_report			= function(){},
	get_lgstcs_circle_report	= function(){},
	start_look_bacth_move		= function(){},
	look_bacth_move_period		= 0,
	is_circle_report = false,
	smp_complete_move_batch		= function(){},
	get_hub_form				= function(){},
	get_store_list				= function(){},
	set_routes_updater			= function(){};
var prod_range_down				= false;
var cur_fid;
var src_gb_id, src_gb_owner, src_gb_gt;
var ins_gb_owner, ins_gb_gt, ins_gb_count;
var gb_table_id, // ссылка на текущую таблицу партий товаров, в которой происходят изменеия
	gb_slide_id;

jQuery(document).ready(function( $ )
{
	
	$("[name=pay_gb]").live({click:function(evt)
	{
		var id		= $(this).attr("bid");
		var count	= $(this).parent().children("#payment_count_" + id).val();
		var accid	= $(this).parent().find("[name=account_" + id + "] option:selected").val();
		close_modal();
		send(['pay_gb', id, count, accid], true);
		//a_alert( "account ID = "+accid+", count = " + count + ", batch ID = "+id);
	}});
	// open execute goods batch window
	$('.execgbtch').live({click:function(evt)
	{
		$(this).append(get_wait_form(0, "color: #FFF;   position: absolute; top: 0; left: 0; margin: 2px; background: rgba(0,0,0,0.5);"));
		check_papa_switcher(this);	
		send(['show_batch_pult', $(this).attr('gbid')], false);
	}});
	$('.pay_gbbt').live({click:function(evt)
	{
		var gid				= $(this).attr('gbid');
		check_papa_switcher(this);			
		send([ 'show_batch_payd', gid ], true);
	}});
	$("#pay_gb_exec").live({click:function(evt)
	{
		var papa			= $(this).closest("#pay_gb_form");
		var gbid			= papa.attr("gbid");
		a_alert(gbid);
	}})
	
	//insert new goods batch from Location display===================
	$("#ins_owner").live({change:function()
	{
		ins_gb_owner = $(this).val();
	}})
	$("#ins_gtype").live({change:function()
	{
		ins_gb_gt = $(this).val();
	}})
	$("#gb_count").live({change:function()
	{
		ins_gb_count = $(this).val();
	}})
	var lib			= getUrlParameter("lid");
	$("#start_insert_gb").live({click:function(e)
	{
		ins_gb_owner		= !ins_gb_owner 	? $("#search_id").val() 	: ins_gb_owner;
		ins_gb_gt			= !ins_gb_gt 		? $("#search_owner").val() 	: ins_gb_gt;
		ins_gb_count		= !ins_gb_count 	? $("#search_gtype").val() 	: ins_gb_count;
		var loc_id			= $(this).attr("loc_id");
		var is_convert		= $(this).attr("is_convert"); 
		send(["insert_new_gb", [ins_gb_owner, ins_gb_gt, ins_gb_count, loc_id, is_convert]], false);
	}});
	//=============== remove goods batch from Location display=======
	$("#search_owner").live({change:function()
	{
		src_gb_owner = $(this).val();
	}});
	$("#search_gtype").live({change:function()
	{
		src_gb_gt = $(this).val();
	}});
	$("#search_id").live({change:function()
	{
		src_gb_id = $(this).val();
	}});
	$("#start_search_gb").live({click:function(evt)
	{
		src_gb_id			= !src_gb_id 	? $("#search_id").val() 	: src_gb_id;
		src_gb_owner		= !src_gb_owner ? $("#search_owner").val() 	: src_gb_owner;
		src_gb_gt			= !src_gb_gt 	? $("#search_gtype").val() 	: src_gb_gt;
		var loc_id			= $(this).attr("loc_id");
		var is_convert		= $(this).attr("is_convert"); 
		//a_alert(getUrlParameter("lid"));
		send(["search_gb", src_gb_id, src_gb_owner, src_gb_gt, loc_id, is_convert ], true);
	}});
	set_routes_updater = function()
	{
		//a_alert("UPDATE ROUTES");
		if($("[route_batchs]").size())
		{		
			$("[route_batchs]").each(function(n, elem)
			{
				var arr= $(elem).attr('route_batchs').split(',');
				for(var i=0; i<arr.length; i++)
				{
					var dat	= arr[i].split("+");
					add_updater_elm( { handler : "blind_send", params : ['smp_complete_move_batch', dat[0]], during : dat[1], id : 'routh_batch_' + dat[0] } );
				};
			});
		}
	}
	
	start_look_bacth_move = function(bid, period, rid)
	{
		/*
		console.log(bid, period, rid);
		look_bacth_move_period	= setInterval(function()
		{
			clearInterval(look_bacth_move_period);
			send(['smp_complete_move_batch', bid, rid], false);
			//a_alert("move batch # " + bid + " complete!");
		}, period* 61000);
		*/
		
	}
	
	
	
	
	get_hub_form = function(arg)
	{
		var pars		= arg.split(",");
		$("#slide_hubs_lists" + pars[1]).empty().append(get_wait_form());
		send(['get_hub_form', pars], false);
		
	}
	get_store_list = function(arg)
	{
		$("#col2").empty().append(get_wait_form());
		send(['get_store_list', arg], false);
		
	}
	//change factory's production in "My Productions" page
	$(".change_goods").live({"change":function(e)
	{
		var hhh		= $("[name= slide_slide_factories" + $(this).attr("fid") + " ]").height();
		$("[name= slide_slide_factories" + $(this).attr("fid") + " ]").empty().append("<div style='height:" + hhh + "px; font-size:30px; text-align:center; display:block; position:relative;'><i class='fa fa-refresh fa-spin'></i></div>");
		send( ['change_my_factory_goods_type', $(this).attr("fid"), $(this).val() ]);
	}});
	/*
	jQuery("[name=change_factory_power]").ionRangeSlider({
			min: 0,
			max: $("[name=change_factory_power]").attr("maximum"), 	
			from: $("[name=change_factory_power]").attr("current"),  
			type: 'single',
			step: 1,
			postfix: " units",
			prettify: true,
			onFinish: function(obj)
			{
				send(['change_factory_power', $("[name=change_factory_power]").attr("fid"),obj.fromNumber]);
			}
			//hasGrid: true
	});
	*/
	$("[name=change_factory_power_]").live({
		"change":function(e)
		{
			send(['change_factory_power', $(this).attr("fid"), $(this).val()]);
		},
		"mousedown":function(e)
		{
			cur_fid			= $(this).attr("fid");
			prod_range_down	= true;
		},
		"mouseup":function(e)
		{
			prod_range_down	= false;
		},
		"mousemove":function(e)
		{
			$("#smp_power_label" + cur_fid).text($(this).val());
		}
	});
	
	$("[name=invoices_slide]").live({"click": function(e)
	{
		smp_send(new Array('get_inners', ''));
	}});
	// production circle reports
	get_circle_report = function(arg)
	{
		if(!is_circle_report)
		{
			is_circle_report = true;
			send(['smp_get_circle_report', arg])
		}
	}
	
	$("[name=choose_circle_id]").live({"change":function(e)
	{
		//alert( $(e.target).val() );
		send(['smp_get_circle_report', $(e.target).val()])
	}})
	//logistics circle reports
	get_lgstcs_circle_report = function(args)
	{
		send(['smp_lgstcs_get_circle_report', args])
	}
	
	
	
	$("[waybill_id]").click(function(e)
	{
		//a_alert(get_wait_form() + $(this).attr("waybill_id"));
		var id		= $(this).attr("waybill_id");
		send(['get_waybill', id]);
	});
	
	//change factory productivity
	$("#ch_productivity").live({"click":function(E)
	{
		send(["ch_fac_productivity", $(this).attr("fid"),  $("#factoty_productivity").val()]);
	}});
	
	//goods batch actions
	
	//to store
	$("[gbbt=to_store]").live({"click":function(e)
	{
		var bid			= $(this).attr("bid");
		var col			= !sale_to_location[bid]	? $("#sale_to_location_" + bid).val() 					: sale_to_location[bid];
		var price		= !prices_good_batch[bid]	? $("#new_price_" + bid).val() 							: prices_good_batch[bid];
		var store		= !is_stor_goods_batch[bid]	? $('#pay_form'+bid).attr('checked')=="checked" ? 1 : 0	: is_stor_goods_batch[bid];
		//a_alert(store);
		var arr			= ['change_my_gb_par','to_store', bid, col, price, store];
		arr[10]			= gb_table_id;
		arr[11]			= gb_slide_id;
		send(arr);
	}}); 
	$("[inbgc]").live({"change":function()
	{		
		sale_to_location[$(this).attr('bid')] = $(this).val();		
	}});
	$("[price_gb_store]").live({"change":function()
	{
		prices_good_batch[$(this).attr('bid')] = $(this).val();	
	}});
	$("[isstoregb]").live({"change":function()
	{
		//a_alert($(this).attr('bid') + "<BR>" + ($(this).attr('checked')=="checked" ? 1 : 0 ) );
		is_stor_goods_batch[$(this).attr('bid')] = $(this).attr('checked')=="checked" ? 1 : 0;	
	}});
	
	
	//to_transfer
	$("[gbbt=to_transfer]").live({"click":function(e)
	{
		var bid			= $(this).attr("bid");
		var col			= !transfer_to_location[bid]	? $("#transfer_to_location_" + bid).val() : transfer_to_location[bid];
		var newDis		= !newDis_good_batch[bid]		? $("#location_" + bid).val() : newDis_good_batch[bid];
		if(newDis <1)
		{
			a_alert("Select new owner.");
			return;
		}
		if(col <1)
		{
			a_alert("Select cont more then 0.");
			return;
		}
		
		send(['change_my_gb_par','to_transfer', bid, col, newDis]);
	}}); 
	$("[intrans]").live({"change":function()
	{		
		transfer_to_location[$(this).attr('bid')] = $(this).val();		
	}});
	$("[seltrans]").live({"change":function()
	{
		newDis_good_batch[$(this).attr('bid')] = $(this).val();	
	}});
	
	
	//to_delete
	$("[gbbt=to_delete]").live({"click":function(e)
	{
		var bid			= $(this).attr("bid");
		var col			= !delete_to_location[bid]	? $("#delete_to_location_" + bid).val() : delete_to_location[bid];
		//a_alert(delete_to_location[bid]);
		send(['change_my_gb_par','to_delete', bid, col]);
	}}); 
	$("[indel]").live({"change":function()
	{
		delete_to_location[$(this).attr('bid')] = $(this).val();
	}});
	//===============================
	$("[cbid]").live(
	{
		"click": function(e)
		{
			var g_batch	= $(this).attr("cbid");
			send(['show_goods_batch', g_batch]);
		},
		"mouseover":function(e)
		{
			div_hint(this);
		},
		"mouseout":function(e)
		{
			$("#smc-hint").detach();
		}
	});
	$(".stor_tab").live({
		"click":function(e)
		{
			//a_alert($(this).parent().attr('obtype'));
			send(["smp_storage_angar", $(this).parent().attr("locid"), $(this).attr('i'), $(this).parent().attr('obtype')]);
		}
	});
	$("#more_factories").live("click", function(e)
	{
		send(['smp_more_factories', $(e.currentTarget).attr("foid")]); 
	});
	$("[hub_picto]").live("click", function(e)
	{
		var href = $(this).attr("href");
		if(location_change_info)
			window.location.assign(href);
		else
		{
			send(['open_hub_widget', $(this).attr("hub_id")]);
		}
	});
	
	
	var _blur_muar = function()
	{
		$("#location-container").addClass("blur_map");
		$("#location-container").parent().append("<div id='location_map_muar'><div class='location_map_muar'></div></div>");
	}
	blur_muar = _blur_muar;
	
	var _clear_location_map_muar = function()
	{	
		$("#location_map_muar").remove();
		$("#location-container").removeClass("blur_map");
	}
	clear_location_map_muar = _clear_location_map_muar;
	
	
	$("[st_gb_id]").live({"click":function(e)
	{
		var g_batch	= $(this).attr("st_gb_id");
		send(['show_goods_batch', g_batch]);
	}});

	$("[give_gt_loc]").live({"click":function(e)
	{
		//alert("owner = "+$(this).attr("give_gt_loc") + " batch=" + $(this).attr("gb_id"));
		//close_modal();
		send(['transfer_goods_batch', $(this).attr("give_gt_loc"), $(this).attr("gb_id")]);
	}});
	
	$("#move_button").live({"click":function(e)
	{
		
		var locs			= $(this).attr("locs");
		current_gb_count	= $(this).attr("count");
		current_gb_id		= $(this).attr("gb_id");
		var curr			= $("[location_id=" + $(this).attr("cur_loc_id") + "]")
		var cur_pos			= curr.offset() ;
		var cur_top			= cur_pos.top + curr.height()/2;
		var cur_left		= cur_pos.left + curr.width()/2;
		var hh				= 24;//curr.height() + 6;
		$("#lp-klapan3").css({'right':-$("#lp-klapan3").width()});
		$("#lp-klapan3").css('z-index', 100);
		is_open_clapan3 	= false;
		close_modal();
		var locations		= locs.split(",");
		blur_muar();
		$("#location_map_muar").append("<div class='smpd_comment'>Выберите Локацию, в которую необходимо перевезти партию.</div>");
		$("#location_map_muar").append("<div id='source_location_position' class='source_location_picto' style='width:"+hh+"px; height:"+hh+"px;'></div>");
		$("#source_location_position").offset({'top': cur_top - hh/2, "left" : cur_left - hh/2});
		$("#location_map_muar").append("<div id='gb_colcont'><div id='gb_col_slider'></div></div>");
		$("#gb_col_slider").ionRangeSlider({
				min: 0,
				max: current_gb_count,           
				from: current_gb_count,           
				type: 'single',
				step: 1,
				postfix: " units",
				prettify: false,
				hasGrid: false,
				hide_min_max: true,
				hide_from_to: true,
				onChange: function (obj) 
				{
					current_gb_count = obj.fromNumber;
				}
		});
		
		for(var i=0; i<locations.length; i++)
		{
			
			var l_data		= locations[i].split("--");
			var src			= $("[location_id=" + l_data[1] + "]");
			var par			= src.parent();
			var top			= $(par).css("top");
			var left		= $(par).css("left");
			//top				= Number(top.substring(0, top.indexOf("px")));
			var clone		= src.clone();
			$(clone).css({
				"position"	: "absolute",
				"top"		: top,
				"left"		: left
			});
			$(clone).attr("locc_id", l_data[1]); 
			$(clone).attr("routh_id", l_data[0]); 
			$("#location_map_muar").append(clone);
			$(clone).click(function(e)
			{
				send(["transfer_gb", $(this).attr("location_id"), $(this).attr("routh_id"), current_gb_id, current_gb_count]);
			});
		}
		$(".location_map_muar").click(function(e)
		{
			clear_location_map_muar();
		});
	}});
	$("#anover_button").live({"click":function(e)
	{
		current_gb_id	= $(this).attr("gb_id");
		send(["show_mine_gbatch", current_gb_id]);
	}});
	$("#select_modifier_modifier").live({"click":function(evt)
	{
		//var ddData = $('#select_modifier').data('ddslick');
		var factory_id = $(this).attr("factory_id");						
		send(['change_factory_modifier', factory_id, ddData, 0, "factory_id"]);
	}});
	
	$("[factory_picto]").live({
		"click": function(e)
		{	
			if(location_change_info)
			{
				var href = $(this).attr("href");
				window.location.assign(href);
			}
			else
			{
				var par	;
				switch( true)
				{
					case $(this).hasClass('factory_bussiness_widget'):
						par = 1;
						break;
					case $(this).hasClass('factory_bussiness_widget2'):
						par = 2;
						break;
					default:
						par = 0;
						break;
						 
				}
				var factory_id	= $(this).attr("factory_id");
				$(".factory_picto").css("opacity", 0.5);
				$(this).css("opacity", 1);
				send(["open_factory_widget", factory_id, par, 0, 'factory_id']);
							
				if($("#produce_content").size())
					$("#produce_content").empty().css({"height":"0"});
			}
		}
	});
	
	
	
	$('[change_factory_gt]').live({'click':function(e)	
	{
		var change_factory_gt	 = $(this).attr("change_factory_gt");
		var factory_id		 = $(this).attr("factory_id");
		send(['change_factory_goods_type', factory_id, change_factory_gt, 0, 'factory_id']);
	}});
	
	
	$(".batching").click(function()
	{
		window.location.assign("/?goods_batch="+$(this).attr("batch_id"));
	});	
	
	/*
	$("[type=range]").live({'change':(function()
		{			
			var newval 	= $(this).val();
			var pid		= $(this).attr("pid");	
			alert($("#ddd" + pid).attr("batch_id"));
			$("#ddd" + pid).html(newval);
		})
	});
	*/
	var init_all_sliders_STORE = function ()
	{
		// alert($("#payment_count_0").length);
		$("#payment_count_0").ionRangeSlider({
				min: 0,
				max: 5000,           
				type: 'single',
				step: 1,
				prefix: "$ ",
				prettify: true,
				hasGrid: true
		});
	}
	//
	//init_all_sliders_STORE();	
	var format_batch_cards = function()
	{
		$(".smp-batch-card").each(function(){
			var is_in_tab	= $(this).parent().parent().hasClass("tab_slide") ;
			var ww			= is_in_tab ? 70 : 50;
			
			var h			= $(this).children('.smp-batch-card-table-cont').height();
			h				= h==0 ? 115 :h;
			$(this).height( h + 80 + ww);
			
			//width		
			var w			= 400;
			if($("#content").size()>0)
			{
				w			= $("#content").width() - $('.smp-pr-title-current').width() - ww; 
			}
			if($(this).attr('type')=='invoice')
			{
				w			= 400;
			}
			$(this).width(w);
			$(".lp-batch-table").width(is_in_tab ? w - 20 : w - 100);
		});
		
		// pay the invoice
		$(".pay_the_invoice").live({"click": function(e)
		{
			var ask			= $(this).attr("ask");
			var invoice_id	= $(this).attr("invoice_id");
			if(confirm(ask))
			{
				smp_send(new Array('pay_the_invoice', invoice_id));
				
			}
		}});
		$(".refuse").live({"click": function(e)
		{
			var ask			= $(this).attr("ask");
			var invoice_id	= $(this).attr("invoice_id");
			if(confirm(ask))
			{
				smp_send(new Array('smp_refuse', invoice_id));
			}
		}});
	}
	format_batch_cards();
	
	// new invoice
	var payee_np='', payer_np='', inv_text_np='', inv_summae=0, new_invoice_ctype=0, new_invoice_summ_ct=0, rate=new Array();
	split_rate_data	= function()
	{
		var rates		= $("[ct_data]").attr("rates");
		var rr			= rates.split(",");
		for(var i=0; i<rr.length; i++)
		{
			var ee	= rr[i].split("+");
			rate[ee[0]]	= ee[1];
		}
		return rate;
	}
	$("#new_invoice_payee").live("change", function(e)
	{
		payee_np				= $(this).val();
	});
	$("#new_invoice_payer").live("change", function(e)
	{
		payer_np				= $(this).val();
	});
	$("#new_invoice_summae").live("change", function(e)
	{
		inv_summae				= $(this).val();
		$("#new_invoice_summ_ct").val( rate[new_invoice_ctype] * inv_summae );
	} );
	$("#new_invoice_summ_ct").live("change", function(e)
	{
		new_invoice_summ_ct				= $(this).val();
		$("#new_invoice_summae").val( Math.floor(new_invoice_summ_ct / rate[new_invoice_ctype]) );
	} );
	$("#new_invoice_ctype").live("change", function(e)
	{
		new_invoice_ctype				= $(this).val();
		$("#new_invoice_summ_ct").val( rate[new_invoice_ctype] * inv_summae );
	});
	$("#new_invoice_text").live("change", function(e)
	{
		inv_text_np				= $(this).val();
	});
	$("#new_invoice_button").live({"click": function(e)
	{
		if(payee_np == '') 
		{
			alert("Set the payee");
			return;
		}
		if(payer_np == '') 
		{
			alert("Set the payer");
			return;
		}
		if(new_invoice_ctype <=0)
		{
			alert("Set the currency type");
			return;
		}
		if(inv_summae <=0)
		{
			alert("Set the summae");
			return;
		}
		var ask			= $(this).attr("ask");
		if(confirm(ask))
		{
			smp_send(new Array('new_invoice', [payee_np, payer_np, inv_text_np, inv_summae, new_invoice_ctype]), false);
			
		}
	}});
	
	//inner invoice switcher form in Bank page
	$("#btn_invoice0").live({"click":function(e)
	{
		//$("#slide_invoice0").empty().append(get_wait_form());
		smp_send(new Array('get_inners', ''), false);
	}});
	
	//outer invoice switcher form in Bank page
	$("#btn_invoice1").live({"click":function(e)
	{
		//$("#slide_invoice1").empty().append(get_wait_form());
		smp_send(new Array('get_outers', ''), false);
	}});
	$("#btn_bank_0").live({"click":function()
	{
		$(".smc_switcher_body").height(600);
	}});
	$("#btn_bank_2").live({"click":function(e)
	{
		get_my_currency_accounts ();
	}});
	//get cur user currency accounts list
	get_my_currency_accounts = function()
	{
		//if( $(this).attr("is_load") != true)
		{
			send(['my_currency_accounts', ''],false);
		}
	}
	
	$(".print_batch_button").live({"click":function()
	{
		var batch_id		= $(this).attr("batch_id");
		//a_alert($("#batch_card_"+batch_id).size());
		close_modal();
		smp_send(new Array('print_batch_card', batch_id));		
		
	}});
	// hub permissions
	var new_permission_hub_id;
	var new_permission_location_id;
	var new_permission_priority;
	var new_permission_goods_type_id;
	var new_permission_limit;
	var set_current_hub_id1 = function(id)
	{
		new_permission_hub_id = id;
	}
	set_current_hub_id = set_current_hub_id1;
	$("#gb_owner_id").live({"change":function()
	{
		new_permission_location_id = $(this).val();
	}});
	$("#new_permission_gb").live({"change":function()
	{
		new_permission_goods_type_id = $(this).val();
	}});
	$("#new_permission_priority").live({"change":function()
	{
		new_permission_priority = $(this).val();
	}});
	$("#new_permission_limit").live({"change":function()
	{
		new_permission_limit = $(this).val();
	}});
	$(".new_hub_permission_button").live({"click":function()
	{
		if(!new_permission_hub_id)
		{
			a_alert("Error: No hub present");
			return;
		}
		if(!new_permission_location_id || new_permission_location_id==-1)
		{
			a_alert("Error: No Location-owner present");
			return;
		}
		if(!new_permission_goods_type_id || new_permission_goods_type_id==-1)
		{
			a_alert("Error: No goods type present");
			return;
		}
		console.log("new_permission_limit = "+new_permission_limit);
		smp_send(new Array('add_new_hub_permission', new_permission_hub_id, new_permission_location_id, new_permission_priority, new_permission_goods_type_id, new_permission_limit), false);
		var par = $(".box-modal");
		par.children().hide();
		par.append(get_wait_form());
	}});
	
	//
	$("[name=hub_permission]").live({"click":function()
	{
		smp_send(new Array('get_hub_permissions', $(this).attr("id_prefix")));
	}});
	
	$(".delete_hp").live({"click":function()
	{
		var id		= $(this).attr("hp");
		var ask		= $(this).attr("ask");
		if(confirm(ask))	
			smp_send(new Array('delete_hub_permissions', id));
	}});
	
	
	
	$(".factory_submit_btn").live({"click":function(e)
	{
		a_alert($(this).attr("fid"));
	}});
	var init_account_operations1 = function()
	{
		$(".smp-range").each(function()
		{
			var max			= $(this).attr("max");
			var count_id	= $(this).attr("count_id");
			//alert($("#ccount-" + count_id).html());
			$(this).ionRangeSlider( {											
												min:0,
												max:max,
												postfix:" $",
												prettify: false,
												onLoad: function(obj)
												{
													$(this).attr("slider_count_id", count_id);													
												},
												onChange: function (obj) 
												{
													$("#ccount-" + count_id).html( obj.fromNumber);
													summ[count_id] = obj.fromNumber;
												}
											});
		});
	}
	init_account_operations=init_account_operations1;
	$(".inputnumber").live({"change":function(e)
		{			
			price		= $(this).parent().attr("price");
			pid			= $(this).parent().attr("pid");
			payment_name= "payment_count_" + pid;
			price_name	= "price_count_" + pid;
			id			=($(this).attr('id'));			
			switch(id)
			{
				case payment_name:
					new_price		= $(this).val() * price;
					$(this).parent().children("#" + price_name).val(new_price);	
					break;
				case price_name:
					new_price		= parseInt($(this).val() / price, 10);
					$(this).parent().children("#" + payment_name).val(new_price);	
					$(this).val(new_price * price);
					break;
				default:
					null;
			}			
					
		}
	});
	$(".select_routh_hub").live({"change":function(e)
	{
		//alert($(this).val());
		var val			= $(this).val();
		$(".select_routh_hub").val(-1);
		$(this).val(val);
		$(".smp_routh_batches").hide();
		$("#smp_routh_batches_" + $(this).val() ).fadeIn("slow");
	}});
	
	
	
	
	$("#btn_invoice2").live({"click":function(e)
	{
		smp_send(new Array('new_invoice_form', ''));
	}});
	//currency transfer
	var summ		= [];
	var reasons		= [];
	var payee_acount_id;
	$("[name='reason']").live({change:function(evt)
	{
		var cid		= $(this).attr("for_acc_id");
		reasons[cid]	= $(this).val();
	}});
	$("[class='smc_vertical_align']").live({change:function(evt)
	{
		var cid		= $(this).attr("count_id");
		summ[cid]	= $(this).val();
	}});
	$(".transfer_button").live({"click":function(e)
	{
		var caid	= $(this).attr("ca_id");
		if(reasons[caid]=="" || !reasons[caid])
		{
			a_alert(__("You must set reason for this transfer"));
			return;
		}
		if(!summ[caid] || summ[caid]<=0)
		{
			a_alert(__("You must set summ for this transfer"));
			return;
		}
		smp_send(new Array('new_transfer', caid, payee_acount_id, summ[caid], reasons[caid]));
	}});
	$(".payee_id").live({"change":function()
	{
		payee_acount_id= $(this).val();
	}});
	
	
	$(".get_permission").live({"change":function()
	{
		var per_batch_id = $(this).parent().attr("per_batch_id");
		if($(this).prop("checked")==true)
		{
				send(new Array('change_transit', per_batch_id, 1), false);
		}
		else
		{
			var ask			= $(this).parent().attr("ask");
			if(confirm(ask))
				send(new Array('change_transit', per_batch_id, 0), false);
			else
				$(this).prop("checked", true)
		}
		
	}});
	
	$("#view_content").live({
		"click": function(e)
		{
			//send( ["open_location_content_widget", $(e.currentTarget).attr("location_id"), $(e.currentTarget).attr("name")] );	
			send( ["switch_location", $(e.currentTarget).attr("location_id")] );	
		}
	});
	
	
	$('select').live({"click":function()
	{
		if($(this).attr("name")=="batch_routh_recivier_")
			trans_routh_id 	= $(this).val();
	}});
	$(".number_tr_gb").live({"click": function(e)
	{
		number_tr_gb		= $(this).val();
		//alert(number_tr_gb);
	}});
	$("[transport_dist_id]").live({"click":function()
	{
		if(!trans_routh_id )	
		{
			a_alert("Choose one of route for transportation");
			return;
		}
		var batch_id			= $(this).parent().attr("batch_id");
		var par 				= $(".box-modal");
		var count				= number_tr_gb == -1 ? $('#batch_count_' + batch_id).val() : number_tr_gb;
		
		par.children().hide();
		par.append(get_wait_form());
		//alert(count);
		send(new Array('transfer_gb', -1, trans_routh_id,  batch_id, count));
		
		
	}});
	
	$('.smp_route_info').live({'click':function(evt)
	{
		route_replace(this);
	}});
	$('[smp_route_p]').live({'click':function(evt)
	{
		route_replace(this);
	}});
	var fff = function(selector )
	{
		if(jQuery.isFunction(print))
		{
			$(selector).print().fadeOut("slow");
		}
		else
			alert('printing engine not present. Sorry.');
	}
	ff=fff;
	
	route_replace = function(obj)
	{
		if(location_change_info)
		{
			window.location.replace($(obj).attr("href"));
		}
		else
		{
			var id = $(obj).attr("rid");
			send(['open_routh_widget_klapan', id, "", 0, "rid"]);
		}
	}
	
	//===========================
	//
	//	ADMIN EdITING MAP
	//
	//============================
	
	function admin_map_edit()
	{
	
	}
	
	//============================
	//
	//	END ADMIN EDITING
	//
	//============================
	
	
	
	
		
	//pic select
	(function (a) 
	{ 
		function g(a, b) 
		{ 
			var c = a.data("ddslick"); 
			var d = a.find(".dd-selected"), 
			e = d.siblings(".dd-selected-value"), 
			f = a.find(".dd-options"), 
			g = d.siblings(".dd-pointer"), 
			h = a.find(".dd-option").eq(b), 
			k = h.closest("li"), l = c.settings, 
			m = c.settings.data[b]; 
			a.find(".dd-option").removeClass("dd-option-selected"); 
			h.addClass("dd-option-selected"); 
			c.selectedIndex = b;
			c.selectedItem = k; 
			c.selectedData = m; 
			if (l.showSelectedHTML) 
			{
				d.html((m.imageSrc ? '<img class="dd-selected-image' + (l.imagePosition == "right" ? " dd-image-right" : "") + '" src="' + m.imageSrc + '" />' : "") + (m.text ? '<label class="dd-selected-text">' + m.text + "</label>" : "") + (m.description ? '<small class="dd-selected-description dd-desc' + (l.truncateDescription ? " dd-selected-description-truncated" : "") + '" >' + m.description + "</small>" : "")) 
			} 
			else 
				d.html(m.text);
			e.val(m.value); c.original.val(m.value); 
			a.data("ddslick", c); 
			i(a); 
			j(a); 
			if (typeof l.onSelected == "function") 
			{ 
				l.onSelected.call(this, c) 
			} 
		} 
		function h(b) 
		{
			var c = b.find(".dd-select"), 
			d = c.siblings(".dd-options"), 
			e = c.find(".dd-pointer"), 
			f = d.is(":visible"); 
			a(".dd-click-off-close").not(d).slideUp(50); 
			a(".dd-pointer").removeClass("dd-pointer-up");
			
			//alert( c.parent().attr("id") );
			if (f) 
			{ 
				d.slideUp("fast"); 
				e.removeClass("dd-pointer-up");
				//alert("VISIBLED");
				//c.parent().append(d);
			} 
			else 
			{ 
				d.slideDown("fast"); 
				e.addClass("dd-pointer-up"); 
				var offset	= c.parent().offset();
				d.appendTo("body").css({"position": "absolute", "top": offset.top + c.height() + "px", "left": offset.left + "px"});
				
			} k(b) 
		}
		function i(a) 
		{ 
			a.find(".dd-options").slideUp(50); 
			a.find(".dd-pointer").removeClass("dd-pointer-up").removeClass("dd-pointer-up") 
		} 
		function j(a) 
		{ 
			var b = a.find(".dd-select").css("height"); 
			var c = a.find(".dd-selected-description"); 
			var d = a.find(".dd-selected-image"); 
			if (c.length <= 0 && d.length > 0) 
			{
				a.find(".dd-selected-text").css("lineHeight", b) 
			} 
		} 
		function k(b) 
		{ 
			b.find(".dd-option").each(
				function () 
				{ 
					var c = a(this); 
					var d = c.css("height"); 
					var e = c.find(".dd-option-description"); 
					var f = b.find(".dd-option-image"); 
					if (e.length <= 0 && f.length > 0) 
					{ 
						c.find(".dd-option-text").css("lineHeight", d) 
					} 
				}) 
		} 
		a.fn.ddslick = function (c) 
		{ 
			if (b[c]) {
				return b[c].apply(this, Array.prototype.slice.call(arguments, 1)) 
			} 
			else if (typeof c === "object" || !c) 
			{ 
				return b.init.apply(this, arguments) 
			} 
			else 
			{ 
				a.error("Method " + c + " does not exists.") 
			} 
		}; 
		var b = {}, 
		c = { data: [], keepJSONItemsOnTop: true, width: 260, height: null, background: "#eee", selectText: "", defaultSelectedIndex: null, truncateDescription: true, imagePosition: "left", showSelectedHTML: true, clickOffToClose: true, onSelected: function () { } },
		d = '<div class="dd-select"><input class="dd-selected-value" type="hidden" /><a class="dd-selected"></a><span class="dd-pointer dd-pointer-down"></span></div>', 
		e = '<ul class="dd-options"></ul>', 
		f = '<style id="css-ddslick" type="text/css">' + ".dd-select{ border-radius:2px; border:solid 1px #ccc; position:relative; cursor:pointer;}" + ".dd-desc { color:#aaa; display:block; overflow: hidden; font-weight:normal; line-height: 1.4em; }" + ".dd-selected{ overflow:hidden; display:block; padding:10px; font-weight:bold;}" + ".dd-pointer{ width:0; height:0; position:absolute; right:10px; top:50%; margin-top:-3px;}" + ".dd-pointer-down{ border:solid 5px transparent; border-top:solid 5px #000; }" + ".dd-pointer-up{border:solid 5px transparent !important; border-bottom:solid 5px #000 !important; margin-top:-8px;}" + ".dd-options{ border:solid 1px #ccc; border-top:none; list-style:none; box-shadow:0px 1px 5px #ddd; display:none; position:absolute; z-index:2000; margin:0; padding:0;background:#fff; overflow:auto;}" + ".dd-option{ padding:10px; display:block; border-bottom:solid 1px #ddd; overflow:hidden; text-decoration:none; color:#333; cursor:pointer;-webkit-transition: all 0.25s ease-in-out; -moz-transition: all 0.25s ease-in-out;-o-transition: all 0.25s ease-in-out;-ms-transition: all 0.25s ease-in-out; }" + ".dd-options > li:last-child > .dd-option{ border-bottom:none;}" + ".dd-option:hover{ background:#f3f3f3; color:#000;}" + ".dd-selected-description-truncated { text-overflow: ellipsis; white-space:nowrap; }" + ".dd-option-selected { background:#f6f6f6; }" + ".dd-option-image, .dd-selected-image { vertical-align:middle; float:left; margin-right:5px; max-width:64px;}" + ".dd-image-right { float:right; margin-right:15px; margin-left:5px;}" + ".dd-container{ position:relative;}? .dd-selected-text { font-weight:bold}?</style>"; 
		
		if (a("#css-ddslick").length <= 0) 
		{ 
			a(f).appendTo("head") 
		} 
		b.init = function (b) 
		{ 
			var b = a.extend({}, c, b); 
			return this.each(function () 
			{ 
				var c = a(this), 
				f = c.data("ddslick"); 
				if (!f) 
				{ 
					var i = [], 
					j = b.data; 
					c.find("option").each(function () 
					{ 
						var b = a(this), 
						c = b.data(); 
						i.push({ text: a.trim(b.text()), value: b.val(), selected: b.is(":selected"), description: c.description, imageSrc: c.imagesrc }) 
					}); 
					if (b.keepJSONItemsOnTop) 
						a.merge(b.data, i); 
					else 
						b.data = a.merge(i, b.data); 
					var k = c, 
					l = a('<div id="' + c.attr("id") + '"></div>'); 
					c.replaceWith(l); 
					c = l; 
					c.addClass("dd-container").append(d).append(e); 
					var i = c.find(".dd-select"), 
					m = c.find(".dd-options"); 					
					m.attr("id", c.attr("id") + "-option");
					
					m.css({ width: b.width, "max-height":500 });
					i.css({ width: b.width, background: b.background }); 
					c.css({ width: b.width });
					if (b.height != null) 
						m.css({ height: b.height, overflow: "auto" }); 
					a.each(b.data, function (a, c) 
							{ 
								if (c.selected)
									b.defaultSelectedIndex = a; 
								m.append("<li>" + '<a class="dd-option">' + (c.value ? ' <input class="dd-option-value" type="hidden" value="' + c.value + '" />' : "") + (c.imageSrc ? ' <img class="dd-option-image' + (b.imagePosition == "right" ? " dd-image-right" : "") + '" src="' + c.imageSrc + '" />' : "") + (c.text ? ' <label class="dd-option-text">' + c.text + "</label>" : "") + (c.description ? ' <small class="dd-option-description dd-desc">' + c.description + "</small>" : "") + "</a>" + "</li>")

							}); 	
					
					var n = { settings: b, original: k, selectedIndex: -1, selectedItem: null, selectedData: null };
					c.data("ddslick", n); 
					if (b.selectText.length > 0 && b.defaultSelectedIndex == null) 
					{ 
						c.find(".dd-selected").html(b.selectText) 
					} 
					else
					{ 
						var o = b.defaultSelectedIndex != null && b.defaultSelectedIndex >= 0 && b.defaultSelectedIndex < b.data.length ? b.defaultSelectedIndex : 0;
						g(c, o) 
					} 
					c.find(".dd-select").on("click.ddslick", function () { 
						h(c);
					}); 
					c.find(".dd-option").on("click.ddslick", function () 
					{
						g(c, a(this).closest("li").index()); 
					}); 
					if (b.clickOffToClose) 
					{ 
						m.addClass("dd-click-off-close"); 
						c.on("click.ddslick", function (a) 
						{ 
							a.stopPropagation() 
						}); 
						a("body").on("click", function ()
						{
							a(".dd-click-off-close").slideUp(50).siblings(".dd-select").find(".dd-pointer").removeClass("dd-pointer-up") 
						}) 
					} 
				} 
			}) 
		}; 
		b.select = function (b) 
		{ 
			return this.each(function () 
			{ 
				if (b.index) g(a(this), b.index) 
			}) 
		}; 
		b.open = function () 
		{ 
			return this.each(function () 
			{ 
				var b = a(this), 
				c = b.data("ddslick"); 
				if (c) h(b) 
			}) 
		};
		b.close = function () 
		{
			return this.each(function () 
			{ 
				var b = a(this), c = b.data("ddslick"); 
				if (c) i(b);
			}); 
		}; 
		b.destroy = function () 
		{ 
			return this.each(function () 
			{ 
				var b = a(this), c = b.data("ddslick"); 
				if (c) 
				{ 
					var d = c.original; 
					b.removeData("ddslick").unbind(".ddslick").replaceWith(d) 
				} 
			}) 
		} 
	})(jQuery)
	
	
					
	/**/
	
});
var init_account_operations = function(){alert("A");};
var set_current_hub_id;
var ff;
var route_replace = function(obj){};
function invoice_card_size()
{
	jQuery(function($)
	{
		$(".smp-batch-card").width(400);
		var h			= $(".smp-batch-card").children('.smp-batch-card-table-cont').height();
		h				= h == 0 ? 115 :h;
		$(".smp-batch-card").height( h + 120);
	});
}

//afterdraw Location map in main Panel
document.documentElement.addEventListener("post_draw", function(e) 
	{
		(function($)
		{
			switch(e.detail.type)
			{
				case 1:
					set_routes_updater();
					break;
			}
		})(jQuery);
	}
);
//===========================
//	init admin manipulation
//===========================

document.documentElement.addEventListener("admin_edit", function(e) 
	{
		
	}
);


function smp_send(params)
{
	var customEvent = new CustomEvent("_sending_", {bubbles : true, cancelable : true, detail : params});
	document.documentElement.dispatchEvent(customEvent);
}

document.documentElement.addEventListener("_send_", function(e) 
{
	( function( $ )
	{
		var dat			= e.detail;
		var command		= dat[0];
		var datas		= dat[1];
		console.log(command);
		switch(command)
		{
			case 1:
				if(datas['expected_time'])
				{
					start_expected_time_interval(datas['expected_time']);
				}
				break;
			case 'my_currency_accounts':
				$("#slide_bank_2").empty().
										append(datas['text']);
				show_factory1($(".smp-pr-title").first());
				init_account_operations();
				break;
			case "new_invoice_form":
				a_alert(datas['text'], null, null, null, datas['title']);				
				split_rate_data();
				$("#new_invoice_form").fadeIn("slow");
				break;
			case 'get_inners':
				$("#slide_invoice0").empty().
											append(datas['text']);
				invoice_card_size();
				break;
			case 'get_outers':
				$("#slide_invoice1").empty().
									append(datas['text']);
				invoice_card_size();
				break;
			case "new_invoice":
				close_modal();
				a_alert(datas['text']);
				break;
			case "smp_refuse":
				$("#invoke_form_"+ datas['text']).hide('slow');
				a_alert(datas['text']);
				break;
			case "pay_the_invoice":
				var dd		= "#invoice_form_params_"+ datas['text'][1];
				$(dd).hide().
					empty().
						append('<div class="is_repaid">' + datas['text'][2] + '</div>').
							fadeIn("slow")	;
				
				$("#ccount-"+ datas['text'][3]).html(datas['text'][4]);
				//window.location.replace(window.location.href);
				break;
			case "new_transfer":
				a_alert(datas['text']);
				break;
			case "add_new_hub_permission":				
				close_modal();
				$("#hub_permissions_"+ datas['text'][1]).hide().prepend(datas['text'][0]).fadeIn("slow");
				break;
			case "get_hub_permissions":
				$("#hub_permissions_"+ datas['text'][1]).empty().append(datas['text'][0]);
				//a_alert(datas['text']);
				break;
			case "delete_hub_permissions":
				//a_alert(datas['text']);
				$("#permission_form_"+datas['text']).fadeOut("slow");
				break;
			case 'change_transit':
				if(jQuery.type(datas['text'])=='array')
				{
					$("[per_batch_id="+datas['text'][1]+"]").children(".get_permission").prop("checked", false);
					a_alert(datas['text'][0]);
				}
				break;
			case 'print_batch_card':
				if(!datas['text'])
				{
					a_alert("Error!");
					break;
				}
				var clone	= $(datas['text']).
					attr('id', 'clone_batch_card').
						appendTo('body').
							show();
				clone.children('[name=exec]').empty();
				clone.children('[name=price]').empty();
				printDiv('clone_batch_card', function(id)
				{
					
				});
				clone.remove();
				/*
				var clone = $("#batch_card_" + datas['text']).clone().
					attr('id', 'clone_batch_card').
						appendTo($("#batch_card_" + datas['text']).parent()).
							css({'width':'500px', 'height':'300px'});
				clone.children('[name=exec]').empty();
				clone.children('[name=price]').empty();
				printDiv('clone_batch_card', function(id)
				{
					a_alert(id);
				});
				clone.empty();
				//$.getScript('wp-content/plugins/Soling-metagame-constructor/js/html5-3.6-respond-1.1.0.min.js', ff("#batch_card_" + datas['text']));
				*/
				break;
			case 'get_current_location_consume':
			case 'get_location_hubs':
			case 'get_current_location_factories':
				//$("#location-container").empty().hide().append(datas['text']).fadeIn("slow");
				$("#location-container").addClass("blur_map");
				$("#lp_loc_data").empty().hide().append(datas['text']).fadeIn("slow");
				break;
			case 'get_current_location_map':
				$("#lp_loc_data").empty();
				$("#lp-sec-cont-2").removeClass("blur_map").empty().hide().append(datas['text']).fadeIn("slow");
				$(".lp-location-container").fadeIn("slow");
				$position();
				break;
			case "open_hub_widget":		
				clear_select();
				//add_select("["+datas['text'][4] + "="+ datas['text'][3] + "]");		
				add_and_open_clapan3("", datas['text'][0], (datas['text'][1]));
				break;
			case "change_factory_modifier":
				//alert( datas['text'][6] );				
				//break;
			case 'change_my_factory_goods_type':
				//a_alert(datas['fid']);
				$("[name= slide_slide_factories" + datas['fid'] + " ]").empty().append(datas['text']).css("display", "none").fadeIn("slow");
				break;
			case 'change_factory_goods_type':
			case "open_factory_widget":	
				ddData = datas['text'][2];
				add_and_open_clapan3("", datas['text'][0], (datas['text'][1]));
				//$(".smc_switcher_head").prepend($("#factory_pic"));
				switch_press(datas['text'][6], "factory_widget_");
				if(datas['text'][5])
				{					
					$('#select_modifier').ddslick({
													data:datas['text'][5],
													width:390,
													//height: 300,
													selectText: "Select modifier",
													imagePosition:"left",
													onSelected: function(selectedData)
													{
														//send(['change_factory_modifier', datas['text'][3], selectedData.selectedData.value]);
														ddData = selectedData.selectedData.value;
													} 													 
												});				
					/*a_alert(JSON.stringify(datas['text'][5]));*/
				}				
				clear_select();
				add_select("["+datas['text'][4] + "="+ datas['text'][3] + "]");		
				$(".smc_switcher_body").height(smc - 120);		
				if($("#factoty_productivity").size())
				{
					set_range(
								"#factoty_productivity", 
								{ 
									min:0, 
									max:$("#factoty_productivity").attr('max'), 
									onFinish:function(obj)
									{
										//console.log(obj.fromNumber);
										
									}
								}
							);
				}
				break;
			
			case 'open_routh_widget_klapan':
				clear_select();
				add_select("["+datas['text'][4] + "="+ datas['text'][3] + "]");		
				add_and_open_clapan3("", datas['text'][0], (datas['text'][1]));
				break;
			case "switch_location":
				window.location.replace(datas['text']);
				break;	
			//партия перемещена в другую Локацию
			case "transfer_gb":
				clear_select();
				close_modal();
				a_alert(datas['text']);
				clear_location_map_muar();
				//перерисовать путь
				$(".lp_eventer").detach();
				$("#externals_0").empty().append(datas['routes']);
				//апгрейдить центры маршрутов
				add_centers();
				//отслеживать окончание пересылки 
				//if(datas['bid']) 		start_look_bacth_move(datas['bid'], datas['period'], datas['rid']);
				set_routes_updater();
				break;
			//партия передана другому владельцу
			case "transfer_goods_batch":
				$("#lp-klapan3").css({'right':-$("#lp-klapan3").width()});
				$("#lp-klapan3").css('z-index', 100);
				is_open_clapan3 = false;
				close_modal();
				a_alert(datas['text']);
				break;	
			case "show_mine_gbatch":
				a_alert(datas['text'][0]);
				switch_press(0, "exec_batch_");	
				//$("#exec_batch_ .smc_switcher_body").height(300);
				break;
			case 'show_goods_batch':
				a_alert(datas['text'], null, null, "width:250px;");
				break;
			case "smp_more_factories":
				a_alert(datas['text']);
				$("#dialog_facs .smp_bevel_form").css({"height": "460px", "overflow-x": "auto", "width": "510px"});
				$(".box-modal").css({"max-width":"520px"});
				break;
			case "smp_storage_angar":
				$("#storage_wig").
					empty().
						//fadeOut().
							append(datas['text'])
								//fadeIn("slow");
				break;
			case "change_my_gb_par":
				close_modal();
				var table	= $("#"+gb_table_id).parent();
				table.empty().append(datas['slide']);				
				//window.location.replace(window.location.href);
				break;
			case "ch_fac_productivity":
				//a_alert(datas['text']);
				break;
			case "get_waybill":
				a_alert(datas['text'], null, null, "min-width:720px; display:inline-block;");
				$("#update_waybill").click(function(e)
				{
					if(confirm("update waybill"))
					{
						a_alert($("[name=waybill_post_title]").val());
					}
				});
				break;
			case "smp_get_circle_report":
				$("[name=slide_get_report_btn]").empty().
										append(datas['text']);
				break;
			case "smp_lgstcs_get_circle_report":
				$("[name=slide_get_lgstcs_report_btn]").empty().
										append(datas['text']);
				break;
			case "smp_complete_move_batch":
				if(datas['complete'])
				{
					set_message(datas['text']);
					smp_complete_move_batch(datas['bid'], datas['lid'], datas['platoon_id']);
					//перерисовываем путь
					$(".lp_eventer").detach();
					$("#externals_0").empty().append(datas['routes']);
					//апгрейдить центры маршрутов
					add_centers();
					set_routes_updater();
					remove_updater_elem( "routh_batch_" + datas['bid'] );
					//если партия принадлежит игроку - оповещаем его
					if(datas['is_owner'])
						a_alert(datas['is_owner']);
				}
				else
				{
					//start_look_bacth_move(datas['bid'], 0.25);
					set_message(datas['text']);
					add_updater_elm({handler:"blind_send", params:['smp_complete_move_batch', datas['bid']], during:2, id : "routh_batch_" + datas['bid'] });
				}
				break;
			case "change_factory_power":
				$("#smp_power_label"+datas['fid']).text(datas['text']);
				$("#prod"+datas['fid']).text(datas['product']);
				break;
			case "get_hub_form":
				$( "#slide_hubs_lists" + datas['id']).empty().append(datas['text']);
				$( "#slide_hubs_lists" + datas['id']).parent().fadeIn("slow");
				//$("#col2").empty();
				//$(datas['text']).appendTo("#col2").fadeIn("slow");
				add_tabs();
				batch_card_resize();
				break;
			case "get_store_list":
				$("#col2").empty();
				$(datas['text']).appendTo("#col2").fadeIn( "slow" );
				//add_tabs();
				batch_card_resize();
				break;
			case "search_gb":
				a_alert(datas['text']);
				break;			
			case 'convert_goods_batch':
				a_alert(datas['text']);
				break;		
			case 'show_batch_pult':
			case 'show_batch_payd':
				a_alert(datas['text'],null, null, null, datas['title']);
				$("[gbid="+datas['gid']+"] .smc_wait_spin").detach();
				break;
			case 'change_store':
				//a_alert(datas['text'],null, null, null, datas['title']);
				break;
			case 'pay_gb':
				a_alert(datas['text'],null, null, null, datas['title']);
				break;
			case "update_production_comment":
				$(".roduction_comment").empty().append(datas['text']);
				break;
		}
		if(datas['expected_time'])
		{
			expected_time = datas['expected_time'];
		}
		if(datas['funct'])
		{
			window[datas['funct']](datas['funct_args']);
		}
	})(jQuery);
});

//	counter expected time for end of production circle
var expected_time	= 0, expected_time_interval = -1;
function convert_time(time)
{
	return  around_time(time/3600) + ":" + around_time((time%3600)/60) + ":" + around_time(time%60);
}
function around_time(time)
{
	time	= String(parseInt(time));
	return (time.length<2)	 ? "0" + time : time;
}
function start_expected_time_interval(time)
{
	expected_time		= time;
	//a_alert("expected_time = "+expected_time)
	expected_time_interval = setInterval(function()
	{
		if(expected_time>0)
		{
			expected_time--;
			if(jQuery(".production_interval_counter").size()>0)
			{
				jQuery(".production_interval_counter").text(convert_time(expected_time));
			}
		}
		else
		{
			send(["update_production_comment"], false);
		}
	}, 1000);
}


//
function draw_map_external()
{
	
}
function printDiv(divID, func) 
{
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML = 
	  "<html><head><title></title></head><body>" + 
	  divElements + "</body>";
	jQuery(function()
	{
		func(divID);
	});	
	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;

  
}