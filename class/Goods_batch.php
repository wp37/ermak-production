<?php
	
	class Goods_Batch extends Moveble
	{
		protected $id;
		
		function __construct()
		{
			
		}	
		function set_price($id)
		{
			$good_type_id		= get_post_meta($id, "goods_type_id", true);
			$good_type_price	= get_post_meta($good_type_id, 'price', true);
			update_post_meta($id, "price", $good_type_price * 10);
			return $good_type_price;	
		}
		
		static function wp_drop_goods_type($params=null)
		{
			$args		= array(
									"numberposts"		=> 100,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'goods_type',
									'post_status' 		=> 'publish',									
								);
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		
		/*
		
		returned array of arrays by pattern:
			$sorted_batches[(string)$goods_type->ID][aray]
		
		
		*/
		public static function sort_batches_by_types($batches)
		{
			global $all_goods_types;
			global $current_goods_type;
			Goods_Type::get_global();
			//
			$sorted_batches		= array();	
			foreach($all_goods_types as $goods_type) 
			{
				$sorted_batches[(string)$goods_type->ID]= array();
			}
			//
			function by_type($batch)
			{
				global $current_goods_type;			
				//echo "---------". Assistants::echo_me(get_post_meta($batch->ID,"goods_type_id",true) == $current_goods_type->ID)."<BR>";
				return get_post_meta($batch->ID,"goods_type_id", true) == $current_goods_type->ID;
			}
			foreach($all_goods_types as $current_goods_type)
			{
				$sorted_batches[(string)$current_goods_type->ID]	= array_filter($batches, "by_type");
				//echo "<BR> --- ".$current_goods_type->post_title."<BR>";
				//var_dump($sorted_batches[(string)$current_goods_type->ID]);			
			}
			return $sorted_batches;
		}
		
		public static function sort_batches_by_industries($batches)
		{
			global $industries;
			global $current_num;
			$industries		= SMP_Industry::get_all_indastries();
			$current_num	= 0;
			
			//
			$sorted_batches		= array();	
			foreach($industries as $industry) 
			{
				$sorted_batches[(string)$industry->ID]= array();
			}
			
			foreach($batches as $batch)
			{
				$b				= SMP_Goods_Batch::get_instance($batch->ID);
				$gtid			= $b->get_meta( "goods_type_id" );
				$iid			= get_post_meta( $gtid, "industry", true );
				$sorted_batches[(string)$iid][]	= $batch;
			}
			return $sorted_batches;
		}
		static function by_industry($batch)
		{
			global $industries;
			global $current_num;	
			$is				= get_post_meta( get_post_meta( $batch->ID,"goods_type_id", true ), "industry", true ) == $industries[$current_num]->ID;
			$current_num	= count($industries) > $current_num + 1 ? $current_num + 1 : 0;	
			return $is	;
		}
		
		static function get_batch_hub_by_route($batch_id, $route_id)
		{
			$par						= get_option("smp_routh_$route_id");
			$is_id						= get_post_meta($batch_id,"dislocation_id", true);
			
			$start_hub_id				= $par['start_hub_id'];
			$finish_hub_id				= $par['finish_hub_id'];
			$start_dislocation			= get_post_meta($start_hub_id, "dislocation_id", true);
			$finish_dislocation			= get_post_meta($finish_hub_id, "dislocation_id", true);
			return $finish_dislocation == $is_id ? $finish_hub_id: $start_hub_id;
		}
		
		
		static function get_all_batches($metas=-1, $only_mine=true, $fields="all",  $numberposts=-1, $offset=0)
		{
			global $Soling_Metagame_Constructor;
			if(!is_array($metas))	$metas	= array();
			$meta_query				= array( 'relation' => 'AND' );
			if($only_mine)
			{
				$locs				= $Soling_Metagame_Constructor->all_user_locations();
				//insertLog("Goods_Batch::get_all_batches", $locs);
				if(count($locs)==0)	return array();				
				$meta_query[]		= array(
												'key'		=> 'owner_id',
												'value'		=> $locs,
												'operator'	=> "OR"
											);
			}
			foreach($metas as $key=>$value)
			{
				switch($key)
				{
					case "best_before":
						$compare	= ">";
						$arr		= array( "key"=>$key, "value"=>$value, "compare"=>$compare, "operators"=>$operators);
						break;
					case "dislocation_id":
						$my_locs	= SMC_Location::get_child_location_ids($value);
						if(count($my_locs) == 0)	return array();	
						$value		= $my_locs;
						$operators	= "OR";
						$arr	 	= array( "key"=>$key, "value"=>$value, "operators"=>$operators);
						break;
					default:
						$compare	= "=";
						$arr	 	= array( "key"=>$key, "value"=>$value, "compare"=>$compare);
						break;						
				}
				$meta_query[]		= $arr; 
			}
			//insertLog("Goods_Batch::get_all_batches", $meta_query);
			$arg		= array(
										'numberposts'	=> $numberposts,
										'offset'    	=> $offset,
										'orderby'  		=> 'id',
										'order'     	=> 'ASC',
										"fields"		=> $fields, 
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										'meta_query'	=> $meta_query
									); 
			$pos			= get_posts($arg);
			return $pos;
		}
		
		static function get_all_batchs_switcher($meta=-1, $only_mine=true, $name="icon_gb", $slide_id=0)
		{
			global $user_iface_color, $current_goods_type;
			if(!is_array($meta))		$meta = array();
			$all_goods_batchses		= static::get_all_batches($meta, $only_mine);
			$all_goods_types		= Goods_Type::get_global();
			$sorted_batches			= array();
			foreach($all_goods_types as $goods_type) 
			{
				$sorted_batches[(string)$goods_type->ID]= array();
			}
			
			$arr		= array();
			foreach($all_goods_types as $current_goods_type)
			{
				$sorted_batches[(string)$current_goods_type->ID]	= array_filter($all_goods_batchses, "by_type");
				//echo "<BR> --- ".$current_goods_type->post_title."<BR>";
				//var_dump($sorted_batches[(string)$current_goods_type->ID]);
				
			}
						
			$i=0;
			foreach($all_goods_types as $goods_type)
			{
				$title		=  Goods_Type::get_picto($goods_type->ID, 42, -1, count($sorted_batches[(string)$goods_type->ID]) );
				$slide		= "<div>";
				$cb			= $sorted_batches[(string)$goods_type->ID];
				$i = 0;
				$slide		.= '<h3><span style=\'font-weight:700; color:#AAA!important\'>'. __("Goods type", "smp").'</span> ' . $goods_type->post_title.'</h3>' ;
				$slide		.= "<table class='goods_type_tbl'>";
				foreach($cb as $goods_batch)
				{
					$gb		= SMP_Goods_Batch::get_instance($goods_batch->ID);
					$slide	.= $gb->get_table_row();				
					$i++;
				}
				if($i==0)
					$slide	.= "<tr><td><div class='smp-comment'>".__("No Goods of this type", "smp")."</div></td></tr>";
				$slide		.= "</table>";
				$slide	.= "</div>";
				$arr[]		= array("title" => $title, "slide" => $slide);
			}	
			$arr			= apply_filters("smp_pre_store_list_array", $arr);
			$html			.= Assistants::get_switcher($arr, $name, array("class"=>"smc_compact", "slide"=>$slide_id));
			return $html;
		}
	}
	function by_type($batch)
	{
		global $current_goods_type;			
		//var_dump(get_post_meta($batch->ID,"goods_type_id",true) == $current_goods_type->ID);
		return get_post_meta($batch->ID,"goods_type_id", true) == $current_goods_type->ID;
	}
?>