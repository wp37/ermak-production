<?php
	
	class SMP_Assistants
	{
		
		static function my_admin_bar_render() 
		{
			if(!current_user_can('administrator'))	return;
			global $wp_admin_bar;
			$wp_admin_bar->add_menu( array(
				'parent' => 'metagame', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'production_panel', // ID ������
				'title' => __('Ermak. Production', "smp"), //��������� ������
				'href' => "/wp-admin/admin.php?page=Metagame_Production_page" //��� �����	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'production_panel', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'industry_panel', // ID ������
				'title' => __('all Industries', 'smp'), //��������� ������
				'href' => "/wp-admin/edit.php?post_type=smp_industry" //��� �����	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'production_panel', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'goods_type_panel', // ID ������
				'title' => __('all Goods types', 'smp'), //��������� ������
				'href' => "/wp-admin/edit.php?post_type=goods_type" //��� �����	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'production_panel', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'factory_panel', // ID ������
				'title' => __('all Factories', 'smp'), //��������� ������
				'href' => "/wp-admin/edit.php?post_type=factory" //��� �����	
			));
			$wp_admin_bar->add_menu( array(
				'parent' => 'production_panel', //'false' ��� ��������� ����
							   //��� ID ������� ����
				'id' => 'goods_batch_panel', // ID ������
				'title' => __('all Goods batches', 'smp'), //��������� ������
				'href' => "/wp-admin/edit.php?post_type=".GOODS_BATCH_NAME //��� �����	
			));
		}
		// get current URL
		static function get_current_URL() 
		{
			$current_url  = 'http';
			$server_https = $_SERVER["HTTPS"];
			$server_name  = $_SERVER["SERVER_NAME"];
			$server_port  = $_SERVER["SERVER_PORT"];
			$request_uri  = $_SERVER["REQUEST_URI"];
			if ($server_https == "on") $current_url .= "s";
			$current_url .= "://";
			if ($server_port != "80") $current_url .= $server_name . ":" . $server_port . $request_uri;
			else $current_url .= $server_name . $request_uri;
			return $current_url;
		}
		
		
		
		//quality diagramm
		static function get_quality_diagramm($qual, $width=100, $height=18)
		{
			$h			= $height-16;
			$left		= ($width * $qual / 100-16);
			if($left>$width)	$left = $width - 16 ;
			$ltitle		= $left;//$qual < 70 ? ( $left + 10 ) : ( $left - 50 );
			$align		= $qual < 70 ? "center" : "center";
			return ' 
			<div style="position:relative; display:inline-block; width:' . $width . 'px; height:' . $height . 'px;" title="' . $qual . '%">
				 <div style="position:absolute; top:0; left:' . $left . 'px; display:inline-block; z-index:10;">
					 <svg width="32" height="20" viewbox="-16 0 32 20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<g>
							<path stroke="none" fill="#000000" d="M-16 0 L0 20 16 0 0 0"/>
						</g>
					 </svg>
				 </div>
				<div style="position:absolute; top:15px; left:0; display:inline-block; width:100% ; height:' . $h . 'px;">
					<div class="spm-quality-sec" style=" background:#960300; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#AC3902; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#BE6D04; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#D0A508; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#E4DF07; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#D0F70A; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#A9F806; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#84FB03; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#5BFB03; height:' . $h . 'px;"></div>
					<div class="spm-quality-sec" style=" background:#33FF02; height:' . $h . 'px;"></div>
				</div>
				<div class="qual_diagramm_title" style="min-width:32px; top:-3px; left:' . $ltitle . 'px;  z-index:11; text-align:' . $align . ';">' . $qual . '%</div>
			</div>
			';
		}
		/*
			$tabs		- array of arrays by:
							- title
							- slide
		*/
		static function get_tabs($tabs, $id_prefix="")
		{
			return Assistants::get_tabs($tabs, $id_prefix);
		}
		static function get_all_batchs_location($location_id, $owner_id=-1, $numberposts=-1, $offset=0, $hide_is_blocked=true)
		{
			$locs			= SMC_Location::get_child_location_ids($location_id);
			$meta			= array(
										'relation'			=> "AND",
										array(
												"key"		=> "dislocation_id",
												"value"		=> $locs,
												'compaire'	=> '=',
												'operator'	=> "OR"
											 ),				
										
									);
			/*
			if($hide_is_blocked)
			{
				$meta[]		= array(
										"key"		=> "is_blocked",
										"value"		=> '0',
										'compaire'	=> '='
									);										
			}
			*/
			if($owner_id !=- 1)
			{
				$meta[]		= array(
										"key"		=> "owner_id",
										"value"		=> $owner_id,
										'compare'	=> "=",
									 );										
			}
			
			$args			= array(
										'numberposts'	=> $numberposts,
										'offset'    	=> $offset,
										'orderby'  		=> 'id',
										"fields"		=> "ids",
										'order'     	=> 'ASC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										'meta_query'	=> $meta,	
									);
			return get_posts($args);
		}
		static function count_of_batchs($location_id, $owner_is=-1)
		{
			return count(self::get_all_batchs_location($location_id, $owner_id));
		}
		
		static function get_form($circle_id="")
		{
			require_once(SMP_REAL_PATH.'class/Circle_Report.php');
			//$html		= "<h1>". __("Circle reports", "smp"). "</h1>";
			$cr			= Circle_Report::get_global_count();
			$circle_id	=	$circle_id=="" ? $cr['current_circle']: $circle_id;
			$rep		= Circle_Report::get_global($circle_id);	
			$selector	= "<select name=choose_circle_id style='padding:3px; height:30px; vertical-align:middle;'>";
			for($i=0; $i<=$cr['current_circle']; $i++)
			{
				$selector	.= "<option value=$i ".selected($i, (int)$rep[0]["report_id"] , false) . ">$i</option>";
			}
			$selector		.= "</select>";
			$html		.= "<h1>". sprintf(__("Report from %s production circle", "smp"), $selector). "</h1>";
			
			if(!count($rep))
			{
				$html		.= "<div class='smp-comment' style='width:98%;'>". __("There are no present report", "smp")."</div>";
			}
			else
			{	
				$html		.= "
				<div class='smp-comment1' style='width:98%;'>".				
					"<table>
						<tr class='ob'>
							<td width='100'>".
								__("start time", "smp").   " 
							</td>
							<td>
								<b>" . date_i18n( __( 'M j, Y - G:i' ), $rep[0]["start_time"]). "</b>".
					"		</td>
						</tr>
						<tr class='ab'>
							<td>".
								__("finish time", "smp"). " 
							</td>
							<td>
								<b>" .  date_i18n( __( 'M j, Y - G:i' ), strtotime($rep[0]["finish_time"])). "</b>".
					"		</td>
						</tr>
					</table>".
				"</div>";
				$html		.= $rep[0]["content"];
			}
			return $html;
		}
		
		static function location_display_tbl1($text, $location, $term_meta, $location_type)
		{
			/*
			$html		= "
			<div class='cell_0'>
				<h3>".__("Anover block", "smp"). "</h3>
			</div>";*/
			return $text.$html;
			
		}
		static function location_display_tbl0($text, $location, $term_meta, $location_type)
		{
			return $text. Factories::get_display_form($location->term_id);
		}
		static function location_display_tbl111($text, $location, $term_meta, $location_type)
		{
			if(!SolingMetagameProduction::is_logistics())	return;
			$rouths		= SMP_Hub::get_all_rouths_by_location($location->term_id);
			$args		= array(
				'numberposts'	=> -1,
				'offset'     	=> 0,
				'orderby' 		=> 'id',
				'order'  		=> 'ASC',											
				'post_status'	=> 'publish',											
				'post_type'		=> GOODS_BATCH_NAME,
				'meta_query'	=> array(
											'relation'		=> 'AND',
											
											array(
												'value'		=> $rouths,
												'key' 		=> 'transportation_id',
												'operator' 	=> 'OR'
												),
											array(
												"key"		=> "dislocation_id",
												"value"		=> $location->term_id,
												"compare"	=> "!="
												),
											array(
												"key"		=> "is_permission",
												"value"		=> true,
												"compare"	=> "="
												)
										)
			);
			$batches	= get_posts( $args );	
			$btch		= "";
			foreach($batches as $batch)
			{
				$b		= SMP_Goods_Batch::get_instance($batch);
				$btch	.= SMP_Goods_Batch::get_picto($batch->ID, 1, array("size"=>100, "left_text"=>"large", "is_menu"=>false));
			}
			$html		.= "
			<div class='cell_0'>
				<h3>".__("Goods batch arrived", "smp"). "</h3>$btch
			</div>";
			return $text.$html;
		}
		static function location_display_tbl20($text, $location, $term_meta, $location_type)
		{
			global $Soling_Metagame_Constructor;			
			$batches	= Batch::search(array('dislocation_id' => $location->term_id, "is_blocked" => "1"));
			$btch		= "<div id='ldgbspodval'>";
			foreach($batches as $batch)
			{
				$b		= SMP_Goods_Batch::get_instance($batch);
				$btch	.= SMP_Goods_Batch::get_picto($batch->ID, 0, array("size"=>100, "is_menu"=>true, "is_owner"=>true));
			}
			$btch	.= "</div>";
			$html	=	"
			<table class='table_podval'>
			<tr>
				<td style='width:80px;'>
					<h3>" . __("Storage", "smp") . "</h3>
					<p style='margin:20px;'></p>".
					static::search_goods_batch_form($location->term_id).
				"</td>
				
				<td>
					<div id='gb_podval'>						
						$btch
						 <div u='navigator' id='gbnavi' style='position:absolute; top:-25px; right:80px; height:23px; width:300px;'>
							
						</div>
					</div>
				</td>
			</tr>
			</table>";
			return $text . $html;
		}
		static function search_goods_batch_form($location_id, $is_convert=true)
		{
			global $Soling_Metagame_Constructor;
			if(current_user_can("manage_options"))
			{
				$add_button	= "<div id='add_button' class='button smc-alert' target_name='add_btn_win' title='".__("Insert new goods batch", "smp"). "' style='padding:7px 12px; width:40px;'><i class='fa fa-plus'></i></div>";
				$add_form	= "
				<div class='lp-hide' id='add_btn_win'>
					<div>
						<label>".__("Owner", "smc")."</label><br>".
							$Soling_Metagame_Constructor->get_location_selecter(array("id"=>"ins_owner", 'class'=>'labll', 'style'=>'width:350px;')).
						"
					</div>
					<div>
						<label>".__("Goods type", "smp")."</label><br>".
							Goods_Type::wp_dropdown_goods_type(array("id"=>"ins_gtype", 'class'=>'labll', 'style'=>'width:350px;')).
						"
					</div>
					<div>
						<label>".__("Count", "smp")."</label><br>
						<input id='gb_count' type='number' min='1' step='1' value='' placeholder='" . __("insert count", "smp") . "' style='width:350px;'/>
						
					</div>
					<div class='button' id='start_insert_gb' style='margin:10px;padding:10px 20px;' loc_id='" . $location_id . "'>".__("Start", "smp")."</div>
				</div>";
			}
			$html	= "
			<div style='margin-bottom:3px;'>
				<div id='search_btn' class='button smc-alert' title='".__("Search goods batch", "smp").  "' target_name='srch_btn_win' style='padding:7px 12px; width:40px;'><i class='fa fa-search'></i></div>					
				$add_button
			</div>
			<div class='lp-hide' id='srch_btn_win' loc_id='" . $location_id. "' is_convert='".$is_convert."' >".
				"<div>
					<label>".__("Search by ID", "smp")."</label><br>
					<input id='search_id' type='number' min='1' step='1' value='' placeholder='" . __("insert ID of serching batch", "smp") . "' style='width:350px;'/>
					
				</div>
				<div>
					<label>".__("Search by owner", "smp")."</label><br>".
						$Soling_Metagame_Constructor->get_location_selecter(array("id"=>"search_owner", 'class'=>'labll', 'style'=>'width:350px;')).
					"
				</div>
				<div>
					<label>".__("Search by goods type", "smp")."</label><br>".
						Goods_Type::wp_dropdown_goods_type(array("id"=>"search_gtype", 'class'=>'labll', 'style'=>'width:350px;')).
					"
				</div>
				<div class='button' id='start_search_gb' style='margin:10px;padding:10px 20px;' loc_id='" . $location_id . "' is_convert='".$is_convert."'>".__("Start", "smp")."</div>
			</div>
			$add_form
			";
			return $html;
		}
		static function ermak_body_script()
		{
			global $post;
			echo "<style>
				body {
					overflow:hidden!important;
				}
			</style>";
			if($post->ID 	!= $Soling_Metagame_Constructor->options['location_display'])
			{
				echo "
				<link rel='stylesheet' id='smp_empty'  href='".SMP_URLPATH ."css/empty.css' type='text/css' media='all' />
				<script type='text/javascript' src='".SMP_URLPATH ."js/empty.js'></script>
				";
				
			}
		}
		
		
		
		static function smc_add_option($options)
		{
			$data								= get_option(SMP);
			unset($data['my_calc_ID']);
			unset($data['store_ID']);
			unset($data['new_mp_ID' ]);
			unset($data['my_gb_ID']);
			unset($data['bank_ID']);
			unset($data['my_routh_ID']);
			unset($data['my_hub_ID']);
			unset($data['report_page_id']);
			unset($data['PTP']);
			$options[SMP]						= array( "t" => array("type"=>"option"), "post_type"=>SMP, "merge_type"=>MERGE_OPTION, "data" => $data );	
			$options['current_circle_id']		= array( "t" => array("type"=>"option"), "post_type"=>'current_circle_id', "merge_type"=>CHANGE_OPTION, "data" => get_option('current_circle_id'));	
			//$options['current_circle_start']	= array( "t" => array("type"=>"option"), "merge_type"=>"change", "data" => get_option('current_circle_start'));	
			return $options;
		}
		static function smc_add_object_type($array)
		{
			$smp_industry						= array();
			$smp_industry['t']					= array('type'=>'post');
			$smp_industry['resourse_name']		= array('type'=>'string');
			$smp_industry['factory_name']		= array('type'=>'string');
			$smp_industry['color']				= array('type'=>'string');
			$smp_industry['_thumbnail_id']		= array('type'=>'media', 	'download'=>true);
			$array['smp_industry']				= $smp_industry;
			//			
			$goods_type							= array();
			$goods_type['t']					= array('type'=>'post');
			$goods_type['description']			= array('type'=>'string');
			$goods_type['industry']				= array('type'=>'id', 'object'=>'smp_industry');
			$goods_type['price']				= array('type'=>'number');
			$goods_type['complexity']			= array('type'=>'number');
			$goods_type['usage_duration']		= array('type'=>'number');
			$goods_type['is_cargo']				= array('type'=>'bool');
			$goods_type['color']				= array('type'=>'string');
			$goods_type['quality_multiplier']	= array('type'=>'number');
			$goods_type['powerfull_multiplier']	= array('type'=>'number');
			$goods_type['capacity_multiplier']	= array('type'=>'number');
			$goods_type['speed_multiplier']		= array('type'=>'number');
			$goods_type['cost_multiplier']		= array('type'=>'number');
			$goods_type['goods_type_equipment']	= array('type'=>'id', 		'object'=>'goods_type');
			$goods_type['is_equipment']			= array('type'=>'bool');
			$goods_type['is_freighter']			= array('type'=>'bool');
			$goods_type['_thumbnail_id']		= array('type'=>'media', 	'download'=>true);
			$goods_type['components']			= array(
															'type'			=> 'array',	
															'object'		=> array(
																'goods_type'	=> array(
																	'type'		=> 'id', 
																	't'			=> "post", 
																	"object"	=> 'goods_type'
																), 
																'goods_value'	=> 'number'
															)
														);
			$goods_type['components2']			= array(
															'type'			=> 'array',	
															'object'		=> array(
																'goods_type'	=> array(
																	'type'		=> 'id', 
																	't'			=> "post", 
																	"object"	=> 'goods_type'
																), 
																'goods_value'	=> 'number'
															)
														);
			$array['goods_type']				= $goods_type;
			//
			$factory							= array();
			$factory['t']						= array('type'=>'post');
			$factory['owner_id']				= array('type'=>'id', "object" => "location");
			$factory['dislocation_id']			= array('type'=>'id', "object" => "location");
			$factory['goods_type_id']			= array('type'=>'id', "object" => "goods_type");
			$factory['modificator']				= array('type'=>'id', "object" => GOODS_BATCH_NAME);
			$factory['industry']				= array('type'=>'id', "object" => "smp_industry");
			$factory['powerfull']				= array('type'=>'number');
			$factory['quality']					= array('type'=>'number');
			$factory['picture_url']				= array('type'=>'string');
			$factory['quality_modificator']		= array('type'=>'id',		"object" => GOODS_BATCH_NAME);
			$factory['is_blocked']				= array('type'=>'bool');
			$factory['available_goods_types']	= array(
															'type'		=> 'array',	
															'object'	=> array( 
																array (
																	"type"			=> "array", 
																	"goods_type"	=> array (
																		'type'		=> 'id', 
																		't'			=> 'post', 
																		'object'	=> 'goods_type' 
																	),
																	"techology_type"=> "number"
																)
															)
														);
														
			$array['factory']					= $factory;
			//
			$goods_batch						= array();
			$goods_batch['t']					= array('type'=>'post');
			$goods_batch['factory_id']			= array('type'=>'id', 'object'=>'factory');
			$goods_batch['dislocation_id']		= array('type'=>'id', 'object'=>'location');
			$goods_batch['owner_id']			= array('type'=>'id', 'object'=>'location');
			$goods_batch['goods_type_id']		= array('type'=>'id', 'object'=>'goods_type');
			$goods_batch['store']				= array('type'=>'bool');
			$goods_batch['count']				= array('type'=>'number');
			$goods_batch['price']				= array('type'=>'number');
			$goods_batch['quality']				= array('type'=>'number');
			$goods_batch['best_before']			= array('type'=>'number');
			$goods_batch['is_blocked']			= array('type'=>'bool');
			$goods_batch['transportation_id']	= array('type'=>'id', 'object'=>'smp_routh');
			$goods_batch['currency_type_id']	= array('type'=>'id', 'object'=>'smp_currency_type');
			$goods_batch['is_permission']		= array('type'=>'bool');
			$goods_batch['waybill_tact']		= array('type'=>'number');
			$array[GOODS_BATCH_NAME]			= $goods_batch;
			//
			$smp_route_type						= array();
			$smp_route_type['t']				= array('type'=>'post');
			$smp_route_type['color']			= array('type'=>'string');		
			$smp_route_type['hub_name']			= array('type'=>'string');		
			$array['smp_route_type']			= $smp_route_type;
			//
			$smp_hub							= array();
			$smp_hub['t']						= array('type'=>'post');
			$smp_hub['dislocation_id']			= array('type'=>'id', 'object'=>'location');
			$smp_hub['owner_id']				= array('type'=>'id', 'object'=>'location');
			$smp_hub['capacity']				= array('type'=>'number');
			$smp_hub['cost']					= array('type'=>'number');
			$array['smp_hub']					= $smp_hub;
			//
			$smp_hub_permission					= array();
			$smp_hub_permission['t']			= array('type'=>'post');
			$smp_hub_permission['preority']		= array('type'=>'number');
			$smp_hub_permission['hub_id']		= array('type'=>'id', 'object'=>'smp_hub');
			$smp_hub_permission['owner_id']		= array('type'=>'id', 'object'=>'location');
			$smp_hub_permission['goods_type_id']= array('type'=>'id', 'object'=>'goods_type');			
			$array['smp_hub_permission']		= $smp_hub_permission;
			//
			$smp_routh							= array();
			$smp_routh['t']						= array('type'=>'taxonomy');
			$smp_routh['type']					= array('type'=>'id', 'object'=>'smp_route_type');
			$smp_routh['location_id']			= array('type'=>'id', 'object'=>'location');	
			$smp_routh['start_hub_id']			= array('type'=>'id', 'object'=>'smp_hub');		
			$smp_routh['finish_hub_id']			= array('type'=>'id', 'object'=>'smp_hub');		
			$smp_routh['cost']					= array('type'=>'number');		
			$smp_routh['capacity']				= array('type'=>'number');		
			$smp_routh['speed']					= array('type'=>'number');		
			$smp_routh['geometry']				= array('type'=>'svg');		
			$smp_routh['class']					= 'smp_routh';		
			$array['smp_routh']					= $smp_routh;
			//
			$smc_currency_type					= array();
			$smc_currency_type['t']				= array('type'=>'post');
			$smc_currency_type['rate']			= array('type'=>'float');
			$smc_currency_type['abbreviation']	= array('type'=>'string');	
			$array[SMP_CURRENCY_TYPE]			= $smc_currency_type;
			//
			$smc_currency_accaunt				= array();
			$smc_currency_accaunt['t']			= array('type'=>'post');
			$smc_currency_accaunt['owner_id']	= array('type'=>'id', 'object'=>'location');
			$smc_currency_accaunt['count']		= array('type'=>'number');
			$smc_currency_accaunt['ctype_id']	= array('type'=>'id', 'object'=>'smc_currency_type');
			$smc_currency_accaunt['is_lock']	= array('type'=>'bool');
			$array[SMP_CURRENCY_ACCOUNT]		= $smc_currency_accaunt;
			//
			$smp_invoice						= array();
			$smp_invoice['t']					= array('type'=>'post');
			$smp_invoice['summae']				= array('type'=>'number');
			$smp_invoice['payer']				= array('type'=>'id', 'object'=>'location');
			$smp_invoice['payee']				= array('type'=>'id', 'object'=>'location');
			$smp_invoice['is_repaid']			= array('type'=>'bool');
			$smp_invoice['delivery_time']		= array('type'=>'number');
			$smp_invoice['repaid_time']			= array('type'=>'number');			
			$array['smp_invoice']				= $smp_invoice;
			//			
			$smp_waybill						= array();
			$smp_waybill['t']					= array('type'=>'post');
			$smp_waybill['goods_type_id']		= array('type'=>'number');
			$smp_waybill['owner_id']			= array('type'=>'number');			
			$smp_waybill['action']				= array(
															'type'			=> 'array', 
															'object' 		=> array(
																"action"	=> "number", 
																"location_id"=> array(
																	"type"	=> "id", 
																	"t"		=> "taxonomy", 
																	"object"=> SMC_LOCATION_NAME
																),
																"counts"	=> "number", 
																"frequency"	=> "number"
															) 
														);				
			$array[SMP_WAYBILL_TYPE]			= $smp_waybill;
			
			$array[SMC_LOCATION_NAME]['currency_type'] = array('type'=>"id", 'object'=>SMP_CURRENCY_TYPE);
			
			return $array;
		}
	}
function around_time($time)
{
	if(strlen($time) <2)
		return "0".$time;
	else
		return $time;
}
?>