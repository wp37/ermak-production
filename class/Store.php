<?php

	class SMP_Store
	{
		function __construct()
		{
			
		}
		static function install()
		{
			$my_post = array(
								  'post_title'   		=> __("Stores", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smp_store]"  ,
								  'post_status'  		=> 'publish',
								   'comment_status'		=> 'closed',
								);

			return	wp_insert_post( $my_post );
		}
		
		static function the_content($post, $content, $before, $after)
		{
			return $content;
		}
	}

?>