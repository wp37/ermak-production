<?php
	class SolingMetagameProduction
	{
		static function smp_deinstall()
		{
			$op						= get_option(SMP);
			$pp						= array(
												"ID"			=> $op['new_mp_ID'],
												"post_title"	=> "DELETED POST!!!"
											);
			wp_update_post($pp);
			wp_delete_post($op['new_mp_ID'], true);
			wp_delete_post($op['my_gb_ID'], true);
			wp_delete_post($op['store_ID'], true);
			wp_delete_post($op['bank_ID'], true);
			wp_delete_post($op['PTP'], true);
			wp_delete_post($op['my_routh_ID'], true);
			wp_delete_post($op['my_hub_ID'], true);
			delete_option(SMP);
			delete_option('current_circle');
		}
		public $options;
		public $report;
		function __construct()
		{
			
			$this->options							= get_option(SMP);	
			if(!is_array($this->options))			$this->options = array();
			define('MAX_BEST_BEFORE', ($this->is_best_before() ? 900 : -1));
			add_action( 'init',						array($this, 'hook_init'));
			add_action( 'wp_head',					array($this, 'hook_css'));
			add_action( 'admin_head',				array($this, 'admin_hook_css'));
			
			add_action( 'admin_enqueue_scripts', 	array($this, 'scripts_add') );
			add_action( 'wp_enqueue_scripts', 		array($this, 'front_script_add'),20 );
			
			add_filter( 'cron_schedules', 			array($this, 'cron_add_production_interval'),10 );
			add_action( 'production_hook', 			array($this, 'production_handler' ));
			add_action( 'admin_menu',				array($this, 'admin_page_handler'), 1);
			add_filter( 'the_title',				array($this, 'the_title'),10,2 );
			add_filter( 'the_content',				array($this, 'the_content') );
			add_filter( 'smc_load_panel_screen',	array($this, 'smc_load_panel_screen'), 10, 2 );
			add_filter( 'smc_display_special_panel',array($this, 'smc_display_special_panel'), 10, 1 );
			
			//НЕ подгружаем виджеты локаций в отъезжающую панель!!!!!!!!!
			//add_filter( 'smc_widget_location_form', array($this, 'smc_widget_location_form'), 10, 2 );
			
			add_filter("smc_login_widget_list",		array($this, "smc_login_widget_list1"), 10);
			add_filter("smc_login_widget_list",		array($this, "smc_login_widget_list2"), 20);
			
			add_action("smc_lp_head_1",				array($this, "smc_lp_head_1"), 1); 	
			add_filter('smc_map_breadcrumb', 		array($this, 'smc_lp_head_1'), 11, 3);
			add_action('smc_myajax_submit', 		array($this, 'smc_myajax_submit'),12);
			add_action('smc_myajax_admin_submit', 	array($this, 'smc_myajax_admin_submit'),12);
			
			if ( ! wp_next_scheduled( 'production_hook' ) ) 
			{
			  wp_schedule_event( time(), 'production_interval', 'production_hook' );
			}
			
			add_filter( "smc_admin_special_pages_choosee", 	array($this, 'smc_admin_special_pages_choosee'));
			add_filter( "smc_widget_infrastructure", 		array($this, 'smc_widget_infrastructure'), 10, 2);
			add_action( "smc_admin_sinc_special_pages",		array($this, 'smc_admin_sinc_special_pages'));
			add_filter( "open_location_content_widget",		array($this, 'open_location_content_widget'), 10, 2);
			
			add_action('wp_dashboard_setup', 				array($this, 'add_dashboard_widgets'));
			add_action( 'init',								array($this, 'init_achive_types') , 2);
			add_filter( 'plugin_action_links', 				array($this, 'plugin_action_links'), 14, 2 );
			add_action( 'admin_notices',					array($this, 'after_install_sticker') , 43);
				//	Metagame menu in frontend		
			add_action( 'wp_before_admin_bar_render', 		array(SMP_Assistants,'my_admin_bar_render' ), 11);
			
			add_action( 'smc_add_option',					array(SMP_Assistants, 'smc_add_option') , 11);
			add_action( 'smc_add_object_type',				array(SMP_Assistants, 'smc_add_object_type') , 11);
			add_filter( "smc_location_type_meta", 			array($this, "smc_location_type_meta"), 11, 2);
			add_action( 'save_post_location_type',			array($this, 'true_save_box_data'), 11);
			
			add_filter("smp_my_tools",						array(Goods_Type, 'smp_my_tools'), 9);				
			//add_action( "updated_post_meta", 				array($this, 'updated_post_meta'), 10, 4 );
			//add_filter("smc_setting_page",					array($this, "smc_setting_page"), 1);
			add_filter("smc_admin_head_style", 				array($this, "smc_add_vocabulary"), 2);
			add_filter("smc_front_head_style", 				array($this, "smc_add_vocabulary"), 11);
			
			//add_filter("location_display_tbl0", 			array(SMP_Assistants, "location_display_tbl0"), 11, 4);
			add_filter("location_display_tbl1", 			array(SMP_Assistants, "location_display_tbl1"), 11, 4);
			add_filter("location_display_tbl111", 			array(SMP_Assistants, "location_display_tbl111"), 11, 4);
			add_filter("location_display_tbl20", 			array(SMP_Assistants, "location_display_tbl20"), 11, 4);
			add_action( 'ermak_body_script',				array(SMP_Assistants, 'ermak_body_script'));
			add_filter( "smc_ajax_data", 					array($this, "smc_ajax_data"));
		}
		function smc_ajax_data($data)
		{
			global $SolingMetagameProduction;
			$data[1]['expected_time']	= $SolingMetagameProduction->get_expected_finish_time();
			//if($data[0]!= "reload_log") insertLog("smc_ajax_data", $data);
			return $data;
		}
		static function is_finance()
		{
			global $SolingMetagameProduction;
			return $SolingMetagameProduction->options['finance_enabled'];
		}
		static function is_logistics()
		{
			global $SolingMetagameProduction;
			return $SolingMetagameProduction->options['logistics_enabled'];
		}
		function updated_post_meta($meta_id, $object_id, $meta_key, $meta_value)
		{
			//insertLog(".updated_post_meta", array("meta_id"=>$meta_id, "object_id"=>$object_id, "meta_key"=>$meta_key, "meta_value"=>$meta_value));
			switch($meta_key)
			{
				case "dislocation_id":					
					$obj			= get_post($object_id);
					$object_type	= $obj->post_type;
					do_action("smp_{$object_type}_change_dislocation_id", $object_id, $object_type, $meta_value);
				case "owner_id":
					$obj			= get_post($object_id);
					$object_type	= $obj->post_type;
					do_action("smp_{$object_type}_change_owner_id", $object_id, $object_type, $meta_value);
					break;
				default:
					return;
			}
		}
		function after_install_sticker()
		{
			$install_ermak_production 		= get_option('install_ermak_production');
			if(!($install_ermak_production == 1))	return;
			echo "
			<div class='updated notice smc_notice_ is-dismissible1' id_btn='install_ermak_production_notice' >
				<p>".
					$this->get_scenario_button().
				"</p>
				<span class='smc_desmiss_button' setting_data='install_ermak_production'>
					<span class='screen-reader-text'>".__("Close")."</span>
				</span>
			</div>
			
			";
		}
		function get_scenario_button()
		{
			return "
			<div class='smc_notice'>
				<h2>".
				__("Ermak Production", "smp").
				"</h2>
				<h4>".
				__("Select one of packeges for install to your project. Package include Industries, Goods Types and and Routh Types.", "smp").
				"</h4>
			</div>
			<div class='smc_notice smp_traditional' style=''>
				<div class='smc_notice_cube2 '>".
					__('traditional package','smp').
				"</div>
			</div>
			<div class='smc_notice smp_fantasy' style=''>
				<div class='smc_notice_cube2 '>".
					__('fantasy package','smp').
				"</div>
			</div>
			<div class='smc_notice smp_modern' style=''>
				<div class='smc_notice_cube2 '>".
					__('modern package','smp').
				"</div>
			</div>
			";
		}
		function is_best_before()
		{
			return $this->options['gb_settings']['best_before'];
		}
		function is_dislocation()
		{
			return $this->options['gb_settings']['dislocation_id'];
		}
		function get_current_circle_id()
		{
			if($this->current_circle_id)
				return $this->current_circle_id;
			$this->current_circle_id				= get_option("current_circle_id");
			return $this->current_circle_id;
		}
		
		function plugin_action_links( $links, $file )
		{
			if ( $file == 'Ermak_production/Ermak_production.php'  ) 
			{
				$links[] = '<a href="' . admin_url( 'admin.php?page=Metagame_Production_page' ) . '">'. __("Settings")  .'</a>';
			}

			return $links;
		}
		function add_dashboard_widgets() 
		{
			if(!current_user_can("manage_options"))	return;
			wp_add_dashboard_widget('ermak_production_widget', __("Metagame Production", "smp"),	array($this, 'production_dashboard_widget_handler'), array($this, 'production_dashboard_setting'));
			wp_add_dashboard_widget('ermak_finance_widget', __("Metagame Finance", "smp"),			array($this, 'finance_dashboard_widget_handler'));
			wp_add_dashboard_widget('ermak_logistics_widget', __("Metagame Logistics", "smp"),		array($this, 'logistics_dashboard_widget_handler'), array($this, "logistics_widget_settings"));
		}
		function production_dashboard_widget_handler()
		{
			global $Soling_Metagame_Constructor, $wpdb;
			$current_circle_id		= get_option("current_circle_id");
			if($_POST['start_all_productions'])
			{
				$this->options['productions_started']		= true;
				update_option('current_circle_start', time());
				echo 'start';
			}
			if($_POST['finish_all_productions'])
			{
				$this->options['productions_started']		= false;
				echo 'finish';
			}
			if($_POST['pushing'])
			{
				echo "<div class='lp-hide' id='circle_result'><h2>".__("Circle results", "smp")."</h2>";
				$this->count_circle();
				echo "</div><div class='smc-alert lp-link button' target_name='circle_result' style='margin-bottom:5px;'>".__("Show results", "smp") ."</div>";
				//echo " <BR>THE END";
			}
			if($_POST['update_current_circle'])
			{
				global $wpdb;
				$ccid				= $_POST['current_circle_id'];
				$query				= "DELETE FROM `". $wpdb->prefix.REPORT_DB_PREFIX . "` WHERE report_id>" . $ccid;
				$wpdb->get_results($query);
				update_option("current_circle_id", $ccid);
			}
			update_option(SMP, $this->options);
			?>
			<form name="settings"  method="post"  enctype="multipart/form-data" id="settings">	
				<div style="position:relative; display:inline-block;">
					<div class="h0">
						<div class="h">
							<board_title><?php echo sprintf(__("Now is %s circle in during", "smp"), $current_circle_id)?></board_title>
						</div>
						<div class="h" style="display: <?php echo $this->options['productions_started'] ? "none" : "display" ?>;">
							<board_title><?php _e("Start All Productions","smp"); ?></board_title>
							<input type="submit" name="start_all_productions" class="smp-button" value="<?php _e("Do Start for all productions","smp");?>"/>
						</div>
						<div class="h"  style="display: <?php echo $this->options['productions_started'] ? "display" : "none" ?>;">
							<board_title><?php _e("Finish All Productions","smp"); ?></board_title>
							<input type="submit" name="finish_all_productions" class="smp-button" value="<?php _e("Do Pause for all productions","smp");?>" style="background:red;"/>
						</div>	
						<div  class="h" style="width:240px;">
							<board_title><?php _e("Pushing execute circle", "smp"); ?></board_title>
							<input type="submit" name="pushing"  value="<?php _e("execute circle","smp");?>" style=""/><BR>
							<input type="number" name="current_circle_id" value="<?php echo $current_circle_id; ?>"/>
							<input type="submit" name="update_current_circle"  value="<?php _e("update current circle","smp");?>" style=""/><BR>
							
						</div>
					</div>
					<div class="h0">
						<div class="h" style="width:240px;">
							<board_title><?php _e("Change productions for Factories's group", "smp"); ?></board_title>
							<div class="smp_switcher_container">
								<input type="radio" name="production_by" id="production_by0" value="0" class="css-checkbox" checked />
								<label for="production_by0" class="css-label"><?php  _e("by Locations", "smp")?></label>
								<BR>
								<input type="radio" name="production_by" id="production_by1" value="1" class="css-checkbox"/>
								<label for="production_by1" class="css-label"><?php  _e("by Godds type", "smp")?></label>
								<BR>
								<input type="radio" name="production_by" id="production_by2" value="2" class="css-checkbox"/>
								<label for="production_by2" class="css-label"><?php  _e("by Industry", "smp")?></label>
								<BR>
								<input type="radio" name="production_by" id="production_by3" value="3" class="css-checkbox"/>
								<label for="production_by3" class="css-label"><?php  _e("personally", "smp")?></label>
							</div>
							<div class="smp_switcher_container" id="factory_production_by_location" style=""  placeh="<?php _e("Select one or few Location", "smp");?>">
								<?php
									echo $Soling_Metagame_Constructor->get_location_selecter(
																								array(
																										"name"		=> "production_fac_loc_for_change[ ]",
																										"id"		=> "production_fac_loc_for_change",
																										"class"		=> "chosen-select1",
																										"multile"	=> true,
																										"style"		=> "width:100%; "
																									  )																			
																							 );
								?>
								
							</div>
							<div class="smp_switcher_container" id="factory_production_by_goods_type" style="display:none;" placeh="<?php _e("Select one or few Goods type", "smp");?>">
								<?php
									echo Goods_Type::wp_dropdown_goods_type(
																				array(
																						"name"		=> "production_fac_gt_for_change[ ]",
																						"id"		=> "production_fac_gt_for_change",
																						"class"		=> "chosen-select1",
																						"multile"	=> true,
																						"style"		=> "width:100%; "
																					  )	
																			);
								?>
							</div>
							<div class="smp_switcher_container" id="factory_production_by_industry" style="display:none;" placeh="<?php _e("Select one or few Industry", "smp");?>">
								<?php
									echo SMP_Industry::wp_dropdown_industry(
																				array(
																						"name"		=> "production_fac_industry_for_change[ ]",
																						"id"		=> "production_fac_industry_for_change",
																						"class"		=> "chosen-select1",
																						"multile"	=> true,
																						"style"		=> "width:100%; "
																					  )	
																			);
								
								?>
							</div>
							<div class="smp_switcher_container" id="factory_production_personally" style="display:none;" placeh="<?php _e("Select one or few Factories", "smp");?>">
								
								<?php echo Factories::wp_dropdown_factories(
																				array(
																						"name"		=> "production_factories_for_change[ ]",
																						"id"		=> "production_factories_for_change",
																						"class"		=> "chosen-select1",
																						"multile"	=> true,
																						"style"		=> "width:100%; "
																					  )
																			  ) ?>
							</div>
							<div class="smp_switcher_container">
								<label for="production_factories_coef"><?php _e("Production coeficient", "smp"); ?></label>
								<input  class="h2" name="production_factories_coef" id='production_factories_coef' value="0"/>
							</div>
							<div>
								<input type="text" class="button" id="change_production_factories" value="<?php _e("Change");?>" question2="<?php _e("Do you really want to make this irreversible action?", "smp")?>" question1="<?php _e("Change  production powerfull ", "smp"); ?>" error="<?php _e("You must select at least one item from the list and change the coefficient.", "smp"); ?>"/>
							</div>
						</div>
					</div>
				</div>
				
			</form>
			<script type="text/javascript">
				var slider1;
				(function($)
				{		
					$("#production_factories_coef").ionRangeSlider({
							min: -100,
							max: 100, 			
							type: 'single',
							step: 1,
							postfix: "%",
							prettify: true,
							hasGrid: true
					});
					set_chosen("#production_fac_loc_for_change", {width:"100%", placeholder_text_multiple:$("#factory_production_by_location").attr("placeh")});
				})(jQuery)
			</script>
			<?php
		}
		function production_dashboard_setting()
		{
			if($_POST['save_gb'])
			{
				$this->options['gb_settings'] 							= array();
				$this->options['gb_settings']['dislocation_id'] 		= $_POST['gb_dislocation']=="on";
				$this->options['gb_settings']['quality'] 				= $_POST['gb_quality']=="on";
				$this->options['gb_settings']['best_before'] 			= $_POST['gb_best_before']=="on";
				$this->options['gb_settings']['factory_id'] 			= $_POST['gb_factory_id']=="on";
				$this->options['gb_settings']['store'] 					= $_POST['gb_shopting']=="on";
				$this->options['gb_settings']['deleting'] 				= $_POST['gb_deleting']=="on";
				$this->options['gb_settings']['combine'] 				= $_POST['gb_combine']=="on";
			}
			$this->update_options();
			?>
			
			<form name="settings"  method="post"  enctype="multipart/form-data" id="settings">	
				<div style="display:inline-block; position:relative;">
				<div class='h0'>							
					<div class="h" style="background:#DADFE3; color:#555!important;">
						<?php //var_dump( $this->options['gb_settings'] )?>
						<board_title><?php ?><?php _e("Availbled Goods Batch Parameters", "smp");?></board_title>
						<p>
							<input class="css-checkbox" name="gb_dislocation" type="checkbox" id="gb_dislocation" <?php checked(1, $this->options['gb_settings']['dislocation_id']);?>/>
							<label class="css-label" for="gb_dislocation"><?php _e("Dislocation", "smp");?></label>
						</p>
						<p>
							<input class="css-checkbox" name="gb_quality" type="checkbox" id="gb_quality" <?php checked(1, $this->options['gb_settings']['quality']);?>/>
							<label class="css-label" for="gb_quality"><?php _e("Quality", "smp");?></label>
						</p>
						<p>
							<input class="css-checkbox" name="gb_best_before" type="checkbox" id="gb_best_before" <?php checked(1, $this->options['gb_settings']['best_before']);?>/>
							<label class="css-label" for="gb_best_before"><?php _e("Best before", "smp");?></label>
						</p>
						<p>
							<input class="css-checkbox" name="gb_factory_id" type="checkbox" id="gb_factory_id" <?php checked(1, $this->options['gb_settings']['factory_id']);?>/>
							<label class="css-label" for="gb_factory_id"><?php _e("Manufacturer", "smp");?></label>
						</p>
						<p>
							<input class="css-checkbox" name="gb_shopting" type="checkbox" id="gb_shopting" <?php checked(1, $this->options['gb_settings']['store']);?>/>
							<label class="css-label" for="gb_shopting"><?php _e("Shoping", "smp");?></label>
						</p>
						<p>
							<input class="css-checkbox" name="gb_deleting" type="checkbox" id="gb_deleting" <?php checked(1, $this->options['gb_settings']['deleting']);?>/>
							<label class="css-label" for="gb_deleting"><?php _e("Deleting", "smp");?></label>
						</p>
						<p>
							<input class="css-checkbox" name="gb_combine" type="checkbox" id="gb_combine" <?php checked(1, $this->options['gb_settings']['combine']);?>/>
							<label class="css-label" for="gb_combine"><?php _e("Combine identical", "smp");?></label>
						</p>
						<p>
							<input type="submit" class="smc_bottom_invert" name="save_gb" value="<?php _e("Save");?>"/>
						</p>
					</div>
					
						
				</div>
				<!--div class="h0">
					<div class="h">
							<h3><?php _e("Set Production Inteval","smp"); ?></h3>
							<input  type="number" step="1" min="1" name="production_interval" value="<?php print_r( $this->options["production_interval"] ); ?>"/>
							<label><?php _e(" minutes", "smp"); ?></label>
						
						</div>
						<div class="h">
							<h3><?php _e("Coefficients","smp"); ?></h3>
							<label><?php _e("Main goods coeficient in procients", "smp"); ?></label><br>
							<input  class="h2" name="main_goods_coef" type="number" min="0" step="1" value ="<?php print_r( $this->options['main_goods_coef'] );?>"/>
						</div>	
				</div-->
				</div>
			</form>
			<?php
		}
		function finance_dashboard_widget_handler()
		{
			global $Soling_Metagame_Constructor;
			global $SMP_Currency;
			if($_POST['give_money_submit'])
			{
				global $wpdb, $table_prefix;
				$locs						= array();
				$upd						= array();
				$where						= array();
				if($_POST['get_location_currency'])
				{
					if($_POST['given_money'] > 0 && $_POST['get_location_currency']>0)		
					{				
						$give_money		= (int)$_POST['given_money'];		
						
						$arg				= array(
														
														'numberposts'	=> -1,
														'offset'    	=> 0,
														'post_type' 	=> SMP_CURRENCY_ACCOUNT,
														'post_status' 	=> 'publish',
														'meta_query'	=> array(	
																					array(
																							'key'		=> 'owner_id',
																							'value'		=> $_POST['get_location_currency'],
																							'operator'	=> 'OR'
																						  ),																					
																				 )
													);
						$accounts			= get_posts($arg);
						echo Assistants::echo_me( $arg, true).Assistants::echo_me($accounts, true);
						$query				= "UPDATE ".$wpdb->postmeta." SET meta_value=meta_value+".$give_money." WHERE (";
						$q					= array();
						foreach($accounts as $account)
						{
							$q[]			= "post_id='".$account->ID."' ";
						}
						$query				.= implode(" OR ", $q);
						$query				.= ") AND meta_key='count'";
						//echo $query;
						$wpdb->query($query);
					}
				}
			}
			
			?>
			<form name="settings"  method="post"  enctype="multipart/form-data" id="settings">
				<div style="position:relative; display:inline-block;">
					<div class="h0">
						<div class="h" id="give_money" placeh="<?php _e("Select one or few Location", "smp");?>">
							<board_title><?php _e("Give money for Locations", "smp"); ?></board_title>
							<label for="get_location_currency"><? _e("Give money", "smp");?></label>
							<p>
								<label for="get_location_currency[ ]"><?php _e("One or few Locations", "smp"); ?></label>
								<?php echo $Soling_Metagame_Constructor->get_location_selecter(
																								array(
																										"name"		=> "get_location_currency[ ]",
																										"id"		=> "get_location_currency",
																										"class"		=> "chosen-select11",
																										"multile"	=> true,
																										"style"		=> "width:100%; "
																									  )
																							  ) ?>
							<br>	
								<!--label for="money_type"><?php _e("Currency Type", "smp"); ?></label>							
								<?php
									echo $SMP_Currency->wp_droplist_currency_type(
																					array(
																							"name"		=> "money_type",
																							"class"		=> "chosen-select",
																							"selected"	=> $this->options['default_currency_type_id'], 
																							"style"		=> "width:100%; "
																						  )																			
																				 );
								?>
							<br-->	
								<label for="given_money"><?php _e("Count", "smp"); ?></label>
								<input type="number" name="given_money" min="0" value="0" style="width:80%;"/> <?php _e("rub.", "smp"); ?>
							</p>
							<p>
								<input type="submit" name="give_money_submit" value="<?php _e("Give money", "smp");?>"/>
							</p>						
						</div>
						
					</div>
					
				</div>
			</form>
			<script type="text/javascript">
				
				(function($)
				{			
					set_chosen("#get_location_currency", {width:"100%", placeholder_text_multiple:$("#give_money").attr("placeh")});
				})(jQuery)
			</script>
			<?php
		}
		
		
		
		
		function logistics_dashboard_widget_handler()
		{
			if($_POST['logistics_submit']) 
			{
				$this->options['logistics_enabled']		= $_POST['enable_logistics']=="on" ? 1 : 0;				
				update_option(SMP, $this->options);
			}
			?>
			<form name="settings"  method="post"  enctype="multipart/form-data" id="settings">
				<div style="position:relative; display:inline-block;">
					<div class="h0">
						<div  class="h" style="width:240px;">
							<div style="margin-bottom:5px;">
								<input name='enable_logistics' id='enable_logistics' type="checkbox" class="css-checkbox" <?php checked(1, SolingMetagameProduction::is_logistics());?> /> 
								<label for="enable_logistics" class="css-label"><?php _e("Enable Logistics", "smp"); ?></label><br>
							</div>
							<input type="submit" name="logistics_submit" value="<?php _e("Save"); ?>"/>
						</div>
					</div>
					<div class="h0">
						<ul>
							<li>
								<a href="/wp-admin/edit.php?post_type=smp_hub">
									<?php _e("all Hubs", "smp");?>
								</a>
							</li>
							<li>
								<a href="http://ermak.3wins.ru/wp-admin/edit-tags.php?taxonomy=smp_routh">
									<?php _e("All Rouths", "smp");?>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</form>
			<?php
		}
		function logistics_widget_settings()
		{
			?>
			
			<?php
		}
		function hook_init()
		{			
			global $smc_main_tor_buttons;
			if(!isset($smc_main_tor_buttons))		$smc_main_tor_buttons = array();
			$smc_main_tor_buttons[]					= array('ico'=>"?", 'targ'=>__('all Factories', "smp"), 'comm'=>'get_current_location_factories');
		}		
		
		function smc_admin_special_pages_choosee($text)
		{
			$posts			= get_posts(array('numberposts' => 500,'post_type'=>"page", 'orderby'=>"ID", 'order'=>'ASC'));
			$new_text		.= '
			<li>
				<h4>'.__("Metagame Production", "smp").'</h3>
			</li>
			<li>
				<label for="new_mp_ID">'.__("My Productions", "smp"). " ( shortcode: [my_production_page] )".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="new_mp_ID" id="new_mp_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['new_mp_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			<li>
				<label for="my_gb_ID">'.__("My Goods Batches", "smp"). " ( shortcode: [my_storage_page] )".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="my_gb_ID" id="my_gb_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['my_gb_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			<li>
				<label for="my_hub_ID">'. __("My Logisitics", "smp"). " ( shortcode: [smp_my_hubs] )".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="my_hub_ID" id="my_hub_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['my_hub_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			<li>
				<label for="store_ID">'.__("Stores", "smp"). " ( shortcode: [smp_store] )".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="store_ID" id="store_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['store_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			<li>
				<label for="bank_ID">'. __("Bank", "smp"). " ( shortcode: [smp_bank] )".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="bank_ID" id="bank_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['bank_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			<li>
				<label for="my_calc_ID">'. __("Personal Tools", "smp"). " ( shortcode: [smp_personal_calc] )".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="my_calc_ID" id="my_calc_ID" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['my_calc_ID'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			<li>
				<label for="report_page_id">'. __("Circle Reports", "smp"). "".'</label><BR>
				<div class="styled-select state rounded w400">
				<select  name="report_page_id" id="report_page_id" class="chosen-select">
					<option  value="-1">---</option>';			
			foreach($posts as $post)
			{
				$selected		= selected($post->ID, $this->options['report_page_id'], false);
				$new_text		.= "<option ".$selected." value='".$post->ID."'>".$post->post_title."</option>";
			}
			$new_text		.= '			
				</select>
				</div>
			</li>
			';
			return $text.$new_text;			
		}
		function smc_admin_sinc_special_pages()
		{
			$this->options['my_calc_ID']				= $_POST['my_calc_ID'];
			$this->options['store_ID']					= $_POST['store_ID'];
			$this->options['new_mp_ID']					= $_POST['new_mp_ID'];
			$this->options['my_gb_ID']					= $_POST['my_gb_ID'];
			$this->options['bank_ID']					= $_POST['bank_ID'];
			$this->options['my_hub_ID']					= $_POST['my_hub_ID'];
			$this->options['report_page_id']			= $_POST['report_page_id'];
			$this->update_options();
		}
		
		function update_options()
		{
			update_option( SMP, $this->options );
		}	
		function admin_page_handler()
		{
			add_menu_page( 
						__('Ermak. Production', "smp"), // title
						__('Ermak. Production', "smp"), // name in menu
						'manage_options', // capabilities
						'Metagame_Production_page', // slug
						array($this, 'menu_production_page'), // options function name
						'none', //'dashicons-hammer',//SMC_URLPATH .'/img/pin.png', // icon url
						'20.501'
						);
		}
		function menu_production_page()
		{
			require_once( SMP_REAL_PATH.'tpl/menu_production_page.php' );
		}
		function cron_add_production_interval( $schedules ) 
		{
			$schedules['production_interval'] = array(
				'interval'		=> $this->options['production_interval'] * 60,
				'display'		=> __( 'production_interval' ),
			);
			return $schedules;
		}
		
		function production_handler()
		{
			if(!$this->options['productions_started'])		return;
			$this->count_circle();
		}
		
		
		protected function count_circle()
		{		
		
			ob_start();
			do_action("smp_before_count_circle");
			require_once(SMP_REAL_PATH.'class/Circle_Report.php');
			$current_circle_id	= get_option("current_circle_id");
			$current_circle_start = get_option("current_circle_start");
			//insertLog("end of prevous production circle", $current_circle_id);
			if($current_circle_start=="")
				$current_circle_start = time();
			$this->report		= new Circle_Report();
			
			//echo "Start productivity circle<BR>";
			
			
			
			$bargs				= array(
											'numberposts'     => -1,
											'offset'          => 0,
											'orderby'         => 'id',
											'order'           => 'DESC',
											'post_type'       => GOODS_BATCH_NAME,
											'post_status'     => 'publish'
										);
			$goods_batchs			= get_posts($bargs);

			foreach($goods_batchs as $goods_batch)
			{
				//goods_batches usage period changing
				if($this->options['gb_settings']['best_before'])
				{
					$best_before		= get_post_meta($goods_batch->ID, "best_before", true);
					if($best_before < MAX_BEST_BEFORE)
						$best_before--;
					update_post_meta($goods_batch->ID, "best_before", $best_before);
				}
				do_action("smp_batch_circle_change", $goods_batch->ID);
			}
			
			$args				= array(
											'numberposts'     => -1,
											'offset'          => 0,
											'fields'		  => 'ids',
											'orderby'         => 'id',
											'order'           => 'DESC',
											'post_type'       => SMP_FACTORY,
											'post_status'     => 'publish'
										);
			$factories			= get_posts($args);
			$successfull		= "";
			$unsuccessfull		= "";
			foreach($factories as $factori_id)
			{
				
				$factory		= Factory::get_factory($factori_id);
				do_action("smp_before_factory_circle_change", $factory);
				$batch			= apply_filters("smp_factory_produce", $factory->create_goods_per_tact($this->report), $factory); 
				do_action("smp_factory_circle_change", $factory);
				if(!is_wp_error($batch))
					$successfull.= $this->report->add_batch($factory, $batch)."<hr>";
				else
				{
					$goods		= Goods_Type::get_instance( (int)$factory->get_post_meta("goods_type_id") );
					$message	= "<p>" . sprintf(
													__("Error creating %s by %s. Reasons: ", "smp") ,  
													"<a href='" . get_permalink($goods->ID) . "'><B>".$goods->post_title."</b></a>",
													"<a href='" . get_permalink($factory->id) . "'><B>".$factory->get("post_title")."</b></a>"
												 ) . "</p>";
					$message	.= $batch->get_error_message();
					$this->report->send_factory_message($factory, $message);
					$unsuccessfull.= $message."<hr>";
				}				
			}
			echo $successfull;
			echo $unsuccessfull;
			
			//echo "finished main circle<BR>";
			do_action("smp_count_circle", (int)$current_circle_id + 1);
			$this->report->send_messages();
			$current_circle_id	= (int)$current_circle_id + 1;
			update_option("current_circle_id", $current_circle_id);
			update_option( "current_circle_start", time());
			//echo "FINISH circle<BR>";
			do_action("smp_after_count_circle", $current_circle_id);			
			$var 	= ob_get_contents();
			ob_end_clean();			
			if(!Circle_Report::save_global($current_circle_id, PRODUCTION_CIRCLE_TYPE, $current_circle_start, $var))
			{
				echo "<div class='smc-comment'>" . __("Error saved Report to DataBase.", "smp") . "</div>";
			}
			/*
			$rep	= get_post( $this->options['report_page_id'] );
			$cont	= "";//$current_circle_id % CLEAN_REPORT_PERIOD == 0 ? "" : $rep->post_content;
			$upp	= array(
								"ID"			=> $this->options['report_page_id'],
								"post_content"	=> "<div><h1>" .
													sprintf(
																__("Report for ending %s circle.", "smp"), 
																"<span class=lp-external-color>". $current_circle_id."</span>" 
															). "</h1>" . $var . "</div>".$cont
							); 
			wp_update_post( $upp );
			*/
			//insertLog("SMPClass.count_circle", array ("circle_id" => $current_circle_id));	
			$content	= sprintf(
				__("Finished %s Production Circle. All Factories made goods.", "smp"),
				$current_circle_id
			);
			
			SMC_Location::add_log(0, $content, FINISH_PRODUCTION_CYRCLE, get_current_user_id());
			echo $var;
		}
		
		// THE_CONTENT
		function the_content($content)
		{
			global $post;
			global $Factories;
			global $Goods_Type;
			global $Goods_Batch;
			$before			= "";
			$after			= "";	
			switch( $post->post_type)
			{
				case "factory":
					$content	= Factories::the_content($post, $content, $before, $after);
					break;
				case "goods_type":
					$content	= Goods_Type::the_content($post, $content, $before, $after);
					break;
				case GOODS_BATCH_NAME:
					$content	= Goods_Batch::the_content($post, $content, $before, $after);
					break;
				case "smp_industry":
					$content	= SMP_Industry::the_content($post, $content, $before, $after);
					break;
				default:
					null;
				
			}
			return $before.$content.$after;
		}
		function the_title($title, $id)
		{
			global $post;
			if($id != $post->ID || is_admin())	return $title;
			$before			= "";
			$after			= "";			
			switch($post->post_type)
			{
				case "factory":
					$ind		= SMP_Industry::get_instance(get_post_meta($post->ID, "industry", true));
					$before	.= "<span class='smp-before-title'>" . $ind->get("factory_name") . "</span> ";
					break;
				case "goods_type":
					$before	.= "<span class='smp-before-title'>".__("Goods type", "smp")."</span> ";
					break;
				case GOODS_BATCH_NAME:
					$before	.= "<span class='smp-before-title'>".__("Goods batch", "smp")."</span> ";
					break;
				case "smp_industry":
					$before	.= "<span class='smp-before-title'>".__("Industry", "smp")."</span> ";
					$after	.= get_the_ID() == $id ? 
									SMP_Industry::wp_dropdown_industry(array("name"=>"specialtype_select", 'slyle'=>'margin:0;')) : "";
					break;
				default:
					null;
			}
			return $before . $title ."  " . $after;
		}
		
		// SMC FILTERS
		function smc_load_panel_screen($html, $currenting_url)
		{
			global $post;
			global $Soling_Metagame_Constructor;
			$div0	= "<div class=lp-wrap-open style=width:100%; height:100%; font-size:16px; font-family:Ubuntu Condensed; color:white; padding:50px;>";
			$div1	= '</div>';
			switch($post->post_type)
			{
				case "smp_hub":		
					
					$trum_id	= get_post_thumbnail_id();
					$trum		= wp_get_attachment_image_src($trum_id, 'full');
					if($trum_id)
						$trum_img	= $trum[0];	
					else
						$trum_img	= "";//SMP_URLPATH."img/hub.jpg"; 
					$loc_id		= get_post_meta($post->ID, "dislocation_id", true);
					$loc		= SMC_Location::get_instance( $loc_id);
					$lt			= $Soling_Metagame_Constructor->get_location_type($loc_id);
					return "open_metagame_panel('special_filtered_panel', ".
					"'<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img); ><center class=lp_opn_mtgm_title><div  class=smc_special_title_cont>" .
					__("Hub", "smp") .
					"</div> <div class=smc_special_title>" . 
								$post->post_title . 
							"</div><div class=lp_opn_pnl_comm>[ <span>".
								 $lt->post_title."</span> ".$loc->name.
							" ]</div>".
						"</center></div>')";
				case "factory":
					$factory	= $post;
					if(has_post_thumbnail() )
					{
						$trum_id	= get_post_thumbnail_id();
						$trum		= wp_get_attachment_image_src($trum_id, 'full');
						$trum_img	= $trum[0];	
					}
					else
					
					{
						$trum_img	= '';//SMP_URLPATH."img/factory.jpg"; 
					}
					
					$loc_id		= get_post_meta($factory->ID, "dislocation_id", true);
					$loc		= SMC_Location::get_instance($loc_id);
					$lt			= $Soling_Metagame_Constructor->get_location_type($loc_id);
					$gt			= Goods_Type::get_trumbnail(get_post_meta($factory->ID, "goods_type_id", true));
					$ow_id		= get_post_meta($factory->ID, "owner_id", true);
					$ow			= SMC_Location::get_instance($loc_id );
					$owt		= $Soling_Metagame_Constructor->get_location_type($loc_id);
					
					$your		= Factories::is_user_owner($ow_id, get_current_user_id()) ? '<div class=smc-your-label>'.__("It's your", "smc").'</div>' : "";
					
					$val 		= "open_metagame_panel('special_filtered_panel', '<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img)><center class=lp_opn_mtgm_title><div class=smc_special_title_cont>" .
						__("Factory", "smp") .
						"</div> <div class=smc_special_title>" . 
									$factory->post_title . 
								"</div><div class=lp_opn_pnl_comm>[ <span>".
									 $lt->post_title."</span> ".$loc->name.
								" ]</div><div class=lp_opn_pnl_comm1>[ <span>".__("Owner","smp") . " " .
									$owt->post_title.
									 "</span> " . 
									 $ow->name.
								" ]</div>".	
								"</center>$your</div>')";								
					//$val		= preg_replace('/[\s[:^print:]]/m', " ", $val);
					return $val;
					
				case "goods_type":
					$goods		= "";// Goods_Type::get_panel_form($post->ID);
					return "open_metagame_panel('special_filtered_panel', '<div class=lp_opn_mtgm_pnl>$goods</div>')";
					
			}
			
			$taxtype 				= get_query_var('taxonomy');
			switch($taxtype)
			{
				case 'smp_routh':
					
					$route		= new SMP_Routh(get_query_var('term_id'));
					return $route->get_panel_form();
			}
			$prfx		= get_bloginfo("url"). "/?page_id=";
			switch($currenting_url)
			{
				case $prfx . $this->options['store_ID']:
					$trum_img	= '';//SMP_URLPATH."img/store.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("Stores", "smp") . "</div><div id=undertitle></div></center></div>')";
				case $prfx . $this->options['new_mp_ID']:
					$trum_img	= '';//SMP_URLPATH."img/factory.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("My Productions", "smp") . "</div><div id=undertitle></div></center></div>')";
				case $prfx . $this->options['my_gb_ID']:
					$trum_img	= '';//SMP_URLPATH."img/storage.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("My Goods Batches", "smp") . "</div><div id=undertitle></div></center></div>')";
				case $prfx . $this->options['bank_ID']:
					$trum_img	= '';//SMP_URLPATH."img/bank2.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("Bank", "smp") . "</div><div id=undertitle></div></center></div>')";
				case $prfx . $this->options['my_hub_ID']:
					$trum_img	= '';//SMP_URLPATH."img/hub.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img);><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("My Logistics", "smp") . "</div><div id=undertitle></div></center></div>')";
				case $prfx . $this->options['my_calc_ID']:
					$trum_img	= '';//SMP_URLPATH."img/calculate.jpg"; 
					return "open_metagame_panel('special_filtered_panel', '" . 
					"<div class=lp_opn_mtgm_pnl style=background-image:url($trum_img); exec=bbb><center class=lp_opn_mtgm_title><div class=smc_special_title>" . 
					__("Personal Tools", "smp") . "</div><div id=undertitle></div></center><div></div>')";
				default: 
					return $html;
			}
			return "open_metagame_panel('special_filtered_panel', '" . $currenting_url . "')";
		}
		
		function smc_display_special_panel($src)
		{
			
			$html 	= array(
								'text'=> $src , 
								'exec'=> '', //'open_metagame_panel', 
								'args'=> '', //array('special_filtered_panel', $src),
							);
			return $html;
		}
		function smc_widget_infrastructure($args, $location_id, $count=9)
		{
			global $Soling_Metagame_Constructor;
			$factories		= Factories::get_all_location_factories($location_id, true, $count, true);
			$html			.= "			
			<div class='klapan3_subtitle lp_clapan_raze'>".
				__("all Factories", "smp").
			'<div class="lp_clapan_raze1"></div>	</div>		
			<div class="smp_bevel_form">';
			$i=0;
			foreach($factories as $fac)
			{
				$html	.= Factories::get_pictogramm($fac, "factory_picto_id='".$fac."' " );
				//$slides	.= Factories::get_widget_form(get_post($fac));
				if(++$i>7 && $count>0)	
				{
					$html	.= '<center><div id="more_factories" foid="' . $location_id . '" class="black_button_2" style="width:345px;">' . __("More") . '</div></center>';
					break;
				}
			}
			$html			.= '</div>';
			
			return $html.$args;
		}
		function smc_widget_location_form($args, $location_id)
		{
			global $Soling_Metagame_Constructor;
			$location	= SMC_Location::get_instance($location_id);
			$slides		= '<div class="lp-hide">';//
			$factories	= Factories::get_all_location_factories($location_id, true, -1, true);
			if(count($factories))
			{
				$html	.= "<div class='klapan3_subtitle'>".__("all Factories", "smp").'<div style="display:inline-block; width:100%; ">';
				foreach($factories as $fac)
				{
					$html	.= Factories::get_pictogramm($fac, "factory_picto_id='".$fac."' " );
					$slides	.= Factories::get_widget_form(get_post($fac));
				}
				$html	.= "</div></div>";
			}
			
			$hub_ids	= SMP_Hub::get_all_hubs_by_dislocation($location_id);
			if(count($hub_ids))
			{
				$html	.= "<div class='klapan3_subtitle'>".__("all Hubs", "smp")."<div>";
				foreach($hub_ids as $fac)
				{
					$html	.= SMP_Hub::get_pictogramm(($fac));
				}
				$html	.= "</div></div>";
			}
			/**/
			//storage	
			$html		.= "
			<div class='klapan3_subtitle'>
				<div style='position:relative; display:block;'>" .
					__("Storage", "smp").
				'</div></div>';
			$html		.= '<div style="position:relative; display:inline-block;">';			
			$batchs			= SMP_Assistants::get_all_batchs_location($location_id);
			$html			.= count($batchs) < 1 ? "<div class='smp_comment_widget'>".__("No Goods of this type", "smp")."</div>" : "";
			foreach($batchs as $gb_id)
			{
				$gbatch		= SMP_Goods_Batch::get_instance($gb_id);
				$html		.= $gbatch->get_widget_picto();
			}
			$html			.=
			"</div>
					
			</div>";
			
			$html			.= "
			<div  class='klapan3_subtitle'>
				<div style='height:200px;'>
				
				</div>
			</div>
			</div>
			";
			$slides			.= '</div>';
			return $args.$html.$slides;
		}
		function get_widget_bussiness_form($args, $location_id)
		{
			global $Factory, $Soling_Metagame_Constructor;
			$location	= SMC_Location::get_instance($location_id);
			
			//now produse goods
			$productions	= Factories::get_produse_by_location($location_id, true);
			$facs		= $productions['produce'];
			$unproduce	= $productions['unproduce'];
			$html1		= "";
			if(count($facs))
			{
				$html1		= "<div class='klapan3_subtitle lp_clapan_raze'>".
					
					"<div style='position:relative; display:block;'>".
						__("Now produse", "smp") . 
					'</div>
				<div class="lp_clapan_raze1"></div>
				</div>';
				$html1		.= "
				<div class='smp_bevel_form'>
				";
				$html1		.= "<div class='produce1'>";
				foreach($facs as $fac)
				{
					$html1	.= $Factory->get_little_picto(
															$fac['factory_id'], 
															32, 
															-1, 
															array(
																	"id"			=> "fac_picto_" . $fac['factory_id'], 
																	"class"			=> 'factory_bussiness_widget',
																	'components'	=> 1
																  ),
															1
														  );
				}
				$html1		.= "</div>
					<div id='produce_content' class='produce_content'></div>
				</div>
				";
			}
			else
			{
				$html1		.= "
				<div class='smp_bevel_form'>
				<div class='smp-comment'>".
					__("no produse", "smp") . 
				"</div></div>";
			}
			//unproduce goods
			if(count($unproduce))
			{
				$html1		.= "<div class='klapan3_subtitle lp_clapan_raze'>
					<div style='position:relative; display:block; '>".
						__("No produse but can", "smp").
				'	</div>
				</div>';
				$html1		.= "
				<div class='smp_bevel_form'>
					";
				$html1		.= "<div class='produce1'>";
				foreach($unproduce as $fac)
				{
					$html1	.= $Factory->get_little_picto(
															(int)$fac['factory_id'], 
															32, 
															(int)$fac['goods_type_id'], 
															array(
																	"id"			=> "fac_picto_2_" . $fac['factory_id'], 
																	"class"			=> 'factory_bussiness_widget2',
																	'components' 	=> 1
																  )
															);
				}
				$html1		.= "</div>
							
						</div>
					<div id='unproduce_content' class='produce_content'></div>
				";
			}
			
			//Location Storage
			
			$ltid		= SMC_Location::get_term_meta($location_id);
			if( !get_post_meta($ltid['location_type'], "show_children_type", true) ) 
			{
				$owid	= -1;
				$locid	= $location_id;
			}
			else
			{
				$owid	= $location_id;
				$locid	= $location->parent;
				
			}			
			
			
			$batchs			= SMP_Assistants::get_all_batchs_location($locid, $owid);
			$count			= count($batchs);	
			$html2			.= "<div class='klapan3_subtitle lp_clapan_raze'>
				<div style='position:relative; display:block; margin-bottom:20px;'>" .
					__("Storage", "smp"). " (". $count. ')' .
				'</div>';		
			$html2			.= "<div class='smp_bevel_form'>
				<div id='storage_wig' style='' class='storage_main'>";			
			$html2			.= $this->draw_storage($batchs, 0, STORAGE_PICE, 0, ceil($count/STORAGE_PICE), $location_id, "SMC_Location");			
			$html2		.= "
				</div>
			</div>
			<div class=lp_clapan_raze1></div>
			</div>";
			
			$html3		.= "
			<div class='klapan3_subtitle lp_clapan_raze'>
				<div style='position:relative; display:block;'>" .
					
				'</div>
				
				<div class="black_button_2" id="view_content" location_id="' . $location_id . '" name="' . $location->name . '">'.
					"<i class='fa fa-play'></i> ". __("View to content", "smp") . 
				'</div>
			</div>
			';			
			$args[0]					= array("title" => "<img src=".SMC_URLPATH."img/buy.png>", 	"slide" => $html1, 'hint' => __("Now produse", "smp"));
			$args1 						= array_slice  ($args, 1);
			$args  = array_merge(
									array(
											$args[0], 
											array("title" => "<img src=".SMC_URLPATH."img/vehicle.png>",	"slide" => $html2, 'hint' => __("Storage", "smp"))
										 ), 
									$args1
								);
			return $args;
		}
		public function draw_storage($batches, $type=0, $count=10, $offset=0, $all_offsets=0, $object_id=0, $object_type=0)		
		{
			require_once (SMP_REAL_PATH."class/SMP_Storage.php");
			$storage			= new SMP_Storage();
			return $type==0 ? 
					$storage->draw($batches, $count, $offset, $all_offsets, $object_id, $object_type) : 
					$storage->draw_pictos($batches, $count, $offset, $all_offsets, $object_id, $object_type);
		}
		
		function smc_login_widget_list1($arr)
		{
			$arr	= array_merge( $arr, array(
				"tools"			=> array(
					"title"		=> __("Personal Tools", "smp"),
					"picto"		=> SMP_URLPATH . "icon/calc_ico.png",
					"url"		=> get_permalink($this->options['my_calc_ID'])
				),
				"factories"		=> array(
					"title"		=> __("My Productions", "smp"),
					"picto"		=> SMP_URLPATH . "icon/product_ico.png",
					"url"		=> get_permalink($this->options['new_mp_ID'])
				),
				"batches" 		=> array(
					"title"		=> __("My Goods Batches", "smp"),
					"picto"		=> SMP_URLPATH . "icon/goods_ico.png",
					"url"		=> get_permalink($this->options['my_gb_ID'])
				)
			));
			
			return $arr;
		}
		function smc_login_widget_list2($arr)
		{
			$arr	= array_merge( $arr, array(
				"bank"			=> array(
					"title"		=> __("Bank", "smp"),
					"picto"		=> SMP_URLPATH . "icon/money_ico.png",
					"url"		=> get_permalink($this->options['bank_ID'])
				),
				"stores"		=> array(
					"title"		=> __("Stores", "smp"),
					"picto"		=> SMP_URLPATH . "icon/shop_ico.png",
					"url"		=>get_permalink($this->options['store_ID'])
				)
			));
			
			return $arr;
		}
		
		//retun about time of end production circle per secunds
		function get_expected_finish_time( $per_hours=false)
		{
			$productions_started	= $this->options['productions_started'];
			if(!$productions_started)	return;
			$expected				= get_option('current_circle_start');
			$secs					= $expected - time()  + $this->options['production_interval'] * 60;
			return $per_hours ? around_time((int)($secs / 3600)) . ":" . around_time((int)(($secs % 3600) / 60)) . ":" . around_time($secs % 60) : $secs;
		}
		function get_production_comment()
		{
			$cpc					= get_option("current_circle_id")+1;
			$productions_started	= $this->options['productions_started'];
			if(!$productions_started)	return sprintf(__("Now during %s Production Circle.", "smp"), "<b>".($cpc)."</b>");
			$help		= sprintf(
				__("The expected finish of %s Production Circle is about %s", "smp") ,
				"<b>".($cpc)."</b>",
				"<span class='production_interval_counter' style='color:#FFF;'>" . $this->get_expected_finish_time(true) ."</span>"
			);
			return $help;
		}
		function smc_lp_head_1($text, $location_id)
		{
			return $text . " <span class='roduction_comment' style='margin-left:20px; color:#CCC;'><span >". $this->get_production_comment() ."</span>";
		}
		
		function open_location_content_widget($text, $params)
		{
			return $this->get_widget_bussiness_form($text, $params[1]);
			//return $this->smc_widget_location_form($text, $params[1]);
		}
		
		
		
		
		//========================
		//
		//	INSTALL | UNINSTALL	
		//
		//========================	
		static function init_DB()
		{
			global $wpdb;
			$query	= "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix . REPORT_DB_PREFIX."` (`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,`report_id` int(10) unsigned NOT NULL,`start_time` datetime NOT NULL,`finish_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`content` longtext NOT NULL,`report_type` int(10) unsigned NOT NULL DEFAULT '0',PRIMARY KEY (`ID`)) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;";
			$wpdb->query($query);
			insertLog("init_DB", $query);
			
			$wpdb->query("CREATE TABLE IF NOT EXISTS `".$wpdb->prefix . "ermak_waybill_meta` (
	`ID` bigint(100) unsigned NOT NULL AUTO_INCREMENT,
	`order_` int(11) unsigned NOT NULL,
	`waybill_id` int(11) unsigned 	NOT NULL,
	`action` int(2) unsigned NOT NULL,
	`location_id` bigint(100) unsigned NOT NULL,
	`target` bigint(100) unsigned NOT NULL,
	`counts` int(10) unsigned NOT NULL DEFAULT '1',
	`frequency` int(10) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1 ;");
			/**/
		}
		
		static function init_settings()
		{
			init_textdomain_smp();
			if(self::is_smc_install())
			{
				SMC_Location::add_properties(array("count_cells", "currency_type"), "BIGINT(20) UNSIGNED ", 0);
			}		
			//self::init_DB();
			require_once(SMP_REAL_PATH.'tpl/register_hook.php');
			require_once(SMP_REAL_PATH.'class/SMP_Routh.php');
			require_once(SMP_REAL_PATH.'class/SMP_Hub.php'); 
			$SolingMetagameProduction->options['consume_interval']				= 30;			
			$SolingMetagameProduction->options['new_mp_ID']						= Factories::install();			
			$SolingMetagameProduction->options['my_gb_ID']						= SMP_Goods_Batch::install();			
			$SolingMetagameProduction->options['store_ID']						= SMP_Store::install();			
			$SolingMetagameProduction->options['bank_ID']						= SMP_Currency::install();			
			$SolingMetagameProduction->options['my_routh_ID']					= SMP_Routh::install();			
			$SolingMetagameProduction->options['my_hub_ID']						= SMP_Hub::install();			
			$SolingMetagameProduction->options['my_calc_ID']					= SMP_Calculator::install();			
			$SolingMetagameProduction->options['PTP']							= $new_loctype_ID;
			$SolingMetagameProduction->options['production_interval']			= 20;
			$SolingMetagameProduction->options['main_goods_coef']				= 100;
			$SolingMetagameProduction->options['logistics_enabled']				= 1;
			$SolingMetagameProduction->options['finance_enabled']				= 1;
			$SolingMetagameProduction->options['logistics_interval']			= 10;
			$SolingMetagameProduction->options['route_hint_radius']				= 4;
			$SolingMetagameProduction->options['install_ermak_production']		= 1;
			$SolingMetagameProduction->options['gb_settings'] 					= array();
			$SolingMetagameProduction->options['gb_settings']['dislocation_id'] = true;
			$SolingMetagameProduction->options['gb_settings']['quality'] 		= false;
			$SolingMetagameProduction->options['gb_settings']['best_before'] 	= false;
			$SolingMetagameProduction->options['gb_settings']['factory_id'] 	= true;
			$SolingMetagameProduction->options['gb_settings']['store'] 			= true;
			$SolingMetagameProduction->options['gb_settings']['deleting'] 		= true;
			$SolingMetagameProduction->options['is_hubs_uncontrolled'] 			= 0;
			$SolingMetagameProduction->options['waybill'] 						= true;
			
			// init Reports List
			$my_post = array(
								  'post_title'   		=> __("Circle Reports", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "",
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);
			
			update_option( SMP, $SolingMetagameProduction->options );
			update_option( "current_circle_id", 0 );			
			return;
		}
		
		
		static function  is_smc_install()
		{
			return is_plugin_active('Ermak/Ermak.php');
		}
		//=========================
		//
		//	css js install
		//
		//=========================
		function scripts_add()
		{
			//return;
			//css
			wp_register_style('smp-css', SMP_URLPATH . '/css/smp_admin.css', array());
			wp_enqueue_style('smp-css');
			
			//js
			wp_register_script('smp-admin', plugins_url( '../js/smp-admin.js', __FILE__ ), array());
			wp_enqueue_script('smp-admin');	
			//drag
			//wp_register_script('smp-drag', plugins_url( '../js/jquery.event.drag.live-2.2.js', __FILE__ ), array());
			//wp_enqueue_script('smp-drag');	
			
		}
		function front_script_add()
		{
			
			//css
			wp_register_style('smp-css', SMP_URLPATH . '/css/smp_front.css', array());
			wp_enqueue_style('smp-css');
			
			//js
			wp_register_script('smp-front', plugins_url( '../js/smp-front.js', __FILE__ ), array());
			wp_enqueue_script('smp-front');		
				
			//flot
			wp_register_script('smp-flot', plugins_url( '../js/jquery.flot.min.js', __FILE__ ), array());
			wp_enqueue_script('smp-flot');		
				
			//flot pie
			wp_register_script('smp-flot-pie', plugins_url( '../js/jquery.flot.pie.min.js', __FILE__ ), array());
			wp_enqueue_script('smp-flot-pie');	
				
			//flot stack
			wp_register_script('smp-flot-stack', plugins_url( '../js/jquery.flot.stack.min.js', __FILE__ ), array());
			wp_enqueue_script('smp-flot-stack');	
				
			//flot selection
			wp_register_script('smp-flot-selection', plugins_url( '../js/jquery.flot.selection.min.js', __FILE__ ), array());
			wp_enqueue_script('smp-flot-selection');		
			
			//flot_axislabels
			wp_register_script('flot_axislabels', plugins_url("../js/jquery.flot.axislabels.js", __FILE__ ), array());
			wp_enqueue_script('flot_axislabels');	
			
			//flot_symbol
			wp_register_script('flot_symbol', plugins_url("../js/jquery.flot.symbol.js", __FILE__ ), array());
			wp_enqueue_script('flot_symbol');
			
			//excanvas
			wp_register_script('excanvas', plugins_url("../js/excanvas.min.js", __FILE__ ), array());
			wp_enqueue_script('excanvas');
			
			//numberformatter
			wp_register_script('numberformatter', plugins_url("../js/jquery.numberformatter-1.2.4.min.js", __FILE__ ), array());
			wp_enqueue_script('numberformatter');
		}
		
		function admin_hook_css()
		{
			global $voc2;
			echo"<style>
				.toplevel_page_Metagame_Production_page, .toplevel_page_Metagame_Production_page:hover, #toplevel_page_Metagame_Production_page.wp-has-current-submenu div.wp-menu-image
				{				
					background-image:url(" . SMP_URLPATH . "img/ermak_production.png);
					background-repeat: no-repeat;
					background-position: 7px center;
				}
				.toplevel_page_Metagame_Finance_page, .toplevel_page_Metagame_Finance_page.wp-has-current-submenu:hover, #toplevel_page_Metagame_Finance_page.wp-has-current-submenu div.wp-menu-image
				{				
					background-image:url(" . SMP_URLPATH . "img/ermak_finance.png);
					background-repeat: no-repeat;
					background-position: 7px center;
				}
				.column-is_blocked, .column-store, .column-transportation_id 
				{
					width:40px;
				}
				.column-actions
				{
					width:50%;
				}
				.column-consume
				{
					width:200px;
				}
			</style>".$voc2;
		}
		function hook_css()
		{
			global $user_iface_color, $voc2;
			echo "<style type='text/css'>
				
				.smp-pr-main h3 a
				{
					font-weight:700; 
					color:".$user_iface_color.";
				}
				.smp-colorized
				{
					color:".$user_iface_color.";
				}
				.smp-colorized:hover
				{
					background:".$user_iface_color.";
					color:#FFF;
					text-decoration:none!important;
				}
				.smp-colorized:active
				{
					background:#000;
				}
				.smp-pr-main h3 a:hover
				{
					background:".$user_iface_color.";
					color:#FFF;
				}
				.smp-pr-main h3 a:active
				{
					background:black;
				}
				.smp-pr-main h3 span
				{				
					margin-right:50px;
				}				
				.smp-colorised-hover:hover
				{
					color:".$user_iface_color.";
					cursor:pointer;
					text-decoration:none!important;
				}								
				.smc-question:hover
				{
					color:".$user_iface_color.";
				}
				.smp_switch_button:hover
				{
					background:".$user_iface_color.";
				}
				.smp_button2:hover
				{
					background:".$user_iface_color.";
				}
				
			</style>".$voc2;
		}
		function smc_add_vocabulary($text)
		{
			return $text."
				vocabulary['You must start draw in the one of Hubs']	= '". __("You must start draw in the one of Hubs", "smp") . "';
				vocabulary['You must set summ for this transfer']		= '". __("You must set summ for this transfer", "smp") . "';
				vocabulary['You must set reason for this transfer']		= '". __("You must set reason for this transfer", "smp") . "';
			";			
		}
		function smc_setting_page($arr)
		{
			if($_POST['save_cron'])
			{
				$this->options['cron_type'] = $_POST['cron'];
				update_option(SMP, $this->options);
				$file 					= ABSPATH . "wp-config.php";
				$fp 					= fopen( $file, "r+" );
				if ( flock( $fp, LOCK_EX ) ) 
				{
					$config 			= fread( $fp, filesize( $file ) );
					$config				= str_replace( "define('ALTERNATE_WP_CRON', true);", "", $config );
					$config				= str_replace( "define('DISABLE_WP_CRON', true);", "", $config );
					
					switch($_POST['cron'])
					{
						case 0:
							break;
						case 1:
							$config				= str_replace( "define('WP_DEBUG', false);", "define('WP_DEBUG', false); 
define('ALTERNATE_WP_CRON', true);", $config );
							break;
						case 2:
							$config				= str_replace( "define('WP_DEBUG', false);", "define('WP_DEBUG', false); 
define('DISABLE_WP_CRON', true);", $config );							
							break;
					}
					/*$hhh				= "<textarea style='width:900px; height:900px;'>".$config."</textarea>";*/					
					ftruncate($fp, 0); 		// предварительная очистка файла, на всякий случай
					$config 			= iconv('ASCII', 'UTF-8', $config);
					fwrite($fp,  $config); 	// пишем в файл данные
					fflush($fp); 			// очищаем буфер
					flock($fp, LOCK_UN); 	// открываем блокировку
					
				}
				else 
				{
					echo "Error write";
				}
				fclose($fp); 
				
				
			}
			$html		= apply_filters("smp_advansing_setting", '
			<div class="smc_form">
				<h3>'.__("Event Listener's Setting", "smp").'</h3>
				<div class="smc-description">'.
					__("Ermak automatically do critical actions. For example, it signed the end of the production cycle production at Factories. This setting helps avoid problems PHP-server configuration some ISPs. Sometimes they cut loopback-compound for the safe. You can either include an alternative configuration wp-cron if an agreement with the provider is not possible. Elso your can configure Cron PHP-server. In the first case the URLs on the site lengthen the recording type '? Doing_wp_cron = 1428264015.9538888931274414062500'. The second method requires the skills of PHP-administration.", "smp").
				'</div>
				<div class=abzaz>
					<p><input type="radio" class="css-checkbox" name="cron" id="cron0" value="0" '.checked(0, (int)$this->options['cron_type'], 0).'/>
					<label for="cron0" class="css-label">'.__("Set default WP-Cron settings", "smp").'</label</p>
					<p><input type="radio" class="css-checkbox" name="cron" id="cron1" value="1" '.checked(1, (int)$this->options['cron_type'], 0).'/>
					<label for="cron1" class="css-label">'.__("Set Alternate WP-Cron", "smp").'</label</p>
					<p><input type="radio" class="css-checkbox" name="cron" id="cron2" value="2" '.checked(2, (int)$this->options['cron_type'], 0).'/>
					<label for="cron2" class="css-label">'.__("Close all WP-Cron Settings and use Cron PHP", "smp").'</label</p>
				</div>
				<div class="submit">
					<input name="save_cron" type="submit" class="button-primary" value="' . __('Save Draft') . '" />
				</div>
			</div>');
			$arr[]		= array("title" => __("Advansing settings", "smp"), "slide"=>$html.$hhh);
			return $arr;
		}
		function armed_goods_batch_iface($gb_id, $is_current_user_owner=0)
		{
			global $SMP_Locistics;
			$disloc_id		= get_post_meta($gb_id, 'dislocation_id', true);
			$count			= get_post_meta($gb_id, "count", true);
			$locs			= array();
			if (SolingMetagameProduction::is_logistics())
			{
				$hub_ids		= SMP_Hub::get_all_hubs_by_dislocation($disloc_id);				
				foreach($hub_ids as $hub_id)
				{
					$gg			= SMP_Hub::get_all_locations_from_rouths($hub_id);
					$g			= array();
					foreach($gg as $ggg)
					{
						$g[]	= $ggg['routh_id'] . '--' . $ggg['loc_id'];
					}
					$datass		= implode(",", $g);	//0 - smp_routh id, 1 -- location id
					$locs[]		= $datass;
					//insertLog("armed_goods_batch_iface", $gg);
				}
			}
			else
			{
				$locations_ids	= SMC_Location::get_all_user_visible_location_ids(get_current_user_id());
				
				foreach($locations_ids as $lid)
				{
					$locs[]		= "1--$lid";
				}
			}			
			$g_locs			= SMC_Location::get_by_types(explode(",", $this->options['all_owner_locations']), false);
			
			$locsss			= "<div><h2>".__("Transfer", "smp").   "</h2><div style='overflow:auto; max-height:360px;'><ul>";
			if(count($locsss))
			{
				foreach($g_locs as $g)
				{
					$locsss	.= "<li><div class='smp-padding-2 smc_margin_normal smp_center smp_button2' gb_id='".$gb_id."' give_gt_loc='" . $g->term_id . "'>".$g->name."</div></li>";
				}
			}
			$locsss			.= "</ul></div></div>";
			
			return "
			<div id='armed_goods_batch_iface' style='width:300px;'>
				<div class='smc-alert smp-padding-2 smc_margin_normal smp_center smp_button2' id='give_button' target_name='give_locations'>".
					__("Give", "smp").
				"</div>	
				<div class='smp-padding-2 smc_margin_normal smp_center smp_button2' gb_id='" . $gb_id . "' count='$count' locs='" . implode(",", $locs) . "' cur_loc_id='".$disloc_id."' id='move_button'>".
					__("Move", "smp").
				"</div>	
				<div class='smp-padding-2 smc_margin_normal smp_center smp_button2' gb_id='" . $gb_id . "' id='anover_button'>".
					__("Anover manipulations", "smp").
				"</div>	
				<div id='give_locations' class='lp-hide'>".
					$locsss.
				"</div>
			</div>
			";
		}
		function smc_location_type_meta($text, $post)
		{
			$sct		= (int)get_post_meta($post->ID, 'show_children_type',true);
			$html		= '			
				<div class=h7>
					<div class="brr">
						<h3>'. __('Show factories and infrastructures like:', "smc"). '</h3>	
						
						<input class="css-checkbox" type="radio"  id="show_children_type1" name="show_children_type" value="0" ' . checked($sct, 0, false) . '/>
						<label class="css-label" for="show_children_type1">'.__("dislocation", "smc").'</label><br>
						<input class="css-checkbox" type="radio"  id="show_children_type2" name="show_children_type" value="1" ' . checked($sct, 1, false) . '/>
						<label class="css-label" for="show_children_type2">'.__("owner", "smc").'</label><br>
						
					</div>
				</div>';
			echo $text . $html;
		}
		
		function true_save_box_data ( $post_id ) 
		{
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
				
			update_post_meta($post_id, 'show_children_type',	$_POST['show_children_type']);
			
			return $post_id;
		}
		
		
		
		
		function smc_myajax_admin_submit($params)
		{
			global $SolingMetagameProduction, $Soling_Metagame_Constructor, $SMP_Locistics, $Factory, $wpdb;
			$html			= 'none!';
			switch($params[0])
			{
				case "change_production_coeficient":
					// 1 - coef
					// 2 - by
					// 3 - array of ids
					$facs		= array();
					switch ($params[2])
					{
						case "0": //by Location							
							foreach($params[3] as $loc)
							{
								$facs	= array_merge($facs, Factories::get_all_location_factories($loc, true));
							}
							break;
						case "1": // by Goods type
							$args			= array(
														'numberposts'	=> -1,
														'offset'		=> 0,
														'post_status' 	=> 'publish',
														'fields'		=> 'ids',
														'post_type'		=> SMP_FACTORY,
														'meta_query'	=> array(
																					array(
																							'key'		=> 'goods_type_id',
																							'value'		=> $params[3],
																							'operator'	=> 'OR'
																						  )
																				),
													);
							$facs			= get_posts($args);
							break;
						case "2": // by Industry
							$args			= array(
														'numberposts'	=> -1,
														'offset'		=> 0,
														'post_status' 	=> 'publish',
														'fields'		=> 'ids',
														'post_type'		=> SMP_FACTORY,
														'meta_query'	=> array(
																					array(
																							'key'		=> 'industry',
																							'value'		=> $params[3],
																							'operator'	=> 'OR'
																						  )
																				),
													);
							$facs			= get_posts($args);
							
							break;
						case "3": // personally
							$facs			= $params[3];
							break;
					}
					foreach($facs as $fac)
					{
						$powerfull		= get_post_meta($fac, "powerfull", true);
						update_post_meta($fac, "powerfull", ceil($powerfull * ((int)$params[1] +100)/100));
					}
					$d					= array(	
													$params[0], 
													array(
															'text'	=> __("Complete"),
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "update_prod_circle_id":
					$ccid = $params[1];
					$query				= "DELETE FROM `". $wpdb->prefix.REPORT_DB_PREFIX . "` WHERE report_id>" . $ccid;
					$wpdb->get_results($query);
					update_option("current_circle_id", $ccid);					
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $ccid,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "update_prod_circle": 
					$production_interval					= $params[1];
					$this->options['production_interval'] 	= $production_interval;	
					$this->update_options();
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $production_interval,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "install_data_package":
					switch($params[1])
					{
						case 'fantasy':
						case 'modern':
							require_once(SMP_REAL_PATH.'tpl/package_fantasy.php');
							break;
					}
					$d					= array(	
													$params[0], 
													array(
															'text'	=> __("Complete"),
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "smp_save_setting":
					switch($params[1])
					{
						case "srt_dsn":
							$this->options['storage_design'] = (int)$params[2];
							break;
					}
					$this->update_options();
					$d					= array(	
													$params[0], 
													array(
															'text'	=> __("Success"),
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
			}
		}
		function smc_myajax_submit($params)		
		{	
			global $SolingMetagameProduction, $Soling_Metagame_Constructor, $SMP_Locistics, $Factory, $start, $wpdb;
			$start			= getmicrotime() ;
			$html			= 'none!';
			switch($params[0])
			{
				case "change_factory_power":
					if( Factories::is_user_owner( get_post_meta($params[1], "owner_id", true), get_current_user_id() ) )
					{
						$factory		= Factory::get_factory($params[1]);
						$powerfull		= $params[2];
						$new_p			= $factory->update_powerfull($powerfull); 
					}
					$d					= array(	
													$params[0], 
													array(
															'text'		=> $new_p,
															'fid'		=> $params[1],
															'product'	=> $factory->get_productivity(),
															'time'		=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				case 'change_factory_modifier':
					if( Factories::is_user_owner( get_post_meta($params[1], "owner_id", true), get_current_user_id() ) )
					{
						$new_modifier_id= $params[2];
						$fac			= get_post($params[1]);		
						$fact			= Factory::get_factory($params[1]);	
						Factories::change_modifier($new_modifier_id, $params[1]);				
						$industry		= SMP_Industry::get_instance($fact->get_industry());			
						$fac_widget		= Factories::get_widget_form($fac);	
					}
					$d					= array(	
													$params[0], 
													array(
															'text'	=> array(
																				"<span class='lp_under_title_klapan'>". $industry->get("factory_name") . "</span> " . $fact->body->post_title, 
																				$fac_widget['html'], 
																				$params[2], 
																				$params[1], 
																				$params[4], 
																				$fac_widget['elements'],
																				2, 
																			),
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				case "change_my_factory_goods_type":
					$fid			= $params[1];
					if(Factories::is_user_owner(get_post_meta($fid, 'owner_id', true), get_current_user_id()))
					{
						$fac		= Factory::get_factory( $fid );
						$Factory->update_goods_type($fid, $params[2]);
						$html		= Factories::get_large_form($fac->body);
					}
					else
					{
						$html		= "You are not owner of Factory #".$fid;
					}
					$d		= array(	
									$params[0], 
								array(
										'text' 			=> $html,
										'fid'			=> $fid,
										'time'			=> ( getmicrotime()  - $start )	
								     )
							);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'change_factory_goods_type':
					if(Factories::is_user_owner(get_post_meta($params[1], 'owner_id', true), get_current_user_id()))
					{
						$Factory->update_goods_type($params[1], $params[2]);
						$fac				= get_post($params[1]);				
						$fac_widget			= Factories::get_widget_form($fac);		
						$fact				= Factory::get_factory($params[1]);				
						$industry			= SMP_Industry::get_instance($fact->get_industry());
						$html				= array(
													"<span class='lp_under_title_klapan'>". $industry->get("factory_name") . "</span> " . $fac->post_title, 
													$fac_widget['html'], 
													$params[2], 	// goods type id
													$params[1], 	// factory id
													$params[4], 	// string "factory_id"
													$fac_widget['elements'],
													(int)$params[3],// 0 
												);
						
					}
					else
					{
						
					}
					$d		= array(	
									$params[0], 
								array(
										'text' 			=> $html,
										'time'	=> ( getmicrotime()  - $start )	
								     )
							);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'open_hub_widget':
					$hub				= get_post($params[1]);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> array("<span class='lp_under_title_klapan'>".__("Hub", "smp"). "</span> " . $hub->post_title, SMP_Hub::get_widget_form($params[1]), $params[2], $params[1], $params[4]),
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				case 'open_factory_widget':
					$fac				= get_post($params[1]);				
					$fac_widget			= Factories::get_widget_form($fac);	
					$fact				= Factory::get_factory($params[1]);				
					$industry			= SMP_Industry::get_instance($fact->get_industry());
					$html3				.= "
						<div class='smc_red_rounded hint hint--left' data-hint='" . __("goto to factory", "smp") . "' view_content='" . get_permalink((int)$params[1]) . "' style='position:absolute; right:32px; top:17px;'>
							<i class='fa fa-play'></i> " . 
						"</div>";
			
					$d					= array(	
													$params[0], 
													array(
															'text'	=> array(
																				"<span class='lp_under_title_klapan'>". $industry->get("factory_name") . "</span> " . $fact->body->post_title . $html3, 
																				$fac_widget['html'], 
																				$params[2], 
																				$params[1], 
																				$params[4], 
																				$fac_widget['elements'],
																				
																			),
															'time'	=> ( getmicrotime()  - $start ),
															"exec"	=> "close_modal"
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;				
				case 'open_routh_widget_klapan':
					$route					= new SMP_Routh($params[1]);
					$type					= $route->get_type();
					$d						= array(	
														$params[0], 
														array(
																'text' 			=> array("<span class='lp_under_title_klapan'>".$type->post_title . "</span> " . $route->data->name, SMP_Routh::get_widget_form($route), $params[2], $params[1], $params[4]),
																'time'			=> ( getmicrotime()  - $start )																
														 
														 )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "switch_location":					
					$d						= array(	
														$params[0], 
														array(
																'text' 			=> SMC_Location::get_term_link((int)$params[1]),
																'time'			=> ( getmicrotime()  - $start )															
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "show_mine_gbatch":
				{
					$bid	= $params[1];
					$html	= SMP_Goods_Batch::serverside_service(array(get_post($bid)));
					$batch	= SMP_Goods_Batch::get_instance($bid);
					if($batch->user_is_owner())
					{
						$ow_id		= get_post_meta($bid, "owner_id", true);
						$ow			= SMC_Location::get_instance($ow_id);
					}
					$gb		= SMP_Goods_Batch::get_instance($bid);
					$html	.= $gb->exec_dialog($gb->body, $ow, "exec_batch_", 1);
					$d		= array(	
									$params[0], 
									array(
											'text'	=> array($html, $params[1]),
											'time'	=> ( getmicrotime()  - $start )	
										 )
								);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				}
				//выдать диалог манипуляций с партией 
				case "show_goods_batch":
					$bid	= $params[1];
					$gb		= SMP_Goods_Batch::get_instance($bid);
					$html	= SMP_Goods_Batch::serverside_service(array(get_post($bid)));
					$ow		= get_current_user_id() == $gb->user_is_owner() ? 1 : -1;
					if($ow == -1 || !is_user_logged_in())
					{
						$html	.= $gb->get_stroke($gb->body, -1, array("width"=>450));
					}
					else
						$html	.= $this->armed_goods_batch_iface($bid);
					
					$d		= array(	
									$params[0], 
									array(
											'text'	=> $html,
											'time'	=> ( getmicrotime()  - $start )
										 )
								);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				//начать перемещение партии в другую Локацию
				case "transfer_gb":
					$loc_id			= $params[1];
					$routh_id		= $params[2];
					$gb_id			= $params[3];
					$count			= $params[4];
					$gb				= SMP_Goods_Batch::get_instance($gb_id);
					if( $gb->user_is_owner())
					{
						if (static::is_logistics())
						{
							$gt_id		= $gb->get_goods_type_id();
							$foo 		= $SMP_Locistics->transportation_batch_change($gb_id, $routh_id, $count);
							if(!is_wp_error($foo))
							{
								$loc	= SMC_Location::get_instance( $loc_id );
								$routh	= get_term_by("id", $routh_id, "smp_routh"); 
								$r_data	= get_option("smp_routh_$routh_id");
								$html	= "<div style='position:relative; display:inline-block; float:left; margin:10px;'>". Goods_Type::get_trumbnail($gt_id).'</div>';
								$html	.= "<div><h2>".__("Transportation", "smp")."</h2>" . sprintf(__("Goods batch of %s transfered to Location %s by %s.", "smp"),  " <B>".get_post($gt_id)->post_title."</B>", "<B>".$loc->name."</B>" , $routh->name);
								$html	.= "<BR>". sprintf(__("Expected time of arrival to the destination after %s minutes", "smp"), (int)$r_data['speed']) . "</div>";
							}
							else
							{
								$html	= $foo->get_error_message();
							}
						}
						else
						{
							$gt_id		= $gb->get_goods_type_id();
							$loc		= SMC_Location::get_instance( $loc_id );
							list($new_gb, $new_count)	= $gb->rize_batch_transportation2( $count, -1, GOODS_BATCH_NAME );							
							$new_gb->change_dislocation($loc_id);
							$html	= "<div style='position:relative; display:inline-block; min-width:400px;'><div style='position:relative; display:inline-block; float:left; margin:10px;'>". Goods_Type::get_trumbnail($gt_id).'</div>';
							$html	.= "<div><h2>".__("Transportation", "smp")."</h2>" . 
								sprintf(__("Goods batch of %s transfered to Location %s", "smp"),  " <B>".get_post($gt_id)->post_title."</B>", "<B>".$loc->name."</B>").
							"<div><div>";
						}
					}
					else
					{
						$html		= "<h2>".__("Error: only owner may do this", "smp")."</h2>";
					}
					$dpar	= array(
											'text'	=> $html,
											'time'	=> ( getmicrotime()  - $start )
										
									);
					if(static::is_logistics())
					{
						if(!is_wp_error($foo))
						{
							$dpar	= array_merge($dpar, array(
																"bid"		=> $foo->id ,
																"period"	=> (int)$r_data['speed'],
																"rid"		=> $routh_id,
																"routes"	=> $SMP_Locistics->smc_map_external_0("", 0)
															  ));
						}
					}
					$d		= array(	
									$params[0], 
									$dpar
								);
					
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				//начать передачу партии другому владельцу
				case "transfer_goods_batch":
					$loc_id			= $params[1];
					$gb_id			= $params[2];
					$batch			= SMP_Goods_Batch::get_instance($gb_id);
					if( $batch->user_is_owner())
					{
						//update_post_meta($gb_id, "owner_id", $loc_id);
						$id			= Batch::rize_batch_owner2($batch, $loc_id, $batch->get_meta("count"));
						$b			= $id[0];
						$owner		= SMC_Location::get_instance( $loc_id);
						$html		= "<h2>".__("Change owner of goods batch!", "smp")."</h2>";
						$html		.= sprintf(__("New owner - %s. New Id of this batch - %s", "smp"), "<B>". $owner->name. "</B>", $b->id);
					}
					else
					{
						$html		= "<h2>".__("Error: only owner may do this", "smp")."</h2>";
					}
					$d		= array(	
									$params[0], 
									array(
											'text'	=> $html,
											'time'	=> ( getmicrotime()  - $start )
										 )
								);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "smp_more_factories":
					$d		= array(	
									$params[0], 
									array(
											'text'	=> "<div style='height: 500px; width:540px;' id='dialog_facs'>".$this->smc_widget_infrastructure("", $params[1], -1) ."</div>",
											'time'	=> ( getmicrotime()  - $start )	
										 )
								);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "smp_storage_angar":
					$owid				= -1;
					switch($params[3])
					{
						case SMP_FACTORY:
						case "smp_hub":							
							$locid		= get_post_meta($params[1], "dislocation_id", true);
							$owid		= get_post_meta($params[1], "owner_id", true);
							$batches	= SMP_Assistants::get_all_batchs_location($locid, $owid);
							break;
						case "SMC_Location":							
							$ltid		= SMC_Location::get_term_meta($params[1]);
							//if( get_post_meta($ltid['location_type'], "show_children_type", true) ) 
							//{
								$owid	= -1;//get_post_meta($params[1], "owner_id", true);
								$locid	= $params[1];
							//}
							//else
							//{
								//$owid	= -1;
								//$locid	= 0;//SMC_Location::get_instance($params[1])->parent;
								
							//}
							$batches	= SMP_Assistants::get_all_batchs_location($locid, $owid);
							break;
						default:
							$batches	= SMP_Assistants::get_all_batchs_location($locid, $owid);
							break;
							
					}
					
					$text	= $this->draw_storage($batches, 0, STORAGE_PICE, $params[2], ceil(count($batches)/STORAGE_PICE), $params[1], $params[3]);
					$d		= array(	
									$params[0], 
									array(
											'text'		=> $text, 
											'offset'	=> $params[2],
											'time'		=> ( getmicrotime()  - $start )
										 )
								);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;	
					break;
				case "ch_fac_productivity":
					$factory_id			= $params[1];
					$new_productivity	= $params[2];
					if(Factories::is_user_owner(get_post_meta($factory_id, "owner_id", true), get_current_user_id()))
					{
						$fac			= new Factory($factory_id);
						$powerfull		= $fac->update_productivity($new_productivity);
						$text			= $powerfull;
					}
					else
					{
						$a_alert 			= "<h2>".__("Error: only owner may do this", "smp")."</h2>";
					}
					$d		= array(	
									$params[0], 
									array(
											'text'		=> $text, 
											'a_alert'	=> $a_alert,
											'time'		=> ( getmicrotime()  - $start )	
										 )
								);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;					
				case "change_my_gb_par":
					$text				= "none";
					$bid				= $params[2];
					$gb_table_id		= $params[10];
					$gb_slide_id		= $params[11];
					$gbatch				= SMP_Goods_Batch::get_instance($bid);
					if( $gbatch->user_is_owner() )
					{
						switch($params[1])
						{
							case "to_store":
								$col		= $params[3];
								$price		= $params[4];
								$store		= $params[5];
								list($new_batch, $new_count) = Batch::rize_batch_store2(
																		$gbatch,
																		$col,
																		$price
																	);	
								$new_batch->update_meta("store", $store);
								$text		= __("Change to store ", 'smp')." " .$new_count." ".__("unit", "smp");								
								break;
							case "to_transfer":
								$col		= $params[3];
								$now		= $params[4];
								list($new_batch, $new_count) = Batch::rize_batch_owner2(
																		$gbatch,
																		$now,
																		$col												
																	);
								$text			= __("Change owner", "smp")." " .$new_count." ".__("unit", "smp");
								break;
							case "to_delete":
								$col		= $params[3];
								$gbatch->rize_batch_delete($col);
								$text			.= __("Delete ", "smp")." " .$new_count." ".__("unit", "smp");
								break;
						}
						$slide			= apply_filters("smp_change_table_goods_batch_par", Goods_Batch::get_all_batchs_switcher( array( "store" => 1 ), false, "store", $gb_slide_id), $gb_table_id, $gb_slide_id);
					}
					else
					{
						$text			= "<h2>".__("Error: only owner may do this", "smp")."</h2>";
					}
					$bb					= $n[0];	
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $text,
															'slide'	=> $slide,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				case "get_waybill":
					$id					= $params[1];
					$text				= SMP_Waybill::get_full_edit_form($id, true);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $text,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				case "smp_get_circle_report":
					
					$cont				= $params[1];
					$text				= SMP_Assistants::get_form($cont);
					$d					= array(	
													$params[0], 
													array(
															'text'	=> $text,
															//'cont'	=> $cont,
															'time'	=> ( getmicrotime()  - $start )																
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;	
				case "get_store_list":
					$gtid				= $params[1];				
					$form				.= Goods_Type::get_store_list($gtid);
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $form,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "search_gb":
					$is_convert			= $params[5];
					$srch				= array('id'=>$params[1], 'owner_id'=>$params[2], 'goods_type_id'=>$params[3], "dislocation_id"=>$params[4]);
					$result				= SMP_Goods_Batch::search($srch);
					$text				= "<h1>".__("Results", "smp")."</h1><div style='display:inline-block; position:relative; background:#222; padding:12px;'>";
					foreach($result as $gb)
					{
						$text			.= SMP_Goods_Batch::get_picto($gb->ID, false, array("size"=>80, "left_text"=>"litle", "is_menu" => $is_convert));
					}
					$text				.= "</div>";
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $text,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "insert_new_gb":
					$srch				= array('owner_id'=>$params[1][0], 'goods_type_id'=>$params[1][1], 'count'=>$params[1][2], "dislocation_id"=>$params[1][3]);
					//if(self::is_finance())
					{
						$term_data		= SMC_Location::get_term_meta($params[1][0]);
						$srch['currency_type_id']	= $term_data['currency_type'];
					}
					$result				= Batch::insert2($srch);
					$text				= "<h1>".__("Results", "smp")."</h1><div style='display:inline-block; position:relative; background:#222; padding:12px;'>";
					$text				.= SMP_Goods_Batch::get_picto($result->id, false, array("size"=>80, "left_text"=>"litle", "is_menu"=>true));					
					$text				.= "</div>";
					$d					= array(	
													$params[0], 
													array(
															'text' 			=> $text,
															'time'			=> getmicrotime()  - $start
														  )
												);
					$d_obj				= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case 'convert_goods_batch': //see Ermak/shortcode/location_display.php
					if(current_user_can("administrator"))
					{
						$bid	= $params[1];
						$batch	= SMP_Goods_Batch::get_instance($bid);
						//$html	= $batch->get_stroke(get_post($bid), -3, array("width"=>550));
						$html	= "
						<table class='clear_table'>".
							$batch->get_table_row().
						"</table>";
						$html	.= "<div class=smc_comment><h2>". __("Attention", "smp"). "</h2>".__("This object is completely removed from the Database. Do not close this window as long as you do not give the Player a certificate of the Goods Batch.", "smp")."</div>";
						//wp_delete_post($bid);
						$batch->remove();
					}
					else
					{
						$html	= __("You can not convert goods batch. Call a Master!", "smp_dragon");
						$bid	= -1;
					}
					$d			= array(	
											$params[0], 
											array(
													'text'	=> $html,
													'exec'	=> 'delete_batch_picto',
													'args'	=> $bid
												 )
										);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "show_batch_pult";
					$id			= $params[1];
					$batch		= SMP_Goods_Batch::get_instance($id);
					$is_owner	= $batch->user_is_owner();
					$html		= $is_owner ? $batch->exec("exec_batch_" . $id, true) : __("You are not owner", "smс");
					$title		= $is_owner ? __("Execute manipulations", "smp") . " | ID ".$id : "";
					$d			= array(	
											$params[0], 
											array(
													'text'	=> $html,
													'title'	=> $title,
													"gid"	=> $id,
													'args'	=> $bid
												 )
										);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "show_batch_payd";
					$id			= $params[1];
					$batch		= SMP_Goods_Batch::get_instance($id);					
					
					if($batch->user_is_owner())
					{
						$html	= $batch->get_paying_form();
						$title	= __("The Sale", "smp");						
					}
					else
					{
						$count	= $batch->get_count();
						if($count<0)
						{
							$html	= "Error summ";
						}
						else if($count>0)
						{
							$html	= $batch->pay_dialog(false);
							$title	= __("Paying", "smp");
						}
						else if(is_user_logged_in() )
						{
							$user_id		= get_current_user_id();
							$gb				= SMP_Goods_Batch::get_instance( $gb_id );
							$locs			= $Soling_Metagame_Constructor->all_user_locations($user_id);
							$loc			= $locs[0];
							if($loc)
							{
								$gb->update_meta("owner_id", 	$loc);
								$gb->update_meta("store",		false);
							}
							else
							{
								$html		= __("You can not make economic transaction. The reason - lack of ownership. Call Master.", "smc"); 
							}
						}
						else
						{
							$alert			= __("Log in please.", "smc");
						}
					}
					$d			= array(	
											$params[0], 
											array(
													'text'	=> $html,
													'title'	=> $title,
													'args'	=> $bid
												 )
										);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "change_store";
					$id			= $params[1];
					$batch		= SMP_Goods_Batch::get_instance($id);	
					if($batch->user_is_owner())
					{
						$html	= "";		
						$store	= $batch->get_meta("store");	
						$batch->update_meta("store", !$store);
					}
					else
					{
						$a_alert= __("You are not owner of this batch!", "smp");
					}
					$d			= array(	
											$params[0], 
											array(
													'text'		=> !$store,
													'a_alert'	=> $a_alert,
													'args'		=> $bid
												 )
										);
					$d_obj		= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "pay_gb";
					$id				= $params[1];
					$count			= $params[2];
					$acc_id			= $params[3];	
					$isac_owner		= SMP_Account::is_user_owner($acc_id);
					if($isac_owner)
					{				
						$acc		= SMP_Account::get_instance($acc_id);
						$batch		= SMP_Goods_Batch::get_instance($id);
						$price		= $batch->get_price();
						$batch_owner_id = $batch->get_owner_id();
						$owner_id	= $acc->get_owner_id();
						$summ		= SMP_Account::see_summ_location( $owner_id, $acc->get_currency_type_id() );
						$need_summ	= $count * $price;
						if($summ > $need_summ)
						{
							$good_type	= Goods_Type::get_instance($batch->get_goods_type_id());
							list($new_batch, $new_count) = Batch::rize_batch_store2(
																	$batch,
																	$count,
																	$need_summ
																);	
							$new_batch->update_meta( "owner_id", $owner_id );
							$new_batch->update_meta( "store", false );
							$new_batch->update_meta( "price", $price );
							$ct			= SMP_Currency_Type::get_instance($acc->get_currency_type_id());
							$reason		= sprintf(
								__("Payment for the purchase of consignment goods batch ID=%s, goods type - %s, count - %s unit.", "smp"), 
								"<b>".$id."</b>", 
								"<b>".$good_type->post_title."</b>", 
								"<b>".$count."</b>"
							);
							$reason2	= sprintf(
								__("Location %s consignment of goods batch %s. Goods Type is %s. Count - %s.", "smp"),
								"<b>" . SMC_Location::get_instance($owner_id)->name . "</b>", 
								"<b>" . $id . "</b>", 
								"<b>" . $good_type->post_title . "</b>", 
								"<b>" . $count . "</b>"
							);
							SMP_Account::remove_summ_from_location( $owner_id, $acc->get_currency_type_id(), $need_summ, $reason );
							SMP_Account::add_summ_to_location($batch_owner_id, $acc->get_currency_type_id(), $need_summ, $acc_id, $reason2 );
							$html		= "<table class='clear_table'><tr><td>" . Goods_Type::get_picto($good_type->ID). "</td><td style='color:#111; width:200px;'>".$reason . " summ - <b>" . $ct->get_price($need_summ)."</b></td></tr></table>";
							$title		= __("Success", "smc");
						}
						else
						{
							$html		= sprinf(__("You don't have the required amount in your Accounts in the required currency. It should be - %s, and there are - %s.", "smp"), $summ, $need_summ);
							$title		= __("Error", "smc");
						}
					}
					else
					{
						$html		=  __("You not owner of this account! Call Master.", "smp");
						$title		= __("Error", "smc");
					}
					$d				= array(	
											$params[0], 
											array(
													'text'			=> $html,
													'title'			=> $title,
													'a_alert'		=> $a_alert,
													'args'			=> $bid
												 )
										);
					$d_obj			= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				case "update_production_comment":					
					$d				= array(	
											$params[0], 
											array(
													'text'			=> $this->get_production_comment(),
												 )
										);
					$d_obj			= json_encode(apply_filters("smc_ajax_data", $d));
					print $d_obj;
					break;
				
			}
		}
		
		
		function smp_bath_card_transit($batch_id)
		{
			global $Soling_Metagame_Constructor, $GoodsType;
			$op							= get_option(SMP);
			$goods_type_id 				= get_post_meta( $batch_id, "goods_type_id", true );
			$count						= get_post_meta( $batch_id, "count", true );;
			$factory_id					= get_post_meta( $batch_id, "factory_id", true );
			$batch_owner_id				= get_post_meta( $batch_id, "owner_id", true );
			$batch_dislocation_id		= get_post_meta( $batch_id, "dislocation_id", true );
			$store						= get_post_meta( $batch_id, "store", true );
			$price						= get_post_meta( $batch_id, "price", true );
			$qual						= get_post_meta( $batch_id, "quality", true);
			$qual						=  !$qual ? 50 : $qual;
			$price_text					= "";	
			$factory					= get_post($factory_id);
			$goods_type					= get_post($goods_type_id);
			$owner						= SMC_Location::get_instance( $batch_owner_id); 
			$best_before				= get_post_meta($batch_id, "best_before", true);
			$transfer_slide	= "
			<div id='gb_transfer_".$batch_id."' style='display:none;'>
				<div class='smp-batch-tab-slide'>
					<div class='smp-table-null'>
						<label for='transfer_to_location_".$batch_id."'>".__("Select count of units for transfer","smp")."</label><br>
						<input  id='transfer_to_location_".$batch_id."' name='transfer_to_location_".$batch_id."' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
						__("unit", "smp").
					"</div>
					<div class='smp-table-null' style='display:table-cell;'>
						<label for='location_".$batch_id."'>".__("Choose Location-reciever","smp")."</label><br>
						".$Soling_Metagame_Constructor->get_location_selecter(array("id"=>"location_".$batch_id,  "name"=>"location_".$batch_id, "style"=>'padding:6px;width:140px;border-radius:4px;'))." 
					</div>
					<div class='smp-table-null' style=''>
						<input name='save_change".$batch_id."' type='submit'  class='smp-colorized smp-submit'  value='".__("Send to other Location", "smp")."'/>
					</div>
				</div>
			</div>";
			$html	= 
			'<h2>' . __("The Transfer", "smp") . '</h2>
			<div>' . 
				$GoodsType->get_trumbnail($batch_id) .
			'</div>' . 
			$transfer_slide;
		}
		
		function init_achive_types()
		{
			global $achivment_types;
			$hub										=  new SMC_Achivment;
			$hub->init( array( 
									"par_type"			=>  'smc_hub', 
									'capability_type'	=>'post',
									"post_meta"			=> array(
																"capacity",
																"cost",
																"dislocation_id",
																"owner_id"
															)
							 ) );
			$achivment_types[__('Hub', "smp")]			= $hub;
			
			$factory									=  new SMC_Achivment;
			$factory->init( array(
									"par_type"=>  SMP_FACTORY, 
									'capability_type'=>'post',
									"post_meta"			=> array(
																"powerfull",
																"goods_type_id",
																"quality",
																"dislocation_id",
																"owner_id"
															)
								  
								  ) );
			$achivment_types[__('Factory', "smp")]	= $factory;			
			$goods_batch							=  new SMC_Achivment;
			$goods_batch->init( array( 
									"par_type"			=> GOODS_BATCH_NAME, 
									'capability_type'	=> 'post',
									"post_meta"			=> array(
																"factory_id",
																"dislocation_id",
																"owner_id",
																"goods_type_id",
																"store",
																"count",
																"quality",
																"best_before",
															)
									  ) );
			$achivment_types[__('Goods batch', "smp")]	= $goods_batch;
		}
		function get_gb_options()
		{
			return $this->options["gb_settings"];
		}
		function get_gb_value($value)
		{
			$opt	= $this->options["gb_settings"];
			return $opt[$val];
		}
		function isBatchOpt($optName) 
		{
			return isset($this->options["gb_settings"][$optName]) && $this->options["gb_settings"][$optName];
		}
		function is_BatchOptEv($optNmae)
		{
			return $this->options["gb_settings"][$optName];
		}
	}
?>