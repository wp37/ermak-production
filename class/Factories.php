<?php

	class Factories
	{
		protected $id;
		
		function __construct()
		{
			global $smp_factories;
			$smp_factories 											= array();
			add_action( 'init', 									array($this, 	'add_factory_type'), 11 );
			
			// мета-поля в редактор
			add_action('admin_menu',								array($this, 	'my_extra_fields_factory'));
			add_action('save_post_'.SMP_FACTORY,					array($this, 	'true_save_box_data'));
			add_filter('manage_edit-'.SMP_FACTORY.'_columns',		array($this, 	'add_views_column'), 4);
			add_filter('manage_edit-'.SMP_FACTORY.'_sortable_columns', array($this,	'add_views_sortable_column'));
			add_filter('manage_'.SMP_FACTORY.'_posts_custom_column', array($this,	'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
			//add_filter('get_sidebar', 							array($this,	'get_sidebar'), 5, 2);
			
			add_filter('smc_location_type_meta', 					array($this,	'smc_location_type_meta'), 5, 2); // meta box in Location Type
			add_action('save_post_location_type',					array($this, 	'location_type_save_box_data'), 11);
			add_filter('manage_edit-location_type_columns', 		array($this, 	'location_type_add_views_column'), 5);
			add_filter('manage_location_type_posts_custom_column', 	array($this,	'location_type_fill_views_column'), 6, 2); 
			add_filter('smc_location_archive_title', 				array($this,	'smc_location_archive_title'), 17, 2); 
			add_filter( 'smc_dot_content',							array($this, 	'smc_dot_content'), 10, 2 );
		}	
			
	
		function add_factory_type()
		{
				$labels = array(
					'name' => __('Factory', "smp"),
					'singular_name' => __("Factory", "smp"), // админ панель Добавить->Функцию
					'add_new' => __("add Factory", "smp"),
					'add_new_item' => __("add new Factory", "smp"), // заголовок тега <title>
					'edit_item' => __("edit Factory", "smp"),
					'new_item' => __("new Factory", "smp"),
					'all_items' => __("all Factories", "smp"),
					'view_item' => __("view Factory", "smp"),
					'search_items' => __("search Factory", "smp"),
					'not_found' =>  __("Factory not found", "smp"),
					'not_found_in_trash' => __("no found Factory in trash", "smp"),
					'menu_name' => __("Factory", "smp") // ссылка в меню в админке
				);
				$args = array(
					'labels' 				=> $labels,
					'public' 				=> true,
					'show_ui' 				=> true, // показывать интерфейс в админке
					'has_archive' 			=> true, 
					'exclude_from_search' 	=> true,
					'menu_position' 		=> 21, // порядок в меню
					'show_in_menu' 			=> "Metagame_Production_page",
					'taxonomies'    		=> array(SMC_LOCATION_NAME),
					'supports' 				=> array(  'title', 'thumbnail')
					//,'capabilities' 		=> 'manage_options'
					,'capability_type' 		=> 'post'
				);
				register_post_type(SMP_FACTORY, $args);
		}

		// мета-поля в редактор
		
		function my_extra_fields_factory() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(&$this, 'extra_fields_box_func'), SMP_FACTORY, 'normal', 'high'  );
			
		}
		
		
		
		function smc_location_archive_title($args, $cur_term)
		{
			global $SolingMetagameProduction;
			$op					= $SolingMetagameProduction->options;
			if($op['use_panel'])	return $args;
			
			return $args.$this->get_location_form($cur_term);
		}
		function get_location_form($cur_term)
		{
			$loc_ids			= SMC_Location::get_child_location_ids($cur_term->term_id);
			global $Soling_Metagame_Constructor;
			global $user_iface_color;
			$rr				= "
			<div class='location-tamplate-content'>
			<hr/><h2>".__("all Factories", "smp").":</h2>";
			
			$ar				= array(										
											'numberposts'	=> -1,
											'offset'    	=> 0,
											'orderby'  		=> 'id',
											'order'     	=> 'DESC',
											'fields' 		=> 'ids',
											'post_type' 	=> SMP_FACTORY,
											'post_status' 	=> 'publish',
											'meta_query' 	=> array(
																		array(
																				"key"		=> "dislocation_id",
																				"value"		=> $loc_ids,
																				"operator"	=> "AND"
																			),
																	),
										);
			$factories			= get_posts($ar);
			if(count($factories))
			{
				foreach($factories as $fid)
				{
					$rr				.= self::get_pictogramm($fid);
				}
				$rr					.= "<hr/></div>";
			}
			else
			{
				$rr					= "";
			}
			return $rr;
		}
		
		public function get_count($factory_id, $good_type_id)
		{
			$powerfull		= get_post_meta($factory_id, 	"powerfull", true);
			$complexity		= get_post_meta($good_type_id, 	"complexity", true);
			if(!$complexity) $complexity = 1;
			return floor ($powerfull/ $complexity);
		}
		
		function extra_fields_box_func( $post )
		{	
			global $SolingMetagameProduction;
			$args			= array(
										 'number' 		=> 100
										,'offset' 		=> 0
										,'orderby' 		=> 'name'
										,"hide_empty"	=> false
									);
			$owners			= get_terms(SMC_LOCATION_NAME, $args);
			
			$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> 'goods_type',
										'post_status' 	=> 'publish'
									);
			$goods					= get_posts($ar);
			$goods_types			= $goods;
			//$url					= get_post_meta( $post->ID, "picture_url", true );
			//$url					= $url == "" ? SMP_URLPATH."img/factory_icon.png" : $url;
			
			$oid 					= get_post_meta( $post->ID, "owner_id", true );
			$did 					= get_post_meta( $post->ID, "dislocation_id", true );
			$gtid 					= get_post_meta( $post->ID, "goods_type_id", true );
			$quality_modificator 	= get_post_meta( $post->ID, "modificator", true );
			
			//modifiers
			$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										
										'meta_query'	=> array(
																	'relation'		=> 'AND',
																	array(
																		'value'		=> $did,
																		'key' 		=> 'dislocation_id',
																		'type' 		=> 'numeric',
																		'compare' 	=> 'LIKE'
																	),
																	array(
																		'value'		=> $oid,
																		'key' 		=> 'owner_id',
																		'type' 		=> 'numeric',
																		'compare' 	=> 'LIKE'
																	),
																)	
																	/**/
									);
			$quality_m		= get_posts($ar);
			$components_type = (int)get_post_meta($post->ID, "components_type", true);
			
			
			?>
			<div style="position:relative; display:inline-block;">
				<div class="h0">
					<div class="h">
						<label for="industry"><?php _e("Industry", "smp");?></label><br>
						<?php echo SMP_Industry::wp_dropdown_industry(array('name'=>'industry', 'class'=>'h2', 'selected'=>get_post_meta( $post->ID, "industry", true ))); ?>
					</div>
					<div class='h'>
						<label for="owner"> <?php echo __("owner", "smp") . "  (id:". $oid .")"; ?></label><br>
						<select name="owner" class="h2">
							<?php
							foreach($owners as $owner)
							{
								echo "<option value='" . $owner->term_id . "' " . selected($owner->term_id, $oid) . " >" . $owner->name. "</option>";
							}
							?>
						</select>
						
					</div>
					<div class='h'>
						<label for="dislocation"> <?php echo __("Dislocation", "smp"). " (id:". $did .")"; ?></label><br>
						<select name="dislocation" class="h2">
							<?php
							foreach($owners as $owner)
							{
								echo "<option value='" . $owner->term_id . "' " . selected($owner->term_id, $did) . " >" . $owner->name. "</option>";
							}
							?>
						</select>
						
					</div>
					<div class='h'>
						<label  class="h2" for="goods_type_id"><?php echo __("Goods type", "smp"). " (id:". $gtid .")"; ?></label><br>
						<!--select name="goods_type_id" class="h2">
							<?php
							
							foreach($goods as $good)
							{
								echo "<option value='" . $good->ID . "' " . selected($good->ID, $gtid) . " >" . $good->post_title. "</option>";
							}
							?>
						</select-->
						<div id="myDropdown"></div>
						<div style="margin-top:3px;">
							<div><?php _e("Technology type", "smp");?></div>
							<!--input type="number" id="components_type" name="components_type" value="<?php echo $components_type; ?>" min="0" max="1" /-->
							<input type="radio" class="css-checkbox" id="components_type1" name="components_type" value="0" <?php checked(0, $components_type) ?> />
							<label for="components_type1" class="css-label"><?php _e("Base", "smp") ?></label>
							<br>
							<input type="radio" class="css-checkbox" id="components_type2" name="components_type" value="1" <?php checked(1, $components_type) ?> />
							<label for="components_type2" class="css-label"><?php _e("Alternative", "smp") ?></label>
							<br>
						</div>
						<input type="hidden" id="goods_type_id" name="goods_type_id" value=""/>
					</div>
					<div class='h'>
						<label  class="h2" for="powerfull"><?php _e("Powerfull", "smp"); ?></label><br>
						<input  class="h2" name="powerfull" type="number" min="0" step="1" value ="<?php print_r( get_post_meta( $post->ID, "powerfull", true ) );?>"/>
					</div>
					<div class='h'>
						<label  class="h2" for="max_powerfull"><?php _e("Maximum powerfull", "smp"); ?></label><br>
						<input  class="h2" name="max_powerfull" type="number" min="0" step="1" value ="<?php print_r( get_post_meta( $post->ID, "max_powerfull", true ) );?>"/>
					</div>
					<div class='h'>
						<label  class="h2" for="quality"><?php _e("Quality", "smp"); ?></label><br>
						<input  class="h2" name="quality" id='quality' value="<?php print_r( get_post_meta( $post->ID, "quality", true ));?>"/>
					</div>
					
					<div class='h'>	
						
						<label  class="h5" for="supported_type_name"><?php echo __("Available Goods Types", "smp"); ?></label>
						
						<div style="vertical-align:middle;" id="scheme">							
						<?php
							$sc		= '
							<div class="consume_scheme_box_element" id="example" style="display:none;">'.
								Goods_Type::wp_dropdown_goods_type(array("class"=>"smco_drop", "name"=>"goods_type")).
								'
								<span>'.$this->wp_dropdown_techology_type(1,array("name"=>"techology_type", "id"=>"techology_type", 'class'=>'tt')).'</span>
								<span class="button minus_button_gt" id="minus" style="vertical-align:middle;"> - </span>
							</div>';
							echo $sc;
							$goods		= get_post_meta($post->ID, "available_goods_types", true);
							//var_dump($goods);
							$i=0;
							if($goods != "")
								if(count($goods)>0)
								{
									foreach($goods as $gd)
									{
										//echo "<BR>goods_type = ".$gd["goods_type"]."<BR>";
										?>
										<div class="consume_scheme_box_element" id="example" style="display:block;">
											<?php echo Goods_Type::wp_dropdown_goods_type(array("selected"=>$gd[0]["goods_type"], "class"=>"smco_drop", "name"=>"goods_type".$i)); ?>
											<span>
												<?php echo $this->wp_dropdown_techology_type((int)$gd[0]["techology_type"], array("name"=>"techology_type".$i, "id"=>"techology_type".$i, 'class'=>'tt'))?>
											</span>
											<span class="button minus_button_gt" id="minus" style="vertical-align:middle;"> - </span>
										</div>
										<?php
										$i++;
									}
								}
						?>
							<input type="hidden" id="i" name="i" value="<?php echo $i; ?>"/>
							<div id="add_button_factory_gt" class="button" style="font-size:20px; margin-top:10px;">
								+
							</div>
						</div>
					</div>
				</div>
				<div class="h0">
					<div class='h'>
						<label  class="h2" for="quality_modificator"><?php _e("Multiplier", "smp"); ?></label><br>
						 <div style="font-style: italic;margin-top:12px; color:#666; font-size:0.9em;" id="myquality_modificator"><?php _e("There are no multipliers in current location. Put goods batch that  have 'Quality multiplier' meta parameter", "smp"); ?></div>
						<input type="hidden" id="quality_modificator" name="quality_modificator" value=""/>
						<input type="hidden" id="is_blocked" name="is_blocked" value="<?php echo get_post_meta($quality_modificator, "is_blocked", true); ?>"/>
						<input type="hidden" id="old_quality_modificator" name="old_quality_modificator" value="<?php echo $quality_modificator; ?>"/>
					</div>
				</div>
				
				<?php 
				//} 
				do_action("smp_admin_factory_box_func", $post->ID);
				?>
			</div>
			<script type="text/javascript">
				var $post_type	= '<?php echo $post->post_type; ?>';
				(function($)
				{		
					$("#quality").ionRangeSlider({
							min: 0,
							max: 100, 			
							type: 'single',
							step: 5,
							postfix: "%",
							prettify: true,
							hasGrid: true
					});
					
					//goods type
					var ddData = [
						<?php
							foreach($goods_types as $gt)
							{
								$tru_id		= get_post_thumbnail_id($gt->ID);
								$img		= wp_get_attachment_image_src($tru_id);
								$img		= (img) ? $img[0] : "";
								echo '
								{
									text: "' . $gt->post_title . '",
									value:' . $gt->ID . ',
									selected: ' . ($gtid == $gt->ID ? "true" : "false") . ',
									description:"id:' . $gt->ID . '",
									imageSrc: "' . $img . '"
								},
								';
							}
							?>
						{
							text: "<?php _e("No multipliers", "smp")?>",
							value:"",
							selected: '<?php ($quality_modificator == "" ? "true" : "false") ?>',
							description:"",
							imageSrc: ""
						},
					];
					$('#myDropdown').ddslick({
												data:ddData,
												width:240,
												height:300,
												selectText: "<?php _e("Goods type", "smp") ?>",
												imagePosition:"left",
												onSelected: function(selectedData)
												{
													//callback function: do something with selectedData;
													$("#goods_type_id").val(selectedData.selectedData.value);
												}   
											});
					
					//quality multiplier
					var qmData = [						
						{
							text: "<?php _e("No multipliers", "smp")?>",
							value:"",
							selected: '<?php ($quality_modificator == "" ? "true" : "false") ?>',
							description:"",
							imageSrc: ""
						},
						<?php
							foreach($quality_m as $qm)
							{
								
								if(get_post_meta($qm->ID, "is_blocked", true) && $quality_modificator != $qm->ID)
								{
									continue;
								}
								$gt						= get_post_meta($qm->ID, "goods_type_id", true);
								$quality_multiplier 	= get_post_meta($gt, "quality_multiplier", true);
								$powerfull_multiplier	= get_post_meta($gt, "powerfull_multiplier", true);
								$quality				= $SolingMetagameProduction->get_gb_value("quality") ? get_post_meta($qm->ID, "quality", true) : 100;
								
								$goodst		= get_post($gt);
								$tru_id		= get_post_thumbnail_id($gt);
								$img		= wp_get_attachment_image_src($tru_id);
								$img		= (img) ? $img[0] : "";
								echo '
								{
									text: "' . $goodst->post_title . get_post_meta($qm->ID, "is_blocked", true).'",
									value:' . $qm->ID . ',
									selected: ' . ($quality_modificator == $qm->ID ? "true" : "false") . ',
									description: "'.__("add Quality", "smp") .' <span>'. ($quality_multiplier * $quality/100) . '</span><BR> '.__("add Powerfull", "smp"). ' <span>' . ($powerfull_multiplier * $quality/100) . '%</span>",
									imageSrc: "' . $img . '"
								},
								';
							}
							?>
						
					];
					$('#myquality_modificator').ddslick({
												data:qmData,
												width:240,
												height:300,
												selectText: "<?php _e("Quality multiplier", "smp") ?>",
												imagePosition:"left",
												onSelected: function(selectedData)
												{
													$("#quality_modificator").val(selectedData.selectedData.value);
												}   
											});
				})(jQuery);
			</script>
			<?php
			
			
			
			wp_nonce_field( basename( __FILE__ ), 'factory_metabox_nonce' );
		}
		function wp_dropdown_techology_type($techology_type=0, $params="")
		{
			if(!is_array($params))
			{
				$params		= array("class"=>"vvv", "name"=>"techology_type", "id"=>"techology_type", "style"=>"width:100px;");
			}
			return "
			<select name='".$params['name']."' id='".$params['id']."' class='".$params['class']."' style='".$params['style']."'>
				<option value='0' ".selected(0, $techology_type,0).">".__("Base", "smp")."</option>
				<option value='0' ".selected(1, $techology_type,0).">".__("Alternative", "smp")."</option>
			</select>";
		}
		function update_goods_type($factory_id, $new_goods_type)
		{
			$old_goods_type					= get_post_meta($factory_id, "goods_type_id", true);
			update_post_meta($factory_id, 'goods_type_id', 		$new_goods_type);
			
			$available_goods_types			= get_post_meta($factory_id, "available_goods_types", true);
			$is_dbl	= false;	
			foreach($available_goods_types as $gt)
			{
				if($gt[0]['goods_type'] == $old_goods_type)
				{
					$is_dbl					= true;
					break;
				}
			}
			$el = array(array('goods_type'=> $old_goods_type, "techology_type"=>get_post_meta($factory_id, "components_type", true)));
			if(!$is_dbl)
			{
				$available_goods_types[] 	= $el;
			}
			update_post_meta($factory_id, 'available_goods_types', 		$available_goods_types);
		}
		function true_save_box_data ( $post_id ) 
		{
			global $table_prefix;
			global $wpdb;			
			// проверяем, пришёл ли запрос со страницы с метабоксом
			if ( !isset( $_POST['factory_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['factory_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
			// 	
			//проверяем доступность Factory Cell
			apply_filters('wp_insert_post_empty_content', "");
			//if(isset($_POST['insert_cell']))
			{
				//$free_cells				= Factory_Cell::get_free_cells_count($_POST['dislocation']);
				//if($free_cells<1)
				//{
				//	echo "<script>alert('You canot create Factory: free cells!');</script>";
				//}
			}
			if($_POST['powerfull']=="")		$_POST['powerfull'] = 20;
			
			update_post_meta($post_id, 'owner_id', 				$_POST['owner']);				
			update_post_meta($post_id, 'industry', 				$_POST['industry']);				
			update_post_meta($post_id, 'dislocation_id', 		$_POST['dislocation']);				
			$powerfull										=	$_POST['powerfull'];	
			update_post_meta($post_id, 'powerfull', 			$powerfull);	
			$max_powerfull									=	$_POST['max_powerfull'];
			if($max_powerfull < $powerfull)
				$max_powerfull = $powerfull;
			update_post_meta($post_id, 'max_powerfull', $max_powerfull);				
			//update_post_meta($post_id, 'goods_type_id', 		$_POST['goods_type_id']);
			update_post_meta($post_id, 'components_type', 		$_POST['components_type']);
			update_post_meta($post_id, 'quality', 				$_POST['quality']);
			update_post_meta($post_id, 'picture_url', 			$_POST['picture_url']);
			//modifiers
			$quality_modificator							= 	$_POST['quality_modificator'];
			update_post_meta($post_id, 'modificator',			$quality_modificator);
			update_post_meta($_POST['old_quality_modificator'],	"is_blocked", false);
			update_post_meta($quality_modificator,				"is_blocked", true);	
			
			$goods							= array();
			for($i=0; $i<$_POST['i']; $i++)
			{
				$g							= array();				
				$goods_t					= $_POST['goods_type'.$i];
				$choose						= $_POST['techology_type'.$i];				
				if($goods_t)
				{
					$g[]					= array("goods_type"=>$goods_t, "techology_type"=>$choose);
				}				
				if(count($g)>0)	$goods[]	= $g;
			}
			update_post_meta($post_id, 'available_goods_types', 		$goods);
			$this->update_goods_type($post_id, $_POST['goods_type_id']);
			do_action("smp_admin_factory_save_box_data", $post_id, $_POST);
			
			if($_POST['post_title']=="")
			{
				$ow							= SMC_Location::get_instance( $_POST['owner'] );
				$disl						= SMC_Location::get_instance( $_POST['dislocation'] );
				$g							= get_post($_POST['goods_type_id']);
				$goods						= $g->post_title;
				$wpdb->update(
								$table_prefix."posts",
								array("post_title" => $goods."-".$ow->name." (". $disl->name.")"),
								array("ID" => $post_id)
							);
			}	
			return $post_id;
		}
		
		
		function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 						=> " ",
				  "IDs"	 					=> __("ID", 'smp'),
				  "goods_type_id" 			=> __("Goods type", 'smp'),
				  "title" 					=> __("Title"),
				  "industry" 				=> __("Industry", "smp"),
				  "owner" 					=> __("Owner", "smp"),
				  "dislocation" 			=> __("Dislocation", 'smp'),
				  "powerfull" 				=> __("Powerfull", 'smp'),
				  "max_powerfull" 			=> __("Maximum powerfull", 'smp'),
				  "available_goods_types" 	=> __("Available Goods Types", "smp"),
			   );
			return $posts_columns;			
		}
		// добавляем возможность сортировать колонку
		function add_views_sortable_column($sortable_columns){		
			$sortable_columns['IDs'] 			= 'IDs';	
			$sortable_columns['owner'] 			= 'owner';
			$sortable_columns['dislocation'] 	= 'dislocation';
			$sortable_columns['goods_type_id'] 	= 'goods_type_id';
			$sortable_columns['powerfull'] 		= 'powerfull';
			$sortable_columns['max_powerfull'] 	= 'max_powerfull';
			$sortable_columns['industry'] 		= 'industry';
			$sortable_columns['available_goods_types'] 		= 'available_goods_types';
			return $sortable_columns;
		}	
		// заполняем колонку данными	
		function fill_views_column($column_name, $post_id)
		{
			$factory		= Factory::get_factory($post_id);
			switch( $column_name) 
			{				
				case 'IDs':
					echo "<br>
					<div class='ids'><span>ID</span>".$post_id. "</div>
					<p>
					<div class='button' add_bid='$post_id' post_type='".SMP_FACTORY."'>" . __("Double", "smc") . "</div>";
					break;			
				case 'owner':
					$owid	= $factory->get_post_meta('owner_id');
					$t 		= SMC_Location::get_instance($owid);
					echo $t->name . "<br><div class='ids'><span>ID</span>".$t->term_id . "</div>";
					break;
				case "dislocation": 
					$owid	= $factory->get_post_meta('dislocation_id');
					$t 		= SMC_Location::get_instance($owid);
					echo $t->name . "<br><div class='ids'><span>ID</span>".$t->term_id . "</div>";
					break;
				case "goods_type_id":
					$goods_type_id	= $factory->get_post_meta('goods_type_id');
					$ct				= (int)$factory->get_post_meta("components_type");
					echo Goods_Type::get_trumbnail($goods_type_id, 70) ."<div>". Goods_Type::get_components_form($goods_type_id, $ct)."</div>". "<br><div class='ids'><span>ID</span>".$goods_type_id . "</div>";
					break;
				case "max_powerfull":
					$max_powerfull		= $factory->get_post_meta('max_powerfull');
					if($max_powerfull<1)
					{
						$powerfull		= $factory->get_post_meta('powerfull');
						update_post_meta($post_id, 'max_powerfull', $powerfull);
						echo $powerfull;
						break;
					}
					
					echo $max_powerfull;
					break;
				case "powerfull":
					echo $factory->get_post_meta('powerfull');
					break;
				case "goods_mass":
					echo (int) $factory->get_post_meta( 'goods_mass');
					break;
				case "industry":
					$ind_meta	= $factory->get_post_meta( "industry");
					$ind 		= $ind_meta ? SMP_Industry::get_instance($ind_meta) : "";
					echo 		$ind_meta ? $ind->body->post_title : "";
					break;
				case "available_goods_types":
					$avs_ar		= $factory->get_post_meta("available_goods_types");
					$avs		= $avs_ar;
					foreach($avs as $av)
					{
						echo Goods_Type::get_trumbnail($av[0]['goods_type'], 25);
					}
					break;
			}		
		}
		
		static function change_modifier($new_modifier_id, $factory_id)
		{
			//modifiers
			$old_goods_type							= get_post_meta($factory_id, "goods_type_id", true);
			$old_modifier_id						= get_post_meta($factory_id, "modificator", true);
			$available_goods_types					= get_post_meta($factory_id, "available_goods_types", true);
			$new_goods_type							= get_post_meta($new_modifier_id, "goods_type_id", true);
			
			//change production goods type
			//search for exist in availbes goods types
			$foo = false;
			foreach($available_goods_types as $a)
			{
				if($a[0]['goods_type'] == $new_goods_type && $a[0]['goods_type'])
				{
					$foo = true;
					break;
				}
			}
			//if not exists in availbles and no modifiers - 
			if(!$foo && !$new_goods_type)
			{
				update_post_meta($factory_id,					"goods_type_id", $available_goods_types[0][0]['goods_type']);
			}
			else
			{
			
			}
			update_post_meta($old_modifier_id,					"is_blocked", false);
			update_post_meta($new_modifier_id,					"is_blocked", true);
			update_post_meta($factory_id,						"modificator", $new_modifier_id);
			
		}
		
		function get_sidebar($params)
		{
			global $post;
			$op = get_option(SMP);
			if( $post->ID == $op["new_mp_ID"])
			{
				//echo "YES!!<BR>";
			}
		}
		
		static function the_content($factory, $content, $before, $after)
		{
			Goods_type::get_global();
			$content = Factories::get_large_form($factory, 0);
			return $content;
		}
		
		
		function smc_location_type_meta($post)
		{
			return;
			?>
				<div class="brr">
					<table>
						<tr>
							<td>
								<input type="radio" name="is_production" id="radio1" class="css-checkbox"  value=0 <?php checked(get_post_meta($post->ID, "is_production", true), 0) ; ?>/> 
								<label for="radio1"><?php _e("No Production's properties","smp"); ?></label>
							</td>
						</tr>
						<tr>
							<td>
								<input type="radio" name="is_production" id="radio2" class="css-checkbox" value=1 <?php checked(get_post_meta($post->ID, "is_production", true), 1) ; ?>/> 
								<label for="radio2"><?php _e("Owner can to found a Production", "smp"); ?></label>
							</td>
						</tr>
						<tr>
							<td>
								<input type="radio" name="is_production" id="radio3" class="css-checkbox" value=2 <?php checked(get_post_meta($post->ID, "is_production", true), 2) ; ?>/> 
								<label for="radio3"><?php _e("Location is Production","smp"); ?></label>
							</td>
						</tr>
						<?php echo apply_filters("smp_production_type_lt_meta", $post->ID);?>
						<tr>
							<td>
								<input type="number" name="is_production" id="radio3" class="css-checkbox" value=2 <?php checked(get_post_meta($post->ID, "is_production", true), 2) ; ?>/> 
								<label for="radio3"><?php _e("Location is Production","smp"); ?></label>
							</td>
						</tr>
					</table>
				</div>
			<?php
		}
		
		function location_type_save_box_data ( $post_id ) 
		{	
			return;
			// проверяем, является ли запрос автосохранением
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// проверяем, права пользователя, может ли он редактировать записи
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;
			// теперь также проверим тип записи	
			update_post_meta($post_id, 'is_production', $_POST['is_production']);
			
			return $post_id;
		}
		function location_type_add_views_column($columns)
		{
			return $columns;
			$columns['is_production']					= __('Production', "smp");
			return $columns;
		}
		// заполняем колонку данными	
		function location_type_fill_views_column($column_name, $post_id) {
				
			return;
			switch( $column_name) 
			{				
				case 'is_production':
					$is_production = get_post_meta($post_id, 'is_production', 1) ;
					switch($is_production)
					{
						case 0:
							echo "";
							break;
						case 1:
							echo "<span title='". __("Owner can to found a Production", "smp")."'><img src='".SMP_URLPATH."/img/capitalist_picto.png'></span>";
							break;
						case 2:
							echo "<span title='". __("Location is Production", "smp")."'><img src='".SMP_URLPATH."/img/plant_picto.png'></span>";
							break;
					}
					break;				
			}		
		}
		
		//=====================================
		//
		//
		//
		//=====================================
		
		static function install()
		{
			$my_post = array(
								  'post_title'   		=> __("My Productions", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[my_production_page]"  ,
								  'post_status'  		=> 'publish',
								   'comment_status'		=> 'closed',
								);

			return	wp_insert_post( $my_post );
		}
		
		
		
		//====================================
		//
		//	Needed global $all_goods_types!!!!!!!! TODO: delete all_goods_types
		//	used by '/tpl/my_productions_shortcode.php'
		//	and by '/class/Factories.the_content()'
		//
		//====================================
		
		static function get_short_form($fid)
		{
			global $Soling_Metagame_Constructor;
			global $Factory;
			$owner			= get_term_by("id", get_post_meta($fid, "owner_id", true), SMC_LOCATION_NAME);
			$dislocation	= get_term_by("id", get_post_meta($fid, "dislocation_id", true), SMC_LOCATION_NAME);
			$goods_type		= get_post(get_post_meta($fid, "goods_type_id", true));
			$powerfull		= get_post_meta($fid, "powerfull", true);
			$count			= $Factory->get_count($fid, $goods_type->ID);
			$help			= 	__("Owner", "smp").":		".$owner->name."<br>".
								__("Dislocation", "smp").":	".$dislocation->name."<br>".
								__("Powerfull", "smp").":	". $powerfull. "<BR>".
								__("Produse", "smp").":	<b>". $goods_type->post_title. "</b><BR>".
								__("In edge of circle factory products ", "smp").": ". $count . " " .  __("unit", 'smp');
			$rr				.= "
								<li> 
									<a href='".get_permalink($fid)."'>
										<table  border='0' class='smp-factory-icon-title'>
											<tr>
												<td class='smp-factory-td'>
														<img src='".SMP_URLPATH."img/factory_icon.png'>
														<span class='smp-factory-table-count' style='background-color:#".get_post_meta($goods_type->ID,"color",true)."'>".$count."</span>
												</td>
											</tr>
											<tr>
												<td class='smp-factory-td'>
													<div>".
														get_the_post_thumbnail($goods_type->ID, array(55, 55)).
													"		
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class='smp-factory-td-title'>".$owner->name."</div>
												</td>
											</tr>
										</table>
									</a>
									<div class='smp-center'>".
										$Soling_Metagame_Constructor->assistants->get_hint_helper($help).
									"</div>
								</li>";
			return $rr;
		}
		static function get_little_picto($fid, $size=18, $gt_id = -1, $params = "------------", $compo=false)
		{
			global $Soling_Metagame_Constructor;
			global $Factory;
			$class			= '';
			$id				= '';
			$factory		= Factory::get_factory($fid);
			$owner_tid		= get_post_meta($fid, "owner_id", true);
			$owner			= SMC_Location::get_instance($owner_tid);
			$dislocation	= SMC_Location::get_instance(get_post_meta($fid, "dislocation_id", true));
			$ind_id			= get_post_meta($fid, 'industry', true); 
			$components_type= get_post_meta($fid, 'components_type', true); 
			$ind			= SMP_Industry::get_instance($ind_id);
			$ind_title		= $ind ? $ind->body->post_title : "";
			$ind_type		= $ind ? $ind->get("resourse_name") : "";
			if(is_array($params) )
			{
				if($params['id'])			$id		= " id='".$params['id']."' ";
				if($params['class'])		$class	= $params['class'];
				if($params['components'])	$compo	= $params['components'];
			}
			if($gt_id === -1)
			{
				$gt_id		= get_post_meta($fid, "goods_type_id", true);
			}
			
			$goods_type		= get_post($gt_id);
			$powerfull		= get_post_meta($fid, "powerfull", true);
			$count			= $Factory->get_count($fid, $goods_type->ID);
			$your_fac		= is_user_logged_in() && Factories::is_user_owner($owner->term_id, get_current_user_id());
			$your			= $your_fac ? 
				"<div class='smc-your-label' style='bottom:0; right:-1px;'>".__("It's your", "smc")."</div>": 
				"";
			$your2			= $your_fac	? 
				$Soling_Metagame_Constructor->assistants->get_short_your_label(15, 2) : 
				"";
			$help			= self::get_help($fid, true, true);
			
			return "
			<div $id factory_picto factory_id='" . $fid . "' href='" . get_permalink($fid) . "' class='div_hinter $class' data-hint='fact_help_".$fid."' style='position:relative; display:block; float:left; margin:0px;'>" . Goods_Type::get_trumbnail( $gt_id, $size, array("is_hint"=>false)).
			$your2 . 
			"</div>" .
			$help;
		}
		static function get_display_form($location_id)
		{
			$fids			= self::get_all_location_factories($location_id, true);
			foreach($fids as $fid)
			{
				$html		.= "<span style='float:left;'>".self::get_help($fid)."</span>";
			}
			return $html;
		}
		static function get_help($fid, $is_hidden=false, $show_components=false)
		{
			global $Factory;
			$factory		= Factory::get_factory($fid);			
			$owner_tid		= $factory->get_owner_id();
			$ind_id			= $factory->get_post_meta('industry'); 
			$ind			= SMP_Industry::get_instance($ind_id);
			$ind_title		= $ind ? $ind->body->post_title : "";
			$ind_type		= $ind ? $ind->get("resourse_name") : "";
			$owner			= SMC_Location::get_instance($owner_tid);
			$dislocation	= SMC_Location::get_instance($factory->get_post_meta("dislocation_id"));
			$gt_id			= $factory->get_post_meta("goods_type_id");
			$goods_type		= Goods_Type::get_instance($gt_id);
			$powerfull		= $factory->get_post_meta("powerfull");
			$count			= $factory->get_productivity();
			$components_type= get_post_meta($fid, 'components_type', true); 
			$your_fac		= is_user_logged_in() && $is_own;
			$your			= $your_fac ? 
				"<div class='smc-your-label' style='bottom:0;right:-10px;'>".__("It's your", "smc")."</div>": 
				"";
			$hide			= $is_hidden ? "lp-hide" : "";
			
			return apply_filters("smp_get_factory_little_picto_hint", "
			<div id='fact_help_".$fid."' style='' class='factory_help $hide'>
			<table width='480' style='width:460px; border:0;'>
				<tr>
					<td colspan='7'>
						<div style='font-size:17px; font-family:Open Sans; Arial, sans serif; font-weight:normal;text-transform: uppercase;'>" . $factory->get('post_title') . "</div>
					</td>
				</tr>
				<tr>
					<td rowspan='9' style='vertical-align:top; style=''>". 
						Goods_Type::get_trumbnail($gt_id, 80).
					"</td>
				</tr>
				<tr>  
					<td>
						<div style='text-align:right; margin:0 4px; display:block; color:#222!important;'>". 
							__("Industry", "smp").": 
						</div>
					</td>
					<td>
						<span>" . $ind_title . "</span>
					</td>
				</tr>
				<tr>  
					<td>
						<div style='text-align:right; margin:0 4px; display:block; color:#222!important;'>".  
							__("Produse", "smp").": 
						</div>
					</td>
					<td>
						<span>" . $ind_type . "</span>
					</td>
				</tr>
				<tr>
					<td>
						<div style='text-align:right; margin:0 4px; display:block; color:#222!important;'>".  
							__("Owner", "smp").": </div></td><td><span>" . $owner->name . "</span>
					</td>
				</tr>
				<tr>
					<td>
						<div style='text-align:right; margin:0 4px; display:block; color:#222!important;'>".
							__("Dislocation", "smp").": </div></td><td><span>".$dislocation->name."</span>
					</td>
				</tr>
				<tr>
					<td>
						<div style='text-align:right; margin:0 4px; display:block; color:#222!important;'>".
							__("Now produse", "smp").": </div></td><td><span><b>". $goods_type->post_title. "</b></span>
					</td>
				</tr>
				<tr>
					<td>
						<div style='text-align:right; margin:0 4px; display:block; color:#222!important;'>". 
							__("In edge of circle factory products ", "smp") .":
						</div>
					</td>
					<td>
						<span>" . $count . " " .  __("unit", 'smp') . "</span>
					</td>					
				</tr>
				<tr>
				<tr>
					<td colspan='2' valign='top'>".
							($show_components ? Goods_Type::get_components_form_2( $gt_id, $components_type, $count ) : "").
					"</td>
				</tr>
				<tr><td height='3'></tr></td>
			</table>
				$your
			</div>", $fid);
		}	
		
		static function get_pictogramm($fid, $div_param='')
		{
			global $Soling_Metagame_Constructor;
			global $Factory;
			$factory		= Factory::get_factory($fid);			
			$owner_tid		= $factory->get_owner_id();
			$ind_id			= $factory->get_post_meta('industry'); 
			$ind			= SMP_Industry::get_instance($ind_id);
			$ind_title		= $ind ? $ind->body->post_title : "";
			$ind_type		= $ind ? $ind->get("resourse_name") : "";
			$owner			= SMC_Location::get_instance($owner_tid);
			$dislocation	= SMC_Location::get_instance($factory->get_post_meta("dislocation_id"));
			$goods_type		= Goods_Type::get_instance($factory->get_post_meta("goods_type_id"));
			$powerfull		= $factory->get_post_meta("powerfull");
			$count			= $factory->get_productivity();
			$is_own			= Factories::is_user_owner($owner->term_id, get_current_user_id());
			$your_fac		= is_user_logged_in() && $is_own;
			$your			= $your_fac ? 
				"<div class='smc-your-label' style='bottom:0;right:-10px;'>".__("It's your", "smc")."</div>": 
				"";
			$your2			= $your_fac	? 
				$Soling_Metagame_Constructor->assistants->get_short_your_label(18, array(6, 2)) : 
				"";
			$help			= apply_filters("smp_get_factory_pictogramm_hint", "
			<div style='position:relative; display:block; width:370px;'>
				<div style='position:relative; display:inline-block; float:left; margin-right:12px; width:90px;; height:100px;'>". 
					Goods_Type::get_trumbnail($goods_type->ID, 80).
				"</div>
				<div style='text-align:left; color:black; padding-bottom:30px;'>".
					__("Industry", "smp").":	"  .$ind_title."<br>".
					__("Produse", "smp").":		" . $ind_type ."<br>".
					__("Owner", "smp").":		" . $owner->name."<br>".
					__("Dislocation", "smp").":	" . $dislocation->name."<br>".
					__("Produse", "smp").":	<b>"  . $goods_type->post_title. "</b><BR>".
					__("In edge of circle factory products ", "smp").": <b>". $count . "</b> " .  __("unit", 'smp')."<br>".
					Goods_Type::get_components_form_2( $goods_type->ID ).
				"</div>
				$your
			</div>", $fid);
			
			$ind_color			= $ind->get('color');
			$img				= $ind->get_picto();
			return "
			<div factory_picto class='factory_picto' id='factory_picto_".$fid."' href='#' factory_id='".$fid."' ".$div_param.">
				<div  border='0' class='smp-factory-icon-title'>".
						
					"<div class='smp_factory_pictogramm_icon' style='background-color:#".$ind_color.";'>
							$img					
					</div>
					<div class='smp_factory_pictogramm_discr'>".
							Goods_Type::get_trumbnail( $goods_type->ID, 25). 				
							
					"</div>
				</div>" . 
				$Soling_Metagame_Constructor->assistants->get_div_hint_helper($help, "help_picto_".$fid, "#000") .
				$your2.
			"</div>";
		}
		
		
		static function get_widget_form($factory)
		{
			global $Soling_Metagame_Constructor, $SolingMetagameProduction, $user_iface_color, $all_goods_types, $GoodsType, $Factory;
			$fid				= $factory->ID;
			$fact				= Factory::get_factory($factory->ID);//new Factory($factory->ID);
			$productivity		= $fact->get_productivity();
			$max_powerfull		= $fact->get_max_productivity();
			$cur_goods_id		= $fact->get_goods_type();
			$cur_goods			= get_post($cur_goods_id);
			$complexity			= get_post_meta($cur_goods_id, "complexity", true);	
			$components			= get_post_meta($cur_goods_id, "components", true);	
			$quality			= get_post_meta($fid, "quality", true);
			$industry_id		= get_post_meta($fid, "industry", true); 
			$fill				= get_post_meta($cur_goods_id, "color", true);
			if(!$fill)	$fill = "333333";
			$industry			= SMP_Industry::get_instance($industry_id);			
			
			//equipment
			$player_change_aquipment= (int)$SolingMetagameProduction->options['player_change_aquipment'];
			$modificator 			= get_post_meta($fid, 'modificator', true);
			$gt_id					= get_post_meta($modificator, "goods_type_id", true);
			$fmid					= get_post_meta($modificator, "factory_id", true);
			$factory_				= get_post($fmid);
			$pn						= (isset($fmid)) ?  $factory_->post_title : __("Not present", "smp");
			$gt_q					= $SolingMetagameProduction->get_gb_value("quality") ? get_post_meta($modificator, "quality", true): 100;
			$m_best_before			= get_post_meta($modificator, "best_before", true);
			if($m_best_before > MAX_BEST_BEFORE || $SolingMetagameProduction->get_gb_value("best_before"))  
				$m_best_before = __("infinitely", "smp");
			$quality_multiplier 	= get_post_meta($gt_id, "quality_multiplier", true);
			$powerfull_multiplier	= get_post_meta($gt_id, "powerfull_multiplier", true);
			$gt_clr					= get_post_meta($gt_id, "color", true);
			$goods_type_equipment	= get_post_meta($gt_id, "goods_type_equipment", true);	
			$gt_i					= get_post_meta($goods_type_equipment, "industry", true);
			$right_industry			= $gt_i == $industry_id;		
			$not_mod			= "<div id='foreign_modifier'><BR><p style='padding:0px 30px!important; line-height:35px;'>" . __("No modifiers present", "smp") . "</p><BR></div>";
			
			$owner_tid			= get_post_meta($fid, "owner_id", true);
			$owner				= SMC_Location::get_instance($owner_tid);
			$owner_type			= $Soling_Metagame_Constructor->get_location_type($owner_tid);
			$powerfull			= get_post_meta($fid, "powerfull", true);
			$availables			= get_post_meta($fid, "available_goods_types", true);
			if($right_industry)
			{
				$availables[] 	= array(array("goods_type"=>$goods_type_equipment));
			}
			$dis_id				= get_post_meta($fid, "dislocation_id", true);
			$location			= SMC_Location::get_instance( $dis_id );						
			$loc_id				= $location->term_id;							
			$loc_type			= $Soling_Metagame_Constructor->get_location_type($loc_id);
			$is_productived		= '';
			$needs				= $fact->get_all_needs();
			$need_list			= '';
			foreach($needs as $need)
			{
				$is_productived .= "<div class=smp_gb_rounded>".
										Goods_Type::get_trumbnail( $need[0]['goods_type'], 32, array('class'=>'smp_good_type_picto_rnd', 'margin'=>-5)).
										"<div class=smp_gb_rnd_val>" . $need[0]['goods_value'] . "</div>".
									"</div>";
				$need_single	= get_post($need[0]['goods_type']);
				$need_list		.= "<div>" . $need_single->post_title.": " . $need[0]['goods_value'] . " ". __('unit','smp')."</div>";
			}
			if(count($needs)) 
			{
				$is_productived = "
				<div class='needs_cont div_hinter' data-hint='need_help_" . $fid. "'>".
					$is_productived .
				"</div>
				<div id='need_help_" . $fid . "' class='lp-hide' style='height:75px;'>".
					__("To produce the goods you need","smp") . $need_list.			
				"</div>";
			}
			$your			= is_user_logged_in() && Factories::is_user_owner($owner_tid, get_current_user_id());
			// Produse current goods type
			
			// quality
			$p_help			= '<h2>'.__("Produse", "smp") . '</h2><div style="width:360px; display:inline-block; position:relative; ">'.
				__("This parameter characterizes the quality of the products. Optional equipment allows you to change it.", "smp").
			'</div>';
			$product		= "
			<div id='factory_widget_form".$fid."' titled='".$factory->post_title."' style='display:block; width:100%;'>
				<div id='gt_".$fid."' style='position:relative; display:block; width:100%;'>
					<div class='klapan3_subtitle lp_clapan_raze'>" . 
						$Soling_Metagame_Constructor->assistants->get_div_hint_helper($p_help, "p_help") .  
						__("Produse", "smp").
					"	<div class=lp_clapan_raze1></div>
					</div>
				</div>" .
				
			"</div>
			
			
			
			<div style='display:block; min-height:80px;'>	
				<div id='factory_chf' style='background-color:#".$fill."; padding:2px; color:white; position:relative; display:block; height:90px;'>
					<div class='factory_widg_current_goods_type' style='float:left;'>".
						Goods_Type::get_trumbnail( $cur_goods_id, 70).
					"</div>
					<div style='font-size:14px;'>".
						$cur_goods->post_title." (". $productivity.")". 	
				"	</div>";
			if($your)
			{
				$product .= $Factory->get_change_form($factory->ID, $productivity, $max_powerfull);
			}		
			$product 	.="</div>";
			$description			= get_post_meta($cur_goods_id, "description", true);
			if($description)
			{
				$product 	.=
				"	<div class='smp_widg_factory_gt_descr'>".
						$description.
				"	</div>". 
						$is_productived;
			}		
			$product 	.="</div>";
			
			$gbs			= SMP_Assistants::get_all_batchs_location($dis_id, $owner_tid);
			
			// quality
			$q_help			= 
			'<div style="width:360px; display:inline-block; position:relative; "><h2>' . __( "Quality", "smp" ) . '</h2>'.
				__("This parameter characterizes the quality of the products. Optional equipment allows you to change it.", "smp").
			'</div>';
			//
			$pow_help		= 
			'<div style="width:360px; display:inline-block; position:relative; "><h2>' . __( "Powerfull", "smp" ) . '</h2>'.
				__("This parameter characterizes the size of the enterprise. The size of the issue also depends on nominklatury products (stationery buttons will be released more than a car, get it?). Change this setting can install additional equipment (see section Equipment)", "smp").
			'</div>';
			//
			
			$lst				= $industry->get_goods_array(3);
			$list				= "";
			foreach($lst as $l)
			{
				$list			.= "<li>".$l->post_title."</li>";
			}
			$industry_help		= 
			'<div style="width:360px; display:inline-block; position:relative; "><h2>' . __( "Industry", "smp" ) .  '</h2>'.
				__("Here is a partial list of goods in this category:", "smp") . "<ul>" . $list . "</ul>" . __("The entire list of available goods can be found at Store", "smp").
			'</div>';
			$params			.= "
			<div class=''>		
				<div class='klapan3_subtitle lp_clapan_raze'>".
					$Soling_Metagame_Constructor->assistants->get_div_hint_helper($q_help, "q_help") .  
					__("Quality", "smp") . 
				'<div class=lp_clapan_raze1></div>
				</div>
				<div class="smp_bevel_form">'.SMP_Assistants::get_quality_diagramm($fact->get_quality(),360, 22)."</div>
			</div>
			<div class='fac_block'>		
				<div class='klapan3_subtitle'>".
					$Soling_Metagame_Constructor->assistants->get_div_hint_helper($pow_help, "pow_help") .  
					__("Powerfull", "smp") . 
				'</div>
				<div class="smp_bevel_form"><b>' . get_post_meta($fid, "powerfull", true) ."</b> ". __(" power unit(s)","smp")."</div>
			</div>
			<div class='fac_block'>		
				<div class='klapan3_subtitle'>".
					$Soling_Metagame_Constructor->assistants->get_div_hint_helper($industry_help, "industry_help") .  
					__("Industry", "smp") . 
				'</div>
				<div class="smp_bevel_form">' . $industry->body->post_title . "</div>
			</div>";
			
			// modifier
			/**/
			$q_modifier		= '<div style="width:360px;"><h2>' . __( "Modifier", "smp" ) . '</h2>'.
				__("Special items, which allows to change the parameters of its products. Modifications are subject to quality, volume, expiration date, and other parameters. To replace the equipment necessary to correct a batch of goods placed in the current location. After that, select it in the window.", "smp").
			'</div>';
			
			$mod_block		= "
			<div class=''>		
				<div class='klapan3_subtitle lp_clapan_raze'>".
					$Soling_Metagame_Constructor->assistants->get_div_hint_helper($q_modifier, "q_modifier") .  
					__("Modifier", "smp") . $player_change_aquipment.
				'	<div class=lp_clapan_raze1></div>
				</div>
			</div>';
			
			if(count($gbs) > 0 )
			{
				if($your && $player_change_aquipment)
				{
					$mod_block	.= 
					'<div id="select_modifier"></div>
					<div id="select_modifier_modifier" class="black_button_2" factory_id="'.$fid.'" >' . __("Update").'</div>';
					
				}
				else
				{
										
						$gt_p					= get_post($gt_id);
						$img					= get_the_post_thumbnail( $gt_id, array(110, 110), array("class"=>"null_border"));
						if(!$img)		$img	= "<div class='simply_div lp-floater' style='width:160px; '></div>";
						if($goods_type_equipment > 0)
						{
							$gte				= get_post($goods_type_equipment );
							$gtt				= __("add Goods type", "smp") . "<span> " . $gte->post_title . "</span>";
						}
						else
						{
							$gtt				= __("add Goods type", "smp") . "<span> " . __("Not present", "smp") . "</span>";
						}
						
						$mod_block	.= $gt_id > 0 ? "<div id='foreign_modifier' style='background:#$gt_clr;'>
							<div style='display:inline-block; position:relative; margin-right:10px; float:left;'>".
								$img . 
							"</div>
							<ttl>" . $gt_p->post_title . "</ttl>".
							($SolingMetagameProduction->get_gb_value("factory_id") ? "<p>" . __("Produce by: ", "smp") . '<span>' . $pn .'</span></p>' : "").
							($SolingMetagameProduction->get_gb_value("quality") ?  '<p>' . __("add Quality", "smp")  . '<span>' . ($quality_multiplier * $gt_q/100)   . '%</span></p>' : "").
							 '<p>' . __("add Powerfull", "smp"). '<span>' . ($powerfull_multiplier * $gt_q/100) . '%</span></p>'.
							($SolingMetagameProduction->get_gb_value("best_before") ?  '<p>' . __("Best before", "smp")  . '<span>' . $m_best_before . '</span></p>' : "").
							 '<p>' . $gtt . '</p>
						</div>': $not_mod;
						
				}
			}
			else
			{
				$mod_block		.= $not_mod;				
			}
			
			
			$storage = "";
				// change goods type
			
			if(is_array($availables) && count($availables))
			{
				$product			.= "<div id='av_gt_".$fid."' class='fac_block '>";
				$q_change			= '<div style="width:360px;"><h2>'. __("Changing the range of products","smp") ."</h3>".
					__("At the end of the economic cycle will attempt to make the selected item in this section. Range of products can be extended to build in the necessary equipment (see section 'Equipment')","smp")."</div>";
				$product			.= count($availables) ? "<div class='klapan3_subtitle'>".$Soling_Metagame_Constructor->assistants->get_div_hint_helper($q_change, "q_change") . __($your ? "Change Production Goods type" : "The nomenclature of manufacture", "smp")."</div>" : "";
				$product			.= "<div class='smp_bevel_form'>";
				foreach($availables as $ava)
				{
					$gtid 		= $ava[0]["goods_type"];
					$scgt		= $gtid == $cur_goods_id ? "sel_cgt" : "";
					$scgt1		= $gtid == $cur_goods_id ? "smc_3d_button_release" : "smc_3d_button";
					$scgt1		= $your ? $scgt1 : "lp-floater-simple" ;
					$change_params	= $your ? "change_factory_gt='".$gtid. "' factory_id='".$fid."'" : "";
					$product	.= "<div $change_params class='$scgt1'><div>" . Goods_Type::get_trumbnail( $gtid, 23, array('style'=>'margin:0!important;', 'is_hint'=>true)) . "</div></div>";
					
				}			
				$product		.= "</div></div>";
			}
			
			
			
			
				
				
			//storage
			
			$storage		= "<div id='storage_".$fid."'  class=''>";
			$count			= count($gbs);
			$storage		.= "<div class='klapan3_subtitle lp_clapan_raze'>".__("Storage products", 'smp'). "	<div class=lp_clapan_raze1></div></div>";
			$storage		.= '
			<div class="smp_bevel_form">
				<div id="storage_wig" style="" class="storage_main">';
			/*
			$storage		.= count($gbs) < 1 ? "<div class='smp_comment_widget'>".__("No Goods of this type", "smp")."</div>" : "";
			foreach($gbs as $gb)
			{
				$storage		.= $GoodsBatch->get_widget_picto($gb);
			}
			*/
			$storage		.= $SolingMetagameProduction->draw_storage($gbs, 0, STORAGE_PICE, 0, ceil($count/STORAGE_PICE), $fid, SMP_FACTORY);
			
			$storage		.= "</div></div></div>";				
			
			
			
			//modifiers		
			$els				= array( 
											array( 
												"text"				=> __("There are no modifier select", "smp"),
												"value"				=> "-1",
												"selected"			=> $modificator == "" ,
												"description"		=> __("add Quality", "smp") .' <span>0%</span><BR> '.__("add Powerfull", "smp"). ' <span>0%</span>',
												"imageSrc"			=> "",						
												)
										);
			$gbs2				= SMP_Assistants::get_all_batchs_location($dis_id, $owner_tid,-1,0,false);						
			foreach($gbs2 as $qm)
			{
				$batch					= SMP_Goods_Batch::get_instance($qm);
				$owner_id				= $batch->get_owner_id();
				$is_block 				= $batch->get_meta("is_blocked" ) ;
				$gt						= $batch->get_meta("goods_type_id");
				$quality_multiplier 	= get_post_meta($gt, "quality_multiplier", true);
				$powerfull_multiplier	= get_post_meta($gt, "powerfull_multiplier", true);
				$goods_type_equipment	= get_post_meta($gt, "goods_type_equipment", true);
				if(
					$owner_id != $owner_tid &&
					( $is_block == true && $modificator != $qm) &&
					$quality_multiplier == 0 	&&
					$powerfull_multiplier == 0 	&&
					$goods_type_equipment == ''
				   )
				{
					continue;
				}
				$quality				= $SolingMetagameProduction->get_gb_value("quality") ? get_post_meta($qm, "quality", true) : 100;
				$factory_id				= get_post_meta($qm, "factory_id", true);
				$g_best_before			= get_post_meta($qm, "best_before", true);
				$g_best_before_d		= ($g_best_before > MAX_BEST_BEFORE) ? __("infinitely", "smp") : $g_best_before . " " . __("circles", "smp");
				$factory_				= get_post($factory_id);
				$pn						= (isset($factory_id)) ?  $factory_->post_title : __("Not present", "smp");
				if($goods_type_equipment >0)
				{
					$gte				= get_post($goods_type_equipment);
					$gtt				= __("add Goods type", "smp") . "<span>: " . $gte->post_title . "</span>";
				}				
				else
				{
					$gtt				= __("add Goods type", "smp") . "<span>: " . __("Not present", "smp"). "</span>";
				}
				$goodst		= get_post($gt);
				$tru_id		= get_post_thumbnail_id($gt);
				$img		= wp_get_attachment_image_src($tru_id);
				$img		= (img) ? $img[0] : "";
				$is_rja		= $g_best_before > 0 || !$SolingMetagameProduction->get_gb_value("best_before");
				
				$img		= $is_rja ? $img : SMP_URLPATH . "img/rja_120_120.jpg";
				$els[]		= array( 
											"text"				=> $goodst->post_title ,
											"value"				=> $qm,
											"selected"			=> $modificator == $qm,
											"description"		=> $is_rja ?
																	($SolingMetagameProduction->get_gb_value("factory_id") ? __("Produce by: ", "smp") . ' <span>' . $pn .'</span><br>' : "") . 
																	($SolingMetagameProduction->get_gb_value("quality") ? __("add Quality", "smp") . ' <span>'. ($quality_multiplier * $quality/100) . '%</span><BR> ' : "") .
																	__("add Powerfull", "smp"). ' <span>' . ($powerfull_multiplier * $quality/100) . '%</span><br>' .
																	($SolingMetagameProduction->get_gb_value("best_before") ? __("Best before", "smp")  . ' <span>' . $g_best_before_d . '</span><br>' : ""). 
																	$gtt : __("Trash", "smp") . ' <span>' . $g_best_before . " " . __("circles", "smp") . '</span><br>',
											"imageSrc"			=> $img,						
									);
			}		
			
			
			$html			.= "</div>";
			$own			= $your ? "<div class=smc-your-label style='right:0px;'>".__("It's your", "smc")."</div>" : "<div class='smc-notyour-label' style='right:0px;'>" . __("It is not your", "smp") . "</div>" . Assistants::get_call_to_owner_hinter($owner_tid, "About factory ".$factory->post_title);
			$html			.= "</div>";
			$ind_pic		= wp_get_attachment_image_src( $industry->get("_thumbnail_id"), array(50, 50) );
			//$pic			= "<div id='factory_pic' style=''><img src=".$ind_pic[0]."></div>";
			
			$owners			= SMC_Location::owners_form($owner_tid, $your, $owner->name);
			
			return array(
								"html"		=> $own . $pic . Assistants::get_switcher(
												array(																										
															array("title" => "<img src=".SMC_URLPATH."img/buy.png>", 		"slide" => $product, 	'hint' => __("Produse", "smp")),
															array("title" => "<img src=".SMC_URLPATH."img/info.png>", 		"slide" => $params, 	'hint' => __("Parameters", "smp")),																										
															array("title" => "<img src=".SMC_URLPATH."img/equipment.png>", 	"slide" => $mod_block, 	'hint' => __("Modifier", "smp")),																										
															array("title" => "<img src=".SMC_URLPATH."img/vehicle.png>", 	"slide" => $storage, 	'hint' => __("Storage", "smp")),																										
															array("title" => "<img src=".SMC_URLPATH."img/user.png>", 		"slide" => $owners, 	'hint' => __("Owners", "smc")),																										
													 ), "factory_widget_"
												),
								"its_your"	=> $your, 
								"elements"	=> $els
						 );
		
		}
		function get_change_form($factory_id, $productivity, $max_powerfull)
		{
			return apply_filters(
				"smp_factory_productiovity_slider", 
				"<div style='width:100%;' id='factory_change_form'>
					<div style='width:170px; margin:8px 0;  float:left;'>
						<input id='factoty_productivity' value='". $productivity."' max='". $max_powerfull."' style='width:100%;'/>
					</div>	
					<right fid='$factory_id' id='ch_productivity' class='black_button_2' style='margin:11px 0 0 11px;'>".
						__("Change", "smp").
					"</right>
				</div>", 
				$factory_id
			);
		}
		
		
		static function get_large_form($factory, $i=0)
		{
			global $Soling_Metagame_Constructor;
			global $user_iface_color;
			global $all_goods_types;
			$fid				= $factory->ID;
			$fact				= Factory::get_factory($fid);
			$is_owner			= $fact->is_user_owner();
			$cur_goods_id		= get_post_meta($fid, "goods_type_id", true);
			$cur_goods			= get_post($cur_goods_id);
			$complexity			= get_post_meta($cur_goods_id, "complexity", true);	
			$ind_id				= get_post_meta($fid, 'industry', true); 
			$ind				= SMP_Industry::get_instance($ind_id);
			$ind_title			= $ind ? $ind->body->post_title : "";
			
			$quality			= get_post_meta($fid, "quality", true);
			
			$html		= '';
			
			$owner_tid	= get_post_meta($fid, "owner_id", true);
			$owner	= SMC_Location::get_instance($owner_tid);	
			if(! $owner)
				return "Factory is haven't Owner!!! Call to Master";//$owner->get_error_message();
			
			$ctid		= $fact->get_currency_type();
			$ct			= SMP_Currency_Type::get_instance($ctid);
			$price2		= $ct ? $ct->get_price($cost): 0;
			$owner_type	= $Soling_Metagame_Constructor->get_location_type($owner_tid);
			$powerfull	= $fact->get_powerfull();
			$max_powerfull	= $fact->get_max_powerfull();
			$cost		= get_post_meta($fid, "cost_price", true);
			$location	= SMC_Location::get_instance(get_post_meta($fid, "dislocation_id", true));						
			$loc_id		= $location->term_id;							
			$loc_type	= $Soling_Metagame_Constructor->get_location_type($loc_id);	
			
			//$price		= SMP_Currency::get_currency_type(get_post_meta($cur_goods_id, "price", true), -1);
			
			$price		= apply_filters("smp_factory_price_production", array("summae" => 1, "abbreviation" => __("UE", "smp") ), $fid);//get_post_meta($cur_goods_id, "price", true);		
				
			$floor		= $complexity!=0 ? floor ($powerfull/ $complexity) : $powerfull;
			$grs		= $fact->get_post_meta("available_goods_types");
			if($is_owner)
			{
				$powerfull_slider = '<input type="range" name="change_factory_power_" value="'.$powerfull.'" max="'.$max_powerfull.'" fid="'.$fid.'" style="width:200px; vertical-align: middle;"/>';
				$change_pr	=  '<tr>
							<td class="smp-table1">'.
								__("Change", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper("<h2>".__("Changing the range of products", "smp")."</h2>".__("At the end of the economic cycle will attempt to make the selected item in this section. Range of products can be extended to build in the necessary equipment (see section 'Equipment')", "smp")).
							'</td>
							<td>
								<label class="smc_input">'.
									"<select id='change_goods_$fid' class='change_goods' fid='$fid'>";
				foreach($grs as $gr)
				{
					$gt_id	= $gr[0]['goods_type'];
					$gt		= Goods_Type::get_instance($gt_id);
					$change_pr	.= "<option value='$gt_id' ".selected($gt_id, $cur_goods->ID, false).">" . $gt->post_title . "</option>";
				}
				$change_pr	.= '	</select>						
								</label>
							</td>
						</tr>';
			}
			
			
			
			$html		.= '
					<table class="smp-pr-cur-good">
						<tr>
							<tr colspan="3">
								<h3>' . __("Parameters", "smp") . '</h3>
							</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Industry", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Industry", "smp")). 
							'</td>
							<td>'.
								"<a class='smp-colorized' href='". SMC_Location::get_term_link($owner_tid)."'>" .$ind_title."</a>".	
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Owner", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Factory belongs to some Location. Owners of Location (for example - Firm) controlled and profit by tham.All maded Production created in this.", "smp")). 
							'</td>
							<td>'.
								$owner_type->picto. " <span class='smp-location_type_name'>" .$owner_type->post_title. "</span> <a class='smp-colorized' href='". SMC_Location::get_term_link($owner_tid)."'>" .$owner->name."</a>".	
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Dislocation", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Factory must disposition somewhere.", "smp")). 
							'</td>
							<td>'.
								$loc_type->picto. " <span class='smp-location_type_name'>" .$loc_type->post_title. "</span> <a class='smp-colorized' href='". SMC_Location::get_term_link($owner_tid)."'>" .$location->name."</a>".
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Quality", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Who not understand?", "smp")).
							'</td>
							<td>'.
								SMP_Assistants::get_quality_diagramm($quality). 
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">' .
								__("Powerfull", "smp") .
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Every time (Circle) Factory created a Production. Count of that characterized by this parameter. It may be more than 100%. If Powerfull  less of  Goods complexity, produce failed..", "smp"), 140).
							'</td>
							<td>'.
								'<div class="smp_power_cnnt">
									<div style="float: left; display: inline-block; position: relative; width: 280px;">
										<span class="smp_power_label" id="smp_power_label'.$fid.'" >'.$powerfull.'</span>'.
										$powerfull_slider.
									'</div>
								</div>'.
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Amortization", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("The amount to be withdrawn from the account of the owner of Factory by each production cycle", "smp"), 140).
							'</td>
							<td>'.
								'<span class="smp_power_cnnt">' .$price2."</span> " .
							'</td>
						</tr>
						<tr>
							<td colspan="3">
								<hr/>
								<h3>'.__("Current Good Type", "smp"). '</h3>
							</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Produse", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Good type of production now.", "smp")).
							'</td>
							<td>'.
								'<a class="smp-colorized smp-goods-2" href="'.get_permalink($cur_goods->ID).'">'.$cur_goods->post_title.'</a>'.
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Cost Price", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Count of money that must applied evety time(Circle) for produse unit of the goods.", "smp"), 120).
							'</td>
							<td>'.
								'<span class="smp_power_cnnt">' . $price['summae']  . '</span>'. " " . $price['abbreviation'].
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Produse Complexity", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Part of Factory Powerfull, that necessary to create goods. Cann't more tham 100%.", "smp")).
							'</td>
							<td>'.
								'<span class="smp_power_cnnt">' . $complexity	."</span> " . __("power units", "smp") .
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Count", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("Count of units of goods, which created in this time(Circle).", "smp")).
							'</td>
							<td>'.
								'<span class="smp_power_cnnt smp_power_label" id="prod'.$fid.'">' . $floor."</span> <span class='smp-question'>".__("unit", "smp")."</span>".
							'</td>
						</tr>
						<tr>
							<td class="smp-table1">'.
								__("Technology type", "smp").
							'</td>
							<td class="smp-tabl2">'.
								$Soling_Metagame_Constructor->assistants->get_hint_helper(__("This is a help", "smp")).	
							'</td>
							<td>'.
								Goods_Type::get_trumbnail($cur_goods->ID, 80) ."<div>". Goods_Type::get_components_form($cur_goods->ID, (int)get_post_meta($fid, "components_type", true))."</div>".
							'</td>
						</tr>'.
						$change_pr.							
					'</table>';
			
			$html		.= '	
					<div class="smp-pr-cur-good">
						<span class="smp-table1">'.
							
						'</span>
						
					</div>
				<hr/>';
			
			return $html;
		}
		static function get_srorage_form($fid)
		{
			$factory		= Factory::get_factory($fid);
			$owner_tid		= $factory->get_post_meta( "owner_id");
			$dislocation_id	= $factory->get_post_meta( "dislocation_id");
			$args			= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'title',
										'order'     	=> 'ASC',
										'post_type' 	=> GOODS_BATCH_NAME,
										'post_status' 	=> 'publish',
										'meta_query'	=> array(
																	'relation'			=> 'AND',
																	array(
																			"key"		=> "owner_id",
																			"value"		=> $owner_tid,
																			'compare'	=> "="
																		 ),
																	array(
																			"key"		=> "dislocation_id",
																			"value"		=> $dislocation_id,
																			'compare'	=> "="
																		 )
																),	
									);
			$goods_batchs	= get_posts($args);
			$html			= '
			<div id="my_storage_'.$fid.'" style="display:block;">
				<h3>'.
					 __("Storage", "smp").'
				</h3>
				<div>';
			if(count($goods_batchs) < 1 )
			{
				$html		.= "<div class=smp-comment >".__("No Goods of this type", "smp")."</div>";
			}
			foreach($goods_batchs as $batch)
			{
				$gb			= SMP_Goods_Batch::get_instance($batch->ID);
				if($gb->user_is_owner())
					$post_data			= 1;
				else		
					$post_data			= -1;
				$html					.= !$gb->get_meta("is_blocked") ? $gb->get_stroke($batch, $post_data) : $gb->get_stroke($batch, -2);
			}
			$html						.= '
				</div>
			</div>';
			return $html;
		}
		
		public function user_is_owner($location_id, $_user_id)
		{
			return Factories::is_user_owner($location_id, $_user_id);
		}
		
		static function is_user_owner($location_id, $_user_id)
		{
			//if($user_id)	null;
			$d				= SMC_Location::get_term_meta( $location_id );//get_option( "taxonomy_$location_id");		
			if($d['owner1'] == $_user_id)
				return true;
			if($d['owner2'] == $_user_id)
				return true;
			if($d['owner3'] == $_user_id)
				return true;
				
			return false;
		}
		/*
			choice - выбирать по положению или по владельцу (для виджетов Панели)
		*/
		static function get_all_location_factories($location_id, $is_children=false, $count=-1, $choice=0)
		{
			//$loc_meta		= get_option("taxonomy_$location_id");
			if($choice)
			{
				$tm			= SMC_Location::get_term_meta($location_id);
				$lockey		= get_post_meta($tm['location_type'], "show_children_type", true)	? "owner_id" : "dislocation_id";				
			}
			else
			{
				$lockey		= "dislocation_id";
			}
			if($is_children)
			{
				$locs		= get_term_children($location_id, SMC_LOCATION_NAME);
				$locs[]		= $location_id;
			}
			else
			{
				$locs		= array( $location_id);
			}
			
			$args			= array(
										'numberposts'	=> $count,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'post_type'		=> SMP_FACTORY,
										'meta_query'	=> array(
																	array(
																			'key'		=> $lockey,
																			'value'		=> $locs,
																			'operator'	=> 'OR'
																		  )
																),
									);
			$factories		= get_posts($args);
			return $factories;
		}
		static function get_all_factories()
		{
			$args			= array(
										'numberposts'	=> -1,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'orderby'  		=> 'title',
										'order'     	=> 'ASC',
										'post_type'		=> SMP_FACTORY,
									);
			$hubs			= get_posts($args);
			return $hubs;
		}
		static function wp_dropdown_factories($params)
		{
			$args		= array(
									"numberposts"		=> 100,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> SMP_FACTORY,
									'post_status' 		=> 'publish',									
								);
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			if(isset($params['multile']))
				$html	.= 'multiple ';
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;		
		}
		/*
			$meta = array($key => $value);
		*/
		static function get_factories($meta=-1)
		{
			if(!is_array($meta))	
				$meta		= array();
			$meta_query1	= array();
			foreach($meta as $m=>$val)
			{
				$meta_query1_1	= array(
					"key"	=> $m,
					"value" => $val
				);
				if(is_array($val))
					$meta_query1_1['operator'] = "OR";
				$meta_query1[]	= $meta_query1_1;
			}
			$args			= array(
										'numberposts'	=> -1,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'post_type'		=> SMP_FACTORY
									);
			if(count($meta_query1))
			{
				$meta_query1[]		= array( 'relation'	=> 'AND');
				$args['meta_query']	= $meta_query1;
			}
			//insertLog("Factory.get_factories", $args);
			$fact				= get_posts($args);
			return $fact;
		}
		static function get_all_user_factories($user_id=-1)
		{
			global $Soling_Metagame_Constructor;
			if($user_id == -1)	$user_id	= get_current_user_id();
			$all_locs		= $Soling_Metagame_Constructor->all_user_locations($user_id);
			$args			= array(
										'numberposts'	=> -1,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'post_type'		=> SMP_FACTORY,
										'meta_query'	=> array(
																	array(
																			'key'		=> 'owner_id',
																			'value'		=> $all_locs,
																			'operator'	=> 'OR'
																		  )
																),
									);
			$fact			= get_posts($args);
			return $fact;
		}
		function smc_dot_content($text, $loc_id)
		{
			$all_facs	= self::get_all_location_factories($loc_id, true);
			$html		= "<div style='max-width:120px;'>";
			foreach($all_facs as $fac)
			{
				
				$html	.= $this->get_little_picto($fac , 14);
			}
			$html		.="</div>";
			return $text. $html;
		}
		
		static function get_produse_by_location($location_id, $choice=1)
		{
			$facs			= self::get_all_location_factories($location_id, true, -1, $choice);
			$factories		= array();
			$unproduce		= array();
			foreach($facs as $fac)
			{
				$factory	= Factory::get_factory($fac);
				$needs		= $factory->get_all_unavailbe();
				if(count($needs)==0)
				{
					$factories[] = array("factory_id" => $fac, "goods_type_id" => $factory->get_goods_type());
				}
				else
				{
					$unproduce[] = array("factory_id" => $fac, "goods_type_id" => $factory->get_goods_type());
				}
				//
				$not_produce_components = $factory->get_not_produce_components();
				$npc			= array();
				foreach($not_produce_components as $np)
				{
					$npc[] 		= array("factory_id" => $fac, "goods_type_id" =>$np);
				}
				$unproduce		= array_merge((array)$npc,(array)$unproduce );
			}
			return array("produce"=>$factories, "unproduce"=>$unproduce);
		}
		static function get_produse_by_goods_type($goods_type_id)
		{
			$args			= array(
										'numberposts'	=> $count,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'post_type'		=> SMP_FACTORY,
										'meta_query'	=> array(
																	array(
																			'key'		=> "goods_type_id",
																			'value'		=> $goods_type_id
																		  )
																),
									);
			$facs			= get_posts($args);
			$produce		= 0;
			$unproduce		= 0;
			foreach($facs as $fac)
			{
				$factory	= Factory::get_factory($fac);
				$count		= $factory->get_productivity();
				$produce 	+= $count;
				$needs		= $factory->get_all_needs();
				if(count($needs)==0)
				{
					$unproduce 	+= $count;
				}
			}
			return array("produce"=>$produce, "unproduce"=>$unproduce);
		}
		
	}
?>