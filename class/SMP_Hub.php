<?php 
	class SMP_Hub extends SMC_Post
	{
		
		
		static function wp_dropdown_hubs($params)
		{
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'smp_hub',
									'post_status' 		=> 'publish',									
								);
			$hubs		= get_posts($args);
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		static function install()
		{
			$my_post = array(
								  'post_title'   		=> __("My Logistics", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smp_my_hubs]"  ,
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);

			return wp_insert_post( $my_post ); 
		}
		static function is_user_owner($id, $user_id=-1)
		{
			global $Soling_Metagame_Constructor;
			if($user_id==-1)
				$user_id=get_current_user_id();
			$owner_loc_id	= get_post_meta($id, "owner_id", true);
			return $Soling_Metagame_Constructor->user_is_owner($owner_loc_id, $user_id);
		}
		static function get_all_hubs_by_dislocation($location_id, $is_children=false)
		{
			//$loc_meta		= get_option("taxonomy_$location_id");
			if($is_children)
			{
				$locs		= get_term_children($location_id, 'location');
				$locs[]		= $location_id;
			}
			else
			{
				$locs		= array( $location_id);
			}
			
			$args			= array(
										'numberposts'	=> -1,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'post_type'		=> 'smp_hub',
										'meta_query'	=> array(
																	array(
																			'key'		=> 'dislocation_id',
																			'value'		=> $locs,
																			'operator'	=> 'OR'
																		  )
																),
									);
			$hubs			= get_posts($args);
			return $hubs;
		}
		static function get_all_rouths_by_location($location_id, $is_children=false)
		{
			$hubs			= self::get_all_hubs_by_dislocation($location_id, $is_children);
			
			$all_routha		= SMP_Routh::get_all_rouths("ids");
			$routhss		= array();
			foreach($all_routha as $all_routh)
			{
				$routh		= SMP_Routh::get_instance($all_routh);
				$term_meta	= SMP_Routh::get_term_meta($all_routh);
				
				if( in_array( $term_meta['start_hub_id'], $hubs ) ||   in_array( $term_meta['finish_hub_id'], $hubs ))
				{
					$routhss[]	= $all_routh; 
				}
			}
			return $routhss;
		}
		static function get_all_hubs()
		{
			//$loc_meta		= get_option("taxonomy_$location_id");
			$args			= array(
										'numberposts'	=> -1,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'orderby'  		=> 'title',
										'order'     	=> 'ASC',
										'post_type'		=> 'smp_hub',
									);
			$hubs			= get_posts($args);
			return $hubs;
		}
		static function get_hub_owner_id($hub_id)
		{
			$owner_id	= get_post_meta($hub_id, "owner_id", true);
			return $owner_id;
		}
		
		static function get_start_hub_of_transiting_batch($batch_id)
		{
			$disl_id			= get_post_meta($batch_id, "dislocation_id", true);
			$routh_id			= get_post_meta($batch_id, "transportation_id", true);
			if($routh_id=="")	return false;
			$meta				= get_option("smp_routh_$routh_id");
			if(get_post_meta($meta["start_hub_id"], "dislocation_id", true) == $disl_id)	
				return $meta["start_hub_id"];
			if(get_post_meta($meta["finish_hub_id"], "dislocation_id", true) == $disl_id)	
				return $meta["finish_hub_id"];
			
		}
		static function get_free_capacities($hub_id)
		{
			global $moveble_types;
			$rouths		= SMP_Routh::get_all_rouths_by_hub($hub_id);
			$disl_id	= get_post_meta($hub_id, "dislocation_id", true);
			$args		= array(
									'numberposts'	=> 0,
									'offset'		=> 0,
									'post_status' 	=> 'publish',
									'fields'		=> 'ids',
									'post_type'		=> $moveble_types,
									'meta_query'	=> array(
																'relation'			=> "AND",
																array(
																		'key'		=> 'transportation_id',
																		'value'		=> $rouths,
																		'operator'	=> 'OR',
																	  ), 
																array(
																		'key'		=> 'is_permission',
																		'value'		=> 1,
																	  ),
																array(
																		'key'		=> 'dislocation_id',
																		'value'		=> $disl_id,
																	  )
															),
								);
			$batchs		= get_posts($args);
			
			$cap		= get_post_meta($hub_id, "capacity", true);
			foreach($batchs as $batch)
			{
				$cap -= get_post_meta($batch, "count", true);
			}
			return $cap;
		}
		
		static function get_titles($id)
		{			
			global $Soling_Metagame_Constructor, $hubs_rouths, $GoodsType, $moveble_types;
			$disl_id			= get_post_meta($id, "dislocation_id", true);
			$disl				= SMC_Location::get_instance($disl_id);// get_term_by("id", $disl_id, "location");
			//$disl_type_id		= get_option("taxonomy_".$disl_id);
			$owner_id			= get_post_meta($id, 'owner_id', true);
			$owner				= SMC_Location::get_instance($owner_id);// get_term_by("id", $owner_id, "location");
			$html				= '
			<table class="lp-batch-table" cellpadding="0" cellspacing="0" align="left" style="margin:1px!importing; display:table;" > 			
				<tr class="lp-batch-table-coll-setting" >
					<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" style="width:200px;">					
						<span class="lp-batch-comment">'.
							__("Dislocation", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("This Hub dislocate in current Location. there it takes one slot infrastructure", "smp")) .
						'</span>
					</td  align="left">
					<td class="lp-batch-table-coll-setting lp-batch-col2">
						<span class="smp-goods-1">'.
								$disl->name . 
							'</span>
					</td>
				</tr>		
				<tr class="lp-batch-table-coll-setting" >
					<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
						<span class="lp-batch-comment">'.
							__("Owner", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owner of Hub may coincide with the location and may not match. Players-Propietors of Location-owner complete management by Hub.", "smp")) .
						'</span>
					</td  align="left">
					<td class="lp-batch-table-coll-setting lp-batch-col2">
						<span class="smp-goods-1">'.
								$owner->name . 
							'</span>
					</td>
				</tr>
				<tr class="lp-batch-table-coll-setting" >
					<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
						<span class="lp-batch-comment">'.
							__("Capacity", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("How many units of Goods type Batchs per time may transit throw this Hub.", "smp")) .
						'</span>
					</td  align="left">
					<td class="lp-batch-table-coll-setting lp-batch-col2">
						<span class="smp-goods-1">'.
								get_post_meta($id, "capacity", true) . " " . __("unit", "smp"). 
							'</span>
					</td>
				</tr>
				<tr class="lp-batch-table-coll-setting" >
					<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
						<span class="lp-batch-comment">'.
							__("Free capabilities", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Free capabilities", "smp")) .
						'</span>
					</td  align="left">
					<td class="lp-batch-table-coll-setting lp-batch-col2">
						<span class="smp-goods-1">'.
								SMP_Hub::get_free_capacities($id) . " " . __("unit", "smp").
							'</span>
					</td>
				</tr>
				<tr class="lp-batch-table-coll-setting" >
					<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
						<span class="lp-batch-comment">'.
							__("Amortization", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("the amount of money written off against the transit of one unit of Batch in favor of the owner", "smp")) .
						'</span>
					</td  align="left">
					<td class="lp-batch-table-coll-setting lp-batch-col2">
						<span class="smp-goods-1">'.
								get_post_meta($id, "cost", true) . 
							'</span>
					</td>
				</tr>
			</table>';
			return $html;			
		}
		
		static function get_form($id)
		{		
			global $Soling_Metagame_Constructor, $hubs_rouths, $GoodsType, $moveble_types;
			$hubs_rouths	= SMP_Routh::get_all_rouths_by_hub($id);		
			$html11			= self::get_titles($id);
			$disl_id		= get_post_meta($id, "dislocation_id", true);
			$is_owner		= static::is_user_owner($id);
			
			/*
			$html11			.=  '
						<div>
						<h2>'.__("Choose the required routh", "smp").'</h2>'.
							SMP_Routh::wp_dropdown_rouths(array('class' => "chosen-select select_routh_hub", "hub_id"=>array($id))).
				'		</div>';
			//$all_batches				= array();
			$rts			= "-- ".Assistants::echo_me($id);*/
			if(is_array($hubs_rouths) )
			{
				foreach($hubs_rouths as $hub_routh)
				{
					$route					= get_term_by("id", $hub_routh, "smp_routh");
					$rts					.=  '
					<div class="smp_routh_batches" id="smp_routh_batches_'.$route->term_id.'" routh_id="'.$route->term_id.'">
						<h4>'.__("Routh","smp")." <b>". $route->name."</b></h4>";		
					$arg		= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'orderby'  		=> 'title',
												'order'     	=> 'ASC',
												'post_type' 	=> $moveble_types,
												'post_status' 	=> 'publish',
												'meta_query'	=> array(
																			'relation'			=> "AND",
																			array(
																					'key'		=> 'transportation_id', 
																					'value'		=> $route->term_id,
																					'compaire'	=> '='
																				 ),
																			array(
																					'key'		=> 'dislocation_id', 
																					'value'		=> $disl_id,
																					'compaire'	=> '='
																				  )
																		 ),
											);
					
					$batchs		= get_posts($arg);
					$rts		.= "<TABLE>";
					$i=0;
					foreach($batchs as $batch)
					{
						$bid		= $batch->ID;	
						$gb			= SMP_Goods_Batch::get_instance($batch->ID);
						//$rts		.=  "<div>". Assistants::echo_me(Assistants::object_property_args( $batch)   )."</div>";
						//$rts		.=  $gb->get_stroke( $gb->body, 3  );
						$permission	= (int)$gb->get_meta("is_permission");
						if($is_owner)
						{
							$content	= "
							<div per_batch_id='$bid'>
								<input type='checkbox' name='is_permission_".$bid."' value='".$permission."' ".checked($permission, true, 0)." name='is_permission_".$bid."' id='is_permi".$bid."' class='css-checkbox1 get_permission'/>
								<label class='css-label1' for='is_permi" . $bid . "'></label>
							</div>";
							
						}
						else
						{
							$content	= $permission	? __("Yes") : __("No");
						}
						
						$rts		.=  $gb->get_table_row( array(array("title"=>__("Permission", "smp"), "content"=>$content)));
						$i++;
					}
					$rts		.= "</table>";
					if($i == 0)
					{
						$rts		.=  "<div class='smp-comment'>" . __("There are no requests for transportation", "smp") . "</div>";
					}
					else
					{
						$rts		.=  '
						<hr/>';
					}
					$rts			.=  '</div>';
					//$all_batches = array_merge($all_batches, $batchs);
				}
			}
			else
			{
				$rts		.=  "<div class='smp-comment'>" . __("There are no routes in Hub", "smp") . "</div>";
			}
			
			//PERMISSIONS			
			$perm = '<div>
					
			<div id="hub_permissions_'.$id.'"></div>';
			global $is_new_permission;
			if(!isset($is_new_permission))
			{
				$perm			.= '
				<div id="new_permission" style="display:none;">
					<h3>'.__("add new Hub permission", "smp"). '</h3>
					<div class="absaz">
						<label for="gb_owner_id">'.__('Owner of Goods Butch', 'smp').'</label><BR>'.
						$Soling_Metagame_Constructor->get_location_selecter(array('class'=>"chosen-select", 'name'=>'gb_owner_id', 'id'=>'gb_owner_id')).
					'</div>
					<div class="absaz">
						<label for="new_permission_gb">'.__('Goods type', 'smp').'</label><BR>'.
						Goods_Type::wp_dropdown_goods_type(array('class'=>"chosen-select", 'name'=>'new_permission_gb', 'id'=>'new_permission_gb')).	
					'</div>
					<div class="absaz">
						<label for="new_permission_priority">'.__('Preority', 'smp').'</label><BR>
						<input type="number" min="0" max="10" value="0" class"chosen-select" id="new_permission_priority"/>
					</div>
					<div class="absaz">
						<label for="new_permission_priority">'.__('Limit', 'smp').'</label><BR>
						<input type="number" min="0" value="0" class"chosen-select" id="new_permission_limit"/>
					</div>
					<div>
						<span class="button smc_padding new_hub_permission_button" hub_routh="'.$hub_routh->term_id.'">'.__("create Hub permission", "smp").'</span>
					</div>
				</div>';
				$is_new_permission = true;
			}
				
			
			
			$perm			.= '</div>';
			
			/**/
			$html			.= Assistants::get_tabs(
															array(
																	array("title" =>__('Parameters', 'smp'), 			'slide' => $html11,	'name'=>'hub_setting'),
																	array("title" =>__('Storage', 'smp'), 				'slide' => $rts,	'name'=>'hub_storage'),
																	array("title" =>__('My Hubs permissions', 'smp'), 	'slide' => $perm,	'name'=>'hub_permission')
																 ), 
															'' . $id 
														);	
			return $html;
		}
		static function get_short_form($hub)
		{
			global $Soling_Metagame_Constructor;
			if(is_numeric ($hub))
			{
				$id				= $hub;
				$hub			= static::get_instance($id);
			}
			else
				$hub			= static::get_instance($hub);
			$id					= $hub->id;
			$disl_id			= get_post_meta($id, "dislocation_id", true);
			$disl				= get_term_by("id", $disl_id, "location");
			$loc_type			= $Soling_Metagame_Constructor->get_location_type($disl_id);
			$owner_id			= get_post_meta($id, 'owner_id', true);
			$owner				= get_term_by("id", $owner_id, "location");
			
			$help				= '
			<div>
			<table>';
			$help				.= '<tr><td>'.__("Dislocation", 'smp').': </td><td>' 	. $disl->name. '</td><tr>';
			$help				.= '<tr><td>'.__("Owner", 'smp').': </td><td>'			. $owner->name. '</td><tr>';
			$help				.= '<tr><td>'.__("Capacity", 'smp').': </td><td>'		. get_post_meta($id, 'capacity', true). __("unit", "smp") . '</td><tr>';
			$help				.= '<tr><td>'.__("Cost", 'smp').': </td><td>'			. get_post_meta($id, 'cost', true). '</td><tr>';
			$help				.= '</table></div>';
			return '<span  class="smp_hub_pictogramm_1">
				<a href="' . get_permalink($id) . '">'.
				$hub->body->post_title.' '. $Soling_Metagame_Constructor->assistants->get_hint_helper($help).
			'	</a>
			</span>';
		}
		
		/*
			$params		- 'class', 'hub_picto'
		*/
		static function get_pictogramm($hub_id, $params=-1)
		{
			global $Soling_Metagame_Constructor;
			$hub				= static::get_instance($hub_id);
			$params				= $params==-1 ? $params=array("hub_picto" => "hub_picto") : $params;
			$disl_id			= get_post_meta($hub_id, "dislocation_id", true);
			$disl				= get_term_by("id", $disl_id, "location");
			$loc_type			= $Soling_Metagame_Constructor->get_location_type($disl_id);
			$owner_id			= get_post_meta($hub_id, 'owner_id', true);
			$owner				= get_term_by("id", $owner_id, "location");
			$your_fac			= is_user_logged_in() && Factories::is_user_owner($owner_id, get_current_user_id());
			$your				= $your_fac && !$params['hide_your'] ? 
				$Soling_Metagame_Constructor->assistants->get_short_your_label() : 
				"";
			$help				= '
			<div><h3>' . $hub->body->post_title . '</h3>'.
			'<table>';
			$help				.= '<tr><td>'.__("Dislocation", 'smp').': </td><td>' 	. $disl->name. '</td><tr>';
			$help				.= '<tr><td>'.__("Owner", 'smp').': </td><td>'			. $owner->name. '</td><tr>';
			$help				.= '<tr><td>'.__("Capacity", 'smp').': </td><td>'		. get_post_meta($hub_id, 'capacity', true). __("unit", "smp") . '</td><tr>';
			$help				.= '<tr><td>'.__("Cost", 'smp').': </td><td>'			. get_post_meta($hub_id, 'cost', true). '</td><tr>';
			$help				.= '</table></div>';
			
			$html				= 
			"<div ".$params["hub_picto"] ." hub_id='$hub_id' href='" . get_permalink($hub_id) . "' class='smp_hub_pictogramm ".$params["class"] ."' id='smp_hub_pictogramm_$hub_id'>
				<div class='smp_hub_picto'><i class='fa fa-truck'></i></div> <div class='smp_hub_picto_title'>" .
				$hub->body->post_title . "</div>".
				$Soling_Metagame_Constructor->assistants->get_hint_helper($help).
				$your.
			"</div>";
			return apply_filters("smp_hub_pictogramm", $html, $hub_id);	
		}
		
		
		static function get_all_locations_from_rouths($hub_id)
		{
			$routh_ids			= SMP_Routh::get_all_rouths_by_hub($hub_id);
			$locs				= array();
			foreach($routh_ids as $rid)
			{
				$r				= get_option("smp_routh_$rid");
				$start_hub_id	= $r['start_hub_id'];
				if($start_hub_id != $hub_id)
				{
					$locs[]		= array( "routh_id"=>$rid, "loc_id"=>get_post_meta($start_hub_id, "dislocation_id", true));
					continue;
				}
				else
				{
					$locs[]		= array( "routh_id"=>$rid, "loc_id"=>get_post_meta($r['finish_hub_id'], "dislocation_id", true)) ;
					continue;
				}
			}
			//insertLog("get_all_locations_from_rouths", $routh_ids);
			return $locs;
		}
		
		
		static function get_widget_form($id)
		{
			require_once('SMP_Hub_Permission.php');
			global $Soling_Metagame_Constructor;
			$owner_tid	= get_post_meta($id, "owner_id", true);	
			$owner		= SMC_Location::get_instance( $owner_tid );
			$your		= is_user_logged_in() && self::is_user_owner($owner_tid, get_current_user_id());
			$routes		= SMP_Routh::get_all_rouths_by_hub($id);
			// block "Parameters"
			$par			= "
			<div class='lp_clapan_raze'>		
				<div class='klapan3_subtitle'>".
					$Soling_Metagame_Constructor->assistants->get_div_hint_helper($p_help, "p_help") .
					__("Parameters", "smp"). 
				'</div>
				<div class="lp_clapan_raze1"></div>
				<div class="smp_bevel_form">' . 
					self::get_titles($id).
				"</div>
			</div>";
			//
			$params		= "<div id='route_widget_form".$routh_object->id."' titled='".$routh_object->data->name."' style='display:block;'>";
			$params		.= "<div id='route_".$routh_object->id."' style='position:relative; display:block;'>";
			$params		.= "
			<div class='lp_clapan_raze'>		
			<div class='klapan3_subtitle'>".
				$Soling_Metagame_Constructor->assistants->get_div_hint_helper($p_help, "p_help") . 
				__("All Rouths", "smp"). 
			'	</div>
			<div class="smp_bevel_form">';
			foreach($routes as $route)
			{
				$r_obj	= SMP_Routh::get_instance($route);
				$params	.= $r_obj->get_picto();
			}			
			$params		.= '</div>
						<div class="lp_clapan_raze1"></div>
					</div>
				</div>
			</div>';
			$permissions	= "
			<div class='lp_clapan_raze'>		
				<div class='klapan3_subtitle'>".
					__("all Hub Permissions", "smp").
			'	</div>
				<div class="smp_bevel_form">'.			
					SMP_Hub_Permission::get_hub_permissions_list($id).
			'	</div>
				<div class="lp_clapan_raze1"></div>
				</div>
			</div>';
			$owners			= SMC_Location::owners_form($owner_tid, $your, $owner->name);
			return Assistants::get_switcher(
											array(																										
														array("title" => "<img src=".SMC_URLPATH."img/info.png>", 				"slide" => $par, 		'hint' => __("Parameters", "smp")),																										
														array("title" => "<img src=".SMP_URLPATH."img/hub_picto.png>", 			"slide" => $params, 	'hint' => __("All Rouths", "smp")),																										
														array("title" => "<img src=".SMP_URLPATH."icon/permission_ico.png>",	"slide" => $permissions,'hint' => __("all Hub Permissions", "smp")),																										
														array("title" => "<img src=".SMC_URLPATH."img/user.png>", 				"slide" => $owners, 	'hint' => __("Owners", "smc")),																										
												 ), "factory_widget_"
											);
		}
	}
?>