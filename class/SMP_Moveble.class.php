<?php
	class Moveble
	{
		function __construct($id)
		{
			parent::__construct($id);
		}
		//elemet for front tables
		public function get_stroke(
								$goods_batch,
								$user_is_owner=-1,
								$params= ''
							)
		{
			global $Soling_Metagame_Constructor;
			if($params =='')
			{
				$params					= array(
													"width"		=> 380,
													"height"	=> 250,
													'is_price'	=> true,
												);
			}
			$op							= get_option(SMP);
			$goods_type_id 				= get_post_meta( $goods_batch->ID, "goods_type_id", true );
			$batch_count				= get_post_meta( $goods_batch->ID, "count", true );;
			$factory_id					= get_post_meta( $goods_batch->ID, "factory_id", true );
			$batch_owner_id				= get_post_meta( $goods_batch->ID, "owner_id", true );
			$batch_dislocation_id		= get_post_meta( $goods_batch->ID, "dislocation_id", true );
			$store						= get_post_meta( $goods_batch->ID, "store", true );
			$price						= get_post_meta( $goods_batch->ID, "price", true );
			$qual						= get_post_meta( $goods_batch->ID, "quality", true);
			$qual						=  !$qual ? 50 : $qual;
			$price_text					= "";	
			$factory					= get_post($factory_id);
			$goods_type					= get_post($goods_type_id);
			$owner						= SMC_Location::get_instance($batch_owner_id); 
			$best_before				= get_post_meta($goods_batch->ID, "best_before", true);
			$bckgr 						= $best_before < 1 ? 'background:url('.SMP_URLPATH.'img/rja.jpg)' : '';
			
			//$disl_text				= '<span class="lp-batch-store">' .__("Stores", "smp").' <a href="/?page_id='.$op['store_ID'].'" class="smp-colorized"></a></span>';
			$sale_text					= $store ? '<div class="smp_batch_sale lp_bg_color_hover  hint  hint--left" data-hint="'. __("Sale", "smp").'"><i class="fa fa-briefcase"></i></div>' : "";
			
			
			$disloc						= SMC_Location::get_instance($batch_dislocation_id); 
			$disloc_type				= $Soling_Metagame_Constructor->get_location_type($batch_dislocation_id);
			$disl_text					= '<span class="lp-batch-location_type">'.
												$disloc_type->post_title.
											'</span>
											<span class="lp-batch-data">'.
												$disloc->name.
											'</span>';
			
			if(!isset($price) || $price=="" )
			{
				$price					= $this->set_price($goods_batch->ID);
			}
			$price_text					= $params['is_price'] && $store ?
												apply_filters("smp_batch_card_price", __("Price", "smp"). "<span> " . $price."/" . $price * $batch_count . "</span> " . __("UE", "smp"), $goods_batch->ID): 
												'';
			$clr						= (get_post_meta($goods_type_id, 'color', true)!= "") ? "#".get_post_meta($goods_type_id, 'color', true) : "#888";
			$owner_type					= $Soling_Metagame_Constructor->get_location_type($batch_owner_id);
			
			
			$btm_pay			= '';
			$btn_settings		= '<i class="fa fa-cogs fa-2x"></i> ';//.__("Settings");
			switch($user_is_owner)
			{
				case 0:
					$exec		= $this->you_are_not_owner($goods_batch, $owner);
					break;
				case 1:
					$exec		= $this->you_are_owner_cont($goods_batch, $owner);
					break;
				case -1:
					$exec		= "";
					break;
				default:
					$exec		= apply_filters("smp_bath_stroke_exec", "", $user_is_owner, $goods_batch, $owner);		
					break;
			}
			$best_before_text	= $this->options['gb_settings']['best_before'] ? '
			<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Best before", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("After the deadline will become a waste product", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">
					<span class="smp-goods-1">'.
							($best_before > MAX_BEST_BEFORE ? __("infinitely", "smp") : $best_before. " " . __("circles", "smp")) . 
						'</span>
				</td>
			</tr>' : '<tr><td></td><td></td></tr>';
			
			$content			= '
<div class="smp-batch-card" style="'.$bckgr.'; width:'. $params['width'] .'px; height:'. $params['height'] .'px;" id="batch_card_'.$goods_batch->ID.'">	
	<div style="background:rgba(0,0,0,0.15); height:30px;">
		<svg width="' . $params['width'] . 'px" height="160px" viewBox="20 0 ' . $params['width'] . ' 166.5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
		style="	" class="smp-goods-card-plate">
		  <defs/>
		  <g>
			<path stroke="none" fill="'.$clr.'" d="M114.6 30.5 L0.15 30.5 0.15 0.2 114.5 0.2 129.7 15.4 114.6 30.5"/>
		  </g>
		</svg>
	</div>
	<div class=bg_stroke_title>'.
			$batch_count. " " . __("unit", "smp").
	'</div>
	<span style="position:absolute; display:inline-block; right:4px; top:4px; padding:2px; background:white; font-size:10px;">' . $goods_batch->ID. '</span>
	<div style="
		position:absolute;
		top:6px;
		left:140px;
		font-family:Arial, serif;">'.
			$goods_type->post_title. 	
	'</div>
	<div class="smp-batch-card-table-cont"
		style="
		position:absolute;
		top:45px;
		left:5px;
		padding:1px!impotant;
		display:inline-block;
		">
		<table class="lp-batch-table" cellpadding="0" cellspacing="0" align="left" style="margin:1px!importing; display:table;" >'. 			
			$best_before_text.
			apply_filters("smp_batch_form_owner",'
			<tr style="padding:0; margin:0;" valign="top"> 
				<td width="130"  align="right"   class="lp-batch-table-coll-setting lp-batch-col1" >
					<span class="lp-batch-comment">'.
						__("Owner", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Owners of this Location  controlled this Goods Batch.", "smp")) .
					'</span>
				</td  align="left">
				<td width="180"  class="lp-batch-table-coll-setting lp-batch-col2">
					<span class="lp-batch-location_type">'.
						" ".$owner_type->post_title.
					'</span>
					<span class="lp-batch-data">'. 
						$owner->name.
					'<span class="lp-batch-data">
					<!--span class="lp_bg_color_hover smp_batch_character_link">
						<i class="fa fa-long-arrow-right"></i> 
					</span-->
				</td  align="left">
			</tr>', 
			$goods_batch->ID ).
			apply_filters("smp_batch_form_dislocation",
			'<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Dislocation", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("The Goods Batch locate in this Location", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">'.
						" " . $disl_text." ".
					'<!--span class="lp_bg_color_hover smp_batch_character_link">
						<i class="fa fa-long-arrow-right"></i> 
					</span-->
				</td>
			</tr>', 
			$goods_batch->ID ). 
			apply_filters("smp_batch_form_productive",
			'<tr class="lp-batch-table-coll-setting" >
				<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" >					
					<span class="lp-batch-comment">'.
						__("Factory", "smp").' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("The Goods Batch made by that Factory", "smp")) .
					'</span>
				</td>
				<td class="lp-batch-table-coll-setting lp-batch-col2">'.
						" " . $factory->post_title." ".
					'<!--span class="lp_bg_color_hover smp_batch_character_link">
						<i class="fa fa-long-arrow-right"></i> 
					</span-->
				</td>
			</tr>', 
			$goods_batch->ID ). 
			apply_filters("smp_batch_add_characteristics",
			"",
			$goods_batch->ID ). '
		</table>
	</div>
	<div class="smp_batch_trumb" style="">'.
		get_the_post_thumbnail( $goods_type_id, array(78, 78)).
	'</div>'.
		$sale_text.
	'
	<div name="exec" style="
		position:absolute;
		left:5ps;
		margin-left:5px;
		margin-bottom:0px!important;
		margin-right:20px;
		bottom:5px;
		display:inline-block;
		">'.
			$exec.
	'</div>
	<div name="price" class="smp_batch_card_price" style="color:'.$clr.';">'.
		$price_text.
	'</div>';
		if($this->user_is_owner())
			$content	.= '<div class="smc-your-label">'.__("It's your", "smc").'</div>';
		if($best_before < 1)
			$content	.= '<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>';
	
	$content		.= '</div>'.
	'<!-- END OF CARD -->';
								
			return $content; 
		}		
		
		function you_are_owner_cont($goods_batch, $owner)
		{	
			return "
					<div style='padding:3px 0; width:110px;'>
						<span class='smc-alert lp-link' target_name='exec_batch_".$goods_batch->ID."' func='add_tabs'><i class='fa fa-cogs fa-2x'></i></span> ".
					"</div>". $this->exec_dialog($goods_batch, $owner, "exec_batch_" . $goods_batch->ID );
		}
		function exec_dialog($goods_batch, $owner, $div_id, $visible=false)
		{
			global $Soling_Metagame_Constructor;
			global $SolingMetagameProduction;
			
			$count			= get_post_meta($goods_batch->ID, "count", true);
			$store			= get_post_meta($goods_batch->ID, "store", true);
			$price			= get_post_meta($goods_batch->ID, "price", true);
			$best_before	= get_post_meta($goods_batch->ID, "best_before", true);
			$store_title	= $store ? __("Remove from Store", "smp") : __("Send to Store", "smp");
			
			$Sale_slide		= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='sale_to_location_".$goods_batch->ID."'>".__("Select count of units for sale","smp")."</label><br>
					<input  inbgc  bid='".$goods_batch->ID."' id='sale_to_location_".$goods_batch->ID."' name='sale_to_location_".$goods_batch->ID."' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
					"<p style='margin-top:10px;'></p>
					<label for='new_price_".$goods_batch->ID."'>".__("Set price for one unit of this batch in Store","smp")."</label><br>
					<input  price_gb_store  bid='".$goods_batch->ID."' id='new_price_".$goods_batch->ID."' name='new_price_".$goods_batch->ID."' type='number' min='0' value='".$price."' style='width:140px;'/>
					".
				"</div>
				<div class='smp-table-null' style=''>
					<div class='smp-colorized smp-submit' gbbt='to_store' bid='".$goods_batch->ID."'>".
						$store_title.
					"</div>					
				</div>
				<hr/>
				
			</div>";
			$transfer_slide	= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='transfer_to_location_".$goods_batch->ID."'>".__("Select count of units for transfer","smp")."</label><br>
					<input intrans bid='".$goods_batch->ID."'  id='transfer_to_location_".$goods_batch->ID."' name='transfer_to_location_".$goods_batch->ID."' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
				"</div>
				<div class='smp-table-null' style='display:table-cell;'>
					<label for='location_".$goods_batch->ID."'>".__("Choose Location-reciever","smp")."</label><br>
					".$Soling_Metagame_Constructor->get_location_selecter(
																			array(
																					"id"			=> "location_".$goods_batch->ID,  
																					"name"			=> "location_".$goods_batch->ID, 
																					"style"			=> 'padding:6px;width:140px;border-radius:4px;',
																					'selector'		=> 'seltrans',
																					'selector_name'	=> '..',
																					'selector2'		=> 'bid',
																					'selector_name2'=> $goods_batch->ID,
																					
																				  )
																		  )." 
				</div>
				<div class='smp-table-null' style=''>
					<div class='smp-colorized smp-submit'  gbbt='to_transfer' bid='".$goods_batch->ID."'>
						".__("Send to other Location", "smp")."
					</div>
				</div>
			</div>";
			$delete_slide	= "
			<div class='smp-batch-tab-slide'>
				<div class='smp-table-null'>
					<label for='delete_to_location_".$goods_batch->ID."'>".__("Select count units for deleting","smp")."</label><br>
					<input bid='".$goods_batch->ID."' indel  id='delete_to_location_".$goods_batch->ID."'  name='delete_to_location_".$goods_batch->ID."' type='number' min='0' max='".$count."' value='".$count."' style='width:140px;'/> ".
					__("unit", "smp").
				"</div>
				<div class='smp-table-null' style=''>
					<div class='smp-colorized smp-submit'  gbbt='to_delete' bid='".$goods_batch->ID."'>".
						__("Delete for good and all","smp").
					"</div>
				</div>
			</div>";
			$prise_slide="
			<div class='smp-batch-tab-slide'>
				
			</div>
			";
			$print	= "";/*"current_user_can("administrator") 
						? "<div style='display:inline-block; width:45%;'>
							<div class='smp_button2 print_batch_button' batch_id='". $goods_batch->ID ."'>".
								__("Print").
							"</div>
						</div> " : "";*/
			
			
			if($SolingMetagameProduction->options['gb_settings']['dislocation_id'])
				$dislocat 	= array("title" => __("The Transfer", "smp"), 	"slide" => $transfer_slide);
			else
				$dislocat	= '';
			if($SolingMetagameProduction->options['gb_settings']['store'])
				$sales 		= array("title" => __("The Sale", "smp"), 		"slide" => $Sale_slide);
			else
				$sales		= '';
			if($SolingMetagameProduction->options['gb_settings']['deleting'])
				$deleting 	= array("title" => __("The Deleting", "smp"), 	"slide" => $delete_slide);
			else
				$deleting	= '';
			$garbage		= $best_before < 1 ? '<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>' : "";
			//$bckgrnd		= $best_before < 1 ? "background-image:url(".SMP_URLPATH."img/rja.jpg); background-size:cover; ": "";
			return "
					<div id='".$div_id."' style='display:".($visible ? "block" : "none")."; " . $bckgrnd. "'>
						<div style='display:inline-block; width:45%;'>
							<h4>". __("Execute manipulations", "smp"). "</h4>
							<div>".
								"id = ".$goods_batch->ID .
							"</div>
							<div>
								<span class='lp-batch-comment'>".__("Owner", "smp")."</span> <span class='lp-batch-data'>". $owner->name."</span>
							</div>
						</div>".
						$print.
						"<form name='new_post_".$goods_batch->ID."' method='post'  enctype='multipart/form-data' id='new_post_".$goods_batch->ID."'>".
								"<hr/>".
								//SMP_Assistants::get_tabs(
								Assistants::get_switcher(
															array(
																	apply_filters("smp_butch_slide_0", $sales, 		$goods_batch->ID),
																	apply_filters("smp_butch_slide_1", $dislocat, 	$goods_batch->ID),
																	apply_filters("smp_butch_slide_2", $deleting, 	$goods_batch->ID),
																	apply_filters("smp_butch_slide_3", "", 			$goods_batch->ID),
																	apply_filters("smp_butch_slide_4", "", 			$goods_batch->ID),
																	apply_filters("smp_butch_slide_5", "", 			$goods_batch->ID), 
																	apply_filters("smp_butch_slide_6", "", 			$goods_batch->ID), 
																	apply_filters("smp_butch_slide_7", "", 			$goods_batch->ID), 
																), 'exec_batch_'
														).								
								"<hr/>".
						"</form>". $garbage.
					"</div>";
		}
		function you_are_not_owner($goods_batch, $owner)
		{	
			global $current_user_accounts;
			$count			= get_post_meta($goods_batch->ID, "count", true);
			$price			= get_post_meta($goods_batch->ID, "price", true);
			$opt			= "";
			if($current_user_accounts)
			{
				foreach($current_user_accounts as $acc)
				{
					$opt	.= "<option value='".$acc->ID."'>".$acc->post_title." ... <span class='smp-select-comment1'>" . get_post_meta($acc->ID, "count", true) . " " . __("UE", "smp") . "</span></option>";
				}
			}
			return "
					<div id='paysment_win_".$goods_batch->ID."' class='paymentss' style='display:none;'>	
						<h4>".__("Pay goods batch", "smp"). "</h4> 
						<form name='pay_".$goods_batch->ID."' method='post'  enctype='multipart/form-data' id='pay_".$goods_batch->ID."'>
							<div class='simple-margin-top' pid='".$goods_batch->ID."' price='".$price."' >". 							
								"<input class='inputnumber' id='payment_count_".$goods_batch->ID."' name='payment_count_".$goods_batch->ID."' 	type='number' min='0' max= '" . $count. "' step='1' value='".$count."' style='width:70px;'/>". " " . __("unit", "smp").
								" ". __(" for ", "smp")." ". 
								"<input class='inputnumber' id='price_count_".$goods_batch->ID."' 	name='price_count_".$goods_batch->ID."'		type='number' min='0' max= '" . $count*$price. "' step='".$price."' value='".$count*$price."' style='width:100px;'/>".
								" ". __("UE", "smp").
								"<p style='margin-top:20px;'>
								<label>".__("Currency Account", "smp")."</label><BR>
								<select name='account_".$goods_batch->ID."' style='width:100%; padding:5px;'>".
									$opt.
								"</select>
								</p>".
								"<p style='margin-top:20px;'> </p>
								<input name='pay_".$goods_batch->ID."' type='submit' class='smp-colorized smp-submit'  value='".__("Pay", "smp")."' bid='".$goods_batch->ID."'>
								<br>".
							"</div> 
						</form>
					</div>".
					
					'<div  class="lp-batch-pay smc-alert" target_name="paysment_win_'.$goods_batch->ID.'">
						<svg width="69.75px" height="23px" viewBox="0 0 69.75 23" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:0;">
							<defs/>
							<g>
								<path stroke="none" fill="#151414" d="M99.9 23.1 L8.25 23.1 0.15 15 0.15 0.1 69.9 0.1 69.9 23.1"/>
							</g>
						</svg>
						<div style="
							position:absolute;
							top:0;
							left:5px;
							width:70px;
							height:23px;">'.
								__("Pay", "smp"). 
						'</div>
					</div>
					';
		}
		
		/*
		$type		0 - standart
					1 - it's your (?)
					2 - by tansit
			
		*/
		function get_widget_picto( $type=0, $forse_block=false)
		{
			
			$is_block		= get_post_meta( $gb, "is_blocked", true );
			$best_before	= get_post_meta( $gb, "best_before", true );
			$best_before_text = $best_before > MAX_BEST_BEFORE ?__("infinitely", "smp") :  $best_before . " " . __("circles", "smp");
			if($is_block && !$forse_block) return "";
			
			global $Soling_Metagame_Constructor;
			$dis_id			= get_post_meta($gb, 'dislocation_id', true);
			$owner_id		= get_post_meta($gb, 'owner_id', true);
			$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
			$itsyour		= $is_owner ? 
				$Soling_Metagame_Constructor->assistants->get_short_your_label(22) : 
				"";
			$location		= get_term_by('id', $dis_id, 'location');
			$owner			= get_term_by('id', $owner_id, 'location');
			$gbt			= get_post_meta($gb, 'goods_type_id', true);
			$gt				= get_post($gbt);
			$count			= get_post_meta($gb, 'count', true);
			$color			= $best_before>0 ? "#" . get_post_meta($gbt, 'color', true) : "url(".SMP_URLPATH."img/rja.jpg)";
			switch($type)
			{
				case 0:
				case 1:
					$help		= 	"<h3 class='help_centered'>".__('Goods batch','smp').": ".$gt->post_title . "</h3>" .
						__("Count", "smp").": <span class='help_select'>". $count."</span><BR>".
						__('Owner', 'smp').": <span>".$owner->name."</span><BR>".
						__("Dislocation", "smp").": <span>". $location->name."</span><BR>".
						__("Best before", "smp").": <span>". $best_before_text . "</span><BR>" .
						"<span class='help_select'>".__("Press pictogramm for execute goods batch", "smp")."</span>"
						;
					$opacity				= 1;
					break;
				case 2:
					$transportation_id		= get_post_meta($gb, 'transportation_id',	true);
					$route					= get_term_by("id", $transportation_id, 	"smp_routh");
					$finish_time			= get_post_meta($gb, "finish_time", 		true);
					$help		= 	"<h3 class='help_centered'>".__('Goods batch','smp').": ".$gt->post_title . "</h3>" .
						__("Count", "smp").": <span class='help_select'>". $count."</span><BR>".
						__('Owner', 'smp').": <span>".$owner->name."</span><BR>".
						__("Best before", "smp").": <span>". $best_before_text . "</span><BR>".
						"<div class='smp_transportation_batch_comm' style=''><i class='fa fa-truck'></i> ".$route->name. "<BR> (" . __("Remaining time", "smp") . " " . floor(($finish_time - time())/60) . __(' minutes', 'smp') . ") </div>";
					$opacity				= .5;
					break;
			}
			
			$your		= $is_owner ? "<div style='position:absolute; display:block; top:0px; left:0px; border:4px solid red; padding:4px 4px; text-align:center; width:30px; height:30px;' ></div>" : "";
			$img		= $best_before > 0 ? Goods_Type::get_trumbnail( $gbt,  40, array("is_hint"=>false)) : "<div class='factory_widg_goods_type goods_type_trumb'><img class='attachment-40x40 wp-post-image' width='40' height='40' src='".SMP_URLPATH."img/rja_120_120.jpg'></div>"; 
			$trashbord	= $best_before > 0 ? " border:1px solid transparent" : ' border:1px solid red';
			$help		= $best_before > 0 ? $help : $help.'<div class="is_repaid" data-deg="'.(20 + rand(0,10)).'deg">'.__("Is garbage", "smp").'</div>';
			$html 		.= "
			<div style='position:relative; display:inline-block;'>
			<div st_gb_id='".$gb."' class='fix_box_size factory_widg_goods_type div_hinter lp-link ' data-hint='help_".$gb."' target_name='exec_batch_gb_".$gb."' func='add_tabs'  style='opacity:" . $opacity . "; $trashbord; position:relative; display:block; height:44px; margin:0; margin-bottom:3px;'>".$img;
			if($count > 900)	
			{
				$st = "letter-spacing:0;";
				$count = ">900";
			}
			else
				$st = '';
			$html		.= "<DIV class='st_gb_count' style='background:".$color."; $st'>" . $count .  "</div>$itsyour</div></div>";
			$html		.= "<div id='help_".$gb."' class='lp-hide help_centered' style='height:145px;'>" . $help .  "</div>";
			
			$html		.= $this->exec_dialog($this->body, $owner, "exec_batch_gb_".$gb);
			return $html;
		}
		
		function get_picto($id, $forse_block=false)
		{
			$is_block		= get_post_meta( $id, "is_blocked", true );
			if($is_block && !$forse_block) return "";
			$count			= get_post_meta( $id, "count", true );
			$price			= get_post_meta( $id, "price", true );
			$goods_type_id 	= get_post_meta( $id, "goods_type_id", true );
			$color			= get_post_meta( $goods_type_id, "color", true );
			$owner_id		= get_post_meta( $id, "owner_id", true );
			$owner			= SMC_Location::get_instance( $owner_id );
			return "<div style='float:left; margin-right:3px;width:60px; line-height:1;display:inline-block;position:relative;'>".'
				<div batch_picto_id="' . $id . '" class=" hint hint--top" data-hint="'.__("Convert goods batch to real Card?", "smp").'">' .
					get_the_post_thumbnail( $goods_type_id, array(55, 55), array('class' => 'smp_good_type_picto')).
					'<div style="background-color:#'.$color.'; padding:2px 7px;display:inline-block; position:absolute; top:0; left:0;">'.
						$count.
					'</div>
				</div>
				<div style="font-family:Open Sans, Arial; font-size:10px;">'.
					$owner->name.
				'</div>
			</div>';
		}
		
		
		static function the_content($post, $content, $before, $after)
		{
			return $this->get_stroke($this->body) . "<BR>" . $content;
		}
		
		
	}
?>