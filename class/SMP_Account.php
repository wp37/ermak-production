<?php
	class SMP_Account extends SMC_Post
	{
		
		static function get_type()
		{
			return SMP_CURRENCY_ACCOUNT;
		}
		public static function save_taxonomy_custom_meta($term_id, $currency_type_id=-1)
		{
			if($currency_type_id==-1) return false;
			$loc		= SMC_Location::get_instance($term_id);
			$pars 		= array(
								  'post_title'   		=> "(".$loc->name.")",
								  'post_type' 			=> SMP_CURRENCY_ACCOUNT,
								  'post_content' 		=> ""  ,
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);

			$ca			=  wp_insert_post( $pars ); 
			update_post_meta($ca, "owner_id", 	$term_id);
			update_post_meta($ca, 'count', 		0);
			update_post_meta($ca, 'ctype_id',	$currency_type_id);
			$ct			= SMP_Currency_Type::get_instance($currency_type_id);
			global $wpdb;
			global $table_prefix;
			$wpdb->update(
									$table_prefix."posts",
									array("post_title" => $ca." (".$loc->name. " [ " . $ct->get("post_title") . "])"),
									array("ID" => $ca)
						);
		}
		static function delete_location_handler($loc_id)
		{
			$ca_args	= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'post_type' 	=> SMP_CURRENCY_ACCOUNT,
									'post_status' 	=> 'publish',
									'meta_query' 	=> array(
																array(
																		"key"		=> "owner_id",
																		"value"		=> $loc_id,
																		'compare'	=> '='		
																	),
															),
								);
			$cas		= get_posts($ca_args);
			foreach($cas as $ca)
				wp_delete_post($ca->ID, true);
		}
		static function add_SMC_Currency()
		{
				$labels = array(
					'name' => __('Currency Account', "smp"),
					'singular_name' => __("Currency Account", "smp"), // ����� ������ ��������->�������
					'add_new' => __("add Currency Account", "smp"),
					'add_new_item' => __("add Currency Account", "smp"), // ��������� ���� <title>
					'edit_item' => __("edit Currency Account", "smp"),
					'new_item' => __("new Currency Account", "smp"),
					'all_items' => __("all Currency Accounts", "smp"),
					'view_item' => __("view Currency Account", "smp"),
					'search_items' => __("search Currency Account", "smp"),
					'not_found' =>  __("Currency Account not found", "smp"),
					'not_found_in_trash' => __("no found Currency Account in trash", "smp"),
					'menu_name' => __("Currency Account", "smp") // ������ � ���� � �������
				);
				$args = array(
					'labels' => $labels,
					'public' => true,
					'show_ui' => true, // ���������� ��������� � �������
					'has_archive' => true, 
					'exclude_from_search' => true,
					'menu_position' => 21, // ������� � ����
					'show_in_menu' => "Metagame_Finance_page",
					'supports' => array(  'title')
					//,'capabilities' => 'manage_options'
					,'capability_type' => 'post'
				);
				register_post_type(SMP_CURRENCY_ACCOUNT, $args);
		}
		
		// ����-���� � ��������
		
		static function my_extra_fields_ca() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func'), SMP_CURRENCY_ACCOUNT, 'normal', 'high'  );
		}
		static function extra_fields_box_func( $post )
		{
			global $Soling_Metagame_Constructor, $SMP_Currency;
			$obj				= self::get_instance($post->ID);
			?>	
				<div style='display:inline-block;'>
					<div class="smp_batch_extra_field_column">
						<div class="h">
							<label for="owner_id"><?php echo __("Owner", "smp") ;?></label><br>
							<?php echo $Soling_Metagame_Constructor->get_location_selecter(array("name"=>"owner_id", 'selected'=>$obj->get_meta( "owner_id")));?>
						</div>				
						<div class="h">
							<label for="count"><?php _e("Count", "smp");?></label><br>
							<input  class="h2" name="count" type="number" min="0" step="1" value ="<?php print_r( $obj->get_meta( "count" ));?>"/>
						</div>			
						<div class="h">
							<label for="ctype_id"><?php _e("Currency Type", "smp");?></label><br>
							<?php echo $SMP_Currency->wp_droplist_currency_type(array("name"=>"ctype_id", "class"=>"h2", 'selected'=> $obj->get_meta('ctype_id'))); ?>
						</div>	
						<div class="h">
							<input name="is_lock" id="is_lock" type="checkbox"  class="css-checkbox" <?php checked( $obj->get_meta( "is_lock"), 1 );?>/> 
							<label for="is_lock" class="css-label"><?php _e("Is locked", "smp");?></label>
							
						</div>	
					</div>
					<div class="smp_batch_extra_field_column">
						<div class="h">
							<label for="reports"><?php _e("Reports", "smp"); ?></label>
							<div class="h2" id="reports">
								<?php 
								$reports		= SMP_Currency::get_report($post->ID);
								foreach($reports as $report)
								{
									$parner_account		= get_post($report['partner_account_id']);
									echo "<p>". $report['summae'] . "	" . $parner_account->post_title . "	" . get_the_time('j M, Y G:i:s', $report['time']). "	" . $report['reason'] ." </p>";
								}
								?>
							</div>
						</div>
					</div>
				</div>
			<?php
			wp_nonce_field( basename( __FILE__ ), 'goods_ca_metabox_nonce' );
		}
		
		static function true_save_box_data ( $post_id) 
		{					
			global $table_prefix;
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['goods_ca_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['goods_ca_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;				 	
			
			update_post_meta($post_id, 'owner_id',	$_POST['owner_id']);
			update_post_meta($post_id, 'count', 	$_POST['count']);
			update_post_meta($post_id, 'ctype_id', 	$_POST['ctype_id']);
			update_post_meta($post_id, 'is_lock', 	$_POST['is_lock']=="on" ? 1 : 0);
			
			
			if($_POST['post_title']=="")
			{
				$dd							= get_term_by ('id', $_POST['owner_id'], 'location');				
				$post->post_title			= $post_id . " (" . $dd->name . ")";	
				
				global $wpdb;
				$wpdb->update(
								$table_prefix."posts",
								array("post_title" => $post->post_title),
								array("ID" => $post_id)
							);
			}				
			return $post_id;
		}
		
		
		static function add_views_column( $columns )
		{
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"	 			=> __("ID", 'smp'),
				  "title" 			=> __("Title"),
				  "owner" 			=> __("Owner", "smp"),
				  "ctype_id" 		=> __("Currency Type", 'smp'),
				  "count" 			=> __("Count", 'smp'),
				  "is_lock" 		=> __("Is locked", 'smp'),
			   );
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column($sortable_columns)
		{
			$sortable_columns['ids'] 			= 'IDs';	
			$sortable_columns['owner'] 			= 'owner';			
			$sortable_columns['ctype_id'] 		= 'ctype_id';
			$sortable_columns['count'] 			= 'count';
			$sortable_columns['is_lock'] 		= 'is_lock';
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case "IDs":	
					echo "<br>
					<div class='ids'><span>ID</span>".$post_id. "</div>
					<p>
					<div class='button' add_bid='$post_id' post_type='SMP_Account'>" . __("Double", "smc") . "</div>";
					break;
				
				case 'owner':
					$term_id	= get_post_meta($post_id, 'owner_id', 1);
					$t 			= SMC_Location::get_instance($term_id);
					echo $t->name . "<br><div class='ids'><span>ID</span>".$term_id."</div>";;
					break;
				case "ctype_id":
					$ctype_id	= get_post_meta($post_id, "ctype_id", true);
					if($ctype_id == -1 || $ctype_id== "")
					{
						echo "--";
						break;
					}
					$ct			= get_post($ctype_id);
					echo $ct->post_title;
					break;		
				case "count":
					$t		= get_post_meta($post_id, "count", true);
					echo $t;
					break;			
				case "is_lock":
					$t		= get_post_meta($post_id, "is_lock", true);
					echo '<img style="display:inline-block; margin-top:5px; " src="'.SMP_URLPATH.'img/'.($t ? 'check_checked' : 'check_unchecked').'.png">' ;
					break;				
			}		
		}
		
		// �������� ������ ��� ���������� �������	
		static function add_column_views_request( $object )
		{
			if($object->get('post_type') != SMP_CURRENCY_ACCOUNT) return $object;
			switch( $object->get('orderby'))
			{
				case 'owner':
					$object->set('meta_key', 'owner_id');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'ctype_id':
					$object->set('meta_key', 'ctype_id');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'count':
					$object->set('meta_key', 'count');
					$object->set('orderby', 'meta_value_num');
					break;
				case 'is_lock':
					$object->set('meta_key', 'is_lock');
					$object->set('orderby', 'meta_value_num');
					break;
				
			}
			return  $object;
		}
		
		static function wp_dropdown_all_by_user($currency_type_id, $params=-1)
		{
			if(!is_array($params))
			{
				$params		= array("id" => "account_", "name"=> "account_");
			}
			$ct				= SMP_Currency_Type::get_instance($currency_type_id);
			$opt			= "";
			
			$current_user_accounts		= SMP_Currency::get_all_user_accounts();
			if($current_user_accounts)
			{
				foreach($current_user_accounts as $acc)
				{
					$accnt	= SMP_Account::get_instance($acc);
					if($accnt->get_meta("ctype_id") != $currency_type_id) continue;
					$ac_cnt	= $accnt->get_count();
					$opt	.= "<option value='".$acc->ID."'>".$acc->post_title." ... <span class='smp-select-comment1'>" . $ct->get_price($ac_cnt) . "</span></option>";
				}
			}
			/**/
			return "
			<label class='smc_input'>
				<select name='".$params['name']."' id='".$params["id"]."' style='width:100%; padding:5px;'>".
					$opt.
				"</select>
			</label>";
		}
		//
		static function see_summ_location( $location_id, $currency_type)
		{
			global $SMP_Currency;
			$all_accounts		= $SMP_Currency->get_locations_accounts_id($location_id, $currency_type);
			$summ				= 0;
			foreach($all_accounts as $acc_id)
			{
				$acc			= self::get_instance($acc_id);
				$summ			+= (int)$acc->get_meta("count");
			}
			return $summ;
		}
		static function remove_summ_from_location($location_id, $currency_type, $summ, $reson='')
		{
			if($summ <= 0)			return true;
			if(self::see_summ_location( $location_id, $currency_type))
			{
				global $SMP_Currency;
				$all_accounts		= $SMP_Currency->get_locations_accounts_id($location_id, $currency_type);
				$ost				= $summ;
				foreach($all_accounts as $acc_id)
				{
					$acc			= self::get_instance($acc_id);
					$count			= (int)$acc->get_meta("count");
					if($ost >= $count)
					{
						$ost		= $ost - $count;
						$summ1		= SMP_Currency::transfer_from( $acc_id, $count, __("System", "smc"), $reson );
					}
					else
					{
						$summ1		= SMP_Currency::transfer_from( $acc_id, $ost, __("System", "smc"), $reson );
						$ost 		= 0;
						return true;
					}
				}
				//echo "<p>". $ost."</p>";
			}
			else
				return new WP_Error("no_summae", __("No summae in accounts of this Location", "smp"));
		}
		static function add_summ_to_location($location_id, $currency_type_id, $summ, $payer="", $reason='')
		{
			global $Direct_Message_Menager, $SMP_Currency;
			$all_accounts		= $SMP_Currency->get_locations_accounts_id($location_id, $currency_type_id);
			$currency_type		= SMP_Currency_Type::get_instance($currency_type_id);
			if(!$all_accounts[0])
			{
				if(isset($Direct_Message_Menager))
				{							
					$owner_id				= get_post_meta($account_id, "owner_id", true); 
					$owner					= SMC_Location::get_instance($owner_id);				
					$you_transfer_text		= array(
														"post_title"	=> sprintf(__("You have not Account by %s.", "smp"), $currency_type->post_title, $summ), 
														"post_content"	=> sprintf(__("An attempt was made to transfer you to %s. However, the system detects you have an account in this type of currencies. This is - a system error. Inform the master of the circumstances and check that they have brought you to the desired account.", "smp"), $currency_type->get_price($summ) )
													);
					$Direct_Message_Menager->send_to_all_location_owners($owner_id, $you_transfer_text, 4);
				}
				return;
			}
			else
			{
				SMP_Currency::transfer_to( $all_accounts[0], $summ, $payer, $reason );
			}
		}
		
		function get_owner_id()
		{
			return $this->get_meta("owner_id");
		}
		function get_currency_type_id()
		{
			return $this->get_meta("ctype_id");
		}
		function get_count()
		{
			return $this->get_meta("count");
		}
		function get_convert()
		{
			$ct			= SMP_Currency_Type::get_instance($this->get_currency_type_id());
			$rate		= $ct->get_rate();
			return $this->get_count() * $rate;
		}
		static function get_by_owner($owner_id)
		{
			
			
		}
		static function is_user_owner($acc_id, $user_id=-1)
		{
			$acc		= static::get_instance($acc_id);
			if(!isset($acc->body)) return false;
			return $acc->isuser_owner($user_id);
		}
		function isuser_owner($user_id = -1)
		{
			global $Soling_Metagame_Constructor;
			if($user=-1)	$user_id	= get_current_user_id();
			return $Soling_Metagame_Constructor->user_is_owner($this->get_owner_id(), $user_id);
		}
	}
?>