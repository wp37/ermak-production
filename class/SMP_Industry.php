<?php
	class SMP_Industry extends SMC_Post
	{
		public $id;
		public $body;
		static $instances;
		function __construct($id)
		{
			$this->id	= $id;
			$this->body	= get_post($id);
		}
		static function init()
		{
			add_action('init', 											array(__CLASS__, 	'add_smp_industry'), 18);			
			add_action('admin_menu',									array(__CLASS__, 	'my_extra_fields_goods_type'));			
			add_action('save_post_'.SMP_INDUSTRY,						array(__CLASS__, 	'true_save_box_data'));
			add_filter('manage_edit-'.SMP_INDUSTRY.'_columns',			array(__CLASS__, 	'add_views_column'), 12);
			add_filter('manage_edit-'.SMP_INDUSTRY.'_sortable_columns',	array(__CLASS__,	'add_views_sortable_column'));
			add_filter('manage_'.SMP_INDUSTRY.'_posts_custom_column', 	array(__CLASS__,	'fill_views_column'), 5, 2); // wp-admin/includes/class-wp-posts-list-table.php
		}
		static function get_type()
		{
			return SMP_INDUSTRY;
		}
		static function get_all_indastries()
		{
			global $all_industries;
			if($all_industries)	return $all_industries;
			$args		= array(
									"numberposts"		=> 100,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'smp_industry',
									'post_status' 		=> 'publish',									
								);
			$all_industries	= get_posts($args);
			return $all_industries;
		}
		static function get_all_indastry_ids()
		{
			global $all_industries_ids;
			if($all_industries_ids)	return $all_industries_ids;
			$args		= array(
									"numberposts"		=> -1,
									"offset"			=> 0,
									'orderby'  			=> 'title',
									'order'     		=> 'ASC',
									'post_type' 		=> 'smp_industry',
									'fields'			=> 'ids',
									'post_status' 		=> 'publish',									
								);
			$all_industries_ids	= get_posts($args);
			return $all_industries_ids;
		}
		static function the_content($post, $content, $before, $after)
		{
			global $GoodsType;
			$ind				= self::get_instance($post->ID);
			$good_types			= $ind->get_goods_array();
			$html				= "<h3>".__("all Goods types", "smp"). "</h3><table class='smp_industry_gt_table'>";
			foreach($good_types as $good_type)
			{
			
				$html			.= "<tr><td>" . 					
					Goods_Type::get_trumbnail($good_type->ID) . " </td><td><h2>" . $good_type->post_title .
				"</h2></td></tr>";
			}
			$html				.= "</table>";
			echo $html;
		}
		static function wp_dropdown_industry($params)
		{
			$hubs		= SMP_Industry::get_all_indastries();
			$html		= "<select ";
			if(!$params)	$params = array('excludes' => array());
			if(!$params['excludes'])
				$params['excludes'] = array();
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			if(isset($params['multile']))
				$html	.= 'multiple ';
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			foreach($hubs as $hub)
			{
				foreach($params['excludes'] as $ex)	if($ex == $hub->ID)	continue;
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		static function wp_numbering_list($params, $values)
		{
			//$html		.=  ($values);
			$hubs		= SMP_Industry::get_all_indastries();
			$html		.= "<table ";
			$class		= $params['class'] ? $params['class'] : "";
			$html		.= " class='" . $class . " smp_industry_list'";
			if($params['style'])
				$html	.= "style='" .  $params['style'] ."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";			
			$id			= $params['id'] ?$params['id'] : time() . rand(0, 1000);
			$html	.= "id='". $id	 . "' >";
			$vals		= $values 	;
			//echo "<BR> <BR>";
			for($i = 0; $i<count($hubs); $i++)
			{	
				$cur_ind			= null;
				$val				= 0;
				if(is_array($vals) && isset($vals))
				{
					foreach($vals as $value)
					{
						//var_dump($value);
						if( $hubs[$i]->ID 	== $value[0]['goods_type'] )
						{
							$cur_ind		= $hub;
							$val			= $value[0]['goods_value'];
							$payments		= $value[0]['payments_summ'];
							
						}					
					}
				}
				if(!$cur_ind)
				{
					$cur_ind		= $hubs[$i];
					
				}
				$html	.= "
				<tr>
					<td><label class='smp_label_numberer'>" . $cur_ind->post_title . "</label></td>
					<td><input type='number' min='0' step='0.2' value='" . $val . "' class='smp_numberer' name='cnt_$i' ></td>
					<td><span style='padding-left:40px;width:120px;'>
						<label class='smp_label_numberer'>" . __("Summ for autopay", "smco") . "</label>
						<input type='number' min='0' step='0.2' value='" . $payments . "' class='smp_numberer' name='cnt_autopay_$i' >
						<label class='smp_label_numberer'>" . __("UE", "smp") . "</label>
					</span></td>
					<td><input type='hidden' name='ids_$i' value='" . $cur_ind->ID . "'/></td>
				</tr>";				
				//reset($hubs);
			}
			$html		.= "</table><input type='hidden' value='" . count($hubs) . "' name='count_industries'/>";
			echo $html;
		}
		static function wp_checked_list($params="")
		{
			if($params == "")
			{
				$params = array("id" => "smp_industries", "values"=>array());
			}
			$id			= $params['id'];
			$values		= $params['values'];
			$disabled	= $params['disabled'] ? " disabled readonly " : "";
			//echo Assistants::echo_me($values, true);
			$hubs		= SMP_Industry::get_all_indastries();
			$html		= "<ul ";
		
			$html	.= "id='". $id	 . "' class='ui_list'>";
			$vals		= $values 	;
			//echo "<BR> <BR>";
			for($i = 0; $i<count($hubs); $i++)
			{	
				$cur_ind			= null;
				$val				= 0;
				if(($vals))
				{
					foreach($vals as $value)
					{
						//var_dump($value);
						if( $hubs[$i]->ID 	== $value[0]['goods_type'] )
						{
							//echo $value[0]['goods_value'] . " " .  $hubs[$i]->ID . "<BR>";
							$cur_ind= $hub;
							$val	= $value[0]['goods_value'];
						}
						/**/					
					}
				}
				if(!$cur_ind)
				{
					$cur_ind		= $hubs[$i];
					
				}
				$n = $cur_ind->ID;
				$html	.= "<li><input type='checkbox' ". $disabled  . checked(1, $values[$n], false) . " class='css-checkbox' name='".$id."_$n' id='".$id."_$n'/><label for='".$id."_$n' class='css-label'>" . $cur_ind->post_title . "</label><input type='hidden' name='".$id."_ids_$i' value='$n'/></li>";				
				//reset($hubs);
			}
			$html		.= "</ul><input type='hidden' value='" . count($hubs) . "' name='".$id."_count'/>";
			echo $html;
		}
		static function add_smp_industry()
		{
				$labels = array(
					'name' => __('Industry', "smp"),
					'singular_name' => __("Industry", "smp"), // ����� ������ ��������->�������
					'add_new' => __("add Industry", "smp"),
					'add_new_item' => __("add new Industry", "smp"), // ��������� ���� <title>
					'edit_item' => __("edit Industry", "smp"),
					'new_item' => __("add Industry", "smp"),
					'all_items' => __("all Industries", "smp"),
					'view_item' => __("view Industry", "smp"),
					'search_items' => __("search Industry", "smp"),
					'not_found' =>  __("Industry not found", "smp"),
					'not_found_in_trash' => __("no found Industry in trash", "smp"),
					'menu_name' => __("Industry", "smp") // ������ � ���� � �������
				);
				$args = array(
					 'labels' => $labels
					,'public' => true
					,'show_ui' => true // ���������� ��������� � �������
					,'has_archive' => true 
					,'exclude_from_search' => true
					,'menu_position' => 0.11 // ������� � ����
					,'show_in_menu' => "Metagame_Production_page"
					,'supports' => array(  'title', 'thumbnail')
					//,'capabilities' => 'manage_options'
					,'capability_type' => 'post'
				);
				register_post_type('smp_industry', $args);
		}
		
		
		// ����-���� � ��������
		
		static function my_extra_fields_goods_type() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func'), 'smp_industry', 'normal', 'high'  );
		}
		static function extra_fields_box_func( $post )
		{
			?>
			<div style='display:inline-block;'>
				<div class="smp_batch_extra_field_column">
						
						<div class="h">
							<label for="resourse_name"><?php _e("Resourse name", "smp");?></label><br>
							<input  class="h2" name="resourse_name" id="resourse_name" value ="<?php print_r( get_post_meta( $post->ID, "resourse_name", true ) );?>"/>
						</div>
						<div class="h">
							<label for="factory_name"><?php _e("Factory name", "smp");?></label><br>
							<input  class="h2" name="factory_name" id="factory_name" value ="<?php print_r( get_post_meta( $post->ID, "factory_name", true ) );?>"/>
						</div>
						<div class="h">
							<label for="color"><?php _e("Color", "smp");?></label><br>
							<input class="color" name="color" id="color" value ="<?php print_r( get_post_meta( $post->ID, "color", true ) );?>"/>
						</div>
				</div>
			</div>
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'smp_industry_metabox_nonce' );
		}
		
		static function true_save_box_data ( $post_id) 
		{	
			//echo "AA=". $_POST['is_cargo'];
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['smp_industry_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['smp_industry_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;	
				
			update_post_meta($post_id, 'resourse_name', 		$_POST['resourse_name']);		
			update_post_meta($post_id, 'factory_name', 			$_POST['factory_name']);		
			update_post_meta($post_id, 'color', 				$_POST['color']);		
		}
		function get_resourse_name()
		{
			return $this->get_meta("resourse_name");
		}
		function get_factory_name()
		{
			$nam		= $this->get_meta("factory_name");
			//return  __("Factory", "smp");
			return $nam == "" ? __("Factory", "smp") : $nam;
		}
		static function add_views_column( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"	 			=> __("ID", 'smp'),
				  "image1"	 		=> __("Image", 'smp'),
				  "title" 			=> __("Title"),		
				  "resourse_name" 	=> __("Resourse name", "smp"),		
				  "factory_name" 	=> __("Factory name", "smp"),		
			   );
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column($sortable_columns){
			$sortable_columns['factory_name'] 			= 'factory_name';						
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column($column_name, $post_id)
		{
			switch( $column_name) 
			{		
				case 'IDs':
					$color				= get_post_meta($post_id, "color", true);
					if($color == "FFFFFF")	
						$font			= "color:#111;";
					echo "<br><div class='ids' style='background:#".$color.";$font'><span>ID</span>".$post_id."</div>
					<p>
					<div class='button' add_bid='$post_id' post_type='SMP_Industry'>" . __("Double", "smc") . "</div>";
					break;	
				case 'resourse_name':					
					echo get_post_meta($post_id, "resourse_name", true);
					break;	
				case 'factory_name':					
					echo get_post_meta($post_id, "factory_name", true);
					break;	
				case 'image1':
					$indd				= self::get_instance($post_id);
					$thumbnail_id		= $indd->get("_thumbnail_id");
					$tr					= wp_get_attachment_image_src( $thumbnail_id, array(50,50) );
					echo "<img src=" . $tr[0] . ">";
					break;	
			}
		}
		function get_picto()
		{
			$factory_name		= $this->get("factory_name");
			$thumbnail_id		= $this->get("_thumbnail_id");
			$tr					= wp_get_attachment_image_src( $thumbnail_id, array(50,50) );
			return  "<div colspan='2' class='smp_factory_pictogramm_icon'><span>$factory_name</span>
							<img src='" . $tr[0] . "'>".						
					"</div>";
		}
		function get_goods_array($max=-1, $fields="all")
		{
			$args				= array(
											'numberposts'	=> $max,
											'offset'    	=> 0,
											'orderby'  		=> 'ID',
											'order'     	=> 'ASC',
											'post_type' 	=> 'goods_type',
											'fields'		=> $fields,
											'post_status' 	=> 'publish',
											'meta_query' 	=> array(
																		'relation'			=> "OR",
																		array(
																				"key" 		=> "industry",
																				"value" 	=> $this->id,
																				'compare'	=> "LIKE"
																			  ),
																	)
										);
			return get_posts($args);
		}
	}
?>