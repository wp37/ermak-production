<?php
	
	class Factory extends SMC_Post
	{
		public $body;
		public $id;
		protected $_productivity;
		protected $currency_type_id;
		protected $goods_type_id;
		static $smp_factories = array();
		
		function __construct($id)
		{
			$this->id		= $id;
			$this->body		= get_post($id);
		}		
		static function get_factory($id, $is_update=false)
		{
			if(!(self::$smp_factories[$id]) || $is_update)
			{
				self::$smp_factories[$id] = new Factory($id);	
			}
			return self::$smp_factories[$id];
		}
		public function get($field)
		{
			return $this->body->$field;
		}
		public function set($field)
		{
			$this->body->$field	= $field;
			wp_update_post($this->body);
		}
		function get_post_meta($key)
		{
			if($this->$key)	return $this->$key;
			$this->$key		= get_post_meta($this->id, $key, true);
			return $this->$key;
		}
		function update_post_meta($key, $value)
		{
			update_post_meta($this->id, $key, $value);
		}
		
		//=====================================
		//
		//	
		//
		//=====================================
		
		
		//	return Batch id or false 	(int)
		
		function find_good_type(
									$goods_type_id,
									$dislocation_id,
									$owner_id,
									$store=0,
									$quality=-1,
									$best_before=-1
								)
		{			
			return Batch::find_good_type(
											$goods_type_id,
											$this->id,
											$dislocation_id,
											$owner_id,
											$store,
											$quality,
											$best_before
										);
		}
		
		// @$count			- (int)
		
		function add_goods($count)
		{
			global $SolingMetagameProduction;
			$gb				= $SolingMetagameProduction->get_gb_options();
			$modify			= $this->get_modify();
			$goods_type_id	= $this->get_goods_type();
			$dislocation_id	= $this->get_dislocation_id();
			$owner_id		= $this->get_owner_id();
			$quality		= $this->get_quality();
			$usage_duration	= $modify['coeficient'] * get_post_meta($goods_type_id, "usage_duration", true );
			$best_before	= (int)(( $usage_duration - 1 ) * $quality / 100) + 1;
			$arg			= array(
										'owner_id'			=> $owner_id,
										'goods_type_id'		=> $goods_type_id,
										'store'			=> 0,
										'count'				=> $count,
										'factory_id'		=> $this->id,
										"is_blocked"		=> 0
									);		
			$arg['dislocation_id']			= $gb['dislocation_id'] ? $dislocation_id 	: "-1";
			$arg['quality']					= $gb['quality']		? $quality			: 100;
			$arg['best_before']				= $gb['best_before']	? $best_before		: 0;
			$r				= Batch::add_batch2( apply_filters("smp_factory_add_batch", $arg, GOODS_BATCH_NAME, $this->id ) );
			return $r;
		}
		function get_factory_type_name()
		{
			$ind_id					= $this->get_industry();
			$industry				= SMP_Industry::get_instance($ind_id);
			return $industry->get_factory_name();
		}
		function get_goods_type()
		{
			if($this->goods_type_id)	return $this->goods_type_id;
			$goods_type_id			= get_post_meta($this->id, 'goods_type_id', true);
			$this->goods_type_id	= $goods_type_id;
			return $this->goods_type_id;
		}
		function get_dislocation_id()
		{
			if($this->dislocation_id)	return $this->dislocation_id;
			$dislocation_id				= get_post_meta($this->id, 'dislocation_id', true);
			$this->dislocation_id		= $dislocation_id;
			return $this->dislocation_id;
		}
		function get_owner_id()
		{
			if($this->owner_id)			return $this->owner_id;
			$owner_id					= get_post_meta($this->id, 'owner_id', true);
			$this->owner_id				= $owner_id;
			return $this->owner_id;
		}
		function get_available_goods_types()
		{
			if($this->available_goods_types)
				return $this->available_goods_types;
			$this->available_goods_types = get_post_meta($this->id, "available_goods_types", true);
			return $this->available_goods_types;
		}
		function control_components($count, $circle_report='')
		{
			global $GoodsType;
			$goods_type_id			= $this->get_goods_type();
			$components				= $this->get_post_meta("components_type")==0 ? $GoodsType->get_components($goods_type_id) : $GoodsType->get_components2($goods_type_id);
			if(!isset($components))	return $count;
			if(count($components))	return $count;
			$batches				= array();
			foreach($components as $component)
			{
				$b					= $this->find_good_type( $goods_type_id, $dislocation_id, -1, -1, -1, -1 );
				insertLog("control_components", array("count"=>count, "b" => $b));
				if(!isset($b))		
					return $this->send_to_report_empty($circle_report);
				if(get_post_meta($b->ID, "count", true)<$count)	return; 
					$this->send_to_report_empty($circle_report);
				$batches[]			= $b;
			}
			foreach($batches as $batch)
			{
				//$b					= new Batch($batch->ID);
				//$b->remove_goods($count);
			}
			return $count;
		}
		function send_to_report_empty($circle_report)
		{
			//TODO: send message to current Report reason of production goods error
			return 0;
		}
		function get_modify()
		{
			global $SolingMetagameProduction;
			if($this->modify)		return $this->modify;
			$powerfull_multiplier 	= 0;
			$quality_multiplier 	= 0;
			$coeficient				= 1;
			$modifier_id			= get_post_meta($this->id, "modificator", true);
			$best_before			= $SolingMetagameProduction->get_gb_value( "best_before" ) ? get_post_meta($modifier_id, "best_before", true) : 1;
			$b						= $best_before > 0;
			if($b)
			{
				$goods_type_id		= get_post_meta($modifier_id, "goods_type_id", true);
				$coeficient			= $SolingMetagameProduction->get_gb_value("quality") ? get_post_meta($modifier_id, "quality", true)/100 : 1;
				$quality_multiplier	= get_post_meta($goods_type_id, "quality_multiplier", true);
				$powerfull_multiplier= get_post_meta($goods_type_id, "powerfull_multiplier", true);
				$usage_duration		= get_post_meta($goods_type_id, "usage_duration", true);
			}
			$this->modify			= array(
												"quality" 			=> 1 + $quality_multiplier * $coeficient, 
												"powerfull" 		=> 1 + $powerfull_multiplier * $coeficient/100,
												"coeficient" 		=> $coeficient,
											);
			
			return $this->modify;
		}
		function get_industry()
		{
			return $this->get_post_meta("industry");
		}
		function update_productivity($new_val)
		{
			//if($this->productivity)	return $this->productivity;
			$goods_type_id			= $this->get_goods_type();
			$max_powerfull			= $this->get_post_meta( 'max_powerfull');
			$complexity				= get_post_meta($goods_type_id, "complexity", true);
			if($complexity==0)		$complexity = 1;
			$modify					= $this->get_modify();
			$new					= (int)($new_val / $modify['powerfull']*$complexity);
			if($new>$max_powerfull)
				$new				= $max_powerfull;
			update_post_meta($this->id, "powerfull", $new);
			return $new;
		}
		function update_powerfull($new_val)
		{
			//if($this->productivity)	return $this->productivity;
			$goods_type_id			= $this->get_goods_type();
			$max_powerfull			= $this->get_post_meta( 'max_powerfull');
			$modify					= $this->get_modify();
			$new					= (int)($new_val / $modify['powerfull']);
			if($new>$max_powerfull)
				$new				= $max_powerfull;
			update_post_meta($this->id, "powerfull", $new);
			return $new;
		}
		function get_powerfull()
		{
			$powerfull				= $this->get_post_meta( 'powerfull');
			$modify					= $this->get_modify();			
			$count					= $this->control_components($powerfull * $modify['powerfull']);	
			return floor($count);
		}
		function get_max_powerfull(  )
		{			
			$max_powerfull			= $this->get_post_meta( 'max_powerfull');			
			$modify					= $this->get_modify();			
			$count					= $this->control_components($max_powerfull * $modify['powerfull'], $circle_report);				
			$max_powerfull			= floor($count);
			return $max_powerfull;			
		}
		function get_productivity($circle_report='', $is_update=false, $goods_type_id=-1)
		{			
			//insertLog($this->productivity && !$is_update);
			//if($this->productivity && !$is_update)	return $this->productivity;
			$goods_type_id			= $goods_type_id==-1 ? $this->get_goods_type() : $goods_type_id;
			$powerfull				= $this->get_post_meta( 'powerfull');
			$complexity				= get_post_meta($goods_type_id, "complexity", true);
			if($complexity==0)		$complexity = 1;
			$modify					= $this->get_modify();			
			$count					= $this->control_components($powerfull/$complexity * $modify['powerfull'], $circle_report);			
			$this->productivity		= floor($count);
			return $this->productivity;			
		}
		function get_max_productivity(  )
		{			
			$goods_type_id			= $this->get_goods_type();
			$max_powerfull			= get_post_meta($this->id, 'max_powerfull', true);
			$complexity				= get_post_meta($goods_type_id, "complexity", true);
			if($complexity==0)		$complexity = 1;
			$modify					= $this->get_modify();			
			$count					= $this->control_components($max_powerfull/$complexity * $modify['powerfull'], $circle_report);				
			$productivity		= floor($count);
			return $productivity;			
		}
		function get_quality()
		{
			$modify					= $this->get_modify();
			$quality				= get_post_meta($this->id, 'quality', true);
			$q						= $quality * $modify['quality'];
			return $q>100 ? 100 : $q;
		}
		
		function create_goods_per_tact($circle_report)
		{	
			global $GoodsType;			
			//insertLog("create_goods_per_tact 0", $this->id);
			$p						= $this->get_productivity($circle_report);
			//insertLog("create_goods_per_tact 1", $p);	
			if($p==0)				return new WP_Error(__("factory stopped by owner.", "smp"));
			$owner_id				= $this->body->post_title;
			//insertLog("create_goods_per_tact 2 1", $owner_id);
			$ev						= $this->get_needs();
			//insertLog("create_goods_per_tact 2", $ev);
			if(is_wp_error($ev))	
			{
				//return $ev;
			}
			//insertLog("create_goods_per_tact 3", "-------------------------");
			$rized					= $this->rize_all_needs();
			//insertLog("create_goods_per_tact 4", $rized);
			if( !is_wp_error($rized) )	
			{				
				$new	= $this->add_goods( $p);
				//insertLog("create_goods_per_tact 5", $new);
				return $new;
			}
			else
				return $rized;
			
		}
		
		// return aray of Locations IDs
		function get_owners()
		{
			$owner_loc_id			= get_post_meta($this->id, "owner_id", true);			
			$tax					= SMC_Location::get_term_meta( $owner_loc_id );//get_option("taxonomy_$owner_loc_id");			
			$owners					= array();
			$owners[]				= $tax['owner1'];
			$owners[]				= $tax['owner2'];
			$owners[]				= $tax['owner3'];
			return $owners;
		}
		function is_user_owner($user_id = -1)
		{
			if($user_id == -1)	$user_id	= get_current_user_id();
			return in_array($user_id, $this->get_owners());
		}
		function get_need_available($good_type_id)
		{
			$batchs				= Batch::find_all_batch2(
															array(
																	"goods_type_id"		=> $good_type_id, 
																	"dislocation_id"	=> $this->get_dislocation_id(),
																	"owner_id"			=> $this->get_owner_id(),
																	//"is_blocked"		=> 0
																 ),
															-1
														);
			$summae				= 0;
			foreach($batchs as $batch)
			{
				$b				= SMP_Goods_Batch::get_instance($batch->ID);
				$summae			+= $b->get_meta( "count" );
			}
			
			return $summae;
		}
		function get_all_needs()
		{
			global $GoodsType;
			$dislocation_id			= $this->get_dislocation_id();
			$owner_id				= $this->get_owner_id();
			$count					= $this->get_productivity();
			$needs					= $GoodsType->get_components($this->get_goods_type());
			$result					= array();
			if(isset($needs) && is_array($needs) )
			{
				foreach($needs as $need)
				{
					$need_val			= $need[0]['goods_value'] * $count;				
					$availeble			= $this->get_need_available($need[0]['goods_type']);
					//if($need_val > $availeble)
					{
						$result1		= array(
													'need_value'	=> $need_val,
													'goods_value'	=> $need_val - $availeble,
													'goods_availeble'=> $availeble,
													'goods_type'	=> $need[0]['goods_type']
												);
						$result[]		= array($result1);
					}
				}
			}
			return $result;
		}
		function get_all_unavailbe()
		{
			global $GoodsType;
			$dislocation_id			= $this->get_dislocation_id();
			$owner_id				= $this->get_owner_id();
			$count					= $this->get_productivity();
			$needs					=  $this->get_post_meta("components_type")==0 ? $GoodsType->get_components($this->get_goods_type()) : $GoodsType->get_components2($this->get_goods_type()); 
			$result					= array();
			if(isset($needs) && is_array($needs) )
			{
				foreach($needs as $need)
				{
					$need_val			= $need[0]['goods_value'] * $count;				
					$availeble			= $this->get_need_available($need[0]['goods_type']);
					if($need_val > $availeble)
					{
						$result1		= array(
													'need_value'	=> $need_val,
													'goods_value'	=> $need_val - $availeble,
													'goods_availeble'=> $availeble,
													'goods_type'	=> $need[0]['goods_type']
												);
						$result[]		= $result1;
					}
				}
			}
			return $result;
		}
		function get_currency_type()
		{
			$this->currency_type_id				= $this->get_post_meta('currency_type_id');
			return $this->currency_type_id;
		}
		
		//counts how  many components needs for production
		function get_needs()
		{
			global $GoodsType, $SMP_Currency;			
			$dislocation_id			= $this->get_dislocation_id();
			$owner_id				= $this->get_owner_id();
			$count					= $this->get_productivity();			
			$goods_type_id			= $this->get_goods_type();	
			$needs					=  $this->get_post_meta("components_type")==0 ? $GoodsType->get_components($goods_type_id) : $GoodsType->get_components2($goods_type_id);
			//insertLog("get_needs", $needs);
			$no						= array();
			foreach($needs as $need)
			{
				$goods_type			= $need[0]['goods_type'];
				$goods_value		= $need[0]['goods_value'];
				$av					= $this->get_need_available($goods_type);
				//if( $av < $goods_value * $count )
				{
					$goods			= Goods_Type::get_instance($goods_type);
					$no[]			= array('goods_type' => $goods->post_title, 'goods_type_id' => $need[0]['goods_type'], "value"=> $goods_value * $count, "av" => $av);
				}
				
			}
			//
			$no						= apply_filters("smp_factory_get_need", $no, $this->id);
			//insertLog("get_needs 2", $no);
			//
			if(count($no))
			{
				$no_text			= "<div><table class='smp_evgt_table'>
				<thread>
				<th class='smp_table_content1' colspan='1'>" . __("Goods type", 'smp'). "</th>
					<th width=15% class='smp_table_content'>" . __("Needed", "smp") . "</th>
					<th width=15% class='smp_table_content'>".__("Enabled", "smp")."</th>
					<th width=15% class='smp_table_content'>".__("Not enabled", "smp")."</th>
					<!--th width=20% class='smp_table_content1'>" . __("Reserved money", 'smp'). "</th-->
				</thread>" ;
				$n=0;
				foreach($no as $nn)
				{
					$class			= ($n++)%2==0 ? "ob" : "ab";
					$need			= (int)$nn['value'] - (int)$nn['av'];
					$gt				= Goods_Type::get_picto( $nn['goods_type_id'], 60, -1, "" );
					$no_text		.= "
					<tr class='smp_table_content $class'>
						<td>
							<div style='clear:right;'>" .
							 $gt. "</div>".$nn['goods_type'] . 
						"</td>
						<td  >" .
							$nn['value'] .
						"</td>
						<td >" .
							 $nn['av']  .
						"</td>
						<td >" .
							 ($need >=0 ? $need : "")   .
						"</td>
						<!--td  >
							--
						</td-->
					</tr>";
				}
				$no_text			.="</table></div>";
				$err				=  new WP_Error(
															"no_factory_component",
															$no_text
													);
				return $err;
			}
			else
			{
				return true;
			}
		}
		function rize_all_needs()
		{
			global $GoodsType;
			
			$dislocation_id			= $this->get_dislocation_id();
			$owner_id				= $this->get_owner_id();
			$count					= $this->get_productivity();			
			$goods_type_id			= $this->get_goods_type();	
			$needs					= $this->get_post_meta("components_type")==0 ? $GoodsType->get_components($goods_type_id) : $GoodsType->get_components2($goods_type_id);
			//insertLog("rize_all_needs 0 ", array($needs, $this->get_post_meta("components_type")), $this->id);
			
			foreach($needs as $need)
			{
				$goods_type			= $need[0]['goods_type'];
				$goods_value		= $need[0]['goods_value'];
				//insertLog("rize_all_needs 1", array( "goods_type"=> $goods_type, "goods_value"=> $goods_value, "count" => $count ));
				if($this->get_need_available($goods_type)< $goods_value * $count)
					return new WP_Error(
											"not_goods_for_products",
											sprintf(
														__("No component %s for productions.", "smp"),
														get_post($goods_type)->post_title
													)
										);/**/
			}
			
			foreach($needs as $need)
			{
				$goods_type			= $need[0]['goods_type'];
				$goods_value		= $need[0]['goods_value'] * $count;
				$this->rize_single_need($goods_type, $goods_value);
				
			}
			return apply_filters( "smp_factory_rize_need", true, $this->id );
		}
		
		function rize_single_need($good_type_id, $value)
		{
			$goods_value		= $value;
			$dislocation_id		= $this->get_dislocation_id();
			$owner_id			= $this->get_owner_id();		
			$batchs				= Batch::find_all_batch2( array(
																	"goods_type_id" 	=> $good_type_id, 
																	"dislocation_id" 	=> $dislocation_id, 
																	"owner_id" 			=> $owner_id, 
																	"best_before" 		=> 0
																)
														);
			//insertLog("rize_single_need 0", count($batchs));
			//insertLog("rize_single_need 1", ($batchs));
			$summae				= 0;
			foreach($batchs as $batch)
			{
				$summae			+= get_post_meta($batch->ID, "count", true);
			}
			//echo "<BR> Summae: $summae, Value: $value";
			if($value > $summae)	
				return false;
			
			foreach($batchs as $batch)
			{
				if($value>0)
				{
					$batch			= new Batch($batch->ID);
					$count			= get_post_meta($batch->id, "count", true);
					//echo '<br> . . . . count='. $count;
					$ost			= $batch->rize_batch_delete($value);
					//insertLog("rize_single_need 2", $value);
					//echo '<br>. . . . . ost='. $ost;				
					$value			-= $ost;					
					$factory_id		= get_post_meta($batch->ID, "factory",true);
					$factory		= self::get_factory($factory_id);
					//echo "<p>". sprintf(__("Execute goods that produce by %s", "smp"), "<b>".$factory->body->post_title."</b>" ) . "</p>";
				}
			}
			/*
			$gt					= Goods_Type::get_instance($good_type_id);
			echo sprintf(
							__("Rize for %s %s units", "smp"), 
							'<b>'.$gt->post_title . "</b>", 
							'<b>'.$goods_value . "</b>"
						 );
			*/
		}
		function get_not_produce_components()
		{
			//return array_diff( $this->get_available_goods_types(), array($this->get_goods_type()) );
			$mgt				= $this->get_goods_type();
			$agt				= $this->get_available_goods_types();
			$agtt				= array();
			if(count($agt)==0)	return $agtt;
			foreach($agt as $gt)
			{
				if($gt[0]['goods_type'] != $mgt)
				{
					$agtt[]		= $gt[0]['goods_type'];
				}
			}
			/**/
			return $agtt;
		}
		function get_availbles()
		{
			$agt				= $this->get_available_goods_types();
			$agtt				= array();
			if(count($agt)==0)	return $agtt;
			foreach($agt as $gt)
			{
				$agtt[]		= $gt[0]['goods_type'];
			}
			return $agtt;
		}
	}
?>