<?php
	
	class SMP_Hub_Permission
	{
		static function get_routh_permission_batchs($routh_id)
		{
			global $moveble_types;
			//permissions
			$permissions		= SMP_Hub_Permission::get_routh_hub_permissions($routh_id);
			$owner_id			= array();
			foreach($permissions as $permission_id)
			{
				$owner_id[]		= get_post_meta($permission_id, "owner_id", true);
			}
			
			$batch_args			= array(
													'numberposts'	=> -1,
													'offset'    	=> 0,
													'post_type' 	=> $moveble_types,
													'post_status' 	=> 'publish',
													'fields'		=> 'ids',
													'meta_query'	=> array(
																				'relation'			=> "AND",
																				array(
																						'key'		=> 'transportation_id',
																						'value'		=> $routh_id
																					 ),
																				array(
																						'key'		=> 'owner_id',
																						'value'		=> $owner_id,
																						'compaire'	=> '=',
																						'operator'	=> "OR"
																					 )
																			 ),
												);
			//var_dump($batch_args);
			$batches				= get_posts($batch_args);
			return $batches;
		}
		
		static function get_routh_hub_permissions($routh_id)
		{
			$routh_meta			= get_option('smp_routh_'.$routh_id);
			$start_hub_id		= $routh_meta['start_hub_id'];
			$finish_hub_id		= $routh_meta['finish_hub_id'];
			$parg				= array(
												'numberposts'	=> -1,
												'offset'    	=> 0,
												'post_type' 	=> 'smp_hub_permission',
												'post_status' 	=> 'publish',
												'fields'		=> 'ids',
												'meta_query'	=> array(
																				'relation'			=> "OR",
																				array(
																						'key'		=> 'hub_id',
																						'value'		=> $start_hub_id
																					 ),
																				array(
																						'key'		=> 'hub_id',
																						'value'		=> $finish_hub_id
																					 )
																		)
											);
			$permissions	= get_posts($parg);
			return $permissions;
		}
		static function get_hubs_by_location($location_id)
		{
			//hubs
			$args			= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'post_type' 	=> 'smp_hub',
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'meta_query'	=> array(
																		'relation'			=> "OR",
																		array(
																				'key'		=> 'dislocation_id',
																				'value'		=> $location_id
																			 )
																)
									);
			$hubs					= get_posts($args);
			return $hubs;
			
		}
		static function is_goods_batch_has_permission($batch_id)
		{
			//if all hubs uncontrolled global
			$op		= get_option(SMP);
			if($op['is_hubs_uncontrolled'])		return true;
			//
			$goods_type_id			= get_post_meta($batch_id, 'goods_type_id', 	true);
			$dislocation_id			= get_post_meta($batch_id, 'dislocation_id', 	true);
			$owner_id				= get_post_meta($batch_id, 'owner_id', 			true);
			$permissions			= SMP_Hub_Permission::get_permissions_by_location($dislocation_id);
			foreach($permissions as $permission)
			{
				if(
					$goods_type_id 	== get_post_meta($permission, "goods_type_id", true) &&
					$owner_id 		== get_post_meta($permission, "owner_id", true)
				  )
				 {
					return true;
				 }
			}
			return false;
		}
		static function get_permissions_by_location($location_id)
		{
			$hubs					= SMP_Hub_Permission::get_hubs_by_location($location_id);
			//permissions
			$meta_query				= array();
			$meta_query['relation']	= 'OR';
			foreach($hubs as $hub)
			{
				$meta_query[]		= array(
												'key'	=> 'hub_id',
												'value'	=> $hub
											);
			}
			$parg			= array(
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'post_type' 	=> 'smp_hub_permission',
										'post_status' 	=> 'publish',
										'fields'		=> 'ids',
										'meta_query'	=> $meta_query
									);
			$permissions	= get_posts($parg);
			return $permissions;
		}
		static function get_short_form($permission_post_data)
		{
			global $Soling_Metagame_Constructor;
			$id			= is_numeric(permission_post_data) ? permission_post_data  : $permission_post_data->ID;
			$hub		= get_post(get_post_meta($id, "hub_id", true));
			$owner		= get_term_by('id', get_post_meta($id, "owner_id", true), 'location');
			$goods_type	= get_post(get_post_meta($id, "goods_type_id", true));
			$preority	= get_post_meta($id, "preority", true);
			$limit		= get_post_meta($id, "limit", true);
			$limit		= $limit ? $limit . " " . __("unit", "smp") : __("At no allowance", "smp");
			if( SMP_Hub::is_user_owner( $hub->ID ) )
			{
				$remove = '<tr class="lp-batch-table-coll-setting" >
						<td  style="margin-top:10px; border-bottom:1px dotted #666;width:100%;" class="lp-batch-table-coll-setting lp-batch-col1" colspan="2">					
							<span id="delete_pemission_btn_'.$id.'" hp="'.$id.'" class="button delete_hp" style="margin-top:10px; " ask="'.__("Are you realy want to remove this Hub's permission",'smp').'">'.
								__("Remove Hub permission", "smp").
							'</span>
							<hr/>
						</td>
					</tr>';
			}
			
			$html		= '
			<div class="hub_permission_form" id="permission_form_'.$id.'">
				<h4>' . __("Hub permission", "smp") . ' #'. $id .'</h4>
				
				<table class="lp-batch-table" cellpadding="0" cellspacing="0" align="left" style="margin:1px!importing; display:block;" > 			
					<tr class="lp-batch-table-coll-setting" >
						<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" style="width:200px;">					
							<span class="lp-batch-comment">'.
								__("Hub", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("The Hub that given permission for transit", "smp")) .
							'</span>
						</td  align="left">
						<td class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
									$hub->post_title . 
								'</span>
						</td>
					</tr>				
					<tr class="lp-batch-table-coll-setting" >
						<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" style="width:200px;">					
							<span class="lp-batch-comment">'.
								__("Location", 'smc').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Location that recieved permission for transit", "smp")) .
							'</span>
						</td  align="left">
						<td class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
									$owner->name . 
								'</span>
						</td>
					</tr>				
					<tr class="lp-batch-table-coll-setting" >
						<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" style="width:200px;">					
							<span class="lp-batch-comment">'.
								__("Goods type", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Goods type that recieved permission for transit", "smp")) .
							'</span>
						</td  align="left">
						<td class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
									$goods_type->post_title . 
								'</span>
						</td>
					</tr>					
					<tr class="lp-batch-table-coll-setting" >
						<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" style="width:200px;">					
							<span class="lp-batch-comment">'.
								__("Preority", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("preorityof this permission for transit", "smp")) .
							'</span>
						</td  align="left">
						<td class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
									$preority . 
								'</span>
						</td>
					</tr>					
					<tr class="lp-batch-table-coll-setting" >
						<td  align="right"  class="lp-batch-table-coll-setting lp-batch-col1" style="width:200px;">					
							<span class="lp-batch-comment">'.
								__("Limit", 'smp').' '. $Soling_Metagame_Constructor->assistants->get_hint_helper(__("Count of goods per 1 production cyrcle.", "smp")) .
							'</span>
						</td  align="left">
						<td class="lp-batch-table-coll-setting lp-batch-col2">
							<span class="smp-goods-1">'.
									$limit . 
								'</span>
						</td>
					</tr>'.					
					$remove.	
				'</table>
			</div>';
			return $html;
		}
		static function get_hub_permissions_list($hub_id)
		{
			$parg				= array(
											'numberposts'	=> 0,
											'offset'    	=> 0,
											'orderby'  		=> 'title',
											'order'     	=> 'ASC',
											'post_type' 	=> 'smp_hub_permission',
											'post_status' 	=> 'publish',
											'meta_query'	=> array(
																		array(
																				'key'		=> 'hub_id',
																				'value'		=> $hub_id
																			 )
																	)
										);
			$permissions	= get_posts($parg);
			$perm			= '';
			foreach($permissions as $permission)
			{
				$perm		.= self::get_short_form($permission);
			}	
			if(count($permissions)==0)
			{
				$perm		.= "<div class='smp-comment'>" . __("You not given Hub permissions in this Hub.", "smp") . "</div>";
			}
			return $perm;
		}
	}
?>