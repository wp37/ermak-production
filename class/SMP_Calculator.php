<?php

	class SMP_Calculator
	{
		static function default_res_table_data()
		{		
			return array(
				array(
						"need"			=> 0,
						"evalble"		=> 0,
						"not_evalble"	=> 0,
						"money"			=> 0,
						"resorse_name"	=> __("Resourse", "smp")
					)
				);
		}
		function __construct()
		{
			
		}
		static function install()
		{
			$my_post = array(
								  'post_title'   		=> __("Personal Tools", "smp"),
								  'post_type' 			=> 'page',
								  'post_content' 		=> "[smp_personal_calc]"  ,
								  'post_status'  		=> 'publish',
								  'comment_status'		=> 'closed',
								);

			return wp_insert_post( $my_post ); 
		}
		function get_adult_form($user_id=-1)
		{
			$html			= "<h2>".__("Personal Tools", "smp")."</h2>";
			
			return $html;
		}
		function smp_personal_calc()
		{
			global $Ermak_Consume;
			global $Soling_Metagame_Constructor;
			global $goods_arr;
			$goods_arr		= array();
			$html			= "<h1>" . __("Personal Calculator", "smp") . "</h1><div class=smp-comment>".__("This is adult tool for calculate you personal needs per current Playing Circle. ", "smp"). "</div>";
			if(!is_user_logged_in())
			{
				$login				= __('Login', 'smc');
				return $html."<div class='smp-comment'>".__("You must logged in!", 'smp')."</div>
				<div><a href='".wp_login_url( home_url())."' title='".$login."'>".$login."</a></div>";
			}
			else
			{
				
			}
			if(!is_plugin_active('Ermak_consume/Ermak-consume.php'))
			{
				//return $html."<div class='smp-comment'>".__("No consumes for you", "smp")."</div>";
			}
			//if($Ermak_Consume->options['supported_types']['user'])
			{
				$html		.= "<h2>".__("Personal Needs", "smp"). "</h2>
				<div class=description>".
					__("Attention! Player consumed goods that market for target consume.", "smp").
				"</div>";
				//------returned array("html", "data")-----------------------------
				$res		= apply_filters( "smp_calc_personal_needs",  static::default_res_table_data() );
				$html		.= $this->get_table($res, 0);
			}
			//if($Ermak_Consume->options['supported_types'][SMC_LOCATION_NAME])
			{
				$locations	= $Soling_Metagame_Constructor->all_user_locations();	
				$html		.= "
				<h2>".__("Locations Needs", "smp"). "</h2>
				<div class=description>".
					__("Attention! Locations consumed every goods that placed in theese Locations and in the chidren of theese Locations. In first choose goods that market for target Location Consume. Later choose all goods that belong to this location.", "smp").
				"</div>";
				foreach($locations as $location)
				{
					$loc	= SMC_Location::get_instance($location); 
					$tx		= SMC_Location::get_term_meta($location);
					$currency_type_id = $tx['currency_type'];
					$locss	.= "<div class='absaz smp_gt_title' >".$loc->name."</div>";
					//------returned array("html", "data")-----------------------------
					$res	= apply_filters( "smp_calc_location_needs",  static::default_res_table_data(), $location );
					//$locss	.= $res['html']; 
					$locss	.= $this->get_table($res, $location, $currency_type_id);
				}
				$html		.= $locss;
			}
			$factories	= Factories::get_all_user_factories();
			$html		.= "<h2>".__("Productions Needs", "smp"). "</h2>
			<div class=description>".
				__("Attention! Factory consumed every godds only from Location that it placed.", "smp").
			"</div>";
			foreach($factories as $factory_id)
			{
				$fac	= new Factory($factory_id);
				$html	.= "<div class='absaz smp_gt_title'>".$fac->get("post_title")."</div>";
				
				$need	= $fac->get_needs();
				if(is_wp_error($need))
				{
					$html	.= Assistants::echo_me($need->get_error_message());
				}
				else
				{
					$html	.=  "<p class='description'>".__("All available", 'smp')."</p>";
				}
				/**/
				
				if($Ermak_Consume->options['supported_types']['factory'] || !$this->is_consume())
				{
					$res	= apply_filters("smp_calc_factory_needs", static::default_res_table_data(), $factory_id );
					$currency_type_id	= get_post_meta($factory_id, "currency_type_id", true);
					$html	.= $this->get_table($res, $factory_id, $currency_type_id);
				}
			}
			return $html;
		}	
		function get_table($datas, $id, $currency_type_id=-1)
		{			
			//if(!isset($datas[0]))	return "<p class='description'>".__("All available", 'smp')."</p>";
			//$locss	.= Assistants::echo_me($datas, true);
			$table		= "<table border='1' style='margin-bottom:3px;'>
			<thread>
				<th width=35% class='smp_table_content1'>" . __("Goods type", 'smp'). "</th>
				<th width=15% class='smp_table_content'>" . __("Needed", "smco") . "</th>
				<th width=15% class='smp_table_content'>".__("Enabled", "smco")."</th>
				<th width=15% class='smp_table_content'>".__("Not enabled", "smco")."</th>
				<th width=20% class='smp_table_content1'>" . __("Reserved money", 'smp'). "</th>
			</thread>
			";
			$i=0;
			if($currency_type_id!=-1)
				$currency_type = SMP_Currency_Type::get_instance($currency_type_id);
			$all	= array();
			$locss1	= $table;
			foreach($datas as $dat)
				if(count($dat))
				{
					$locss1		.= "<tr><td colspan='9' style='background:#AAA;'>" .$dat["name"]."</td></tr>";
					foreach(@$dat as $key=>$data)
					{
						if(!is_array($data))	continue;
						if($data['need'] <=0)	continue;
						$ttx					= static::default_res_table_data();
						$all[$key]				= $this->merge(  isset($all[$key]) ? $all[$key] : $ttx[0], $data);
						$money	= $currency_type ? $currency_type->get_price($data['money'], "gt_price") : "<st class='gt_price'>".$data['money'] ."</st> " . __("UE", "smp");
						$class	= $i++%2 == 0 ? 'ob' :'ab';
						$locss1	.= "
						<tr class='smp_table_content $class'>
							<td class='smp_table_content1'>" . $data['resorse_name'] . "</td>
							<td>" . $data['need'] . "</td>
							<td>" . $data['evalble'] . "</td>
							<td>" . $data['not_evalble'] . "</td>
							<td>" . $money . "</td>
						</tr>";
					}
				}
				else
				{
					$locss1	.= "<tr><td colspan='5'><div class=smp-comment>".__("Nothing", "smp")."</div></td></tr>";
				}
				$locss1			.= "</table>";
			/**/
			$locss		.= $table;//."<tr><td colspan='9' style='background:#AAA;'>" . __("Consume scheme", "smc"). " - <B>" .$dat["name"]."</B></td></tr>";
			foreach($all as $key=>$data)
			{
				if(!is_array($data))	continue;
				//$all[$key]				= $this->merge(static::default_res_table_data()[0], $data);
				$class	= $i++%2 == 0 ? 'ob' :'ab';
				$no		= ($data['not_evalble'] - $data['evalble']);
				if($no<=0) $no='--';
				$money	= $currency_type ? $currency_type->get_price($data['money'], "gt_price") : "<st class='gt_price'>".$data['money'] ."</st> " . __("UE", "smp");
				$locss	.= "
				<tr class='smp_table_content $class'>
					<td class='smp_table_content1'>" . $data['resorse_name'] . "</td>
					<td>" . $data['need'] . "</td>
					<td>" . $data['evalble'] . "</td>
					<td>" . $no . "</td>
					<td>" . $money . "</td>
				</tr>";
				
			}
			$locss			.= "</table><div class='smc-alert button' target_name='tabl$id' style='margin-bottom:15px; margin-top:0;'>".__("details", "smp")."</div><div class=lp-hide id='tabl$id'>".$locss1. "</div>";
			//$locss			.= Assistants::echo_me($all, true);
			return $locss;
		}
		function is_consume()
		{
			return is_plugin_active('Ermak_consume/Ermak-consume.php');
		}
		function merge($arr1, $arr2)
		{
			foreach($arr1 as $key=>$val)
			{
				$arr1[$key]	= $key!="resorse_name"	? (int)$val + (int)$arr2[$key] : $arr2[$key];
			}
			return $arr1;
		}
		
	}
?>