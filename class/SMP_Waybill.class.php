<?php
	
	  
	class SMP_Waybill extends SMC_Post
	{
		
		static function init()
		{
			add_action( 'init', 									array(__CLASS__, 'add_SMP_Waybill'), 12 );
			add_action( 'save_post_smp_waybill',					array(__CLASS__, 'true_save_box_data_waybill'));
			add_action( 'admin_menu',								array(__CLASS__, 'my_extra_fields_waybill'));
			add_action( 'smp_count_circle',							array(__CLASS__, 'smp_count_circle'),20.11);
			
			add_filter( 'manage_edit-smp_waybill_columns',			array(__CLASS__, 'add_views_column_waybill'), 4);
			add_filter( 'manage_edit-smp_waybill_sortable_columns',	array(__CLASS__, 'add_views_sortable_column_waybill'));
			add_filter( 'manage_smp_waybill_posts_custom_column',	array(__CLASS__, 'fill_views_column_waybill'), 5, 2); 
			add_filter( "smp_goods_batch_change_owner_id", 			array(__CLASS__, 'smp_goods_batch_change_owner_id'), 14, 3 );
			add_filter( "smp_goods_batch_change_change_dislocation_id", array(__CLASS__, 'smp_goods_batch_change_change_dislocation_id'), 14, 3 );
			
			//����������� ������ � ����� ���������� (SMP_Logistics.php)
			add_filter( "smp_goods_batch_change_meta", 				array(__CLASS__, 'smp_goods_batch_change_meta'), 14, 3 );
			add_filter( "smp_batch_circle_change", 					array(__CLASS__, 'smp_batch_circle_change'), 5 );
		}
		
		static function add_properties($names, $types, $defaults)
		{	
			global $wpdb;
			if(!is_array($names))			$names 		= array($names);
			if(!is_array($types))			$types 		= array($types);			
			if(!is_array($defaults))		$defaults 	= array($defaults);			
			$ct								= count($types);
			$cn								= count($names);
			$cd								= count($defaults);
			if($cn > $ct) 					$types		= array_merge( $types,		array_fill(0, $cn-$ct,  $types[0]));
			if($cn > $cd) 					$defaults	= array_merge( $defaults,	array_fill(0, $cn-$cd,  $defaults[0]));
			$query							= "ALTER IGNORE TABLE '" . $wpdb->prefix .  "ermak_waybill_meta' ADD COLUMN (";
			$values							= array();
			$i=0;
			foreach($names as $nm)
			{
				$values[]					= $nm . " " . $types[$i] . " NOT NULL DEFAULT " . $defaults[$i];
				$i++;
			}
			$query							.= implode(", ", $values). ");";
			return $wpdb->query($query);
		}
		static function smp_count_circle($circe_num)
		{
			
		}
		//WayBill
		static function add_SMP_Waybill()
		{
				$labels = array(
					'name' => __('WayBill', "smp"),
					'singular_name' => __("WayBill", "smp"), // ����� ������ ��������->�������
					'add_new' => __("add WayBill", "smp"),
					'add_new_item' => __("add WayBill", "smp"), // ��������� ���� <title>
					'edit_item' => __("edit WayBill", "smp"),
					'new_item' => __("add WayBill", "smp"),
					'all_items' => __("all WayBills", "smp"),
					'view_item' => __("view WayBill", "smp"),
					'search_items' => __("search WayBill", "smp"),
					'not_found' =>  __("WayBill not found", "smp"),
					'not_found_in_trash' => __("no found WayBill in trash", "smp"),
					'menu_name' => __("WayBill", "smp") // ������ � ���� � �������
				);
				$args = array(
					'labels' => $labels,
					'public' => true,
					'show_ui' => true, // ���������� ��������� � �������
					'has_archive' => true, 
					'exclude_from_search' => true,
					'menu_position' => 21, // ������� � ����
					'show_in_menu' => "Metagame_Logistics_page",
					'supports' => array(  'title', 'thumbnail')
					//,'capabilities' => 'manage_options'
					,'capability_type' => 'post'
				);
				register_post_type(SMP_WAYBILL_TYPE, $args);
		}
		static function get_action_form($i="", $actions="")
		{
			global $Soling_Metagame_Constructor;
			if($actions=="")
			{
				$actions		= array();
				$vis			= array('none', "block", 'none');
			}
			if($i !== "")	$style="display:block;";
			
			switch($actions['action'])
			{
				case 1:
					$vis	= array("block", 'none', 'none');
					break;
				case 2:
					$vis	= array('none', 'none', "block");
					break;
				default:
					$vis	= array('none', "block", 'none');
					break;
			}
			return '
			<div class="waybill_box_element" id="ex_waybill'.$i.'" style="'.$style.'" >
				<div>'.
					//Assistants::echo_me($actions, true).
				'</div>
				<div class="waybill_1">'.
					self::wp_dropdown_waybill_actions( array ("name"=>"action_s$i", "selected"=>$actions['action'], 'class' =>"action__inn", 'type'=>$i) ).
				'</div>
				<div class="waybill_1">'.
					$Soling_Metagame_Constructor->get_location_selecter( array ("name"=>"location$i", "selected"=>$actions['location_id'], 'class' =>"waybill_1_inn" ) ).
				'</div>
				<div  class="waybill_2" >
					<div id="target_routh" style="display:' . $vis[0] . '">'.
						SMP_Routh::wp_dropdown_rouths(array("class"=>"waybill_2_inn", "name"=>"rouths$i", "id"=>"rouths$i", 'selected'=>$actions['target'])).
					'</div>
					<div rd="target_deleting"  style="display:' . $vis[1] . '; width=160px; border:1px solid transparent; margin-left:5px;">
					
					</div>
					<div id="target_location" style="display:' . $vis[2] . '">'.
						$Soling_Metagame_Constructor->get_location_selecter( array ("name"=>"t_location".$i, "selected"=>$actions['target'], 'class' =>"waybill_2_inn" ) ).
					'</div>
				</div>
				<div  class="waybill_3">
					<input type="number" style="width:90%;" min="1" name="counts'.$i.'" class="waybill_3_inn" value="'.$actions['counts'].'"/>
				</div>
				<div class="waybill_4">
					<input type="number" style="width:100px;" min="0" step="1" name="frequency'.$i.'" class="waybill_3_inn" value="'.$actions['frequency'].'"/>
					<span class="button minus_button_wb" id="minus" style="vertical-align:middle;"> - </span>
				</div>'.
			'</div>';
		}
		static function get_edit_form($post_id)
		{
			$wb					= self::get_instance($post_id);
			$actions			= self::get_actions($post_id);
			$html				.= '
			<div class="smp_batch_extra_field_column" >
			<div class="h" style="vertical-align:middle; width:720px;; max-width:720px;">	
				<board_title>'.__("Actions").'</board_title>
				<div class="smc-cont">
					<div class="waybill_1">'.
						__("Type of action", "smp").
					'</div>
					<div class="waybill_1">'.
						__("Location", "smc").
					'</div>
					<div class="waybill_2">'.
						__("Target object", "smp").
					'</div>
					<div class="waybill_3">'.
						__("Count per units", "smp").
					'</div>
					<div class="waybill_4">'.
						__("Update frequency (production circles)", "smp").
					'</div>
				</div>
					
				<div id="scheme">';					
			$sc					= self::get_action_form();
			$html				.= $sc;
			$i=0;
			if($actions != "")
				if(count($actions)>0)
				{
					foreach($actions as $action)
					{
						$i++;
						$html	.= self::get_action_form($i, $action);
					}
				}
				

			$html	.= '<input type="hidden" disabled id="i" name="i" value="'. $i.'"/>
						<div id="add_button_waybill" class="button" style="font-size:20px; margin-top:10px;">
							+
						</div>
					</div>
				</div>	
			</div>	
			';
			return $html;
		}
		static function get_full_edit_form($post_id, $by_user=false)
		{
			global $Soling_Metagame_Constructor;
			$wb				= self::get_instance($post_id);
			if(isset($wb))
				$post_title	= $wb->body->post_title;
			$drop			= $by_user ?
				$Soling_Metagame_Constructor->wp_drop_location_by_user_owner(array('class'=>'disbalance', "name"=>"wb_owner_id", 'selected'=>get_post_meta($post_id, "owner_id", true))) :
				$Soling_Metagame_Constructor->get_location_selecter(array('class'=>'disbalance', "name"=>"wb_owner_id", 'selected'=>get_post_meta($post_id, "owner_id", true)));
			$html			.= '
			<table>
				<tr>
					<td width=33%>
						<board_title>'.__("Name").'</board_title>
						<input style="margin-top:20px;" name="waybill_post_title" placeholder="'.__("set title", "smp").'" value="' . $post_title . '" class="disbalance"/><br>
					</td>
					<td width=33%>
						<board_title>'. __("Owner", "smp"). '</board_title><br>'.
						$drop.				
					'</td>			
					<td width=33%>
						<board_title>' . __("Goods type", "smp") . '</board_title><br>'.
						Goods_Type::wp_dropdown_goods_type(array("class"=>"disbalance", "name"=>"wb_goods_type_id", "id"=>"goods_type_id", "selected"=>get_post_meta($post_id, "goods_type_id", true) )) .
					'</td>
				</tr>
			</table>';
			$html		.= self::get_edit_form($post_id);
			$html		.= "<div><div class='button' id='update_waybill'>".__("Update")."</div>";
			return $html;
		}
		
		static function my_extra_fields_waybill() 
		{
			add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func_waybill'), SMP_WAYBILL_TYPE, 'normal', 'high'  );
		}
		static function extra_fields_box_func_waybill( $post )
		{
			global $Soling_Metagame_Constructor, $SMP_Routh;
			?>
			<div class='smc-cont'>	
				
					
						
						<?php
						
							//echo "<BR>"; 
							//var_dump($actions);
							//echo "<BR>";
							echo self::get_edit_form($post->ID);
						?>
						
						
				<div class="smp_batch_extra_field_column">
					<div class="h">
						<board_title><?php echo __("Owner", "smp") ;?></board_title><br>
						<?php echo $Soling_Metagame_Constructor->get_location_selecter(array( 'class'=>'h2', "name"=>"owner_id", 'selected'=>get_post_meta($post->ID, "owner_id", true))); ?>				
					</div>			
					<div class="h">
						<board_title><?php echo __("Goods type", "smp") ;?></board_title><br>
						<?php echo Goods_Type::wp_dropdown_goods_type(array("class"=>"h2", "name"=>"goods_type_id", 'style'=>'height:77px;', "id"=>"goods_type_id", "selected"=>get_post_meta($post->ID, "goods_type_id", true) )); ?>
					</div>
				</div>
			</div>
			<?php
			echo apply_filters("waybill_extra_fields", "", $post);
			wp_nonce_field( basename( __FILE__ ), 'goods_ca_type_metabox_nonce' );
		}
		
		static function true_save_box_data_waybill ( $post_id) 
		{					
			global $table_prefix;
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['goods_ca_type_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['goods_ca_type_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;				 	
			
			update_post_meta($post_id, 'owner_id',			$_POST['owner_id']);
			update_post_meta($post_id, 'goods_type_id',		$_POST['goods_type_id']);
			
			$actions						= array();			
			for($i=1; $i<20; $i++)
			{
				$action						= array();
				$action['action']			= $_POST['action_s'.$i];
				$action['location_id']		= $_POST['location'.$i];
				$action['counts']			= $_POST['counts'.$i];
				$action['frequency']		= $_POST['frequency'.$i];
				switch($action['action'])
				{
					case 1:
						$action['target']	= $_POST['rouths'.$i];
						break;
					case 2:
						$action['target']	= $_POST['t_location'.$i];
						break;
					case 3:
						$action['target']	= "";
						break;
					case 0:
					default:
						break;
				}
				if( ( $action['action'] ) )	
					$actions[]		=	$action;
			}
			
			
			$wb			= self::get_instance($post_id);
			$wb->update_actions($actions);
			
			if($_POST['post_title']=="")
			{
				global $wpdb;
				$wpdb->update(
								$table_prefix."posts",
								array( "post_title" => __("WayBill", "smp")." #".$post_id ),
								array( "ID" => $post_id )
							);
			}	
			
			
			return $post_id;
		}
		
		static function add_views_column_waybill( $columns )
		{
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "IDs"				=> "ID",
				  "title"			=> __("Title"),
				  "actions" 		=> __("Actions"),
				  "owner_id" 		=> __("Owner", "smp"),
				  "goods_type_id" 	=> __("Goods type", "smp"),
			   );
			return $posts_columns;			
		}
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column_waybill($sortable_columns){
			$sortable_columns['IDs'] 			= 'ids';	
			$sortable_columns['owner_id'] 		= 'owner_id';
			$sortable_columns['goods_type_id'] 	= 'goods_type_id';
			$sortable_columns['actions'] 		= 'actions';
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column_waybill($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{				
				case "IDs":	
					echo $post_id;
					break;
				case "owner_id":
					$t		= SMC_Location::get_instance(get_post_meta($post_id, "owner_id", true))->name;
					echo $t;
					break; 	
				case "actions":
					$t		= self::get_instance($post_id);					
					echo $t->get_form();
					break; 	
				case "goods_type_id":
					echo Goods_Type::get_trumbnail(get_post_meta($post_id, "goods_type_id", true), 56, array('margin'=>5));
					break; 			
			}		
		}
		
		function update_actions($actions)
		{
			$this->clear_actions();
			$this->update_multi_meta( $actions, $this->id);
		}
		static function get_actions($id=-1)
		{
			global $wpdb;
			return $wpdb->get_results("SELECT * FROM ".$wpdb->prefix .  "ermak_waybill_meta WHERE waybill_id = " . $id . " ORDER BY order_ ASC", ARRAY_A );
		}
		
		function clear_actions()
		{
			global $wpdb;
			$wpdb->query("DELETE FROM ".$wpdb->prefix . "ermak_waybill_meta WHERE waybill_id = '".$this->id."'");
		}
		
		/*
			@update	- multiple array.
			forexample:
			array(
					array(
							"waybill_id"	=> 1298,
							"order_"		=> 0,
							"action"		=> 1,
							"location"		=> 10,
							"target"		=> 1024,
							"frequency"		=> 1,
							"counts"		=> 2000
						 ),
					array(
							"waybill_id"	=> 1298,
							"order_"		=> 0,
							"action"		=> 2,
							"location"		=> 11,
							"target"		=> 2024,
							"frequency"		=> 2,
							"counts"		=> 300
						 )
				  )
						 
		*/
		function update_multi_meta( $update, $waybill_id)
		{
			insertLog("WB.update_multi_meta",  array('update'=>$update, "waybill_id"=>$waybill_id) );
			global $wpdb;
			$query			= "REPLACE INTO ".$wpdb->prefix . "ermak_waybill_meta " ;
			$keys			= array_keys( $update[0]);
			$keys[]			= "waybill_id";
			$keys[]			= "order_";
			$names			= " (". implode( ",", $keys )  . ") ";
			$values			= array();
			$i				= 0;
			foreach($update as $key)
			{
				$key[]		= $waybill_id;
				$key[]		= $i++;
				$values[]	= " (". implode(",", array_values($key)) . ") ";				
			}
			$query			.= $names. " VALUES ".implode(",", $values);
			insertLog("WB.update_multi_meta", $query);
			$wpdb->query($query);
			return $query;
		}
		
		
		function get_form()
		{
			$actions	= self::get_actions($this->id);
			if( !count($actions) )
			{
				$html		.= "<div class='lp-comment'>" . __("Waybill is not complete.", "smp") . "</div>";
			}
			else
			{
				$html		.= "<div class=smp_calc>
				<table>
					<thread>
						<th>
							<span class=' hint hint--bottom' data-hint='" . __("Type of action", "smp") . "'><i class='fa fa-coffee fa-2x'></i></span>
						</th>
						<th>
							<span class=' hint hint--bottom' data-hint='" . __("Location", "smc") . "'><i class='fa fa-star  fa-2x'></i></span>
						</th>
						<th>
							<span class=' hint hint--bottom' data-hint='" . __("Target object", "smp")  . "'><i class='fa fa-circle fa-2x'></i></span>
						</th>
						<th>
							<span class=' hint hint--bottom' data-hint='" . __("Count per units", "smp") . "'><i class='fa fa-dashboard fa-2x'></i></span>
						</th>
						<th>
							<span class=' hint hint--bottom' data-hint='" . __("Update frequency (production circles)", "smp") . "'><i class='fa fa-clock-o fa-2x'></i></span>
						</th>
					</thread>";
				foreach($actions as $action)
				{
					switch($action['action'])
					{
						case 2:
							$target	= SMC_Location::get_instance($action['target'])->name;
							break;
						case 1:
							$target = get_term_by("id", $action['target'], "smp_routh")->name;
							break;
						default:
							
							break;
					}
					$tx		= self::get_waybill_actions();
					$html	.= "
					<tr>
						<td>".
							$tx[$action['action']-1]['title'].
						"</td>
						<td>".
							SMC_Location::get_instance($action['location_id'])->name.
						"</td>
						<td>".
							$target.
						"</td>
						<td>".
							$action['counts'].
						"</td>
						<td>".
							$action['frequency'].
						"</td>
					</tr>";
				}
				$html	.= "<thread><th colspan=5> <ab waybill_id='" . $this->id . "'>". __("Edit"). "</ab></th></thread>";
				$html	.= "</table></div>";
			}
			return $html;			
		}
		function get_full_form()
		{
			$html		= "<div style='margin-top:10px;'>". $this->get("post_title"). ": <b>" . SMC_Location::get_instance($this->get_meta("owner_id"))->name . "</b> (". Goods_Type::get_instance($this->get_meta("goods_type_id"))->post_title . ")</div>";
			$html		.= "<div style='width:97%; margin-bottom:10px;'>". self::get_form()."</div>";
			return $html;
		}
		static function wp_dropdown_smp_waybill($params)
		{
			
			$hubs		= self::get_all();
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub->ID."' ".selected($hub->ID, $params['selected'], false).">".$hub->post_title."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		static function get_all_by_user($user_id=0, $fields="ids")
		{
			global $Soling_Metagame_Constructor;
			if($user_id == -1)	$user_id	= get_current_user_id();
			$all_locs		= $Soling_Metagame_Constructor->all_user_locations($user_id);
			$args			= array(
										'numberposts'	=> -1,
										'offset'		=> 0,
										'post_status' 	=> 'publish',
										'fields'		=> $fields,
										'post_type'		=> 'smp_waybill',
										'meta_query'	=> array(
																	array(
																			'key'		=> 'owner_id',
																			'value'		=> $all_locs,
																			'operator'	=> 'OR'
																		  )
																),
									);
			$smp_waybill	= get_posts($args);
			return $smp_waybill;
		}
		static function get_all()
		{
			$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> SMP_WAYBILL_TYPE,
										'post_status' 	=> 'publish'
									);
			$wbs			= get_posts($ar);
			return $wbs;
		}
		static function get_waybill_actions()
		{
			return apply_filters(
									"smp_waybill_actions", 
									array(
											array( "ID"=> CHANGE_DISLOCATION_WAYBILL_ACTION, 	"action" =>'change_dislocation', 	"title"	=> __('Move', "smp") ),
											array( "ID"=> CHANGE_OWNER_WAYBILL_ACTION, 			"action" =>'change_owner', 			"title"	=> __('Change owner', "smp") ),
											array( "ID"=> DELETE_WAYBILL_ACTION, 				"action" =>'delete', 				"title"	=> __( 'Delete', "smp") )
										 )
								);
		}
		
		static function wp_dropdown_waybill_actions($params="")
		{
			if($params == "")
				$params = array();
			$hubs		= self::get_waybill_actions();
			$html		= "<select ";
			if($params['class'])
				$html	.= "class='".$params['class']."' ";
			if($params['style'])
				$html	.= "style='".$params['style']."' ";
			if($params['name'])
				$html	.= "name='".$params['name']."' ";
			if($params['id'])
				$html	.= "id='".$params['id']."' ";
			if($params['type'])
				$html	.= "type='".$params['type']."' ";
			$html		.= " >";
			$html		.= "<option value='-1'>---</option>";
			
			foreach($hubs as $hub)
			{
				$html	.= "<option value='".$hub['ID']."' ".selected($hub['ID'], $params['selected'], false).">". $hub['title'] ."</option>";
			}
			$html		.= "</select>";
			return $html;
		}
		static function smp_batch_circle_change($batch_id)
		{
			$batch				= Batch::get_instance($batch_id);
			$owner_id			= $batch->get_meta("owner_id");
			$dislocation_id		= $batch->get_meta("dislocation_id");
			self::smp_goods_batch_change_meta($batch_id, $owner_id, $dislocation_id);
		}
		static function smp_goods_batch_change_owner_id($object_id, $object_type, $meta_value)
		{
			if($object_type != GOODS_BATCH_NAME)	return;
			$batch			= Batch::get_instance($object_id);
			$dislocation_id	= $batch->get_dislocation_id();
			self::smp_goods_batch_change_meta($object_id, $meta_value, $dislocation_id);
		}
		static function smp_goods_batch_change_change_dislocation_id($object_id, $object_type, $meta_value)
		{
			if($object_type != GOODS_BATCH_NAME)	return;
			$batch			= Batch::get_instance($object_id);
			$owner_id		= $batch->get_owner_id();
			self::smp_goods_batch_change_meta($object_id, $owner_id, $meta_value);
		}
		static function smp_goods_batch_change_meta($batch_id, $owner_id, $dislocation_id)
		{
			$batch			= Batch::get_instance($batch_id);
			if(!$batch)		return;
			$goods_id		= $batch->get_goods_type_id();
			self::search_wb(	$batch_id,
								array(
									"goods_type_id"		=> $goods_id,
									"owner_id"			=> $owner_id,
									"dislocation_id"	=> $dislocation_id
								  ));
		}
		static function search_wb( $batch_id, $metas )
		{
			//insertLog("search_wb", array ("action" => "search_wb", "id" => $batch_id, "metas" => $metas));
			global $wpdb, $SMP_Locistics;
			$goods_type_id	= $metas['goods_type_id'];
			$owner_id	 	= $metas['owner_id'];
			$dislocation_id	= $metas['dislocation_id'];
			$post_meta		= array(
										'relation'		=> 'AND',
										array(
											'value'		=> $goods_type_id,
											'key' 		=> 'goods_type_id',
											'compare' 	=> '='
											),
										array(
											'value'		=> $owner_id,
											'key' 		=> 'owner_id',
											'compare' 	=> '='
											)	
									);
			$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'fields'		=> 'ids',
										'post_type' 	=> SMP_WAYBILL_TYPE,
										'post_status' 	=> 'publish',
										'meta_query'	=> $post_meta
									);
			$wbs			= get_posts($ar);
			if(!count($wbs))			return;
			$query			= "SELECT * FROM ".$wpdb->prefix .  "ermak_waybill_meta WHERE (waybill_id=" . implode(" OR waybill_id=", $wbs) . ") AND location_id=$dislocation_id ORDER BY order_ ASC";
			$wb_metas		= $wpdb->get_results($query, ARRAY_A );
			if(!count($wb_metas))		return;
			foreach($wb_metas as $key=>$val)
			{
				$batch			= Batch::get_instance( $batch_id );
				$waybill_tact	= $batch->get_meta("waybill_tact");
				if( $waybill_tact >= $val['frequency']-1 )
				{
					$batch->update_meta("waybill_tact", 0);
				}
				else
				{
					$batch->update_meta( "waybill_tact", $waybill_tact + 1 );
					return;
				}
				switch( $val['action'] )
				{
					case 0:						
						break;
					case CHANGE_DISLOCATION_WAYBILL_ACTION:	
						insertLog("search_wb", array("change disocation"=>$val['target'], "count"=>$val['counts'], "id"=>$batch_id));
						$SMP_Locistics->transportation_batch_change($batch_id, $val['target'], $val['counts']);
						break;
					case CHANGE_OWNER_WAYBILL_ACTION:
						insertLog("search_wb", array("change owner"=>$val['target'], "count"=>$val['counts'], "id"=>$batch_id));
						Batch::rize_batch_owner2( $batch, $val['target'], $val['counts'] );
						break;
					case DELETE_WAYBILL_ACTION:	
						$batch->rize_batch_delete( $val['counts'] );
						break;
					default:
						break;
				}
			}
		}
	}
?>