<?php 
	class Factory_Cell
	{
		function __construct()
		{
			add_action( 'location_edit_form_fields', 			array($this, 'edit_location_type_to_location'), 11, 2 );
			add_action( "delete_location",						array( $this, 'delete_location_handler'), 11);
			add_action( 'edit_location', 						array($this, 'save_taxonomy_custom_meta'), 11); 
			//add_filter("manage_edit-location_columns", 		array($this,'location_columns'), 11); 
			//add_filter("manage_location_custom_column", 		array($this,'manage_location_columns'), 11, 3);
			//add_filter("smc_add_grandchildren_element_to_map", 	array($this, 'smc_add_grandchildren_element_to_map'), 10, 2);
		 
		}
		
		function edit_location_type_to_location($term, $tax_name)
		{
			// put the term ID into a variable
			$t_id = $term->term_id;
		 
			// retrieve the existing value(s) for this meta field. This returns an array
			$term_meta = get_option( "taxonomy_fcell_$t_id" ); 
			//var_dump($term_meta);
			?>
			<tr class="form-field">
				<th scope="row" valign="top">
					<label for="term_meta[count_cells]"><?php _e('Count of Factory Cells', "smp"); ?></label></th>
				</th>
				<td>
					<input  class="h2" name="term_meta[count_cells]" type="number" min="0" step="1" value ="<?php print_r( $term_meta["count_cells"] );?>"/>
				</td>
			</tr>
		<?php
		}
		function delete_location_handler($iLocationId)
		{
			delete_option("taxonomy_fcell_$iLocationId");
		}
		
		//=============================================
		//
		// Save extra taxonomy fields callback function.
		//
		//=============================================
		
		function save_taxonomy_custom_meta( $term_id ) {
			if ( isset( $_POST['term_meta'] ) ) {
				$t_id = $term_id;
				$term_meta = get_option( "taxonomy_fcell_$t_id" );
				$cat_keys = array_keys( $_POST['term_meta'] );
				foreach ( $cat_keys as $key ) {
					if ( isset ( $_POST['term_meta'][$key] ) ) 
					{
						switch($key)
						{
							case "count_cells":
								$cells			= (int)$_POST['term_meta'][$key];
								$child_args		 = array(
														'number' 		=> 0														
														,'hide_empty' 	=> false
														,'fields' 		=> 'all'
														,'child_of' 	=> $term_id
													);
								$children		= get_terms("location", $child_args);
								
								foreach($children as $child)
								{	
									$child_id	= $child->term_id;	
									$tax		= get_option("taxonomy_fcell_$child_id");
									$cells		+= (int)$tax['count_cells'];
									$term_meta['ch'] = null;
								}
								$term_meta['full_count_cells'] = $cells;
							default:
								$term_meta[$key] = $_POST['term_meta'][$key];
						}
					}
				}
				// Save the option array.
				update_option( "taxonomy_fcell_$t_id", $term_meta );
			}
		}
		function location_columns($theme_columns) 
		{
			$theme_columns['count_cells']		= __('count cells', 'smp');
			return $theme_columns;
		}
		function manage_location_columns($out, $column_name, $theme_id) 
		{
			$tax	= get_option("taxonomy_fcell_$theme_id");
			switch ($column_name) {
				case 'count_cells':
					$out 		.= $tax['count_cells'];
					break;			
			}
			return $out;    
		}
		function smc_add_grandchildren_element_to_map($text, $term_id)
		{
			global $all_goods_types;
			Goods_type::get_global();
			$t				= get_term_by("id", $term_id, "location");
			//========================
			// get children's factories
			//========================
			
			$f				= array();
			$terms			= get_term_children($term_id, 'location');
			
			$meta_query						= array(array('value'=>$term_id, 'key'=>'dislocation_id', 'compare'=>'LIKE'));
			$meta_query['relation']			= 'OR';
			foreach($terms as $idd)
			{
				$arr						= array();
				$arr['value']				= $idd;
				$arr['key']					= 'dislocation_id';
				$arr['compare']				= 'LIKE'; 
				array_push($meta_query, $arr);
			}
			$ar				= array(										
											'numberposts'	=> -1,
											'offset'    	=> 0,
											'orderby'  		=> 'id',
											'order'     	=> 'ASC',
											'post_type' 	=> array('factory', 'smp_hub'),
											'post_status' 	=> 'publish',
											'meta_query' 	=> $meta_query
										);
			$factories			= get_posts($ar);	
			
			//===========
			//
			//===========
			
			$tax			= get_option("taxonomy_fcell_$term_id");
			$count_cells	= $tax['full_count_cells'];
			$html			= Assistants::get_hint_helper(__("Factories cells are infrastructure fullless place for city's, factories's or military's builds.", 'smc'), 30, null)."<br>";
			
			for($i=0; $i < $count_cells; $i++)
			{
				$factory	= $factories[$i];
				if(isset($factory))
				{
					$goods_type_id		= get_post_meta($factory->ID, "goods_type_id", true);
					$color				= get_post_meta($goods_type_id, 'color', true);
					$color				= $color ? $color : '000';
					$html	.= '
					<a href="/?factory='.$factory->ID.'">
						<div class="smp_factory_cell_map hint hint--left" style="background:#'.$color.'; border:rgba(255,255,255, 0.75) solid 1px;" data-hint="'.$factory->post_title.'">
						 
						</div>
					</a>';
				}
				else
				{
					$html	.= '<div class="smp_factory_cell_map hint hint--left" data-hint="'.__('empty factory cell', 'smp').'"> </div>';
				}
			}
			return 		$text. $html;
		}
		static function get_free_cells_count($location_id)
		{
			$loc_data		= get_option("taxonomy_fcell_$location_id");
			$count_cells	= $tax['full_count_cells'];
			
			$ar				= array(										
											'numberposts'	=> 0,
											'offset'    	=> 0,
											'orderby'  		=> 'id',
											'order'     	=> 'ASC',
											'post_type' 	=> array('factory', 'smp_hub'),
											'post_status' 	=> 'publish',
											'meta_query' 	=> array(
																		array(
																				'key'		=> 'dislocation_id',
																				'value'		=> $location_id,
																			  )
																	)																		
										);
			$factories			= get_posts($ar);
			return $count_cells - count($factories);
		}
	}

?>