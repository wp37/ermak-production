<?php 
class SMP_Routh_Type extends SMC_Post
{
	
		static function add_smp_route_type()
		{
			global $smc_main_tor_buttons;
			if(!isset($smc_main_tor_buttons))						$smc_main_tor_buttons = array();
			$smc_main_tor_buttons[]									= array('ico'=>"?", 'targ'=>__("all Hubs", "smp"), 'comm' =>'get_location_hubs');
		
			$labels 	= array
			(
				'name' => __('Route Type', "smp"),
				'singular_name' => __("Route Type", "smp"), // ����� ������ ��������->�������
				'add_new' => __("add Route Type", "smp"),
				'add_new_item' => __("add Route Type", "smp"), // ��������� ���� <title>
				'edit_item' => __("edit Route Type", "smp"),
				'new_item' => __("add Route Type", "smp"),
				'all_items' => __("all Route Types", "smp"),
				'view_item' => __("view Route Type", "smp"),
				'search_items' => __("search Route Type", "smp"),
				'not_found' =>  __("Route Type not found", "smp"),
				'not_found_in_trash' => __("no found Route Type in trash", "smp"),
				'menu_name' => __("Route Type", "smp") // ������ � ���� � �������
			);
			$args 		= array
			(
				'labels' => $labels,
				'public' => true,
				'show_ui' => true, // ���������� ��������� � �������
				'has_archive' => true, 
				'exclude_from_search' => true,
				'menu_position' => 21, // ������� � ����
				'show_in_menu' => "Metagame_Logistics_page",
				'supports' => array(  'title', 'thumbnail')
				//,'capabilities' => 'manage_options'
				,'capability_type' => 'post'
			);
			register_post_type('smp_route_type', $args);
		}
		// ����-���� � ��������		
		static function my_extra_fields_smp_route_type() 
		{
			add_meta_box( 'extra_fields', __('Parameters of Route Type', "smp"), array(__CLASS__, 'extra_fields_box_func_smp_route_type'), 'smp_route_type', 'normal', 'high'  );
		}
		
		static function extra_fields_box_func_smp_route_type( $post )
		{		
			global $Soling_Metagame_Constructor;			
			?>
			<div style='display:inline-block;'>
				<div class="smp_batch_extra_field_column">					
					<div class="h">
						<label for="color"><?php echo __("Color", "smp");?> </label><br>
						<input class="color" id="color" name="color" value="<?php echo get_post_meta($post->ID,'color', true); ?>"/>
					</div>									
					<div class="h">
						<label for="hub_name"><?php echo __("Name of Hub type", "smp");?> </label><br>
						<input  id="hub_name" name="hub_name" value="<?php echo get_post_meta($post->ID,'hub_name', true); ?>"/>
					</div>										
					<div class="h">
						<?php echo SMP_Industry::wp_checked_list(array("id"=>"industries", "values"=>get_post_meta($post->ID,'industries', true))); ?>
					</div>					
				</div>				
			</div>				
			<?php 
			wp_nonce_field( basename( __FILE__ ), 'smp_route_metabox_nonce' );
		}
		
		static function true_save_box_smp_route_type ( $post_id) 
		{	
			// ���������, ������ �� ������ �� �������� � ����������
			if ( !isset( $_POST['smp_route_metabox_nonce'] )
			|| !wp_verify_nonce( $_POST['smp_route_metabox_nonce'], basename( __FILE__ ) ) )
				return $post_id;
			// ���������, �������� �� ������ ���������������
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;
			// ���������, ����� ������������, ����� �� �� ������������� ������
			if ( !current_user_can( 'edit_post', $post_id ) )
				return $post_id;							
			update_post_meta($post_id, 'color', 				$_POST['color']);				
			update_post_meta($post_id, 'hub_name', 				$_POST['hub_name']);	
			$industries				= array();
			for($i=0; $i<$_POST['industries_count']; $i++)
			{
				$id= $_POST['industries_ids_'.$i];
				//$industries[$_POST['industries_ids'.$i]] = (int)$_POST['']=="on";
				$industries[$id] = (int)($_POST['industries_'.$id]=="on");
			}
			update_post_meta($post_id, 'industries', 			$industries);
			return $post_id;
		}
		
		static function add_views_column_smp_route_type( $columns ){
			//$columns;
			$posts_columns = array(
				  "cb" 				=> " ",
				  "title"			=> __("Title"),
				  "IDs"	 			=> __("ID", 'smp'),
				  "color"			=> __("Color", 'smp'),	
				  "hub_name"		=> __("Name of Hub type", 'smp'),	
				  "industries"		=> __("all Industries", 'smp'),	
			   );
			return $posts_columns;			
		}	
		
		// ��������� ����������� ����������� �������
		static function add_views_sortable_column_smp_route_type($sortable_columns)
		{
			$sortable_columns['IDs'] 					= 'IDs';						
			$sortable_columns['color'] 					= 'color';								
			$sortable_columns['hub_name'] 				= 'hub_name';								
			$sortable_columns['industries'] 			= 'industries';								
			return $sortable_columns;
		}	
		// ��������� ������� �������	
		static function fill_views_column_smp_route_type($column_name, $post_id)
		{
			$post			= get_post($post_id);
			switch( $column_name) 
			{		
				case 'IDs':
					echo $post_id;
					break;	
				case 'color':
					$fill_id	= get_post_meta($post_id, "color", true);
					//$type		= get_post($type_id);
					$fill		= ($fill_id == "") ? "000" : $fill_id;
					
					echo "<div style='width:15px; height:15px; display:inline-block; position:relative; background:#".$fill."' > </div>";
					break;	
				case 'hub_name':
					$hub_name	= get_post_meta($post_id, "hub_name", true);					
					echo $hub_name;
					break;	
				case 'industries':
					$hub_name	= get_post_meta($post_id, "hub_name", true);					
					echo SMP_Industry::wp_checked_list(array("id"=>"industries", "values"=>get_post_meta($post_id,'industries', true), "disabled"=>true));
					break;					
					
			}		
		}
		
		
}
?>