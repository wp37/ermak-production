<?php
	function smp_store()
	{
		if(!is_user_logged_in())
		{
			return "<div class='smp-comment'>".__("You must logged in!", 'smp')."</div>
			<div><a href='".wp_login_url( home_url())."' title='Login'>".__('Login', 'smc')."</a></div>";
		}
		return Goods_Batch::get_all_batchs_switcher( array( "store" => 1 ), false, "store", $gb_slide_id);
		/*
		
		global $user_iface_color, $current_goods_type, $GoodsBatch, $Soling_Metagame_Constructor, $current_user_accounts, $all_goods_types;
		$all_goods_types			= Goods_Type::get_global();
		
		$current_user_accounts		= SMP_Currency::get_all_user_accounts();		
		
		$arg		= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'orderby'  		=> 'title',
									'order'     	=> 'ASC',
									'post_type' 	=> GOODS_BATCH_NAME,
									'post_status' 	=> 'publish',
									"meta_query"	=> array(
																array(
																			"key"		=> "store",
																			"value"		=> 1,
																			"compare"	=> "=",
																	 )
															)
								);
		$all_goods_batchses	= get_posts($arg);
		if(isset($_POST))
		{
			foreach($all_goods_batchses as $batch)
			{
				if($_POST['pay_' . $batch->ID])
				{
					//PAY ALIAN
					$price							= get_post_meta($batch->ID, "price", true);
					$owner_id						= get_post_meta($batch->ID, "owner_id", true);
					$resivier_accounts				= get_posts(array(
																		'numberposts'	=> -1,
																		'offset'    	=> 0,
																		'post_type' 	=> 'smc_currency_accaunt',
																		'post_status' 	=> 'publish',
																		'meta_query'	=> array(
																									array(
																											"key"		=> "owner_id",
																											"value"		=> $owner_id,
																											"compare"	=> "=",
																										 )
																								),
																	  ));
					$resivier_account				= $resivier_accounts[0];
					
					// переводим деньги	
					$par_col						= $_POST["payment_count_" . $batch->ID]; 
					$summae							= $par_col * $price;
					$current_account_id				= $_POST["account_" . $batch->ID]; 	
					$summ							= SMP_Currency::transfer_from($current_account_id, $summae);
					$new_par_col					= (int)($summ / $price);
					SMP_Currency::transfer_to($resivier_account->ID, $summ);
					
					// передаём партию					
					$new_owner_id					= get_post_meta($current_account_id, "owner_id", true);
					$baa							= Batch::get_instance($batch->ID);
					$baa->rize_batch_owner2(
											$baa,					// Batch Object
											$new_owner_id,			// Location tax term_id
											$new_par_col
										);				
				}
				// CONTROL OWN
				$goods_batch		= $batch;
				if($_POST['save_change'.$goods_batch->ID.''])
				{	
					if(get_current_user_id() != Goods_Batch::user_is_owner($goods_batch->ID)) continue;
					// передать часть партии другой Локации
					if($_POST['location_'.$goods_batch->ID]>0 && $_POST['to_location_'.$goods_batch->ID]>0)
					{
						//update_post_meta($goods_batch->ID, "owner_id", $_POST['location_'.$goods_batch->ID]);
						$n		= Batch::rize_batch_owner(
															new Batch($goods_batch->ID),
															$_POST['location_'.$goods_batch->ID],
															$_POST['to_location_'.$goods_batch->ID]													
														);
						$new_batch		= $n[0];
						$new_count		= $n[1];
						$serv			.= "<BR>".__("Change owner", "smp")." " .$new_count." ".__("unit", "smp") . "<BR>";
						
					}
				}
				if($_POST["to_store".$goods_batch->ID])
				{
					echo "STORE";
					if(get_current_user_id() != Goods_Batch::user_is_owner($goods_batch->ID)) continue;
					if($_POST['to_location_'.$goods_batch->ID]>0)
					{
						$n		= Batch::rize_batch_store(
															new Batch($goods_batch->ID),
															$_POST['to_location_'.$goods_batch->ID],
															!get_post_meta($goods_batch->ID, "store", true)
														 );
						$new_batch		= $n[0];
						$new_count		= $n[1];
						$serv			.= "<BR>".__("Change store", "smp")." " .$new_count." ".__("unit", "smp") . "<BR>";
					}
				}
				if($_POST['delete_'.$goods_batch->ID])
				{
					if(get_current_user_id() != Goods_Batch::user_is_owner($goods_batch->ID)) continue;
					if($_POST['to_location_'.$goods_batch->ID]>0)
					{
						$baa				= new Batch($goods_batch->ID);
						$baa->rize_batch_delete($_POST['to_location_'.$goods_batch->ID]);
					}
				}
				if($_POST['change_price_'.$goods_batch->ID])
				{
					if(get_current_user_id() != Goods_Batch::user_is_owner($goods_batch->ID)) continue;
					if($_POST['new_price_'.$goods_batch->ID]>0)
					{
						update_post_meta( $goods_batch->ID, "price", (int)$_POST['new_price_'.$goods_batch->ID]);
					}
				}
				do_action("smp_send_batch_changes", $batch->ID);
			}
		}
		$sorted_batches		= array();	
		foreach($all_goods_types as $goods_type)
		{
			$sorted_batches[(string)$goods_type->ID]	= array();
		}
		
		foreach($all_goods_types as $current_goods_type)
		{
			$sorted_batches[(string)$current_goods_type->ID]	= array_filter($all_goods_batchses, "by_type");
			//echo "<BR> --- ".$current_goods_type->post_title."<BR>";
			//var_dump($sorted_batches[(string)$current_goods_type->ID]);			
		}
		$arr				= array();
		$i					= 0;
		foreach($all_goods_types as $goods_type)
		{
			$title			=  $goods_type->post_title . "<span class='smp-colorized' style='font-weight:700;'> (".count($sorted_batches[(string)$goods_type->ID]).")</span>";
			$slide			= '';
			if($i == 0)
			{
				$slide		.= "<div class='smp-pr-main' id='production-".$goods_batch->ID."' button_id='".$ii++."' factory_id='".$goods_batch->ID."' style=''>";
				$slide		.= '<h3>'. __("Goods type", "smp").' <span style=\'font-weight:700; color:'.$user_iface_color.'!important\'><a href=\'/?goods_type='.$goods_type->post_name.'\'>' . $goods_type->post_title.'</a></span></h3>';
				$cb			= $sorted_batches[(string)$goods_type->ID];
				$i = 0;
				$slide		.= "<div class='smp-store-batch-list'>";
				foreach($cb as $goods_batch)
				{
					$gb		= SMP_Goods_Batch::get_instance($goods_batch->ID);
					if($gb->user_is_owner())
					{
						$post_data			= 1;
					}
					else
					{				
						$post_data			= 0;
					}
					$slide	.= $gb->get_stroke($gb->body, $post_data);				
					$i++;
				}
				$slide		.= "</div>";
				$slide		.= "</div>";	
			}
			$arr[]			= array("title"=>$title, "slide"=>$slide, 'action'=>'get_store_list', 'args'=>$goods_type->ID);
		}	
		$arr				= apply_filters("smp_pre_store_list_array", $arr);
		$html				.= Assistants::get_lists($arr);
		*/
		/*
		$html				= "<div class='smp-comment' style='margin-bottom:10px;'>".__("Choose Cood Type you need (from left collumn) for see all supplied butches, than you can buy.", "smp")."</div>";
		$html				.= "<div class='smp-production-container'>
			<div id='col1' class='smp-cl1'>";
		$i=0;
		foreach($all_goods_types as $goods_type)
		{
			$html			.=  "<div class='smp-pr-title' style='text-align:right;' id='production-btn-" . $goods_type->ID . "' button_id=" . $i++ . ">" . $goods_type->post_title . "<span class='smp-colorized' style='font-weight:700;'> (".count($sorted_batches[(string)$goods_type->ID]).")</span></div>";
		}		
		$html				.= "
			</div>
			<div id='col2' class='smp-cl2'>"; 
		$ii = 0;
		foreach($all_goods_types as $goods_type)
		{
			
			$html			.= "<div class='smp-pr-main' id='production-".$goods_batch->ID."' button_id='".$ii++."' factory_id='".$goods_batch->ID."' style=''>";
			$html			.= '<h3>'. __("Goods type", "smp").' <span style=\'font-weight:700; color:'.$user_iface_color.'!important\'><a href=\'/?goods_type='.$goods_type->post_name.'\'>' . $goods_type->post_title.'</a></span></h3>';
			$cb				= $sorted_batches[(string)$goods_type->ID];
			$html			.= "<div class='smp-store-batch-list'>";
			if(!count($cb))
			{
				$html		.= "<div class=smp-comment>".__("No Goods of this type", "smp")."</div>";
			}
			else
			{
				foreach($cb as $goods_batch)
				{
					$gb		= SMP_Goods_Batch::get_instance($goods_batch->ID);
					$html	.= $gb->get_stroke($goods_batch, $gb->user_is_owner());
				}
			}
			$html				.= "</div>";
			$html		.= "</div>";
		}
		$html				.= '												
						</div>
					</div>	
						';
						*/
		unset($current_goods_type);		
		echo  "<div id=smc_content>" . $html . "</div>";
	}	
?>