<?php
	function smp_my_hubs()
	{		
		if(!is_user_logged_in())
		{
			echo "<div class='smp-comment'>".__("You must logged in!", 'smp')."</div>
			<div><a href='".wp_login_url( home_url())."' title='Login'>".__('Login', 'smc')."</a></div>";
			return;
		}
		$bts			= get_hubs_list();
		/*
		if(count($hubs)==0)
		{
			$bts			.= "<div class='smp-comment'>" . __("You haven't no one Hub in ownership. Call to Masters.","smp" ) . "</div>";
		}
		else
		{
			
		
			$bts			.= '
			<div class="smp-production-container">
				<div id="col1" class="smp-cl1">';
				
				$i=0;
				$current	= 1;
				foreach($hubs as $hub)
				{
					$fid				= $hub->ID;
					$bts				.='<div class="smp-pr-title" id="production-btn-' . $fid . '" button_id=' . $i . '>' . $hub->post_title . '</div>';
					$i++;
				} 
			
			$bts				.='
			</div>
			<div id="col2" class="smp-cl2">
				<form name="form1" method="post"  enctype="multipart/form-data">';
				$i=0;
				foreach($hubs as $hub)
				{
					$bts				.= "<div class='smp-pr-main' id='production-".$hub->ID."' button_id='".$i."' factory_id='".$hub->ID."' style=''>";
					$bts				.= '<h3>'.$hub->post_title.'</h3>';//Factories::get_large_form($hub, $i);
					$bts				.= SMP_Hub::get_form($hub->ID);					
					$bts				.= "</div>";
					$i++;
				}	
				$bts	.= '					
					</form>
				</div>
			</div>';	
		}
		*/
		$logists		= array(
									array('title' => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/CARGO_ICONS_OP.png'></div>", "hint" => __("My Hubs","smp"), "slide" => $bts)
									
							   );
		if($SMP_Locistics->options['waybill'])
		{
			$waybills		= "<h1>". __("My Waybills","smp") . "</h1>
			<div id='my_willbill_actions'>
				<div class='black_button_2' waybill_id >".
					__("add WayBill", "smp").
			"	</div>
			</div>";	
			$wbs					= SMP_Waybill::get_all_by_user();
			if(count($wbs)==0)
			{
				$waybills			.= "<div class='smp-comment'>" . __("No one Waybill be create.", "smp" ) . "</div>";
			}
			else
			{			
				foreach($wbs as $wb)
				{
					$w				= SMP_Waybill::get_instance($wb);
					$waybills		.= $w->get_full_form();
				}
			}
			$logists[]	= array('title' => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/waybill_icon_op.png'></div>", "hint" => __("My Waybills","smp"), "slide" => $waybills);
		}
		$html			.= Assistants::get_switcher(
														apply_filters("smp_my_logistics", $logists),
														'my_tools'
													);
													
		
		/*											
		$goods_types	= Goods_Type::get_all_gt_ids();
		
		$scr	= '
		<script type="text/javascript">				
		(function($)
		{	
			//goods type
			var ddData = [';
						
		foreach($goods_types as $gt_id)
		{
			$gt			= get_post($gt_id);
			$tru_id		= get_post_thumbnail_id($gt->ID);
			$img		= wp_get_attachment_image_src($tru_id);
			$img		= (img) ? $img[0] : "";
			$scr		.= '
			{
				text: "' . $gt->post_title . '",
				value:' . $gt->ID . ',
				selected: ' . ($gtid == $gt->ID ? "true" : "false") . ',
				description:"id:' . $gt->ID . '",
				imageSrc: "' . $img . '"
			},
			';
		}
		$scr			.='					
				{
					text: "'.__("No multipliers", "smp").'",
					value:"",
					selected: "' . ($quality_modificator == "" ? "true" : "false") . '",
					description:"",
					imageSrc: ""
				},
			];
			$("#myDropdown").ddslick11({
										data:ddData,
										width:240,
										height:300,
										selectText: "'.__("Goods type", "smp") .'",
										imagePosition:"left",
										onSelected: function(selectedData)
										{
											//callback function: do something with selectedData;
											$("#goods_type_id").val(selectedData.selectedData.value);
										}   
									});
			
					
				})(jQuery);
			</script>';
		
		*/											
													
		return  "<div id=smc_content style='width:100%;'>" . $html . "</div>";
		
	}
	
	function get_hubs_list($only_mine=true)
	{
		global	$SMP_Locistics;
		$html							= '';		
		global $wb_query, $user_iface_color, $Soling_Metagame_Constructor;
		
		$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'ASC',
										'post_type' 	=> 'smp_hub',
										'post_status' 	=> 'publish'
									);
		if($only_mine)
		{
			$ow_locations_ids			= $Soling_Metagame_Constructor->all_user_locations();
			if(count($ow_locations_ids) == 0)	return "nothing";
			$ar['meta_query']			= array(
												array(
														'key'		=> 'owner_id',
														'value'		=> $ow_locations_ids,
														'operator'	=> "OR"
												)
											);
		}
		$hubs			= get_posts($ar);
		
		global $hubs_rouths; // array from SMP_Routh::wp_dropdown_rouths
		
		//$bts				= "<h1>".__("My Hubs","smp")."</h1>";	
		$tab				= array();
		$bts				.= "<form name='form1' method='post'  enctype='multipart/form-data'>";
		$i=0;
		foreach($hubs as $hub)
		{		
			$hub				= SMP_Hub::get_instance($hub);
			$slide				= "<div class='smp-pr-main' id='production-".$hub->id."' button_id='".$i."' factory_id='".$hub->id."' style=''>";
			$slide				.= '<h3>'.$hub->body->post_title.'</h3>';
			$slide				.= SMP_Hub::get_form($hub->id);					
			$slide				.= "</div>";
			$title				= $hub->get_pictogramm($hub->id, array('hub_picto' => "", "class"=>"smc_prevert_event", "hide_your"=>true));
			if( SMP_Hub::is_user_owner( $hub->id ) )
				$title		.= Assistants::get_short_your_label(23, array(-1, -1));
			$tab[]			= array( "title"=>$title, "slide"=> $i==0 ? $slide : "", "exec"=>"get_hub_form", "args"=>$hub->id.','. $i );
			$i++;				
		}
		$bts				.= Assistants::get_switcher($tab, "hubs_lists") . "</form>";
		return $bts;
	}
?>