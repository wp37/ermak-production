<?php
	function my_production_page( $only_mine= true)
	{
		if(!is_user_logged_in())
		{
			echo "<div class='smp-comment'>".__("You must logged in!", 'smp')."</div>
			<div><a href='".wp_login_url( home_url())."' title='Login'>".__('Login', 'smc')."</a></div>";
			return;
		}
	
		global $Factory, $Soling_Metagame_Constructor, $user_iface_color, $SMP_Calculator;
		Goods_type::get_global();
		$ar				= array(										
										'numberposts'	=> -1,
										'offset'    	=> 0,
										'orderby'  		=> 'id',
										'order'     	=> 'DESC',
										'post_type' 	=> 'factory',
										'post_status' 	=> 'publish',
									);
		
		if($only_mine)
		{
			$ow_locations_ids				= $Soling_Metagame_Constructor->all_user_locations();
			if(count($ow_locations_ids) == 0)	return "nothing";			
			$meta_query						= array();
			$meta_query['relation']			= 'OR';
			foreach($ow_locations_ids as $idd)
			{
				$arr						= array();
				$arr['value']				= $idd;
				$arr['key']					= 'owner_id';
				$arr['compare']				= 'LIKE'; 
				array_push($meta_query, $arr);
			}
			$ar['meta_query']				= array(
														array(
																'key'		=> 'owner_id',
																'value'		=> $ow_locations_ids,
																'operator'	=> "OR"
															 )
													);
		}
		$factories			= get_posts($ar);
		//var_dump($ar);
		
		//
		$arg		= array(
									'numberposts'	=> -1,
									'offset'    	=> 0,
									'orderby'  		=> 'title',
									'order'     	=> 'ASC',
									'post_type' 	=> GOODS_BATCH_NAME,
									'post_status' 	=> 'publish',
									'meta_query'	=> array(
																array(
																		'key'		=> 'owner_id',
																		'value'		=> $ow_locations_ids,
																		'operator'	=> "OR"
																	 )
															),
								);
		
		$all_goods_batchses	= get_posts($arg);
		$html				= "";
		
		//обрабатываем форму
		$comm_visible		= "none";
		$serv				= "";
		if ( wp_verify_nonce( $_POST['my_production_sh'], basename( __FILE__ ) ) )
		{
			
			foreach($factories as $factory)
			{
				$fid			= $factory->ID;
				if($_POST['factory_submit_btn-'.$fid])
				{
					$is_owner	= Factories::is_user_owner(get_post_meta($fid, "owner_id", true), get_current_user_id());
					if(!$is_owner)	continue;
					$serv		.= __("Edited Factory", "smp"). " ".$fid;
				}
				//do_action("smp_send_batch_changes", $goods_batch->ID);
			}
		}		
		if($serv !="")	
		{
			$comm_visible	= "display";		
		}		
		$html		.= "<div class='lp-comment_server' style='display:".$comm_visible.";'>".$serv."</div>";	


		//==========================
		//
		//from server
		//
		//==========================
		$html				= SMP_Goods_batch::serverside_service($all_goods_batchses);


		if(count($factories)==0)
		{
			$html	.= "<div class='smp-comment'>" . __("You haven't no one Location in ownership. Call to Masters.","smp" ) . "</div>";
		}
		else
		{
			$i=0;
			foreach($factories as $factory)
			{
				$fid			= $factory->ID;
				$factory_obj	= Factory::get_factory($fid);
				$slide			= '<div class="smp-pr-main" id="production-'. $fid .'" button_id="'.$i .'" factory_id="'.$fid.'" style="">';
				$slide			.= '<h3>'. $factory_obj->get_factory_type_name() .' <span style="font-weight:700; color:'.$user_iface_color.'"><a href="'.get_permalink($fid) . '">' . $factory->post_title .'</a></span></h3>';
				//consume
				$need			= $factory_obj->get_needs();
				if(is_wp_error($need))
				{
					$consume	= Assistants::echo_me($need->get_error_message());
				}
				else
				{
					$consume	=  "<p class='description'>".__("All available", 'smp')."</p>";
				}		
				$slide			.= Assistants::get_switcher(
										array(
												apply_filters("smc_my_production_0", array("title" => __("Settings", "smc"),	"slide" => Factories::get_large_form($factory, $i), "name"=>"slide_factories".$fid),	$factory),
												apply_filters("smc_my_production_1", array("title" => __("Consume", "smc"),	"slide" => $consume, "name"=>"slide_consume".$fid),	$factory),
												apply_filters("smc_my_production_2", array("title" => __("Storage", "smp"), 	"slide"	=> Factories::get_srorage_form( $fid ), "name"=>"slide_storage"), 	$factory),
												//apply_filters("smc_my_production_2", "", $factory),
												//apply_filters("smc_my_production_3", "", $factory),
												//apply_filters("smc_my_production_4", "", $factory),
											  ),
											  "factories_"
										);
				$slide			.= "</div>";					
				$i++;
			
				$title		= $factory->post_title;
				if($factory_obj->is_user_owner())
					$title	.= Assistants::get_short_your_label(23, array(-1, -1));
				$arr[]		= array(
					'title'	=> $title,
					'slide'	=> $slide
				);
			}
			$html			.= Assistants::get_lists($arr, '', '');
			
			return "<div id=smc_content>" . $html . "</div>";
		}
	}
?>