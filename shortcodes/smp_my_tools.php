<?php
	function smp_my_tools()
	{
		global $SMP_Calculator;
		$arr		= apply_filters( "smp_my_tools", 
					  array( 
								array(	"title" => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/calc_ico_op.png'></div>", 
										"hint"	=> __("Personal Calculator", "smp"), 
										"slide" => $SMP_Calculator->smp_personal_calc()
									  ),								
								array(	"title" => "<div class='smp_tool_icon'><img src='" . SMP_URLPATH . "img/circle_icon.png'></div>", 
										"hint"	=> __("Production circle reports", "smp"), 
										"slide" => "" , 
										"name" 	=> "get_report_btn",
										"exec"	=> "get_circle_report", 
										"args"	=> "" 
									 ),								
						   )
					);
		return "<div id=smc_content>" . Assistants::get_switcher($arr, "outer_head") . "</div>";
	}
	
?>