<?php
	function get_bank_page()
	{
		global $user_iface_color;
		global $Soling_Metagame_Constructor;
		global $smp_my_invoices;
		global $SMP_Currency;
		$html				= '';
		$comm_visible		= "none";
		$serv				= "";
		$class				= "";
		
		$html				.= "<div class='lp-comment_server ".$class."' style='display:".$comm_visible.";'>".$serv."
								</div>";
		$html				.= "
		<div class='smp-production-container'>
			<form name='new_tr' method='post'  enctype='multipart/form-data' id='new_tr'>";
		//Reports
		$accs				=	SMP_Currency::get_all_user_accounts(get_current_user_id());
		$r					= "<div class='acc_report' style='display:inline-block; position:relative; width:100%;'>";
		if(count($accs)==0)	
			$r				.= "<div class='smp-comment'>".__("Nothing", "smp")."</div>";
		else
		{
			foreach($accs as $acc)
			{
				$acid			= $acc->ID;
				$account		= SMP_Account::get_instance($acc);
				$report			= $account->get_meta("report");
				$ctid			= $account->get_meta("ctype_id");
				$count			= $account->get_meta("count");
				$ct				= SMP_Currency_Type::get_instance($ctid);
				$r				.= "
				<div class='acc_r_block'>
				<h3>" . $acc->post_title. "</h3> 
				<div>" . sprintf(__("%s operations", "smp"),  count($report) ) . "</div>
				<div>".
					__("Currency Type", "smp"). " - <b>". $ct->body->post_title. "</b>". 
				"</div>
				<div class='report_acc_comm'>" . SMP_Currency_Type::get_object_price($ctid, $count) . "</div>
				";
				if(is_array($report) && count($report))
				{
					$report		= array_reverse ($report);
					$r			.= "
						<div class='abzaz'>
							<table class='bank_report'>
								<tr>
									<th>".
										__("Balance", "smp").
									"</th>
									<th>". 
										__("Action").
									"</th>
									<th>". 
										__("Purpose of payment", "smp").
									"</th>
									<th>". 
										__("Account ID", "smp").
									"</th>
									<th>". 
										__('Repaid time', 'smp') . 
									"</th>
								</tr>";
					$i 			= 0;
					$class		= "";
					foreach($report as $dat)
					{
						if(++$i ==10)
						{
							$r			.= "				
								<tr>
									<td colspan='12' style='padding:0;'>
										<div prev_more_acc='$acid' class='button' style='display:block;'>".
											__("prevous actions", "smp").
									"	</div>
									</td>
								</tr>";
							$class = "replace_$acid lp-hide";
						}
						$r			.= "
								<tr class='$class'>
									<td class='balance'>
										<b>".$dat['balance'] ."</b>
									</td>
									<td class='summae'>".
										 "<span class='account_report_summae'>" . $dat['summae'] . "</span>" .
									"<br> <b>".$dat['count'] ."</b>".
									"</td>
									<td class='reason'>".
										$dat['reason'].
									"</td>
									<td>".
										"<b>".$dat['partner_account_id'] ."</b>".
									"</td>
									<td class='time'>".
										"<b>" . date_i18n( 'M j, Y  G:i', $dat['time']) . "</b>" .
									"</td>
								</tr>";
					}
					$r			.= "				
							</table>
						</div>";
				}
				else
				{
					$r			.= "
						<div class='abzaz'>
							<div class='smp-comment'>".
								__("no move in this Currency Account", "smp").
							"</div>
						</div>";
				}
				$r				.= "</div><HR>";
				
			}
		}
		$r					.= "</div>";
		$reports			= "<h2>".__("Reports", "smp"). "</h2>".
			$r;
		$my_invoices		= '<h2>'.__("My Invoices", "smp").'</h2>
		<div>'.
			smp_my_invoices().
		'</div>';
		$tabs				= array(
										array("title" => __("Reports", "smp"), 	 	"slide" => $reports),
										array("title" => __("Invoices", "smp"), 	"slide" => $my_invoices, "name" => "invoices_slide")										
									);
		if($SMP_Currency->options['currency_transactions'])
		{
			$tabs[]			= 			array("title" => __("Operations", "smp"), 	"slide" => '', 				"exec"  => "get_my_currency_accounts" );
		}
		$tabs				= apply_filters( "smp_my_bank", 
											$tabs, 
											$ca
							  );
		$html				.= Assistants::get_switcher($tabs, 'bank_', " label='tabs'", "bank_page" );
		$html				.= '
			</form>
		</div>';
			
		return  "<div id=smc_content>" . $html . "</div>
		<script>
			jQuery('[prev_more_acc]').click(function(evt)
			{
				var prev_more_acc = (jQuery(this).attr('prev_more_acc'));
				jQuery('.replace_' + prev_more_acc).removeClass('lp-hide');
				jQuery(this).parent().hide();
			});
		</script>
		";
	}
	function smp_bank()
	{
		if(!is_user_logged_in())
		{
			$login			= __('Login', 'smc');
			return "<div class='smp-comment'>".__("You must logged in!", 'smp')."</div>
			<div><a href='".wp_login_url( home_url())."' title='".$login."'>".$login."</a></div>";
		}
		echo get_bank_page();
	}
	
?>